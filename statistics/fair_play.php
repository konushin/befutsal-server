<?php

$root = false;
$page_title = "Fair Play";
$description = "���������� ���������, �������������� � �������� ������ �������";
include("../includings/header.php");

print('
<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0">
 <tr>');

$league = (int)$_GET["league"];

include("../includings/league menu.php");

print('
 <tr>
  <td height="30" bgcolor="ffffff" style="background-image:url(../graphics/h_bg_2.gif);background-repeat:repeat-x;background-position:top"><img src="../graphics/0.gif" width="1" height="30" border="0" title=""></td>
 </tr>
</table>

<!----------------------- End of Header ---------------------------->

<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff">
 <tr>');

include("../includings/main menu.php");

print('
<!------------------------ Central Part.Right Part ---------------------------->

  <td valign="top" style="padding:7px 20px 10px 20px">
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#5BC1D7">
	 <tr>
	  <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_left.gif" width="14" height="35" border="0" title=""></td>
	  <td valign="top" background="../graphics/cp_blue_bg.gif" bgcolor="#5BC1D7" style="padding-left:20px;padding-top:4px;" class="cp_header">' .$s_season. ' - ' .$s_comp. '</td>
	  <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_right.gif" width="14" height="35" border="0" title=""></td>
	 </tr>
	</table>
	</div>
	<br><br>
');

print('
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
	 <tr>
      <td width="10" height="29"><img src="../graphics/grey_tab2_left.gif" width="10" height="29" border="0" title=""></td>
	  <td align="center" height="29" background="../graphics/grey_tab2_bg.gif" bgcolor="#F7F7F7"><strong>����������� �� ���� Fair Play</strong>&nbsp;&nbsp;
');

print('
	  </td>
	  <td width="10" height="29"><img src="../graphics/grey_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	 </tr>
	</table>
	</div>
	<br><br>');

//----------------------------������� � ����������----------------------------------

$query = "SELECT competitor.id_competitor AS id_team, competitor.title AS title, SUM(yellow) AS yellows, SUM(red) AS reds, SUM(yellow+red*3) AS total
FROM staff, competitor, application
WHERE staff.id_player = application.id_applicant
 AND application.id_competitor = competitor.id_competitor
 AND competitor.id_competition = $league
 AND application.active = 1
GROUP BY competitor.id_competitor
ORDER BY total ASC";

$res_fairplay = mysql_query($query, $db_connection);

//--------------������� ������� �� �����

print('
	<div align="center">
	   <table cellpadding="0" cellspacing="0" border="0">
	    <tr>
		 <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
		</tr>
	    <tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td width="10"><img src="../graphics/green_tab2_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/green_tab2_bg.gif" bgcolor="#0FC020" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="w_text_b" width="36">M</td>
		    <td align="center" class="w_text_b" width="170">�������</td>
		    <td align="center" class="w_text_b" width="24">����</td>
		    <td align="center" class="w_text_b" width="24"><img src="../graphics/red_card.gif" width="13" height="22" border="0" title="������� ��������"></td>
		    <td align="center" class="w_text_b" width="20"><img src="../graphics/yellow_card.gif" width="13" height="22" border="0" title="������ ��������"></td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/green_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>');

for ($i=0; $i<mysql_num_rows($res_fairplay); $i++) {
 $place = $i+1;
 $id_team = mysql_result($res_fairplay, $i, "id_team");
 $s_team = mysql_result($res_fairplay, $i, "title");
 $faults = mysql_result($res_fairplay, $i, "total");
 $bookings = mysql_result($res_fairplay, $i, "yellows");
 $sentoffs = mysql_result($res_fairplay, $i, "reds");
 print('
	    <tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td width="10"><img src="../graphics/grey_tab2_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/grey_tab2_bg.gif" bgcolor="#F7F7F7" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="bl_text_b" width="36">' .$place. '</td>
		    <td width="170"><a href="teams.php?id=' .$id_team. '" class="b_link3">' .$s_team. '</a></td>
		    <td align="center" class="bl_text_b" width="40">' .$faults. '</td>
		    <td align="center" class="bl_text_b" width="24">' .$sentoffs. '</td>
		    <td align="center" class="bl_text_b" width="24">' .$bookings. '</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/grey_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>
 ');
}

print('
	   </table>
	   </div><br><br>
	   <div align="center">
	   <table width="600" cellpadding="0" cellspacing="0" border="0" height="22">
	    <tr>
		 <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_left.gif" width="10" height="22" border="0" title=""></td>
		 <td background="../graphics/orange_tab_bg.gif" style="padding-left:10px;" bgcolor="#FF8625" height="22" class="w_text_b">�� ������ �������� ������� ������ 3 �������� ����, �� ������ �������������� - 1 ����</td>
		 <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_right.gif" width="10" height="22" border="0" title=""></td>
		</tr>
	   </table>
	   </div>
  </td>
 </tr>
</table>  ');

require("../includings/big footer.php");

?>