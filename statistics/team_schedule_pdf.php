<?php

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

include("../database_main.php");

require_once("../tcpdf/config/lang/rus.php");
require_once("../tcpdf/tcpdf.php");
$pdf = new TCPDF(P, pt, 'A4', true, 'UTF-8', false);
$pdf->SetCreator('Andrey');
$pdf->SetAuthor('���� ����-��������!');
$pdf->SetKeywords('���� ����-��������, ������, ������, ��������, �����, ����������, �������, ����, ����, ����, ����');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(TRUE);
$pdf->setLanguageArray($l);
$pdf->SetFont('dejavusans', '', 10);
$pdf->AddPage();

$id = (int)$_GET["id"];

$res_comp = mysql_query("SELECT competition.title AS s_comp, season.title AS s_season, competitor.title AS s_team FROM competitor, competition, season WHERE competition.id_competition = competitor.id_competition AND season.id_season = competition.season AND competitor.id_competitor = $id", $db_connection);
$s_comp = mysql_result($res_comp, 0, "s_comp");
$s_season = mysql_result($res_comp, 0, "s_season");
$s_team = mysql_result($res_comp, 0, "s_team");

$body = "";

$res_schedule = mysql_query("SELECT * FROM schedule WHERE team_1 = $id OR team_2 = $id", $db_connection);
$n_scheduled = mysql_num_rows($res_schedule);
if ($n_scheduled > 0) {
    for ($i = 0; $i < $n_scheduled; $i++) {
        $n_tour = mysql_result($res_schedule, $i, "tour");
        $dt_game = mysql_result($res_schedule, $i, "date");
        $s_year = substr($dt_game, 0, 4);
        $s_month = ruMonth(substr($dt_game, 5, 2));
        $s_day = substr($dt_game, 8, 2);
        $tm_game = substr(mysql_result($res_schedule, $i, "time"), 0, 5);
        $pitch = mysql_result($res_schedule, $i, "pitch");
        $n_team1 = mysql_result($res_schedule, $i, "team_1");
        $n_team2 = mysql_result($res_schedule, $i, "team_2");
        $res_team1 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $n_team1", $db_connection);
        $res_team2 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $n_team2", $db_connection);
        $s_team1 = mysql_result($res_team1, 0, "title");
        $s_team2 = mysql_result($res_team2, 0, "title");
        $body = '<tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td width="10"><img src="../graphics/grey_tab2_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/grey_tab2_bg.gif" bgcolor="#F7F7F7" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td class="bl_text_b" width="120" align="left">$s_day $s_month $s_year</td>
		    <td class="bl_text_b" width="50">$tm_game</td>
		    <td class="bl_text_b" width="60">$pitch</td>
		    <td class="bl_text_b" width="300">$s_team1 - $s_team2</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/grey_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>';
    }
} else {
    mysql_close($db_connection);
    $body = '<tr>
	     <td width="10"><img src="../graphics/green_tab2_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/green_tab2_bg.gif" bgcolor="#0FC020" class="w_text_b" align="center">��� ���������� ������� � ��������� ������� �� ������� �� ������ ����� � ����������.</td>
		 <td width="10"><img src="../graphics/green_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>';
    die();
}

$header = '
<table width="70%" cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff" align="center">
 <tr>
  <td valign="top" style="padding:7px 20px 10px 20px" coslpan="2">
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#5BC1D7">
	 <tr>
	  <td valign="top" coslpan="2" width="14" height="35"><img src="../graphics/cp_blue_left.gif" width="14" height="35" border="0" title=""></td>
	  <td valign="top" coslpan="2" background="../graphics/cp_blue_bg.gif" bgcolor="#5BC1D7" style="padding-left:20px;padding-top:4px;" class="cp_header">$s_season - $s_comp. ���������� ��� ������� $s_team.</td>
	  <td valign="top" coslpan="2" width="14" height="35"><img src="../graphics/cp_blue_right.gif" width="14" height="35" border="0" title=""></td>
	 </tr>
	</table>
	</div>
	<br>
	<div align="center">
	   <table cellpadding="0" cellspacing="0" border="0">';

$footer = "	   </table>
	  </td>
   </div>
  </td>
 </tr>
</table>  ";

$html = $header . $body . $footer;

$pdf->writeHTML($html, true, false, true, false, C);
$pdf->Output('schedule.pdf', 'D');

?>