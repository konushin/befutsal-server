<?php

$root = false;
$page_title = "������� �������";
$description = "���������� �� �������� �������, ������ � ��������";
include("../includings/header.php");

print('
<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0">
 <tr>');

print('
  <td height="6" background="' . $add_string . 'graphics/h_bg_1.gif" bgcolor="#28C638"><img src="' . $add_string . 'graphics/0.gif" width="1" height="6" border="0" title=""></td>
 </tr>
 <tr>
  <td height="1" bgcolor="#FFFFFF"><img src="' . $add_string . 'graphics/0.gif" width="1" height="1" border="0" title=""></td>
 </tr>');

print('
 <tr>
  <td height="30" bgcolor="ffffff" style="background-image:url(../graphics/h_bg_2.gif);background-repeat:repeat-x;background-position:top"><img src="../graphics/0.gif" width="1" height="30" border="0" title=""></td>
 </tr>
</table>

<!----------------------- End of Header ---------------------------->

<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff">
 <tr>');

include("../includings/main menu.php");

$season = (int)$_GET["season"];

print('
<!------------------------ Central Part.Right Part ---------------------------->

  <td valign="top" style="padding:7px 20px 10px 20px">
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#5BC1D7">
	 <tr>
	  <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_left.gif" width="14" height="35" border="0" title=""></td>
	  <td valign="top" background="../graphics/cp_blue_bg.gif" bgcolor="#5BC1D7" style="padding-left:20px;padding-top:4px;" class="cp_header">������� ������� ������</td>
	  <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_right.gif" width="14" height="35" border="0" title=""></td>
	 </tr>
	</table>
	</div>
	<br><br>
');

$res_last_date = mysql_query("SELECT DATE_FORMAT(MAX(date), '%Y-%m-%e') AS last FROM schedule, competition WHERE schedule.league = competition.id_competition AND competition.season = $season", $db_connection);
$date = mysql_result($res_last_date, 0, "last");

if ($date == NULL) {
    $date = date("Y-m-d", mktime(date(H), date(i), date(s), date("m"), date("d"), date("Y")));;
}

$s_year = substr($date, 0, 4);
$s_month = ruMonth(substr($date, 5, 2));
$s_day = substr($date, 8, 2);

//----------------------�� ��������------------------------

$res_teams = mysql_query("
 SELECT competitor.id_competitor AS id_team, competitor.title AS title, competition.title AS league_title, competition.id_competition AS league_id, AVG(EXTRACT(YEAR FROM DATE_SUB('$date', INTERVAL DATE_FORMAT(player.birthdate, '%Y') YEAR))) AS age
 FROM player, competitor, competition, application
 WHERE player.id_player = application.id_player
  AND competitor.id_competitor = application.id_competitor
  AND competitor.id_competition = competition.id_competition
  AND competition.season = $season
  AND player.birthdate > '1900-01-01'
  AND application.active = 1
 GROUP BY application.id_competitor
 ORDER BY age ASC
", $db_connection);

$num_teams = mysql_num_rows($res_teams);

//-------------------�������--------------------------------

print('
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
	 <tr>
      <td width="10" height="29"><img src="../graphics/grey_tab2_left.gif" width="10" height="29" border="0" title=""></td>
	  <td align="center" height="29" background="../graphics/grey_tab2_bg.gif" bgcolor="#F7F7F7"><strong>6 ����� ������� ������');

if ($season != $current_season) {
    print(" �� ������ ��������� ������ ($s_day $s_month $s_year)");
} else {
    print(" �� ������� ������");
}

print('</strong></td>
	  <td width="10" height="29"><img src="../graphics/grey_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	 </tr>
	</table>
	</div>
	<br><br>
	<div align="center">
	   <table cellpadding="0" cellspacing="0" border="0">
	    <tr>
	     <td width="10"><img src="../graphics/green_tab2_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/green_tab2_bg.gif" bgcolor="#0FC020" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="w_text_b" width="36">#</td>
		    <td align="center" class="w_text_b" width="180">�������</td>
		    <td align="center" class="w_text_b" width="180">������</td>
		    <td align="center" class="w_text_b" width="100">��. �������*</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/green_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>');

$color = FALSE;

for ($i = 0; $i < MIN($num_teams, 6); $i++) {
    $color = !$color;
    if ($color) {
        $img = "grey_tab2";
        $bgcolor = "#F7F7F7";
    } else {
        $img = "green_tab";
        $bgcolor = "#E5FEE7";
    }

    $number = $i + 1;
    $league_id = mysql_result($res_teams, $i, "league_id");
    $competition = new Tournament();
    $competition->getCompetitionInfo($league_id);
    $league_title = cutString($competition->title, 25);
    $id_team = mysql_result($res_teams, $i, "id_team");
    $team_title = mysql_result($res_teams, $i, "title");
    $age = round(mysql_result($res_teams, $i, "age"), 1);
    $res_players = mysql_query("SELECT COUNT(player.id_player) AS cnt
  FROM player, application
  WHERE player.id_player = application.id_player
   AND application.id_competitor = $id_team
   AND player.birthdate > '1900-01-01'
   AND application.active = 1", $db_connection);
    $num_players = mysql_result($res_players, 0, "cnt");

    print('
	    <tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td width="10"><img src="../graphics/' . $img . '_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/' . $img . '_bg.gif" bgcolor="' . $bgcolor . '" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="bl_text_b" width="36">' . $number . '</td>
		    <td style="padding-left:10px;" width="180"><a href="teams.php?id=' . $id_team . '" class="b_link3">' . $team_title . '</a></td>
		    <td style="padding-left:10px;" width="180"><a href="../' . $competition->page . '" class="b_link3">' . $league_title . '</a></td>
		    <td align="center" class="bl_text_b" width="100">' . $age . ' ��� (' . $num_players . ')</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/' . $img . '_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>');
}

print('
	    <tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_left.gif" width="10" height="22" border="0" title=""></td>
		 <td background="../graphics/orange_tab_bg.gif" style="padding-left:10px;" bgcolor="#FF8625" height="22" class="w_text_b">* � ������� ������� ����� �������� ������� �������</td>
		 <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_right.gif" width="10" height="22" border="0" title=""></td>
	    </tr>
	   </table>
   </div>');

//-------------------����������--------------------------------

print('
	<br><br>
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
	 <tr>
      <td width="10" height="29"><img src="../graphics/grey_tab2_left.gif" width="10" height="29" border="0" title=""></td>
	  <td align="center" height="29" background="../graphics/grey_tab2_bg.gif" bgcolor="#F7F7F7"><strong>6 ����� ���������� ������');

if ($season != $current_season) {
    print(" �� ������ ��������� ������ ($s_day $s_month $s_year)");
} else {
    print(" �� ������� ������");
}

print('</strong></td>
	  <td width="10" height="29"><img src="../graphics/grey_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	 </tr>
	</table>
	</div>
	<br><br>
	<div align="center">
	   <table cellpadding="0" cellspacing="0" border="0">
	    <tr>
	     <td width="10"><img src="../graphics/green_tab2_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/green_tab2_bg.gif" bgcolor="#0FC020" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="w_text_b" width="36">#</td>
		    <td align="center" class="w_text_b" width="180">�������</td>
		    <td align="center" class="w_text_b" width="180">������</td>
		    <td align="center" class="w_text_b" width="100">��. �������*</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/green_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>');

$color = FALSE;

for ($i = $num_teams - 1; $i >= MAX($num_teams - 6, 6); $i--) {
    $color = !$color;
    if ($color) {
        $img = "grey_tab2";
        $bgcolor = "#F7F7F7";
    } else {
        $img = "green_tab";
        $bgcolor = "#E5FEE7";
    }

    $number = $num_teams - $i;
    $league_id = mysql_result($res_teams, $i, "league_id");
    $competition = new Tournament();
    $competition->getCompetitionInfo($league_id);
    $league_title = cutString($competition->title, 25);
    $id_team = mysql_result($res_teams, $i, "id_team");
    $team_title = mysql_result($res_teams, $i, "title");
    $age = round(mysql_result($res_teams, $i, "age"), 1);
    $res_players = mysql_query("SELECT COUNT(player.id_player) AS cnt
  FROM player, application
  WHERE player.id_player = application.id_player
   AND application.id_competitor = $id_team
   AND player.birthdate > '1900-01-01'
   AND application.active = 1", $db_connection);
    $num_players = mysql_result($res_players, 0, "cnt");

    print('
	    <tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td width="10"><img src="../graphics/' . $img . '_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/' . $img . '_bg.gif" bgcolor="' . $bgcolor . '" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="bl_text_b" width="36">' . $number . '</td>
		    <td style="padding-left:10px;" width="180"><a href="teams.php?id=' . $id_team . '" class="b_link3">' . $team_title . '</a></td>
		    <td style="padding-left:10px;" width="180"><a href="../' . $competition->page . '" class="b_link3">' . $league_title . '</a></td>
		    <td align="center" class="bl_text_b" width="100">' . $age . ' ��� (' . $num_players . ')</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/' . $img . '_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>');
}

print('
	    <tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_left.gif" width="10" height="22" border="0" title=""></td>
		 <td background="../graphics/orange_tab_bg.gif" style="padding-left:10px;" bgcolor="#FF8625" height="22" class="w_text_b">* � ������� ������� ����� �������� ������� �������</td>
		 <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_right.gif" width="10" height="22" border="0" title=""></td>
	    </tr>
	   </table>
   </div>');

//----------------------�� �����------------------------

$res_leagues = mysql_query("
 SELECT competition.title AS title, competition.id_competition AS league, SUBSTRING(AVG(EXTRACT(YEAR FROM DATE_SUB('$date', INTERVAL DATE_FORMAT(player.birthdate, '%Y') YEAR))), 1, 4) AS age
 FROM player, competitor, competition, application
 WHERE player.id_player = application.id_player
  AND competitor.id_competitor = application.id_competitor
  AND competitor.id_competition = competition.id_competition
  AND competition.season = $season
  AND player.birthdate > '1900-01-01'
  AND application.active = 1
 GROUP BY competitor.id_competition
 ORDER BY age ASC
", $db_connection);
$num_leagues = mysql_num_rows($res_leagues);

print('
	<br><br>
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
	 <tr>
      <td width="10" height="29"><img src="../graphics/grey_tab2_left.gif" width="10" height="29" border="0" title=""></td>
	  <td align="center" height="29" background="../graphics/grey_tab2_bg.gif" bgcolor="#F7F7F7"><strong>������� ������� ��������');

if ($season != 'current') {
    print(" �� ������ ��������� ������ ($s_day $s_month $s_year)");
} else {
    print(" �� ������� ������");
}

print('</strong></td>
	  <td width="10" height="29"><img src="../graphics/grey_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	 </tr>
	</table>
	</div>
	<br><br>
	<div align="center">
	   <table cellpadding="0" cellspacing="0" border="0">
	    <tr>
	     <td width="10"><img src="../graphics/green_tab2_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/green_tab2_bg.gif" bgcolor="#0FC020" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="w_text_b" width="36">#</td>
		    <td align="center" class="w_text_b" width="180">������</td>
		    <td align="center" class="w_text_b" width="100">��. �������</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/green_tab2_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>');

$color = FALSE;

for ($i = 0; $i < $num_leagues; $i++) {
    $color = !$color;
    if ($color) {
        $img = "grey_tab2";
        $bgcolor = "#F7F7F7";
    } else {
        $img = "green_tab";
        $bgcolor = "#E5FEE7";
    }

    $number = $i + 1;
    $league = mysql_result($res_leagues, $i, "league");
    $competition = new Tournament();
    $competition->getCompetitionInfo($league);
    $age = mysql_result($res_leagues, $i, "age");

    print('
	    <tr>
	     <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
	    </tr>
	    <tr>
	     <td width="10"><img src="../graphics/' . $img . '_left.gif" width="10" height="29" border="0" title=""></td>
		 <td background="../graphics/' . $img . '_bg.gif" bgcolor="' . $bgcolor . '" valign="top">
	      <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
		   <tr>
		    <td align="center" class="bl_text_b" width="36">' . $number . '</td>
		    <td style="padding-left:10px;" width="180"><a href="../' . $competition->page . '" class="b_link3">' . $competition->title . '</a></td>
		    <td align="center" class="bl_text_b" width="100">' . $age . ' ���</td>
		   </tr>
		  </table>
		 </td>
		 <td width="10"><img src="../graphics/' . $img . '_right.gif" width="10" height="29" border="0" title=""></td>
	    </tr>');
}

//------------------------�����-----------------------------------

$res_total = mysql_query("SELECT SUBSTRING(AVG(EXTRACT(YEAR FROM DATE_SUB('$date', INTERVAL DATE_FORMAT(player.birthdate, '%Y') YEAR))), 1, 4) AS age FROM player WHERE player.birthdate > '1900-01-01'", $db_connection);
$avg = mysql_result($res_total, 0, 'age');

print('
	   </table>
       </div><br><br>
	   <div align="center">
	   <table width="600" cellpadding="0" cellspacing="0" border="0" height="22">
	    <tr>
		 <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_left.gif" width="10" height="22" border="0" title=""></td>
		 <td background="../graphics/orange_tab_bg.gif" style="padding-left:10px;" bgcolor="#FF8625" height="22" class="w_text_b">�� ������ ��������� ������ ������� ������� ���� ������������������ �������: ' . $avg . ' ���</td>
		 <td valign="top" width="10" height="22"><img src="../graphics/orange_tab_right.gif" width="10" height="22" border="0" title=""></td>
		</tr>
	   </table>
	   </div>
  </td>
 </tr>
</table>  ');

require("../includings/big footer.php");

?>
