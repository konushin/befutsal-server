﻿<?php

$class_active["search"] = "active";
$page_title = "Поиск";
$description = "Поиск игроков, арбитров, команд и документов";
include("header.php");

if (isset($_GET["r"])) {
    $search_string = mysql_real_escape_string($_GET["r"]);
} else {
    $search_string = "init";
}

print('
    <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
                    <div class="effect-cover">
                        <h3 class="txt-advert animated">Поиск</h3>
                        <p class="txt-advert-sub animated">игроков, арбитров, команд и документов</p>
                    </div>
              </div>
        
        <section id="news" class="container secondary-page">
          <div class="general general-results players">
           <div class="top-score-title right-score col-md-12">
                <h3>РЕЗУЛЬТАТЫ <span>поиска</span><span class="point-little">.</span></h3>');

print('
              <div class="top-score-title right-score col-md-12">
                <div class="col-md-12">       
                    <form id="search_form" method="get" class="search-form" action="search.php">         
                        <div class="name">
                            <input id="request" name="r" type="text" minlength="3" placeholder="Кого ищем?" title="Не менее 3 символов" required="" autofocus/><span>&nbsp;</span><button>Найти</button><br> <br>
                        </div>');

//удаляем пробелы
$search_normalized = str_replace(' ', '', $search_string);

print('                ');

if ($search_normalized == '') {
    print('
                <div class="col-md-12 news-page">
                  <div class="col-md-10 data-news-pg">
                    <p>Мы не смогли понять, что вы ищете...</p>
                  </div>');

} else {
    if ($search_normalized != 'init') {

        //ищем игроков
        $res_player = mysql_query("SELECT * FROM player WHERE (LOCATE(LCASE('$search_normalized'), LCASE(REPLACE(REPLACE(name_1, ' ', ''), '-', '')))<>0) or (LOCATE(LCASE('$search_normalized'), LCASE(REPLACE(REPLACE(name_2, ' ', ''), '-', '')))<>0) ORDER BY name_2, name_1");
        $num_player = mysql_num_rows($res_player);

        //ищем судей
        $res_referee = mysql_query("SELECT * FROM referee WHERE (LOCATE(LCASE('$search_normalized'), LCASE(REPLACE(REPLACE(name_1, ' ', ''), '-', '')))<>0) or (LOCATE(LCASE('$search_normalized'), LCASE(REPLACE(REPLACE(name_2, ' ', ''), '-', '')))<>0) ORDER BY name_2, name_1");
        $num_referee = mysql_num_rows($res_referee);

        $num_search = $num_player + $num_referee;

        print('
                <div class="col-md-12 news-page">
                  <div class="col-md-10 data-news-pg">
                    <p class="news-dd">Участники соревнований</p>');

        if ($num_search > 0) {

            if ($num_search > 250) {
                print("<p>По вашему запросу найдено слишком много участников, попробуйте его уточнить.</p>");
            } else {
                print("<p><strong>Всего найдено участников - " . $num_search . "</strong></p>");

                //выводим игроков на экран
                for ($i = 0; $i < $num_player; $i++) {
                    $player_id = mysql_result($res_player, $i, "id_player");

                    $player = new PlayerPersonal();
                    $player->getPlayerPersonal($player_id);

                    if ($player->photo_path == '') {
                        $player->photo_path = "photo_default.PNG";
                    }

                    $id_applicant = mysql_result(mysql_query("SELECT * FROM application WHERE id_player = $player_id ORDER BY id_applicant DESC"), 0, "id_applicant");
                    $number = $i + 1;
                    print('
                     <p>' . $number . '. <a href="player.php?p=' . $id_applicant . '"><img src="photos/' . $player->photo_path . '" style="max-height:25px; max-width:25px"> ' . $player->name_2 . ' ' . $player->name_1 . '</a></p>'); //TODO здесь должна быть ссылка на карьеру игрока
                }

                //выводим судей на экран
                /*
                print("<br>");
                for ($i = 0; $i < $num_referee; $i++) {
                    $referee_id = mysql_result($res_referee, $i, "id_referee");
                    $name_1 = mysql_result($res_referee, $i, "name_1");
                    $name_2 = mysql_result($res_referee, $i, "name_2");
                    $number = $num_player + $i + 1;
                    print('<p>' . $number . '. ' . $name_2 . ' ' . $name_1 . '</b> - <a href="#">Арбитр</a></font>'); // TODO здесь ссылка на арбитра
                }
                */
            }
        } else {
            print("<p>К сожалению, по вашему запросу участников не найдено.</p>");
        }

        print('</div>
               </div>');

        print('
                <div class="col-md-12 news-page">
                  <div class="col-md-10 data-news-pg">
                    <p class="news-dd">Команды</p>');

        //ищем команды
        $res_search = mysql_query("SELECT DISTINCT id_team FROM competitor WHERE LOCATE(LCASE('$search_normalized'), LCASE(REPLACE(REPLACE(title, ' ', ''), '-', '')))<>0 ORDER BY title");
        $num_search = mysql_num_rows($res_search);
        if ($num_search > 0) {
            if ($num_search > 150) {
                print("<p>По вашему запросу найдено слишком много команд, попробуйте его уточнить.</p>");
            } else {
                print("<p><strong>Всего найдено команд - " . $num_search . "</strong></p>");

                for ($i = 0; $i < $num_search; $i++) {
                    $id_team = mysql_result($res_search, $i, "id_team");

                    $team = new TeamInfo();
                    $team->getTeamInfo($id_team);

                    $res_competitor = mysql_query("SELECT * FROM competitor WHERE id_team = $id_team ORDER BY id_competitor DESC");
                    $id_competitor = mysql_result($res_competitor, 0, "id_competitor");
                    $number = $i + 1;
                    print('<p>' . $number . '. <a href="club.php?t=' . $id_team . '"><img src="emblems/' . $team->emblem_path . '" style="max-height:25px; max-width:25px"> ' . $team->title . '</a></p>');
                }
            }
        } else {
            print("<p>К сожалению, по вашему запросу команд не найдено.</p>");
        }

        print('</div>
               </div>');

        print('
                <div class="col-md-12 news-page">
                  <div class="col-md-10 data-news-pg">
                    <p class="news-dd">Турниры</p>');

        //ищем турниры
        $res_search = mysql_query("SELECT DISTINCT c.id_competition, c.title, c.season, s.title FROM competition AS c, season AS s WHERE LOCATE(LCASE('$search_normalized'), LCASE(REPLACE(REPLACE(c.title, ' ', ''), '-', '')))<>0 AND c.season = s.id_season ORDER BY c.title");
        $num_search = mysql_num_rows($res_search);
        if ($num_search > 0) {
            if ($num_search > 150) {
                print("<p>По вашему запросу найдено слишком много турниров, попробуйте его уточнить.</p>");
            } else {
                print("<p><strong>Всего найдено турниров - " . $num_search . "</strong></p>");

                for ($i = 0; $i < $num_search; $i++) {
                    $id_competition = mysql_result($res_search, $i, "c.id_competition");
                    $tournament = new Tournament();
                    $tournament->getCompetitionInfo($id_competition);
                    $title = mysql_result($res_search, $i, "c.title");
                    $season = mysql_result($res_search, $i, "s.title");
                    $number = $i + 1;
                    print('<p>' . $number . '. <a href="' . $tournament->link . '">' . $tournament->title . ' (' . $tournament->season_details->title . ')</a></p>');
                }
            }
        } else {
            print("<p>К сожалению, по вашему запросу турниров не найдено.</p>");
        }

        print('</div>
               </div>');
    }
}

print('</form>');

print('</div>');

print('
 
           </div><!--Close Top Match-->
         </div>
        </section>
');

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');

?>