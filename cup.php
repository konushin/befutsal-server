﻿<?php
$class_active["season"] = "active";
$page_title = $league->title;
$description = "Статистика турнира " . $league->title;
include("header.php");

$id_league = $_GET["l"];

$league = new Tournament();
$league->getCompetitionInfo($id_league);

//----формируем уникальное имя таблицы
$res_create = mysql_query("CREATE TEMPORARY TABLE temp_played_games (id_schedule INT, id_protocol INT, tour INT, tour_title VARCHAR(255), team_1 INT, team_2 INT, score_end_1 INT, score_end_2 INT, forfeited INT, on_penalty INT)");

//информация о матчах
$query = "SELECT *
 FROM schedule
 RIGHT JOIN cup_round 
 ON schedule.tour = cup_round.id_tour
 WHERE schedule.league = $id_league
  AND cup_round.id_competition = schedule.league 
 ORDER BY schedule.tour";
$res_scheduled = mysql_query($query);
$n_scheduled = mysql_num_rows($res_scheduled);

for ($i = 0; $i < $n_scheduled; $i++) {
    $n_schedule = mysql_result($res_scheduled, $i, "id_schedule");
    $tour = mysql_result($res_scheduled, $i, "schedule.tour");
    $team_1 = mysql_result($res_scheduled, $i, "team_1");
    $team_2 = mysql_result($res_scheduled, $i, "team_2");
    $tour_title = mysql_result($res_scheduled, $i, "cup_round.title");
    $res_protocol = mysql_query("SELECT * FROM protocol WHERE id_schedule = $n_schedule");
    $forfeited = null;
    if (mysql_num_rows($res_protocol) > 0) {
        $protocol = new Protocol();
        $protocol->getProtocolData($res_protocol, 0);
        $query = "INSERT INTO temp_played_games VALUES ($n_schedule, $protocol->id_protocol, $tour, '$tour_title', $team_1, $team_2, $protocol->score_end_1, $protocol->score_end_2, "; //начало SQL-запроса
        if ($protocol->forfeited == 1) {
            $query = $query . "1, NULL)";
        } else {
            if ($protocol->on_penalty == 1) {
                $query = $query . "NULL, 1)";
            } else {
                $query = $query . "NULL, NULL)";
            }
        }
    } else {
        $query = "INSERT INTO temp_played_games VALUES ($n_schedule, 0, $tour, '$tour_title', $team_1, $team_2, 0, 0, null, null)";
    }
    //попытка записать во временную таблицу
    $res_ins = mysql_query($query);
}

print('
  <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
                <div class="effect-cover">
                    <h3 class="txt-advert animated">' . $league->category[0]->title . '<span class="point-little">.</span></h3>
                </div>
           </div>
    
    <section id="allmatch" class="container secondary-page">
      <div class="general general-results">
           <div class="top-score-title right-score col-md-9">
                <h3>' . $league->title . '</span></h3>
                <div class="main">
                        <div class="tabs animated-slide-2 matches-tbs">
                            <ul class="tab-links-matches">');

$res_tours = mysql_query("SELECT DISTINCT tour_title FROM temp_played_games ORDER BY tour");
$n_tours = mysql_num_rows($res_tours);
$tours = array();

for ($i = 0; $i < $n_tours; $i++) {
    $tour = mysql_result($res_tours, $i, "tour_title");
    print('<li><a href="#tour' . $i . '">' . $tour . '</a></li>');
}

$res_scorers = topScorers($id_league, "LIMIT 0, 10");
$n_scorers = mysql_num_rows($res_scorers);
if ($n_scorers > 0) {
    print('
                                <li><a href="#topscorers">Бомбардиры</a></li>');
}

$res_goalies = mysql_query("SELECT * FROM application, competitor WHERE application.id_competitor = competitor.id_competitor AND competitor.id_competition = $id_league AND application.goalie = 1 AND application.active = 1");
$n_goalies = mysql_num_rows($res_goalies);
if ($n_goalies > 0) {
    print('
                                <li><a href="#goalies">Вратари</a></li>');
}

$now_date = date("Y-m-d");
$res_bans = getBans($now_date, $id_league);
$num_bans = mysql_num_rows($res_bans);
if ($num_bans > 0) {
    print('
                                <li><a href="#bans">Дисквалификации</a></li>');
}

print('
                            </ul>
                            <div class="tab-content">
');

for ($n = 0; $n < $n_tours; $n++) {
    $tour = mysql_result($res_tours, $n, "tour_title");
    $res_schedule = mysql_query("SELECT * FROM temp_played_games WHERE tour_title='$tour' ORDER BY id_schedule");
    $n_schedule = mysql_num_rows($res_schedule);

    print('
                                <div id="tour' . $n . '" class="tab active">
                                  <table class="match-tbs">
                                    <tr><td class="match-tbs-title" colspan="5">' . $tour . '</td></tr>');

    $prev_date = 0;

    for ($i = 0; $i < $n_schedule; $i++) {
        $team_1 = mysql_result($res_schedule, $i, "team_1");
        $team_2 = mysql_result($res_schedule, $i, "team_2");

        $competitor_home = new TeamCompetitor();
        $competitor_home->getTeamCompetitor($team_1);

        $competitor_away = new TeamCompetitor();
        $competitor_away->getTeamCompetitor($team_2);

        $id_protocol = mysql_result($res_schedule, $i, "id_protocol");
        $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
        $query = "SELECT DATE_FORMAT(date, '%d.%m %a') as s_date, s.*
                  FROM schedule s
                  WHERE s.id_schedule = $id_schedule";
        $res_match = mysql_query($query);
        $time = substr(mysql_result($res_match, 0, "s.time"), 0, 5);
        $date = mysql_result($res_match, 0, "s_date");
        $day = (int) substr($date, 0, 2);
        $n_month = substr($date, 3, 2);
        $eng_week_day = substr($date, 6, 3);
        $date = $day . " " . ruMonth($n_month) . ", " . ruWeekDay($eng_week_day);
        $date = iconv('windows-1251', 'utf-8', $date);
        $pitch = mysql_result($res_match, 0, "s.pitch");
        $forfeited = mysql_result($res_schedule, $i, "forfeited");
        $on_penalty = mysql_result($res_schedule, $i, "on_penalty");

        if ($date != $prev_date) {
            print('                                    <tr class="match-sets"><td colspan="5" style="text-align: left;">' . $date . '</td></tr>');
        }

        include("include_matchlist.php"); //перед вызовом должен быть задан $id_protocol

        $prev_date = $date;
    }

    print('
                                     </table>
                                </div>');
}

//БОМБАРДИРЫ
if ($n_scorers > 0) {
    include("include_scorers.php");

    print ("</div>");
}

//ВРАТАРИ
if ($n_goalies > 0) {
    include("include_goalies.php");

    print ("</div>");
}

//ДИСКВАЛИФИКАЦИИ
//перед вызовом должен быть определен $res_bans (см. в блоке меню)
if ($num_bans > 0) {
    include("include_bans.php");
}

print('

                            </div>
                            
                        </div>
                    </div>
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include("footer.php");

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');
?>