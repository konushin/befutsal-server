<?php

/**
 * Created by PhpStorm.
 * User: Konushin
 * Date: 23.11.2021
 * Time: 18:34
 */
$num_teams = mysql_num_rows($league_table);
$colspan = $num_teams + 1;

if ($num_teams > 0) {
    print('
                                 </table>
                                </div>
                                <div id="chess" class="tab active">
                                  <table class="match-tbs" style="table-layout: fixed">
                                    <tr><td class="match-tbs-title" colspan="' . $colspan . '">Шахматка</td></tr>
                                    <tr class="match-sets"><td>&nbsp;</td>');

    $competitor = array();

    for ($i = 0; $i < $num_teams; $i++) {
        $id_team = mysql_result($league_table, $i, "id_competitor");
        $competitor[$i] = new TeamCompetitor();
        $competitor[$i]->getTeamCompetitor($id_team);
        print('<td class="chess" style="vertical-align: bottom"><a href="competitor.php?c=' . $id_team . '" title="' . $competitor[$i]->title . '"><img src="/emblems/' . $competitor[$i]->team_details->emblem_path . '" style="max-height: 25px;" title="' . $competitor[$i]->title . '"><br>' . $competitor[$i]->shortname . '</a></span></td>');
    }
    print('</tr>');

    for ($i = 0; $i < $num_teams; $i++) {
        print('
		<tr>
		 <td class="chess"><a href="competitor.php?c=' . $competitor[$i]->id_competitor . '" title="' . $competitor[$i]->title . '"><strong>
		 ' . $competitor[$i]->shortname . '</strong></a></td>
		 ');

        for ($n = 0; $n < $num_teams; $n++) {
            if ($i == $n) {
                print('<td class="chess" style="background: #212121;"></td>');
            } else {
                $id_team = $competitor[$i]->id_competitor;
                $id_rival = mysql_result($league_table, $n, "id_competitor");
                $res_result1 = mysql_query("SELECT * FROM protocol WHERE team_1 = $id_team AND team_2 = $id_rival");

                if (mysql_num_rows($res_result1) > 0) {
                    $is_forfeited = mysql_result($res_result1, 0, "forfeited");

                    if ($is_forfeited == NULL) {
                        $result1 = '<a href="match.php?p=' . mysql_result($res_result1, 0, "id_protocol") . '">' . mysql_result($res_result1, 0, "score_end_1") . ':' . mysql_result($res_result1, 0, "score_end_2") . '</a>';
                    } else {
                        if (mysql_result($res_result1, 0, "score_end_1") == 0) {
                            $score_end_1 = "-";
                        } elseif (mysql_result($res_result1, 0, "score_end_1") > 0) {
                            $score_end_1 = "+";
                        }

                        if (mysql_result($res_result1, 0, "score_end_2") == 0) {
                            $score_end_2 = "-";
                        } elseif (mysql_result($res_result1, 0, "score_end_2") > 0) {
                            $score_end_2 = "+";
                        }
                        $result1 = '<a href="match.php?p=' . mysql_result($res_result1, 0, "id_protocol") . '">' . $score_end_1 . ':' . $score_end_2 . '</a>';
                    }
                } else {
                    $result1 = "&nbsp;";
                }

                $res_result2 = mysql_query("SELECT * FROM protocol WHERE team_2 = $id_team AND team_1 = $id_rival");

                if (mysql_num_rows($res_result2) > 0) {
                    $is_forfeited = mysql_result($res_result2, 0, "forfeited");

                    if ($is_forfeited == NULL) {
                        $result2 = '<a href="match.php?p=' . mysql_result($res_result2, 0, "id_protocol") . '">' . mysql_result($res_result2, 0, "score_end_1") . ':' . mysql_result($res_result2, 0, "score_end_2") . '</a>';
                    } else {
                        if (mysql_result($res_result2, 0, "score_end_1") == 0) {
                            $score_end_1 = "-";
                        } elseif (mysql_result($res_result2, 0, "score_end_1") > 0) {
                            $score_end_1 = "+";
                        }

                        if (mysql_result($res_result2, 0, "score_end_2") == 0) {
                            $score_end_2 = "-";
                        } elseif (mysql_result($res_result2, 0, "score_end_2") > 0) {
                            $score_end_2 = "+";
                        }
                        $result2 = '<a href="match.php?p=' . mysql_result($res_result2, 0, "id_protocol") . '">' . $score_end_1 . ':' . $score_end_2 . '</a>';
                    }
                } else {
                    $result2 = "&nbsp;";
                }

                print('<td class="chess">' . $result1 . '<br>' . $result2 . '<br></td>');
            }
        }

        print('</tr>');
    }
}

print('

                                    </table>
                                
');
?>