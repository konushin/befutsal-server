﻿<?php
$class_active["season"] = "active";
include("header.php");

$id_league = $_GET["l"];
$tour_date = "9999-12-31 23:59:59";

$league_table = getLeagueTable($id_league, $tour_date);

$res_last_date = mysql_query("SELECT DATE_FORMAT(MAX(dt_protocol), '%Y-%m-%e') AS last FROM protocol WHERE league = $id_league");
$date = mysql_result($res_last_date, 0, "last");

if ($date == NULL) {
    $date = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
}

$s_year = substr($date, 0, 4);
$s_month = iconv('windows-1251', 'utf-8', ruMonth(substr($date, 5, 2)));
$s_day = substr($date, 8, 2);

$league = new Tournament();
$league->getCompetitionInfo($id_league);

print('
  <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
                <div class="effect-cover">
                    <h3 class="txt-advert animated">' . $league->category[0]->title . '<span class="point-little">.</span></h3>
                </div>
           </div>
    
    <section id="allmatch" class="container secondary-page">
      <div class="general general-results">
           <div class="top-score-title right-score col-md-9">
                <h3>' . $league->title . '</span></h3>
                <div class="main">
                        <div class="tabs animated-slide-2 matches-tbs">
                            <ul class="tab-links-matches">
                                <li class="active"><a href="#ttable">Таблица</a></li>');

$res_scorers = topScorers($id_league, "LIMIT 0, 10");
$n_scorers = mysql_num_rows($res_scorers);
if ($n_scorers > 0) {
    print('
                                <li><a href="#topscorers">Бомбардиры</a></li>');
}

$res_goalies = mysql_query("SELECT * FROM application, competitor, staff WHERE application.id_competitor = competitor.id_competitor AND competitor.id_competition = $id_league AND application.goalie = 1 AND application.active = 1 AND staff.id_player = application.id_applicant");
$n_goalies = mysql_num_rows($res_goalies);
if ($n_goalies > 0) {
    print('
                                <li><a href="#goalies">Вратари</a></li>');
}

print('
                                <li><a href="#results">Расписание</a></li>');

print('
                                <li><a href="#chess">Шахматка</a></li>');

$now_date = date("Y-m-d");
$res_bans = getBans($now_date, $id_league);
$num_bans = mysql_num_rows($res_bans);
if ($num_bans > 0) {
    print('
                                <li><a href="#bans">Дисквалификации</a></li>');
}

print('
                            </ul>
                            <div class="tab-content">
');

print('
                                <div id="ttable" class="tab active">
                                   <table class="match-tbs">
                                         <tr><td class="match-tbs-title" colspan="9">Турнирная таблица на ' . $s_day . ' ' . $s_month . ' ' . $s_year . ' года</td></tr>
                                         <tr class="match-sets"><td>#</td><td></td><td></td><td class="fpt">Очки</td><td class="fpt">Игры</td><td class="fpt">В</td><td class="fpt">Н</td><td class="fpt">П</td><td class="only-desktop">Мячи</td></tr>
');

for ($i = 0; $i < mysql_num_rows($league_table); $i++) {
    $place = $i + 1;

    if ($place <= $league->promoted) {
        $bgcolor = 'style="background-color: #FFD700;"';
    } elseif ($place > (mysql_num_rows($league_table) - $league->relegated)) {
        $bgcolor = 'style="background-color: #FA8072;"';
    } else {
        $bgcolor = "";
    }

    $team = mysql_result($league_table, $i, "name");
    if (strlen($team) > 30) {
        $s_team = substr($team, 0, 27) . "...";
    } else {
        $s_team = $team;
    }
    $id_team = mysql_result($league_table, $i, "id_competitor");
    $competitor = new TeamInfo();
    $competitor->getTeamInfoByCompetitor($id_team);
    $played = mysql_result($league_table, $i, "pld");
    $won = mysql_result($league_table, $i, "won");
    $drew = mysql_result($league_table, $i, "drew");
    $lost = mysql_result($league_table, $i, "lost");
    $goalsf = mysql_result($league_table, $i, "goalsfor");
    $goalsa = mysql_result($league_table, $i, "goalsagainst");
    $goalsd = $goalsf - $goalsa;
    if ($goalsd > 0) {
        $goalsd = "+" . $goalsd;
    }
    $points = mysql_result($league_table, $i, "pts");

    $href = "window.location.href='competitor.php?c=" . $id_team . "'";

    print('
                                        <tr onclick="' . $href . '; return false"><td class="fpt" ' . $bgcolor . '>' . $place . '</td><td><img src="/emblems/' . $competitor->emblem_path . '" style="max-height: 25px;"></td><td class="fpt" style="text-align: left; padding-left: 2px;">' . $team . '</td><td class="fpt"><b>' . $points . '</b></td><td class="fpt">' . $played . '</td><td class="fpt">' . $won . '</td><td class="fpt">' . $drew . '</td><td class="fpt">' . $lost . '</td><td class="only-desktop">' . $goalsf . '-' . $goalsa . ' (' . $goalsd . ')</td></tr>
');
}


print('

                                    </table>
                                </div>
');

//БОМБАРДИРЫ
if ($n_scorers > 0) {
    include("include_scorers.php");

    print ("</div>");
}

//ВРАТАРИ
if ($n_goalies > 0) {
    include("include_goalies.php");
}

//РАСПИСАНИЕ
print('
                                 </table>
                                </div>
                                <div id="results" class="tab active">
                                  <table class="match-tbs">
                                    <tr><td class="match-tbs-title" colspan="5">Расписание и результаты игр</td></tr>');

$query = "SELECT DATE_FORMAT(date, '%d.%m %a') as s_date, s.*, p.*
FROM schedule s
 LEFT JOIN protocol p ON s.id_schedule = p.id_schedule 
WHERE s.league = $id_league 
ORDER BY s.date, s.time";
$res_schedule = mysql_query($query);
$n_schedule = mysql_num_rows($res_schedule);

$prev_date = 0;

for ($i = 0; $i < $n_schedule; $i++) {
    $team_1 = mysql_result($res_schedule, $i, "s.team_1");
    $team_2 = mysql_result($res_schedule, $i, "s.team_2");
    $time = substr(mysql_result($res_schedule, $i, "s.time"), 0, 5);
    $tour = mysql_result($res_schedule, $i, "s.tour");
    $id_schedule = mysql_result($res_schedule, $i, "s.id_schedule");
    $pitch = mysql_result($res_schedule, $i, "s.pitch");
    $date = mysql_result($res_schedule, $i, "s_date");
    $day = (int) substr($date, 0, 2);
    $n_month = substr($date, 3, 2);
    $eng_week_day = substr($date, 6, 3);
    $date = $day . " " . ruMonth($n_month) . ", " . ruWeekDay($eng_week_day);
    $date = iconv('windows-1251', 'utf-8', $date);

    $competitor_home = new TeamCompetitor();
    $competitor_home->getTeamCompetitor($team_1);

    $competitor_away = new TeamCompetitor();
    $competitor_away->getTeamCompetitor($team_2);

    $id_protocol = mysql_result($res_schedule, $i, "p.id_protocol");

    if ($date != $prev_date) {
        print('                                    <tr class="match-sets"><td colspan="5" style="text-align: left;">' . $date . '</td></tr>');
    }

    include("include_matchlist.php"); //перед вызовом должен быть задан $id_protocol

    $prev_date = $date;
}

print ("</div>");

//ШАХМАТКА
//перед вызовом должен быть определен $id_league
if ($n_schedule > 0) {
    include("include_chess.php");
}
print ("</div>");

//ДИСКВАЛИФИКАЦИИ
//перед вызовом должен быть определен $res_bans (см. в блоке меню)
if ($num_bans > 0) {
    include("include_bans.php");
}


print('
                            
                        </div>
                    </div>
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include("footer.php");

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');