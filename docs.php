﻿<?php

$class_active["docs"] = "active";
$page_title = "Регламенты и документы";
$description = "Официальные документы турниров: регламенты, кодексы, положения и т.п.";
include("header.php");

print('
    <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
                    <div class="effect-cover">
                        <h3 class="txt-advert animated">Регламенты и документы</h3>
                        <p class="txt-advert-sub animated">турниров и федераций</p>
                    </div>
              </div>
        
        <section id="news" class="container secondary-page">
          <div class="general general-results players">
           <div class="top-score-title right-score col-md-12">
                <h3>ВЫБЕРИТЕ <span>сезон</span><span class="point-little">.</span></h3>');

//Делаем выборку сезонов
$res_seasons = mysql_query("SELECT DISTINCT d.season, s.title FROM documents AS d LEFT JOIN season AS s ON d.season = s.id_season ORDER BY season DESC");

for ($n = 0; $n < mysql_num_rows($res_seasons); $n++) {
    $id_season = mysql_result($res_seasons, $n, "season");
    $season_title = mysql_result($res_seasons, $n, "title");

    switch ($id_season) {
        case 999999:
            $season_title = "Общие пособия";
            break;
        case 999998:
            $season_title = "Универсальные документы";
            break;
    };

    print('
                <div class="col-md-12 news-page">
                
                  <div class="col-md-10 data-news-pg">
                    <p class="news-dd">' . $season_title . '</p>');

    //Делаем выборку доступных документов
    $res_docs = mysql_query("SELECT * FROM documents WHERE season = $id_season ORDER BY title", $db_connection);
    for ($i = 0; $i < mysql_num_rows($res_docs); $i++) {
        $doc_title = mysql_result($res_docs, $i, "title");
        $doc_path = mysql_result($res_docs, $i, "path");

        print('
                     <p><a href="../documents/' . $doc_path . '" class="ca-more" target="_blank">' . $doc_title . ' <i class="fa fa-angle-double-right"></i></a></p>');
    }

    print('
                  </div>
                </div>');
}

print('
 
           </div><!--Close Top Match-->
         </div>
        </section>
');

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');

?>