<?php

class Season
{
    public $id_season;
    public $title;

    /**
     * Текущий сезон
     */
    public function getCurrentSeason()
    {
        $query = "SELECT * FROM season ORDER BY id_season DESC LIMIT 0,1";
        $res_current_season = mysql_query($query);
        $this->id_season = mysql_result($res_current_season, 0, "id_season");
        $this->title = mysql_result($res_current_season, 0, "title");
    }
}

class Competition
{
    public $id_competition;
    public $title;
    public $icon;
}

class CompetitionListEnvelope
{
    public $competitions;
}

class Team
{
    public $id_team;
    public $title;
    public $title_history;
    public $founded;
    public $emblem_path;
    public $competitions;

    /**
     * Список соревнований, в которых участвует команда
     * @param int $id_season Сезон
     */
    public function getCompetitions($id_season)
    {
        $this->competitions = array();
        $competitions_query = "SELECT * FROM competition, competitor WHERE competitor.id_team = $this->id_team AND competition.id_competition = competitor.id_competition AND season = $id_season ORDER BY IFNULL(rate, 100), competition.id_competition DESC";
        $res_competitions = mysql_query($competitions_query);
        $num_competitions = mysql_num_rows($res_competitions);
        for ($n = 0; $n < $num_competitions; $n++) {
            $competition = new Competition();
            $competition->id_competition = (int)mysql_result($res_competitions, $n, "id_competition");
            $competition->title = iconv('windows-1251', 'UTF-8', mysql_result($res_competitions, $n, "title"));
            $this->competitions[$n] = $competition;
        }
    }

    /**
     * Список исторических имен команды
     */
    public function getHistoricalTitles()
    {
        $this->title_history = array();
        $query = "SELECT DISTINCT title FROM competitor WHERE id_team = $this->id_team";
        $res_titles = mysql_query($query);
        for ($i = 0; $i < mysql_num_rows($res_titles); $i++) {
            $history_title = iconv('windows-1251', 'UTF-8', mysql_result($res_titles, $i, "title"));
            if ($this->title != $history_title) {
                $this->title_history[] = $history_title;
            }
        }
    }
}

class TeamDetails extends Team
{
    public $competitor_id;
    public $competitor_title;
    public $shortname;
    public $tshirt_home;
    public $tshirt_away;
    public $pld;
    public $won;
    public $drew;
    public $lost;
    public $scored;
    public $missed;
    public $pts;
    public $players;
    public $next_matches;
    public $protocols;
    public $table;
}

class Player
{
    public $id_player;
    public $name_1;
    public $name_2;
    public $birthdate;
    public $photo;
    public $goalie;
    public $played;
    public $scored;
    public $booked;
    public $sentoff;
    public $captain;
    public $number;
    public $season_performance;
}

class PlayerList
{
    public $players;

    /**
     * Список игроков
     * @param int $id_competitor id команды
     */
    public function getPlayers($id_competitor)
    {
        global $base_url;
        $this->players = array();
        $query = "SELECT a.goalie, a.number, p.*, a.id_applicant, COUNT(s.id_player) AS played, SUM(s.scored) AS scored, SUM(s.yellow) AS booked, SUM(s.red) AS sentoff, a.represent
              FROM application AS a
              INNER JOIN player AS p ON p.id_player = a.id_player
              LEFT JOIN staff AS s ON s.id_player = a.id_applicant
              WHERE a.id_competitor = $id_competitor
                AND a.id_player = p.id_player
                AND a.active = 1
                AND a.goalie <> 2
              GROUP BY a.id_applicant
              ORDER BY a.goalie DESC, IF(a.number IS NULL,1,0), a.number, p.name_2, p.name_1";
        $res_players = mysql_query($query);
        for ($n = 0; $n < mysql_num_rows($res_players); $n++) {
            $player = new Player();
            $player->id_player = mysql_result($res_players, $n, "p.id_player");

            //временно возвращаю имя в фамилии, а фамилию в имени, чтобы умещалось на экране
            $player->name_1 = iconv('windows-1251', 'UTF-8', mysql_result($res_players, $n, "p.name_2"));
            $player->name_2 = iconv('windows-1251', 'UTF-8', mysql_result($res_players, $n, "p.name_1"));

            $player->birthdate = mysql_result($res_players, $n, "p.birthdate");
            if (mysql_result($res_players, $n, "p.photo_path") != null) {
                $player->photo = $base_url . "photos/" . mysql_result($res_players, $n, "p.photo_path");
            }
            $player->goalie = mysql_result($res_players, $n, "a.goalie");
            $player->played = mysql_result($res_players, $n, "played");
            $player->scored = mysql_result($res_players, $n, "scored");
            if ($player->scored == null) {
                $player->scored = 0;
            }
            $player->booked = mysql_result($res_players, $n, "booked");
            $player->sentoff = mysql_result($res_players, $n, "sentoff");
            $player->captain = mysql_result($res_players, $n, "represent");
            $player->number = mysql_result($res_players, $n, "number");
            $this->players[$n] = $player;
        }
    }
}

class PlayerInGame extends Player
{
    public $scored_in_match;
    public $owngoal;
    public $booked;
    public $sentoff;
}

class PlayerDetails
{
    public $player_in_competition;
}

class DetailedProtocol
{
    public $protocol;
    public $players_home;
    public $players_away;

    /**
     * Состав команды в матче
     * @param resource $res_staff id протокола
     * @return array Массив игроков
     */
    public function getPlayerInGame($res_staff)
    {
        //$res_staff - выборка из Staff игроков одной команды в одном матче

        global $base_url;

        $staff = array();

        for ($i = 0; $i < mysql_num_rows($res_staff); $i++) {
            $id_applicant = mysql_result($res_staff, $i, "id_player");
            $query = "SELECT a.goalie, p.*, a.id_applicant, a.number
                          FROM application AS a
                          INNER JOIN player AS p ON p.id_player = a.id_player
                          WHERE a.id_applicant = $id_applicant
                            AND a.id_player = p.id_player";
            $res_player = mysql_query($query);
            $player = new PlayerInGame();
            $player->id_player = mysql_result($res_player, 0, "p.id_player");
            $player->name_1 = iconv('windows-1251', 'UTF-8', mysql_result($res_player, 0, "p.name_1"));
            $player->name_2 = iconv('windows-1251', 'UTF-8', mysql_result($res_player, 0, "p.name_2"));
            $player->birthdate = mysql_result($res_player, 0, "p.birthdate");
            if (mysql_result($res_player, 0, "p.photo_path") != null) {
                $player->photo = $base_url . "photos/" . mysql_result($res_player, 0, "p.photo_path");
            }
            $player->goalie = mysql_result($res_player, 0, "a.goalie");
            if (mysql_result($res_staff, $i, "yellow") == 1) {
                $player->booked = TRUE;
            } else {
                $player->booked = FALSE;
            }
            if (mysql_result($res_staff, $i, "red") == 1) {
                $player->sentoff = TRUE;
            } else {
                $player->sentoff = FALSE;
            }
            $player->scored_in_match = mysql_result($res_staff, $i, "scored");
            $player->owngoal = mysql_result($res_staff, $i, "owngoal");
            $player->number = mysql_result($res_staff, $i, "number");
            $staff[$i] = $player;
        }

        //возвращаем массив игроков 
        return $staff;
    }
}

class Schedule
{
    public $id_schedule;
    public $league;
    public $team_1;
    public $team_1_emblem;
    public $team_2;
    public $team_2_emblem;
    public $date;
    public $time;
    public $pitch;
    public $bet_rate;
    public $bet;

    /**
     * Сыграна или нет игра
     * @return bool
     */
    public function isPlayed()
    {
        $query = "SELECT * FROM protocol WHERE id_schedule = $this->id_schedule";
        $res_query = mysql_query($query);
        if (mysql_num_rows($res_query) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

class ScheduleList
{
    public $schedules;

    /**
     * Список предстоящих игр
     * @param bool $isCompetitor TRUE=Команда участник, FALSE=Команда историческая
     * @param int $id_competitor id команды
     * @param int $limit Лимит
     */
    public function getNextMatches($isCompetitor, $id_competitor, $limit)
    {
        global $base_url;
        global $user_id;

        //если участник турнира, берем все игры
        if ($isCompetitor) {
            $where_clause = "s.team_1 = $id_competitor OR s.team_2 = $id_competitor";
        } else {
            //если общая команда, берем только игры в будущем
            $date = date('Y-m-d');
            $where_clause = "(s.date >= '" . $date . "' AND (s.team_1 IN (SELECT id_competitor FROM competitor WHERE id_team IN ($id_competitor)) OR s.team_2 IN (SELECT id_competitor FROM competitor WHERE id_team IN ($id_competitor))))";
        }
        if ($limit > 0) {
            $limit_clause = "LIMIT 0," . $limit;
        } else {
            $limit_clause = null;
        }
        $query = "SELECT *
      FROM schedule AS s
      LEFT OUTER JOIN protocol AS p
      ON s.id_schedule = p.id_schedule
      WHERE p.id_protocol IS NULL AND (" . $where_clause . ")
      ORDER BY s.date, s.time " . $limit_clause;
        $res_next_matches = mysql_query($query);
        $this->schedules = array();
        for ($n = 0; $n < mysql_num_rows($res_next_matches); $n++) {
            $schedule = new Schedule();
            $schedule->id_schedule = mysql_result($res_next_matches, $n, "id_schedule");
            $id_competition = mysql_result($res_next_matches, $n, "league");
            $schedule->league = iconv('windows-1251', 'UTF-8', mysql_result(mysql_query("SELECT * FROM competition WHERE id_competition = $id_competition"), 0, "title"));
            $team_1 = mysql_result($res_next_matches, $n, "team_1");
            $team_2 = mysql_result($res_next_matches, $n, "team_2");
            $schedule->team_1 = iconv('windows-1251', 'UTF-8', mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_1"), 0, "title"));
            $schedule->team_2 = iconv('windows-1251', 'UTF-8', mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_2"), 0, "title"));
            $res_team1 = mysql_query("SELECT * FROM team, competitor WHERE competitor.id_competitor = $team_1 AND competitor.id_team = team.id_team");
            if (mysql_result($res_team1, 0, "emblem_path") != null) {
                $schedule->team_1_emblem = $base_url . "emblems/" . mysql_result($res_team1, 0, "emblem_path");
            }
            $res_team2 = mysql_query("SELECT * FROM team, competitor WHERE competitor.id_competitor = $team_2 AND competitor.id_team = team.id_team");
            if (mysql_result($res_team2, 0, "emblem_path") != null) {
                $schedule->team_2_emblem = $base_url . "emblems/" . mysql_result($res_team2, 0, "emblem_path");
            }
            $schedule->date = mysql_result($res_next_matches, $n, "date");
            $schedule->time = mysql_result($res_next_matches, $n, "time");
            $schedule->pitch = cutString(iconv('windows-1251', 'UTF-8', mysql_result($res_next_matches, $n, "pitch")), 8);

            //добавляем информацию о ставках
            if (mysql_result($res_next_matches, $n, "bet_1") != null) {
                $bet_rate = new BetRate();
                $bet_rate->bet_1 = mysql_result($res_next_matches, $n, "bet_1");
                $bet_rate->bet_0 = mysql_result($res_next_matches, $n, "bet_0");
                $bet_rate->bet_2 = mysql_result($res_next_matches, $n, "bet_2");
                $bet_rate->bet_total = mysql_result($res_next_matches, $n, "bet_total");
                $bet_rate->bet_total_more = mysql_result($res_next_matches, $n, "bet_total_more");
                $bet_rate->bet_total_less = mysql_result($res_next_matches, $n, "bet_total_less");
                $schedule->bet_rate = $bet_rate;
            }

            $bet = new Bet();
            $bet->getBetDetails($user_id, $schedule->id_schedule);
            if ($bet->bet_type != null) {
                $schedule->bet = $bet;
            }

            $this->schedules[$n] = $schedule;
        }
    }
}

class Protocol
{
    public $id_protocol;
    public $league;
    public $dt_protocol;
    public $ref_1;
    public $ref_2;
    public $id_team_1;
    public $team_1;
    public $team_1_emblem;
    public $id_team_2;
    public $team_2;
    public $team_2_emblem;
    public $score_half_1;
    public $score_end_1;
    public $score_half_2;
    public $score_end_2;
    public $fouls_half_1;
    public $fouls_end_1;
    public $fouls_half_2;
    public $fouls_end_2;
    public $forfeited;
    public $on_penalty;
    public $extratime;

    /**
     * Детали протокола
     * @param resource $res_protocols Выборка протокола
     * @param int $row Строка из выборки протоколов
     * @return $this Протокол
     */
    public function getProtocolData($res_protocols, $row)
    {

        global $base_url;

        $this->id_protocol = mysql_result($res_protocols, $row, "id_protocol");
        $id_competition = mysql_result($res_protocols, $row, "league");
        $this->league = iconv('windows-1251', 'UTF-8', mysql_result(mysql_query("SELECT * FROM competition WHERE id_competition = $id_competition"), 0, "title"));
        $ref_1 = mysql_result($res_protocols, $row, "ref_1");
        if ($ref_1 > 0) {
            $ref_1_name = mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $ref_1"), 0, "name_1") . " " . mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $ref_1"), 0, "name_2");
        } else {
            $ref_1_name = null;
        }
        $ref_2 = mysql_result($res_protocols, $row, "ref_2");
        if ($ref_2 > 0) {
            $ref_2_name = mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $ref_2"), 0, "name_1") . " " . mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $ref_2"), 0, "name_2");
        } else {
            $ref_2_name = null;
        }
        $this->ref_1 = iconv('windows-1251', 'UTF-8', $ref_1_name);
        $this->ref_2 = iconv('windows-1251', 'UTF-8', $ref_2_name);
        $this->id_team_1 = mysql_result($res_protocols, $row, "team_1");
        $this->id_team_2 = mysql_result($res_protocols, $row, "team_2");
        $this->team_1 = iconv('windows-1251', 'UTF-8', mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $this->id_team_1"), 0, "title"));
        $this->team_2 = iconv('windows-1251', 'UTF-8', mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $this->id_team_2"), 0, "title"));
        $res_team1 = mysql_query("SELECT * FROM team, competitor WHERE competitor.id_competitor = $this->id_team_1 AND competitor.id_team = team.id_team");
        if (mysql_result($res_team1, 0, "emblem_path") != null) {
            $this->team_1_emblem = $base_url . "emblems/" . mysql_result($res_team1, 0, "emblem_path");
        }
        $res_team2 = mysql_query("SELECT * FROM team, competitor WHERE competitor.id_competitor = $this->id_team_2 AND competitor.id_team = team.id_team");
        if (mysql_result($res_team2, 0, "emblem_path") != null) {
            $this->team_2_emblem = $base_url . "emblems/" . mysql_result($res_team2, 0, "emblem_path");
        }
        $this->dt_protocol = mysql_result($res_protocols, $row, "dt_protocol");
        $this->score_half_1 = mysql_result($res_protocols, $row, "score_half_1");
        $this->score_end_1 = mysql_result($res_protocols, $row, "score_end_1");
        $this->score_half_2 = mysql_result($res_protocols, $row, "score_half_2");
        $this->score_end_2 = mysql_result($res_protocols, $row, "score_end_2");
        $this->score_half_1 = mysql_result($res_protocols, $row, "score_half_1");
        $this->fouls_half_1 = mysql_result($res_protocols, $row, "fouls1_1");
        $this->fouls_end_1 = mysql_result($res_protocols, $row, "fouls1_2");
        $this->fouls_half_2 = mysql_result($res_protocols, $row, "fouls2_1");
        $this->fouls_end_2 = mysql_result($res_protocols, $row, "fouls2_2");
        $this->forfeited = mysql_result($res_protocols, $row, "forfeited");
        $this->on_penalty = mysql_result($res_protocols, $row, "on_penalty");
        $this->extratime = mysql_result($res_protocols, $row, "extratime");
        return $this;
    }
}

class ProtocolList
{
    public $protocols;

    /**
     * Список протоколов
     * @param bool $isCompetitor TRUE=Команда-участница, FALSE=Команда историческая
     * @param int $id_competitor
     * @param string $order_clause
     * @param int $limit
     */
    public function getProtocols($isCompetitor, $id_competitor, $order_clause, $limit)
    {
        global $base_url;
        //участник турнира или общая команда
        if ($isCompetitor) {
            $where_clause = "team_1 = $id_competitor OR team_2 = $id_competitor";
        } else {
            $where_clause = "team_1 IN (SELECT id_competitor FROM competitor WHERE id_team IN ($id_competitor)) OR team_2 IN (SELECT id_competitor FROM competitor WHERE id_team IN ($id_competitor))";
        }
        if ($limit > 0) {
            $limit_clause = "LIMIT 0," . $limit;
        } else {
            $limit_clause = null;
        }
        $query = "SELECT * FROM protocol WHERE " . $where_clause . " ORDER BY dt_protocol " . $order_clause . " " . $limit_clause;
        $res_protocols = mysql_query($query);
        $this->protocols = array();
        for ($n = 0; $n < mysql_num_rows($res_protocols); $n++) {
            $protocol = new Protocol();
            $this->protocols[$n] = $protocol->getProtocolData($res_protocols, $n);
        }
    }
}

class TableItem
{
    public $competition_id;
    public $team_id;
    public $position;
    public $title;
    public $emblem_path;
    public $pld;
    public $won;
    public $drew;
    public $lost;
    public $scored;
    public $missed;
    public $pts;
    public $highlight;
}

class TableEnvelope
{
    public $table;
}

class ImmediateGames
{
    public $protocols;
    public $schedules;
}

class TeamsListEnvelope
{
    public $teams;
}

class TeamDetailsEnvelope
{
    public $teamdetails;
}

class DetailedProtocolEnvelope
{
    public $detailedProtocol;
}

class BetRate
{
    public $bet_1;
    public $bet_0;
    public $bet_2;
    public $bet_total;
    public $bet_total_more;
    public $bet_total_less;
}

class Bet
{
    public $bet_type;
    public $bet_total;
    public $bet_rate;
    public $bet_amount;
    public $bet_played;
    public $bet_protocol;
    public $bet_result;
    public $bet_profit;

    /**
     * Наличие и сумма ставки конкретным игроком
     * @param int $user_id id пользователя
     * @param int $schedule_id id игры в расписании
     * @return Bet Bet
     */
    public function getBetDetails($user_id, $schedule_id)
    {
        $query = "SELECT * FROM bet WHERE user_id = $user_id AND id_schedule = $schedule_id";
        $res_bet = mysql_query($query);
        if (mysql_num_rows($res_bet) == 1) {
            $this->bet_type = (int)mysql_result($res_bet, 0, "bet_type");
            $this->bet_total = (float)mysql_result($res_bet, 0, "bet_total");
            $this->bet_rate = mysql_result($res_bet, 0, "bet_rate");
            $this->bet_amount = (float)mysql_result($res_bet, 0, "bet_amount");
            $this->bet_played = (int)mysql_result($res_bet, 0, "bet_played");
            $this->bet_protocol = mysql_result($res_bet, 0, "bet_protocol");
            $this->bet_result = mysql_result($res_bet, 0, "bet_result");
            $this->bet_profit = (float)mysql_result($res_bet, 0, "bet_profit");
        } else {
            $this->bet_amount = null;
        }
        return $this;
    }
}

class BetLine
{
    public $line_id;
    public $name;
    public $description;
    public $valid_to;
    public $min_bets;
    public $init_amount;

    /**
     * Получение текущей линии ставок
     * @return BetLine|null
     */
    public function getCurrentBetLine()
    {
        $query = "SELECT * FROM bet_line ORDER BY line_id DESC LIMIT 0,1";
        $res_line = mysql_query($query);
        if (mysql_num_rows($res_line) == 1) {
            $this->line_id = (int)mysql_result($res_line, 0, "line_id");
            $this->name = iconv('windows-1251', 'UTF-8', mysql_result($res_line, 0, "name"));
            $this->description = mysql_result($res_line, 0, "description");
            $this->valid_to = mysql_result($res_line, 0, "valid_to");
            $this->min_bets = (int)mysql_result($res_line, 0, "min_bets");
            $this->init_amount = (float)mysql_result($res_line, 0, "init_amount");
        }
        return $this;
    }
}

class BetAccount
{
    public $user_id;
    public $position;
    public $username;
    public $amount;
    public $num_bets;
    public $amount_in_play;
    public $highlight;

    /**
     * Получение деталей аккаунта в ставках
     * @return $this
     */
    public function getBetAccount()
    {
        $betline = new BetLine();
        $betline->getCurrentBetLine();
        $query = "SELECT * FROM bet_account b, m_user u WHERE b.user_id = u.user_id AND b.user_id = $this->user_id AND b.bet_line = $betline->line_id";
        $res_account = mysql_query($query);
        if (mysql_num_rows($res_account) == 1) {
            $this->username = iconv('cp1251', 'utf8', mysql_result($res_account, 0, "u.username"));
            $this->getAmount();
            $this->getNumBets();
            $this->getAmountInPlay();
            $this->highlight = TRUE;
        }
        return $this;
    }

    /**
     * Создание аккаунда для ставок
     * @param int $user_id id пользователя из m_user
     * @param double $init_amount Начальная сумма
     * @param int $bet_line Линия ставок
     * @return $this|null Аккаунт
     */
    public function createBetAccount($user_id)
    {
        $betline = new BetLine();
        $betline->getCurrentBetLine();

        //проверяем, что конкурс еще не завершился
        if (time() < $betline->valid_to) {
            $query = "INSERT IGNORE INTO bet_account (user_id, amount, bet_line) VALUES ($user_id, $betline->init_amount, $betline->line_id)";
            $res_create = mysql_query($query);
            if ($res_create) {
                $this->user_id = (int)$user_id;
                $this->amount = $betline->init_amount;
                $this->num_bets = 0;
                $this->amount_in_play = 0;
                return $this;
            } else {
                //если учетка не создалась, то возвращаем null
                return null;
            }
        } else {
            //если конкурс завершился, то возвращаем null
            return null;
        }
    }

    /**
     * проверка существования аккаунта в ставках
     * @param $user_id int id пользователя
     * @return bool
     */
    public
    function hasBetAccount()
    {
        $betline = new BetLine();
        $betline->getCurrentBetLine();

        $query = "SELECT * FROM bet_account WHERE user_id = $this->user_id AND bet_line = $betline->line_id";
        $res_bet_account = mysql_query($query);

        if (mysql_num_rows($res_bet_account) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Текущий остаток на счету
     * @return float Остаток
     */
    public
    function getAmount()
    {
        $betline = new BetLine();
        $betline->getCurrentBetLine();

        $query = "SELECT amount FROM bet_account WHERE user_id = $this->user_id AND bet_line = $betline->line_id";
        $res_amount = mysql_query($query);
        $this->amount = (float)mysql_result($res_amount, 0, "amount");
        return $this->amount;
    }

    /**
     * Количество сделанных ставок
     * @return int Количество ставок
     */
    public
    function getNumBets()
    {
        $betline = new BetLine();
        $betline->getCurrentBetLine();

        $query = "SELECT COUNT(id_schedule) AS num_bets FROM bet WHERE user_id = $this->user_id AND bet_line = $betline->line_id";
        $res_num_bets = mysql_query($query);
        $this->num_bets = (int)mysql_result($res_num_bets, 0, "num_bets");
        return $this->num_bets;
    }

    /**
     * Сумма в розыгрыше
     * @return float Сумма в розыгрыше
     */
    public
    function getAmountInPlay()
    {
        $betline = new BetLine();
        $betline->getCurrentBetLine();

        $query = "SELECT SUM(bet_amount) AS amount_in_play FROM bet WHERE user_id = $this->user_id AND bet_played != 1 AND bet_line = $betline->line_id";
        $res_amount_in_play = mysql_query($query);
        $this->amount_in_play = (float)mysql_result($res_amount_in_play, 0, "amount_in_play");
        if ($this->amount_in_play == null) {
            $this->amount_in_play = 0;
        }
        return $this->amount_in_play;
    }

    /**
     * Пополнение счета
     * @return boolean Сумма в розыгрыше
     */
    public
    function chargeBetAccount($amount = 0)
    {
        $betline = new BetLine();
        $betline->getCurrentBetLine();

        $query = "UPDATE bet_account SET amount = amount + $amount WHERE user_id = $this->user_id AND bet_line = $betline->line_id";
        //$message = "\n" . $now . " User " . $user_id. " CHARGED BET ACCOUNT ON " .$amount;
        $message = "\n" . $now . " DEBUG: " . $query;
        writeLog($log_filename, $message);
        //$res_charge = mysql_query($query);
        $res_charge = TRUE;
        if ($res_charge) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

class BetLineTable
{
    public $betAccounts;
}

class UserBet extends Bet
{
    public $id_schedule;
    public $league;
    public $team_1;
    public $team_1_emblem_path;
    public $team_2;
    public $team_2_emblem_path;
    public $date;
    public $time;
}

class UserBetList
{
    public $userbets;
}

class ContactPerson
{
    public $name;
    public $title;
    public $worktel;
    public $mobtel;
    public $hometel;
}

class ContactFederation
{
    public $title;
    public $contact;
}

class Contacts
{
    public $contacts;
}

?>