<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

if (isset($_GET["protocol_id"])) {
    $id_protocol = intval($_GET["protocol_id"]);
} else {
    $error = new Error();
    $error->sendErrorMessage("Неверные параметры запроса", 400);
}

$res_protocol = mysql_query("SELECT * FROM protocol WHERE id_protocol = $id_protocol");
if (mysql_num_rows($res_protocol) == 1) {
    $protocol = new Protocol();
    $protocol->getProtocolData($res_protocol, 0);
} else {
    $error = new Error();
    $error->sendErrorMessage("Неверные параметры запроса", 400);
}

$current_season = new Season();
$current_season->getCurrentSeason();

$staff_home_query = "SELECT *
FROM staff AS s, application AS a
WHERE s.id_protocol = $protocol->id_protocol
      AND a.id_competitor = $protocol->id_team_1
      AND s.id_player = a.id_applicant
ORDER BY a.goalie DESC, IF(a.number IS NULL,1,0), a.number";

$staff_away_query = "SELECT *
FROM staff AS s, application AS a
WHERE s.id_protocol = $protocol->id_protocol
      AND a.id_competitor = $protocol->id_team_2
      AND s.id_player = a.id_applicant
ORDER BY a.goalie DESC, IF(a.number IS NULL,1,0), a.number";

$res_staff_home = mysql_query($staff_home_query);
$res_staff_away = mysql_query($staff_away_query);

//формируем объект
$detailedProtocol = new DetailedProtocol();
$detailedProtocol->protocol = $protocol;
$detailedProtocol->players_home = array();
$detailedProtocol->players_home = $detailedProtocol->getPlayerInGame($res_staff_home);
$detailedProtocol->players_away = array();
$detailedProtocol->players_away = $detailedProtocol->getPlayerInGame($res_staff_away);

//оборачиваем для возврата
$detailedProtocol_envelope = new DetailedProtocolEnvelope();
$detailedProtocol_envelope->detailedProtocol = $detailedProtocol;

http_response_code(200);
echo json_encode($detailedProtocol_envelope, JSON_UNESCAPED_UNICODE);

?>
