<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

$id_team = intval($_GET["team_id"]);

$res_team = mysql_query("SELECT * FROM team WHERE id_team = $id_team");

if (!$res_team OR mysql_num_rows($res_team) != 1) {
    $error = new Error();
    $error->sendErrorMessage("Не удалось найти информацию о команде", 400);
}

//получаем список сыгранных матчей
$protocollist = new ProtocolList();
$protocollist->getProtocols(FALSE, $id_team, "DESC", 1);

//получаем список сыгранных матчей
$schedulelist = new ScheduleList();
$schedulelist->getNextMatches(FALSE, $id_team, 2);

//оборачиваем для ответа
$immediate_games = new ImmediateGames();
$immediate_games->protocols = $protocollist->protocols;
$immediate_games->schedules = $schedulelist->schedules;

http_response_code(200);
echo json_encode($immediate_games, JSON_UNESCAPED_UNICODE);

?>
