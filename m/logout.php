<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');

/* выбивание с нескольких устройств
if (!empty($_SESSION["user_id"])) {
    $user_id = $_SESSION["user_id"];
    $query_stop = "UPDATE m_session SET is_active = 0 WHERE user_id = $user_id AND SID = '" . session_id() . "'";
    mysql_query($query_stop);
}
*/

//стартуем сессию
session_set_cookie_params(getParameter("mobile_app_session_lifetime"), getParameter("mobile_app_session_path"));
session_start();
//$message = "Session start on logout";
//writeLog($log_filename, $message);
$destroy = session_destroy();

$result = new Result();
$result->result = $destroy;

//удаляем uid, чтобы не отправлять пуш незалогиненным пользователям
if (!empty($_SESSION["user_id"]) AND !empty($_SESSION["onesignal_id"])) {
    $user_id = $_SESSION["user_id"];
    $device_id = $_SESSION["onesignal_id"];
    $query = "DELETE FROM m_uid WHERE user_id = $user_id AND device_id = '$device_id'";
    //$message = "DEBUG: " . $query;
    //writeLog($log_filename, $message);
    mysql_query($query);
}

if ($destroy) {
    http_response_code(200);
} else {
    http_response_code(400);
}

echo(json_encode($result, JSON_UNESCAPED_UNICODE));

?>
