<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');
include('user_structure.php');

$current_season = new Season();
$current_season->getCurrentSeason();

// Открываем на чтение поток ввода и получаем содержимое потока
$thread = fopen('php://input', 'r');
$post_data = stream_get_contents($thread);

if ($post_data) {
    $login_data = json_decode($post_data);

    if (empty($login_data->email) OR empty($login_data->password)) {
        $message = "Empty email or password";
        writeLog($log_filename, $message);
        $error = new Error();
        $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
    } else {
        $login = iconv('utf8', 'cp1251',mysql_real_escape_string(strtolower(trim($login_data->email))));
        $password = mysql_real_escape_string($login_data->password);
        if (!empty($login_data->password)) {
            $device_uid = mysql_real_escape_string(trim($login_data->uid));
        } else {
            $device_uid = null;
        }

    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
}

$query = "SELECT * FROM m_user WHERE (LOWER(email) = '$login' OR LOWER(username) = '$login') AND password = '$password'";
$res_auth = mysql_query($query);

if (mysql_num_rows($res_auth) == 1) {

    if (mysql_result($res_auth, 0, "activated") != 1) {
        $message = "Trying to login non-activated user: " . $login;
        writeLog($log_filename, $message);
        $error = new Error();
        $error->sendErrorMessage("Необходимо активировать учетную запись", 400);
    }

    //определяем объект
    $current_time = time();
    $MUser = new MUser();
    $MUser->user_id = mysql_result($res_auth, 0, "user_id");
    $MUser->username = mysql_result($res_auth, 0, "username");
    $MUser->email = mysql_result($res_auth, 0, "email");
    $MUser->payload_value = mysql_result($res_auth, 0, "payload_value");
    $MUser->payload_timestamp = mysql_result($res_auth, 0, "payload_timestamp") + getParameter("mobile_app_payload_validity"); //+1 год;
    if ($MUser->payload_timestamp < $current_time) {
        $MUser->payload_timestamp = NULL;
        $MUser->payload_value = NULL;
    }
    $MUser->user_player = mysql_result($res_auth, 0, "user_player");
    $MUser->avatar = mysql_result($res_auth, 0, "avatar");
    $MUser->device_id = $device_uid;
    $MUser->getFavourites();
    $result = new MUserEnvelope();
    $result->user = $MUser;
    $res_update = mysql_query("UPDATE m_user SET last_login = $current_time WHERE user_id = $MUser->user_id");

    //проверяем, нет ли у пользователя других активных сессий
    /*
    $query = "SELECT * FROM m_session WHERE user_id = $MUser->user_id AND SID <> '" . session_id() . "'";
    if (mysql_num_rows(mysql_query($query)) > 0) {
        //если есть, то помечаем остальные как неактивные
        $query_stop = "UPDATE m_session SET is_active = 0 WHERE user_id = $MUser->user_id";
        $res_stop = mysql_query($query_stop);
    }

    //записываем новую сессию как активную
    $query_start = "INSERT INTO m_session (SID, user_id, is_active) VALUES ('" . session_id() . "', $MUser->user_id, 1) ON DUPLICATE KEY UPDATE user_id = $MUser->user_id, is_active = 1";
    if (!mysql_query($query_start)) {
        $error = new Error();
        $error->sendErrorMessage("Произошла ошибка при старте сесии", 400);
    }
    */

    $user_agent = $headers["User-Agent"];

    //записываем uid. Если кто-то уже входил с такого uid, то перезаписываем user_id
    $query_uid = "INSERT INTO m_uid (user_id, device_id, `user-agent`) VALUES ($MUser->user_id, '$MUser->device_id', '$user_agent') ON DUPLICATE KEY UPDATE user_id = $MUser->user_id, `user-agent` = '$user_agent'";

    //$message = "DEBUG: " . $query_uid;
    //writeLog($log_filename, $message);
    if (!mysql_query($query_uid)) {
        $message = "Unable to write OR update UID: " . $query_uid;
        writeLog($log_filename, $message);
    }

    //стартуем сессию
    session_set_cookie_params(getParameter("mobile_app_session_lifetime"), getParameter("mobile_app_session_path"));
    session_start();
    //$message = "Session start on login";
    //writeLog($log_filename, $message);
    $_SESSION['user_id'] = $MUser->user_id;
    $_SESSION['onesignal_id'] = $MUser->device_id;

    $message = "Success login: user_id=" . $MUser->user_id . ", uid=" . $MUser->device_id .", login=" .$MUser->username;
    writeLog($log_filename, $message);

    http_response_code(200);
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
} else {
    $message = "Failed login: login=" . $login . ", password=" . $password . ", uid=" . $device_uid . ", header=" . $headers["User-Agent"];
    writeLog($log_filename, $message);
    //session_destroy();
    $error = new Error();
    $error->sendErrorMessage("Неверный логин или пароль", 400);
}

?>
