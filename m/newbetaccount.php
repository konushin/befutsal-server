<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');
include('user_structure.php');

$betaccount = new BetAccount();
$betaccount->user_id = $user_id;

if ($betaccount->hasBetAccount()) {
    $error = new Error();
    $error->sendErrorMessage("Вы уже согласились с условиями конкурса", 400);
} else {
    $betaccount->createBetAccount($user_id);
    $message = "\n" . $now . " NEW BET ACCOUNT REGISTERED: " . $user_id;
    writeLog($log_filename, $message);
    http_response_code(200);
    echo json_encode($betaccount, JSON_UNESCAPED_UNICODE);
}

?>
