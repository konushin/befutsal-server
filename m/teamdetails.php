<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

if (isset($_GET["team_id"]) AND isset($_GET["competition_id"])) {
    $id_team = intval($_GET["team_id"]);
    $league = intval($_GET["competition_id"]);
} else {
    $error = new Error();
    $error->sendErrorMessage("Неверные параметры запроса", 400);
}

$current_season = new Season();
$current_season->getCurrentSeason();

//получаем общие детали команды
$teamdetails_query = "SELECT * FROM team AS t, competitor AS c WHERE t.id_team = $id_team AND c.id_team = t.id_team AND c.id_competition = $league";

$res_teamdetails = mysql_query($teamdetails_query);
$num_teamdetails = mysql_num_rows($res_teamdetails);

if (!$res_teamdetails OR $num_teamdetails != 1) {
    $error = new Error();
    $error->sendErrorMessage("Не удалось найти информацию о команде", 400);
}

//формируем объект
$teamdetails = new TeamDetails();
$teamdetails->id_team = mysql_result($res_teamdetails, 0, "t.id_team");
$teamdetails->title = iconv('windows-1251', 'UTF-8', mysql_result($res_teamdetails, 0, "t.title"));
$teamdetails->founded = mysql_result($res_teamdetails, 0, "t.founded");
if (mysql_result($res_teamdetails, 0, "t.emblem_path") != null) {
    $teamdetails->emblem_path = $base_url . "emblems/" . mysql_result($res_teamdetails, 0, "t.emblem_path");
}
$teamdetails->getCompetitions($current_season->id_season);
$teamdetails->competitor_id = mysql_result($res_teamdetails, 0, "c.id_competitor");
$teamdetails->competitor_title = iconv('windows-1251', 'UTF-8', mysql_result($res_teamdetails, 0, "c.title"));
$tshirt_home = mysql_result($res_teamdetails, 0, "tshirt_home");
if ($tshirt_home != null) {
    $res_tshirt = mysql_query("SELECT * FROM `t-shirt` WHERE id_tshirt = $tshirt_home");
    $teamdetails->tshirt_home = $base_url . "graphics/" . mysql_result($res_tshirt, 0, "picture");
}
$tshirt_away = mysql_result($res_teamdetails, 0, "tshirt_away");
if ($tshirt_away != null) {
    $res_tshirt = mysql_query("SELECT * FROM `t-shirt` WHERE id_tshirt = $tshirt_away");
    $teamdetails->tshirt_away = $base_url . "graphics/" . mysql_result($res_tshirt, 0, "picture");
}

//заполняем структуру таблицы
$res_table = getLeagueTable($league, "9999-12-31 23:59:59");

$teamdetails->table = array();

for ($i = 0; $i < mysql_num_rows($res_table); $i++) {
    $table_item = new TableItem();
    $table_item->competition_id = mysql_result($res_table, $i, "id_league");
    $id_competitor = mysql_result($res_table, $i, "id_competitor");
    $res_team = mysql_query("SELECT * FROM competitor WHERE id_competitor = $id_competitor");
    $table_item->team_id = mysql_result($res_team, 0, "id_team");
    $table_item->position = $i+1;
    $table_item->title = iconv('windows-1251', 'UTF-8', mysql_result($res_table, $i, "name"));
    $table_item->pld = mysql_result($res_table, $i, "pld");
    $table_item->won = mysql_result($res_table, $i, "won");
    $table_item->drew = mysql_result($res_table, $i, "drew");
    $table_item->lost = mysql_result($res_table, $i, "lost");
    $table_item->scored = mysql_result($res_table, $i, "goalsfor");
    $table_item->missed = mysql_result($res_table, $i, "goalsagainst");
    $table_item->pts = mysql_result($res_table, $i, "pts");
    if (mysql_result($res_table, $i, "id_competitor") == $teamdetails->competitor_id) {
        $row_number = $i;
        $table_item->highlight = true;
    } else {
        $table_item->highlight = false;
    }
    $teamdetails->table[$i] = $table_item;
}

//заполняем детали команды
if (!isset($row_number)) {
    $error = new Error();
    $error->sendErrorMessage("Не удалось найти информацию о выступлении команды в выбранном турнире", 400);
}

$teamdetails->pld = mysql_result($res_table, $row_number, "pld");
$teamdetails->won = mysql_result($res_table, $row_number, "won");
$teamdetails->drew = mysql_result($res_table, $row_number, "drew");
$teamdetails->lost = mysql_result($res_table, $row_number, "lost");
$teamdetails->scored = mysql_result($res_table, $row_number, "goalsfor");
$teamdetails->missed = mysql_result($res_table, $row_number, "goalsagainst");
$teamdetails->pts = mysql_result($res_table, $row_number, "pts");

//получаем список игроков
$player_list = new PlayerList();
$player_list->getPlayers($teamdetails->competitor_id);
$teamdetails->players = $player_list->players;

//получаем список следующих матчей
$next_matches = new ScheduleList();
$next_matches->getNextMatches(TRUE, $teamdetails->competitor_id, 0);
$teamdetails->next_matches = $next_matches->schedules;

//получаем список сыгранных матчей
$protocol_list = new ProtocolList();
$protocol_list->getProtocols(TRUE, $teamdetails->competitor_id, "DESC", 0);
$teamdetails->protocols = $protocol_list->protocols;

//оборачиваем для возврата
$teamdetails_envelope = new TeamDetailsEnvelope();
$teamdetails_envelope->teamdetails = $teamdetails;

http_response_code(200);
echo json_encode($teamdetails_envelope, JSON_UNESCAPED_UNICODE);

?>
