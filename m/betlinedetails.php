<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

$betline = new BetLine();
$betline->getCurrentBetLine();

http_response_code(200);
echo json_encode($betline, JSON_UNESCAPED_UNICODE);

?>