<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');
include('user_structure.php');

// Открываем на чтение поток ввода и получаем содержимое потока
$thread = fopen('php://input', 'r');
$post_data = stream_get_contents($thread);

$betaccount = new BetAccount();
$betaccount->user_id = $user_id;

$betline = new BetLine();
$betline->getCurrentBetLine();

if ($betaccount->hasBetAccount()) {

    if ($post_data) {
        $bet_data = json_decode($post_data);
        $id_schedule = intval($bet_data->schedule_id);
        $bet_type = intval($bet_data->bet_type);
        $bet_amount = floatval($bet_data->amount);

        //проверяем размер ставки
        if ($bet_amount < 0.01) {
            //$message = "\n" . $now . " DEBUG: Неверная сумма ставки - " . $bet_amount;
            //writeLog($log_filename, $message);
            $error = new Error();
            $error->sendErrorMessage("Неверная сумма", 400);
        }

        //проверяем существование ставки в расписании и что матч еще не начался
        $query = "SELECT * FROM schedule WHERE id_schedule = $id_schedule AND bet_1 IS NOT NULL AND CONCAT_WS(' ', date, time) > NOW()";
        $res_schedule = mysql_query($query);
        if (mysql_num_rows($res_schedule) != 1) {
            //$message = "\n" . $now . " DEBUG: " . $query;
            //writeLog($log_filename, $message);
            $error = new Error();
            $error->sendErrorMessage("Прогноза не существует или матч уже начался", 400);
        } else {
            //проверяем, что матч начнется не позже окончания конкурса
            $schedule_date = mysql_result($res_schedule, 0, "date");
            $schedule_date_unix = strtotime($schedule_date);

            //разрешаем пользователю ilushka делать ставки, для отладки
            //if ($schedule_date_unix > $betline->valid_to AND $user_id != 420) {
            if ($schedule_date_unix > $betline->valid_to) {
                $message = "FAILED BET INSERT: id_schedule=" . $id_schedule . " out of betline date (" . $schedule_date . " (" . $schedule_date_unix . ") > " . $betline->valid_to . ")";
                writeLog($log_filename, $message);
                $error = new Error();
                $error->sendErrorMessage("Матч начнётся позже окончания конкурса прогнозов", 400);
            } else {
                $bet_total = mysql_result($res_schedule, 0, "bet_total");
                switch ($bet_type) {
                    case 1:
                        $bet_rate = mysql_result($res_schedule, 0, "bet_1");
                        break;
                    case 0:
                        $bet_rate = mysql_result($res_schedule, 0, "bet_0");
                        break;
                    case 2:
                        $bet_rate = mysql_result($res_schedule, 0, "bet_2");
                        break;
                    case 3:
                        $bet_rate = mysql_result($res_schedule, 0, "bet_total_more");
                        break;
                    case 4:
                        $bet_rate = mysql_result($res_schedule, 0, "bet_total_less");
                        break;
                }
            }
        }


    } else {
        $error = new Error();
        $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
    }

    $query = "SELECT * FROM m_user WHERE user_id = $user_id";
    $res_user = mysql_query($query);

    if (mysql_num_rows($res_user) == 1) {

        //проверяем, что пользователь принял условия розыгрыша
        $query = "SELECT * FROM bet_account WHERE user_id = $user_id AND bet_line = $betline->line_id";
        $bet_account = mysql_query($query);
        if (mysql_num_rows($bet_account) == 1) {
            $account = new BetAccount();
            $account->user_id = $user_id;
            $account->getAmount();

            //проверяем достаточность средств на счету
            if ($account->amount >= $bet_amount) {

                //если ставка уже была, нужно учесть ее при списывании средств
                $query = "SELECT bet_amount FROM bet WHERE user_id = $user_id AND id_schedule = $id_schedule";
                //$message = "\n" . $now . " DEBUG: " . $query;
                //writeLog($log_filename, $message);
                $res_bet = mysql_query($query);
                if (mysql_num_rows($res_bet) == 1) {
                    $amount_to_return = mysql_result($res_bet, 0, "bet_amount");
                } else {
                    $amount_to_return = 0;
                }

                //средств достаточно, сохраняем ставку и списываем средства
                $current_time = time();
                $query1 = "INSERT INTO bet (user_id, bet_line, id_schedule, bet_type, bet_total, bet_rate, bet_amount, bet_timestamp, bet_played) VALUES ($user_id, $betline->line_id, $id_schedule, $bet_type, $bet_total, $bet_rate, $bet_amount, $current_time, 0) ON DUPLICATE KEY UPDATE bet_type = $bet_type, bet_total = $bet_total, bet_rate = $bet_rate, bet_amount = $bet_amount, bet_timestamp = $current_time;";
                $query2 = "UPDATE bet_account SET amount = amount + $amount_to_return - $bet_amount WHERE user_id = $user_id;";
                //$message = "\n" . $now . " DEBUG: " . $query1 . "\n" . $query2;
                //writeLog($log_filename, $message);
                $res_query1 = mysql_query($query1);
                $res_query2 = mysql_query($query2);

                if ($res_query1 AND $res_query2) {
                    http_response_code(200);
                    $result = new Result();
                    $result->result = true;
                    echo json_encode($result, JSON_UNESCAPED_UNICODE);
                    $message = "\n NEW BET: user=" . $betaccount->user_id . ", schedule=" . $id_schedule . ", type=" . $bet_type . ", amount=" . $bet_amount . "\n";
                    writeLog($log_filename, $message);
                } else {
                    $message = "\n FAILED BET INSERT: " . $query1 . "\n" . $query2;
                    writeLog($log_filename, $message);
                    $error = new Error();
                    $error->sendErrorMessage("Произошла непредвиденная ошибка", 400);
                }
            } else {
                $error = new Error();
                $error->sendErrorMessage("Недостаточно средств на счёте", 400);
            }
        } else {
            $error = new Error();
            $error->sendErrorMessage("Вы не приняли условия конкурса прогнозов", 400);
        }
    } else {
        $error = new Error();
        $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Вы не приняли условия конкурса прогнозов", 423);
}

?>
