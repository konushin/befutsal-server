<?php

$now = date("Y-m-d H:i:s");
$pre_date = date("Ym");

$log_filename = "../logs/mobile" . $pre_date . ".log";
$base_url = "https://befutsal.ru/";

$headers = apache_request_headers();

include("../database_main.php");
include("../includings/functions.php");

class Result
{
    public $result;
}

class Error
{
    public $errorMsg;

    public function sendErrorMessage($message, $code)
    {
        $this->errorMsg = $message;
        http_response_code($code);
        die(json_encode($this, JSON_UNESCAPED_UNICODE));
    }
}

//определяем поддерживаемую версию, в зависимости от ОС клиента
if (stripos(strtolower($headers["User-Agent"]), "android") !== false) {
    $version_supported = getParameter("mobile_app_android_v_supported");
    $version_start_symbol = stripos($headers["User-Agent"], "-") + 1;
    $version = substr($headers["User-Agent"], $version_start_symbol);
    $major_version = str_replace(".", "", $version);
} elseif (stripos(strtolower($headers["User-Agent"]), "ios-") !== false) {
    $version_supported = getParameter("mobile_app_ios_v_supported");
    $version_start_symbol = stripos($headers["User-Agent"], "-") + 1;
    $version = substr($headers["User-Agent"], $version_start_symbol);
    $major_version = str_replace(".", "", $version);
} else {
    $version_supported = 0.2;
    $major_version = 1;
}

//$message = "DEBUG: v=" . $version . ", major_version=" . $major_version . ", header=" . $headers["User-Agent"];
//writeLog($log_filename, $message);

if ($major_version < $version_supported) {
    $message = "Old client: v=" . $version . ", major_version=" . $major_version . ", header=" . $headers["User-Agent"];
    writeLog($log_filename, $message);
    $error = new Error();
    $error->sendErrorMessage("Необходимо обновить версию приложения", 426);
}

//проверяем при любом действии
//если НЕ находимся на register, login, logout, changepassword
//и клиент определен
if ($version_supported > 0 AND stripos($_SERVER["REQUEST_URI"], "login") === FALSE AND stripos($_SERVER["REQUEST_URI"], "register") === FALSE AND stripos($_SERVER["REQUEST_URI"], "logout") === FALSE AND stripos($_SERVER["REQUEST_URI"], "changepassword") === FALSE) {

    session_set_cookie_params(getParameter("mobile_app_session_lifetime"), getParameter("mobile_app_session_path"));
    //session_save_path($_SERVER['DOCUMENT_ROOT'] . getParameter("mobile_app_session_path"));
    //ini_set('session.gc_maxlifetime', getParameter("mobile_app_session_lifetime"));
    session_start();

    $need_to_prolong = TRUE;

    //если сессия истекла
    if (empty($_SESSION["user_id"])) {
        if (isset($_COOKIE["PHPSESSID"])) {
            $message = "Unable to continue! Empty user_id, PHPSESSID=" . $_COOKIE["PHPSESSID"];
        } else {
            $message = "Unable to continue! Empty SESSION attribute user_id.";
        }
        writeLog($log_filename, $message);
        $is_active = FALSE;
    } else {
        $user_id = $_SESSION["user_id"];
        //$message = "DEBUG: User_id = ".$user_id;
        //writeLog($log_filename, $message);
        /* выбивание с нескольких устройств
        $query = "SELECT * FROM m_session WHERE SID = '" . session_id() . "' AND user_id = $user_id AND is_active = 1";
        $res_session = mysql_query($query);
        if (mysql_num_rows($res_session) > 0) {
            $is_active = TRUE;
        } else {
            $is_active = FALSE;
        }
        */
        $is_active = TRUE;
    }
} else {
    //находимся на странице register, login, logout или changepassword или клиент не определен (<2.x)
    $is_active = TRUE;
    $need_to_prolong = FALSE;
}

if ($is_active) {
    if ($need_to_prolong) {
        //сессия жива, продлеваем её
        setcookie(session_name(), session_id(), time() + getParameter("mobile_app_session_lifetime"), getParameter("mobile_app_session_path"));

        //$message = "Session prolong in common: " . $_COOKIE["PHPSESSID"];
        //writeLog($log_filename, $message);
    }
} else {
    //пользователь неавторизован
    //session_destroy();
    $message = "Unauthorized action. " . $_SERVER["REQUEST_URI"];
    writeLog($log_filename, $message);
    $error = new Error();
    $error->sendErrorMessage("Необходимо авторизоваться", 401);
}

?>
