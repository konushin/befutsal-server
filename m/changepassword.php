<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('user_structure.php');

// Открываем на чтение поток ввода и получаем содержимое потока
$thread = fopen('php://input', 'r');
$post_data = stream_get_contents($thread);

if ($post_data) {
    $user_data = json_decode($post_data);

    if (isset($user_data->login)) {
        $login = mysql_real_escape_string($user_data->login);
        //$message = "DEBUG: login: " .$login;
        //writeLog($log_filename, $message);
    } else {
        $error = new Error();
        $error->sendErrorMessage("Неверный запрос", 400);
        $message = "ERROR: no login received";
        writeLog($log_filename, $message);
    }

    if (isset($user_data->new_password) and isset($user_data->old_password)) {
        $new_password = mysql_real_escape_string($user_data->new_password);
        $old_password = mysql_real_escape_string($user_data->old_password);
        $using_generator = "NO";
    } elseif ((isset($user_data->new_password) and !isset($user_data->old_password)) OR (!isset($user_data->new_password) and isset($user_data->old_password))) {
        $error = new Error();
        $error->sendErrorMessage("Неверный запрос", 400);
        $message = "ERROR: no passwords received though declared for: " . $login;
        writeLog($log_filename, $message);
    } else {
        $new_password_raw = mt_rand(1000, 9999);
        $new_password = md5($new_password_raw);
        $using_generator = $new_password_raw;
        $res_old_password = mysql_query("SELECT password FROM m_user WHERE (`username` = '$login' OR email = '$login') AND activated = 1");
        if (mysql_num_rows($res_old_password) > 0) {
            $old_password = mysql_result($res_old_password, 0, "password");
        } else {
            $error = new Error();
            $error->sendErrorMessage("Пользователь не найден", 400);
            $message = "ERROR: user not found: " . $login;
            writeLog($log_filename, $message);
        }
        $new_password_text = "Ваш новый пароль: " . $new_password_raw . ".";
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Неверный запрос", 400);
    $message = "ERROR: no postdata received";
    writeLog($log_filename, $message);
}

$query = "SELECT * FROM m_user WHERE (`username` = '$login' OR email = '$login') AND activated = 1";
$res_user = mysql_query($query);

if (mysql_num_rows($res_user) == 1) {
    $id_user = mysql_result($res_user, 0, "user_id");
    $query = "SELECT * FROM m_user WHERE user_id = $id_user AND password = '$old_password'";
    $res_password = mysql_query($query);
    if (mysql_num_rows($res_password) != 1) {
        $error = new Error();
        $error->sendErrorMessage("Текущий пароль введен неверно.", 401);
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Пользователь не найден.", 400);
    $message = "ERROR: no or more than one user found: " . $login;
    writeLog($log_filename, $message);
}

$query = "UPDATE m_user SET new_password = '$new_password' WHERE user_id = $id_user";
$res_update = mysql_query($query);

if ($res_update) {
    $email = mysql_result($res_user, 0, "email");

    $message = "New password set for user: " . $id_user . ", using generator: " . $using_generator;
    writeLog($log_filename, $message);

    $email_subject = "Восстановление пароля мобильного приложения Живи мини-футболом!";
    $email_headers = 'Content-type: text/html; charset="utf-8"' . "\r\n";
    $email_headers .= 'From: ' . getParameter("email_from") . "\r\n";
    $email_message = "<BODY>
<FONT FACE=Tahoma SIZE=2>
<P ALIGN=left>Здравствуйте.</P>
<P ALIGN=left>Вами был запрошен новый пароль для мобильного приложения befutsal. " . $new_password_text . " Для завершения процесса пройдите по <A HREF='" . getParameter("url_mobile") . getParameter("mobile_app_change_password_link") . $new_password . "'>ссылке</A> или скопируйте ее ниже и вставьте в строку браузера.</P>
<P ALIGN=left>" . getParameter("url_mobile") . getParameter("mobile_app_change_password_link") . $new_password . "</P>
<P ALIGN=left>Если у вас остались вопросы или вы не запрашивали смену пароля в мобильном приложении befutsal, то мы приносим свои извинения и просим вас обратиться к администрации проекта <strong>Живи мини-футболом!</strong> письмом на адрес info@befutsal.ru.</P>
<P ALIGN=left>С уважением,<BR>коллектив сайта Живи мини-футболом!</P>
<P ALIGN=left><FONT SIZE=1>Это письмо сгенерировано роботом, не отвечайте на него!</FONT></P>
</FONT>
</BODY>";

    $message = "Trying to send email to: " . $email . ", subject: " . $email_subject . ", headers: " . $email_headers;
    writeLog($log_filename, $message);

    $sendmail = mail($email, $email_subject, $email_message, $email_headers);
    //$sendmail = true;

    if ($sendmail) {
        $message = "Confirmation link for user " . $login . " sent to: " . $email;
        writeLog($log_filename, $message);
    } else {
        $error = new Error();
        $error->sendErrorMessage("Не удалось отправить ссылку для подтверждения на e-mail", 400);
        $message = "ERROR: Unable to send confirmation link to email: " . $email;
        writeLog($log_filename, $message);
    }

    $result = new Result();
    $result->result = "Ссылка для подтверждения отправлена на " . $email;
    http_response_code(200);
    echo(json_encode($result, JSON_UNESCAPED_UNICODE));
} else {
    $error = new Error();
    $error->sendErrorMessage("Произошла непредвиденная ошибка", 400);
    $message = "ERROR: unexpected error";
    writeLog($log_filename, $message);
}

?>