<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');
include('user_structure.php');

$current_season = new Season();
$current_season->getCurrentSeason();

// Открываем на чтение поток ввода и получаем содержимое потока
$thread = fopen('php://input', 'r');
$post_data = stream_get_contents($thread);

if ($post_data) {
    $user_data = json_decode($post_data);
    $add = mysql_real_escape_string($user_data->add);
    $team_id = intval($user_data->team_id);
    if ($team_id == NULL) {
        $error = new Error();
        $error->sendErrorMessage("Не передана команда", 400);
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
}

$user_id = $_SESSION["user_id"];
$query = "SELECT * FROM m_user WHERE user_id = $user_id";
$res_user = mysql_query($query);

if (mysql_num_rows($res_user) == 1) {

    if ($add) {
        $query = "INSERT IGNORE INTO m_favourites (user_id, team_id) VALUES ($user_id, $team_id)";
    } else {
        $query = "DELETE FROM m_favourites WHERE team_id = $team_id AND user_id = $user_id";
    }
    $res_query = mysql_query($query);

    $MUser = new MUser();
    $MUser->user_id = $user_id;

    if ($res_query) {
        $MUser->getFavourites();
        $favourites = new FavouritesEnvelope();
        $favourites->favourites = $MUser->favourites;
        http_response_code(200);
        echo json_encode($favourites, JSON_UNESCAPED_UNICODE);
    } else {
        $error = new Error();
        $error->sendErrorMessage("Не удалось изменить данные", 400);
    }
}

?>
