<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

if (isset($_GET["player_id"]) AND isset($_GET["player_id"])) {
    $id_player = intval($_GET["player_id"]);
} else {
    $error = new Error();
    $error->sendErrorMessage("Неверные параметры запроса", 400);
}

$current_season = new Season();
$current_season->getCurrentSeason();

$query = "SELECT
  a.*,
  p.*,
  COUNT(s.id_player) AS played,
  SUM(s.scored)      AS scored,
  SUM(s.yellow)      AS booked,
  SUM(s.red)         AS sentoff,
  cn.*,
  cr.*
FROM application AS a
  INNER JOIN player AS p ON p.id_player = a.id_player
  INNER JOIN competitor AS cr ON cr.id_competitor = a.id_competitor
  INNER JOIN competition AS cn ON cn.id_competition = cr.id_competition
  LEFT JOIN staff AS s ON s.id_player = a.id_applicant
WHERE a.id_player = p.id_player
      AND a.id_player = $id_player
      AND cn.season = $current_season->id_season
GROUP BY a.id_applicant
ORDER BY p.name_2, p.name_1";

$res_apps = mysql_query($query);
$num_apps = mysql_num_rows($res_apps);

if ($num_apps < 1) {
    $error = new Error();
    $error->sendErrorMessage("Не удалось найти информацию об игроке", 400);
}

$player_details = new PlayerDetails();
$player_details->player_in_competition = array();

for ($n = 0; $n < mysql_num_rows($res_apps); $n++) {
    $player = new Player();
    $player->id_player = mysql_result($res_apps, $n, "a.id_player");
    $player->name_1 = iconv('windows-1251', 'UTF-8', mysql_result($res_apps, $n, "p.name_1"));
    $player->name_2 = iconv('windows-1251', 'UTF-8', mysql_result($res_apps, $n, "p.name_2"));
    $player->birthdate = mysql_result($res_apps, $n, "p.birthdate");
    if (mysql_result($res_apps, $n, "p.photo_path") != null) {
        $player->photo = $base_url . "photos/" . mysql_result($res_apps, $n, "p.photo_path");
    }
    $player->goalie = mysql_result($res_apps, $n, "a.goalie");
    $player->played = mysql_result($res_apps, $n, "played");
    $player->scored = mysql_result($res_apps, $n, "scored");
    if ($player->scored == null) {
        $player->scored = "0";
    }
    $player->booked = mysql_result($res_apps, $n, "booked");
    if ($player->booked == null) {
        $player->booked = "0";
    }
    $player->sentoff = mysql_result($res_apps, $n, "sentoff");
    if ($player->sentoff == null) {
        $player->sentoff = "0";
    }
    $player->captain = mysql_result($res_apps, $n, "represent");
    $player_details->player_in_competition[$n] = $player;
}

http_response_code(200);
echo json_encode($player_details, JSON_UNESCAPED_UNICODE);

?>
