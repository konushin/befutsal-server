<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');
include('../includings/contacts_federations.php');

http_response_code(200);
echo json_encode($contacts, JSON_UNESCAPED_UNICODE);

?>