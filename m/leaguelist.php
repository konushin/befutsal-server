<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

$current_season = new Season();
$current_season->getCurrentSeason();

//получаем общие детали лиги
$leaguelist_query = "SELECT * 
FROM competition AS c, competition_by_category AS cc, competition_category AS cat
WHERE c.season = $current_season->id_season
  AND c.id_competition = cc.id_competition
  and cc.id_category = cat.id_category
ORDER BY cat.`order` DESC, IFNULL(c.rate, 100), c.title";

$res_leaguelist = mysql_query($leaguelist_query);
$num_leaguelist = mysql_num_rows($res_leaguelist);

if (!$res_leaguelist OR $num_leaguelist < 1) {
    $error = new Error();
    $error->sendErrorMessage("Не удалось найти информацию о турнирах сезона", 400);
}

$leaguelist = array();

for ($i = 0; $i < mysql_num_rows($res_leaguelist); $i++) {
    $competition = new Competition();
    $competition->id_competition = mysql_result($res_leaguelist, $i, "c.id_competition");
    $competition->title = iconv('windows-1251', 'UTF-8', mysql_result($res_leaguelist, $i, "c.title"));
    $competition->icon = $base_url . "graphics/" . mysql_result($res_leaguelist, $i, "cat.picture");

    $leaguelist[$i] = $competition;
}

//оборачиваем для возврата
$leaguelist_envelope = new CompetitionListEnvelope();
$leaguelist_envelope->competitions = $leaguelist;

http_response_code(200);
echo json_encode($leaguelist_envelope, JSON_UNESCAPED_UNICODE);

?>
