#15.07
ALTER TABLE befutsal_main.m_user CHANGE ads_free payload VARCHAR(100);
ALTER TABLE befutsal_main.m_user DROP device_uid;

CREATE TABLE befutsal_main.m_uid
(
  user_id INTEGER NOT NULL,
  device_id VARCHAR(40) NOT NULL,
  `user-agent` BLOB
);
CREATE INDEX m_uid_device_id_index ON befutsal_main.m_uid (device_id);
ALTER TABLE befutsal_main.m_uid ADD CONSTRAINT `m_uid_user_id_device_id_user-agent_pk` UNIQUE (user_id, device_id, `user-agent`(100));

DROP TABLE m_session;

ALTER TABLE befutsal_main.m_user CHANGE payload payload_value VARCHAR(100) DEFAULT NULL;
ALTER TABLE befutsal_main.m_user ADD payload_timestamp INT(15) NULL AFTER payload_value;

update m_user set last_login = NULL, register_time=NULL ;
ALTER TABLE befutsal_main.m_user MODIFY register_time INT(15);
ALTER TABLE befutsal_main.m_user MODIFY last_login INT(15);
ALTER TABLE befutsal_main.m_user ADD activation_code VARCHAR(13) NULL;
ALTER TABLE befutsal_main.m_user ADD activated TINYINT(1) NULL;
ALTER TABLE befutsal_main.m_user CHANGE payload payload_value VARCHAR(100);
ALTER TABLE befutsal_main.m_user ADD payload_timestamp INT(15) NULL AFTER payload_value;
ALTER TABLE befutsal_main.m_user MODIFY register_time INT(15);

DELETE FROM m_uid;
ALTER TABLE befutsal_main.m_uid ADD PRIMARY KEY (user_id, device_id, `user-agent`(255));

INSERT INTO settings (parameter) VALUES ('mobile_app_ios_badge');
INSERT INTO settings (parameter) VALUES ('mobile_app_session_path');
INSERT INTO settings (parameter) VALUES ('mobile_app_email_activation_link');
INSERT INTO settings (parameter) VALUES ('mobile_app_payload_validity');
INSERT INTO settings (parameter, value) VALUES ('mobile_app_android_v_supported', '2');
INSERT INTO settings (parameter) VALUES ('mobile_app_ios_v_supported');
UPDATE settings SET value = '2592000' WHERE parameter = 'mobile_app_session_lifetime';
UPDATE settings SET value = ' <a href="https://itunes.apple.com/ru/app/befutsal/id1131508337?l=ru&ls=1&mt=8" style="display:inline-block;overflow:hidden;background:url(https://linkmaker.itunes.apple.com/images/badges/ru-ru/badge_appstore-lrg.svg) no-repeat;width:180px;height:40px;"></a>' WHERE parameter = 'mobile_app_ios_badge';
UPDATE settings SET value = '/m/' WHERE parameter = 'mobile_app_session_path';
UPDATE settings SET value = '/activate?c=' WHERE parameter = 'mobile_app_email_activation_link';
UPDATE settings SET value = '/m/' WHERE parameter = 'mobile_app_session_path';
UPDATE settings SET value = '31536000' WHERE parameter = 'mobile_app_payload_validity';

#21.07
ALTER TABLE befutsal_main.m_user ADD new_password VARCHAR(32) NULL;
INSERT INTO settings (parameter) VALUES ('mobile_app_change_password_link');
UPDATE settings SET value = '/newpassword?p=' WHERE parameter = 'mobile_app_change_password_link';
INSERT INTO settings (parameter, value) VALUES ('mobile_app_activation_off', TRUE);

#10.08
INSERT INTO settings (parameter, value) VALUES ('email_from', 'support@befutsal.ru');

###################################################
# RELEASE 2 #
###################################################

#06.09
DELETE FROM befutsal_main.m_uid;
ALTER TABLE befutsal_main.m_uid DROP PRIMARY KEY;
DROP INDEX m_uid_device_id_index ON befutsal_main.m_uid;
CREATE UNIQUE INDEX m_uid_device_id_uindex ON befutsal_main.m_uid (device_id);

###################################################
# RELEASE 3 #
###################################################

#05.11
ALTER TABLE befutsal_main.schedule ADD bet_1 DECIMAL(4,2) NULL;
ALTER TABLE befutsal_main.schedule ADD bet_0 DECIMAL(4,2) NULL;
ALTER TABLE befutsal_main.schedule ADD bet_2 DECIMAL(4,2) NULL;
ALTER TABLE befutsal_main.schedule ADD bet_total DECIMAL(3,1) NULL;
ALTER TABLE befutsal_main.schedule ADD bet_total_more DECIMAL(4,2) NULL;
ALTER TABLE befutsal_main.schedule ADD bet_total_less DECIMAL(4,2) NULL;
ALTER TABLE befutsal_main.schedule ADD bet_confirmed INT(1) NULL;

CREATE TABLE befutsal_main.bet
(
  user_id INT NOT NULL,
  id_schedule INT NOT NULL,
  bet_type INT,
  bet_total DECIMAL(3,1),
  bet_rate DECIMAL(4,2),
  bet_amount DECIMAL(10,2),
  bet_timestamp INT(15),
  bet_played INT(1),
  CONSTRAINT bet_user_id_id_schedule_pk PRIMARY KEY (user_id, id_schedule)
);
CREATE INDEX bet_id_schedule_index ON befutsal_main.bet (id_schedule);
CREATE INDEX bet_user_id_index ON befutsal_main.bet (user_id);

CREATE TABLE befutsal_main.bet_account
(
  user_id INT NOT NULL,
  amount DECIMAL(10,2),
  bet_line INT
);
CREATE INDEX bet_account_user_id_bet_line_index ON befutsal_main.bet_account (user_id, bet_line);

CREATE TABLE bet_line
(
  line_id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  description BLOB,
  valid_to INT(15),
  min_bets INT(11)
);
CREATE INDEX bet_line_line_id_index ON bet_line (line_id);

INSERT INTO befutsal_main.settings VALUES (null, "bet_form_weight", 0.6), (null, "bet_max_rate", 300);

ALTER TABLE befutsal_main.bet_line ADD init_amount DECIMAL(10,2) NOT NULL;

ALTER TABLE befutsal_main.bet ADD bet_protocol INT(11) NULL;
ALTER TABLE befutsal_main.bet ADD bet_result VARCHAR(5) NULL;
ALTER TABLE befutsal_main.bet ADD bet_profit DECIMAL(10,2) NULL;

#10.10.2017
INSERT INTO befutsal_main.settings VALUES (null, "bet_default_amount_to_charge", 50);

#29.11.2017
ALTER TABLE befutsal_main.bet ADD bet_line INT(11) NULL;
ALTER TABLE befutsal_main.bet
  MODIFY COLUMN bet_line INT(11) AFTER user_id;