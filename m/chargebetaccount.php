<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');
include('user_structure.php');

$betaccount = new BetAccount();
$betaccount->user_id = $user_id;
$betaccount->getBetAccount();

// Открываем на чтение поток ввода и получаем содержимое потока
$thread = fopen('php://input', 'r');
$post_data = stream_get_contents($thread);

if ($post_data) {
    $user_data = json_decode($post_data);
    $amount = mysql_real_escape_string($user_data->amount);
    if (empty($amount)) {
        $amount = getParameter("bet_default_amount_to_charge");
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
}

if ($betaccount->hasBetAccount()) {
    $betaccount->chargeBetAccount();
    http_response_code(200);
    echo json_encode($betaccount, JSON_UNESCAPED_UNICODE);
} else {
    $message = "\n" . $now . " WARNING! Trying to charge unregistered bet account: " . $user_id;
    writeLog($log_filename, $message);
    $error = new Error();
    $error->sendErrorMessage("Вы не приняли условия конкурса прогнозов", 423);
}

?>
