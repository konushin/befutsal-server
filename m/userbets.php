<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

$betaccount = new BetAccount();
$betaccount->user_id = (int)$user_id;

$betline = new BetLine();
$betline->getCurrentBetLine();

if ($betaccount->hasBetAccount()) {
    $bets_query = "SELECT *
 FROM bet AS b, schedule AS s 
 WHERE b.user_id = $user_id
  AND s.id_schedule = b.id_schedule
  AND b.bet_line = $betline->line_id
 ORDER BY b.bet_played DESC, s.date ASC, s.time ASC, b.bet_timestamp DESC";
    $res_bets = mysql_query($bets_query);
    $num_bets = mysql_num_rows($res_bets);
    $bets = new UserBetList();

    for ($i = 0; $i < $num_bets; $i++) {
        $userbet = new UserBet();
        $userbet->id_schedule = (int)mysql_result($res_bets, $i, "id_schedule");
        $id_competition = mysql_result($res_bets, $i, "league");
        $userbet->league = iconv('windows-1251', 'UTF-8', mysql_result(mysql_query("SELECT * FROM competition WHERE id_competition = $id_competition"), 0, "title"));
        $team_1 = mysql_result($res_bets, $i, "team_1");
        $team_2 = mysql_result($res_bets, $i, "team_2");
        $res_team_1 = mysql_query("SELECT * FROM competitor AS c, team AS t WHERE c.id_competitor = $team_1 AND c.id_team = t.id_team");
        $res_team_2 = mysql_query("SELECT * FROM competitor AS c, team AS t WHERE c.id_competitor = $team_2 AND c.id_team = t.id_team");
        $userbet->team_1 = iconv('windows-1251', 'UTF-8', mysql_result($res_team_1, 0, "title"));
        $userbet->team_1_emblem_path = $base_url . "emblems/" . mysql_result($res_team_1, 0, "emblem_path");
        $userbet->team_2 = iconv('windows-1251', 'UTF-8', mysql_result($res_team_2, 0, "title"));
        $userbet->team_2_emblem_path = $base_url . "emblems/" . mysql_result($res_team_2, 0, "emblem_path");
        $userbet->date = mysql_result($res_bets, $i, "date");
        $userbet->time = mysql_result($res_bets, $i, "time");
        $userbet->getBetDetails($user_id, $userbet->id_schedule);

        $bets->userbets[$i] = $userbet;
    }

    http_response_code(200);
    echo json_encode($bets, JSON_UNESCAPED_UNICODE);
} else {
    $error = new Error();
    $error->sendErrorMessage("Вы не приняли условия конкурса прогнозов", 423);
}


?>
