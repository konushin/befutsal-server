<?php

class MUser
{
    public $user_id;
    public $username;
    public $email;
    public $avatar;
    public $payload_value;
    public $payload_timestamp;
    public $user_player;
    public $favourites;
    public $device_id;

    /**
     * Список избранных команд
     */
    public function getFavourites()
    {
        global $base_url;
        global $current_season;

        $query = "SELECT * FROM m_favourites WHERE user_id = $this->user_id";
        $res_favourites = mysql_query($query);
        $this->favourites = array();
        for ($n = 0; $n < mysql_num_rows($res_favourites); $n++) {
            $id_team = mysql_result($res_favourites, $n, "team_id");
            $query = "SELECT * FROM team WHERE id_team = $id_team";
            $res_team = mysql_query($query);
            if (mysql_num_rows($res_team) == 1) {
                $team = new Team();
                $team->id_team = $id_team;
                $team->title = iconv('windows-1251', 'UTF-8', mysql_result($res_team, 0, "title"));
                $team->founded = mysql_result($res_team, 0, "founded");
                if (mysql_result($res_team, 0, "emblem_path") != null) {
                    $team->emblem_path = $base_url . "emblems/" . mysql_result($res_team, 0, "emblem_path");
                }
                $team->getHistoricalTitles();
                $team->getCompetitions($current_season->id_season);
                $this->favourites[] = $team;
            }

        }
    }
}

class MUserEnvelope
{
    public $user;
}

class FavouritesEnvelope
{
    public $favourites;
}

class PushNotification
{
    public $app_id;
    public $devices;
    public $header;
    public $text;
    public $url;

    public function sendNotification()
    {
        $contents = array(
            "en" => $this->text,
            "ru" => $this->text
        );

        $headings = array(
            "en" => $this->header,
            "ru" => $this->header
        );

        $fields = array(
            'app_id' => $this->app_id,
            'include_player_ids' => $this->devices,
            'headings' => $headings,
            'contents' => $contents,
            'url' => $this->url
        );

        $fields = json_encode($fields);
        //print("\nJSON sent:\n");
        //print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic MjVmYzc0YzItZjEwMS00NGYxLTkxODQtZmI5Yjc4NTdmYjNl'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function sendGCM()
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => $this->devices,
            'notification' => array("title" => $this->header, "body" => $this->text, "sound" => "default")
        );
        $fields = json_encode($fields);

        $headers = array(
            'Authorization: key=' . "AAAAL5hCHNM:APA91bEQWs1QvXnZoBDre2zo2OFmDsuB886QVrB5nEDVLokbzxor65ULtua7dLRQjRFBr2kfMM7YfkK_yaaFkKrsEIiIhosy9DNlFUP2dqOcdHCxWUF6VhPFeOMzjZf7kpTD1Rn9372z",
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;

        //описание ошибок отправки см. https://firebase.google.com/docs/cloud-messaging/http-server-ref
    }

}

?>
