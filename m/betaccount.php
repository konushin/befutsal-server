<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');
include('user_structure.php');

$betaccount = new BetAccount();
$betaccount->user_id = $user_id;

if ($betaccount->hasBetAccount()) {
    $betaccount->getBetAccount();
    http_response_code(200);
    echo json_encode($betaccount, JSON_UNESCAPED_UNICODE);
} else {
    //$message = "DEBUG: 423 Trying to open betaccount for non-accepted conditions. User: " .$user_id;
    //writeLog($log_filename, $message);
    $error = new Error();
    $error->sendErrorMessage("Вы не приняли условия конкурса прогнозов", 423);
}

?>
