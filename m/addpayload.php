<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('user_structure.php');

$user_id = $_SESSION["user_id"];

// Открываем на чтение поток ввода и получаем содержимое потока
$thread = fopen('php://input', 'r');
$post_data = stream_get_contents($thread);

if ($post_data) {
    $user_data = json_decode($post_data);
    $payload = mysql_real_escape_string($user_data->payload);
    if (empty($payload) OR empty($user_id)) {
        $error = new Error();
        $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Произошла ошибка при передаче данных", 400);
}

$current_time = time();
$query = "UPDATE m_user SET payload_value = '$payload', payload_timestamp = $current_time WHERE user_id = $user_id";
$res_payload = mysql_query($query);

if ($res_payload) {
    http_response_code(200);
    $result = new Result();
    $result->result = true;
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    $message = "Addpayload SUCCESS! user_id=" . $user_id . ", payload=" . substr($payload, 0, 6) . "...";
    writeLog($log_filename, $message);
} else {
    $message = "Addpayload FAILED! user_id=" . $user_id . ", payload=" . substr($payload, 0, 6) . "...";
    writeLog($log_filename, $message);
    $error = new Error();
    $error->sendErrorMessage("Не удалось сохранить покупку!", 400);
}

?>
