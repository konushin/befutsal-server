<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

if (isset($_GET["competition_id"])) {
    $league = intval($_GET["competition_id"]);
} else {
    $error = new Error();
    $error->sendErrorMessage("Неверные параметры запроса", 400);
}

//получаем общие детали лиги
$leaguedetails_query = "SELECT * FROM competition AS c WHERE c.id_competition = $league";

$res_leaguedetails = mysql_query($leaguedetails_query);
$num_leaguedetails = mysql_num_rows($res_leaguedetails);

if (!$res_leaguedetails OR $num_leaguedetails != 1) {
    $error = new Error();
    $error->sendErrorMessage("Не удалось найти информацию о турнире", 400);
} else {
    $promoted = mysql_result($res_leaguedetails, "0", "promoted");
}

//заполняем структуру таблицы
$res_table = getLeagueTable($league, "9999-12-31 23:59:59");

$leaguetable = array();

for ($i = 0; $i < mysql_num_rows($res_table); $i++) {
    $table_item = new TableItem();
    $table_item->competition_id = mysql_result($res_table, $i, "id_league");
    $id_competitor = mysql_result($res_table, $i, "id_competitor");
    $res_team = mysql_query("SELECT * FROM competitor WHERE id_competitor = $id_competitor");
    $table_item->team_id = mysql_result($res_team, 0, "id_team");
    $table_item->position = $i + 1;
    $table_item->title = iconv('windows-1251', 'UTF-8', mysql_result($res_table, $i, "name"));
    $table_item->pld = mysql_result($res_table, $i, "pld");
    $table_item->won = mysql_result($res_table, $i, "won");
    $table_item->drew = mysql_result($res_table, $i, "drew");
    $table_item->lost = mysql_result($res_table, $i, "lost");
    $table_item->scored = mysql_result($res_table, $i, "goalsfor");
    $table_item->missed = mysql_result($res_table, $i, "goalsagainst");
    $table_item->pts = mysql_result($res_table, $i, "pts");
    if ($i < $promoted) {
        $table_item->highlight = true;
    } else {
        $table_item->highlight = false;
    }

    $leaguetable[$i] = $table_item;
}

//оборачиваем для возврата
$leaguetable_envelope = new TableEnvelope();
$leaguetable_envelope->table = $leaguetable;

http_response_code(200);
echo json_encode($leaguetable_envelope, JSON_UNESCAPED_UNICODE);

?>
