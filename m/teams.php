<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

$current_season = new Season();
$current_season->getCurrentSeason();

$teams_query = "SELECT DISTINCT t.*, cr.title
FROM team AS t, competitor AS cr, competition AS cn 
WHERE t.id_team = cr.id_team 
    AND cr.id_competition = cn.id_competition 
    AND cn.season = $current_season->id_season 
ORDER BY cr.title";
$res_teams = mysql_query($teams_query);
$num_teams = mysql_num_rows($res_teams);
$teams = array();

for ($i = 0; $i < $num_teams; $i++) {
    $teams[$i] = new Team();
    $teams[$i]->id_team = mysql_result($res_teams, $i, "id_team");
    $teams[$i]->title = iconv('windows-1251', 'UTF-8', mysql_result($res_teams, $i, "cr.title"));
    $teams[$i]->founded = mysql_result($res_teams, $i, "founded");
    if (mysql_result($res_teams, $i, "emblem_path") != null) {
        $teams[$i]->emblem_path = $base_url . "emblems/" . mysql_result($res_teams, $i, "emblem_path");
    }

    $teams[$i]->getCompetitions($current_season->id_season);
    $teams[$i]->getHistoricalTitles();
}

//оборачиваем для возврата
$teams_list = new TeamsListEnvelope();
$teams_list->teams = $teams;

http_response_code(200);
echo json_encode($teams_list, JSON_UNESCAPED_UNICODE);

?>
