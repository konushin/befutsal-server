<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('stats_structure.php');

$betaccount = new BetAccount();
$betaccount->user_id = $user_id;

if ($betaccount->hasBetAccount()) {

    $betLineTable = new BetLineTable();
    $betline = new BetLine();
    $betline->getCurrentBetLine();

    $query = "SELECT * FROM bet_account WHERE bet_line = $betline->line_id ORDER BY amount DESC";
    $res_betaccounts = mysql_query($query);

    $position = 0;

    for ($i = 0; $i < mysql_num_rows($res_betaccounts); $i++) {
        $betlinetableitem = new BetAccount();
        $betlinetableitem->user_id = (int)mysql_result($res_betaccounts, $i, "user_id");
        $betlinetableitem->position = $position + 1;

        $res_user = mysql_query("SELECT * FROM m_user WHERE user_id = $betlinetableitem->user_id");
        $betlinetableitem->username = iconv('cp1251', 'utf8', mysql_result($res_user, 0, "username"));

        $betlinetableitem->getAmount();
        $betlinetableitem->getNumBets();
        $betlinetableitem->getAmountInPlay();

        if ($user_id == $betlinetableitem->user_id) {
            $betlinetableitem->highlight = TRUE;
        } else {
            $betlinetableitem->highlight = FALSE;
        }

        if ((time() > $betline->valid_to AND $betlinetableitem->num_bets < $betline->min_bets) OR ($betlinetableitem->num_bets == 0 AND $betlinetableitem->user_id != $user_id)) {
            //не включаем в выборку, если конкурс завершен и ставок сделал меньше пооложенного
        } else {
            //включаем в таблицу, если время не вышло ИЛИ ставок больше
            $betLineTable->betAccounts[] = $betlinetableitem;
            $position++;
        }
    }

    http_response_code(200);
    echo json_encode($betLineTable, JSON_UNESCAPED_UNICODE);
} else {
    //$message = "DEBUG: 423 Trying to open betlinetable for non-accepted conditions. User: " .$user_id;
    //writeLog($log_filename, $message);
    $error = new Error();
    $error->sendErrorMessage("Вы не приняли условия конкурса прогнозов", 423);
}

?>
