<?php

header('Content-Type: application/json; charset=UTF-8');

include('common.php');
include('user_structure.php');

// Открываем на чтение поток ввода и получаем содержимое потока
$thread = fopen('php://input', 'r');
$post_data = stream_get_contents($thread);

if ($post_data) {
    $user_data = json_decode($post_data);

    if (empty($user_data->username) OR empty($user_data->email) OR empty($user_data->uid) OR empty($user_data->password)) {
        //$message = "DEBUG: New user was not created due to: username=" . $user_data->username . ", email=" . $user_data->email . " uid= " . $user_data->uid . ", psw=" . $user_data->password;
        //writeLog($log_filename, $message);
        $error = new Error();
        $error->sendErrorMessage("Неверный запрос", 400);
    } else {
        $MUser = new MUser();
        $MUser->username = iconv('utf8', 'cp1251', mysql_real_escape_string(trim($user_data->username)));
        $MUser->email = mysql_real_escape_string(trim($user_data->email));
        $MUser->device_id = mysql_real_escape_string(trim($user_data->uid));
        if (!empty($login_data->avatar)) {
            $MUser->avatar = mysql_real_escape_string($user_data->avatar);
        }
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Неверный запрос", 400);
}

if (iconv_strlen($MUser->username, "cp1251") < 3 OR iconv_strlen($MUser->username, "cp1251") > 16) {
    //$message = "DEBUG: username=" . $user_data->username . ", length=" . strlen($user_data->username);
    //writeLog($log_filename, $message);
    $error = new Error();
    $error->sendErrorMessage("Имя пользователя должно содержать от 3 до 16 символов", 400);
}

if (!filter_var($MUser->email, FILTER_VALIDATE_EMAIL, FILTER_VALIDATE_DOMAIN)) {
    $error = new Error();
    $error->sendErrorMessage("Некорректный формат e-mail", 400);
}

if ($user_data->password == "d41d8cd98f00b204e9800998ecf8427e") {
    $error = new Error();
    $error->sendErrorMessage("Пароль не может быть пустым", 400);
}

$query = "SELECT * FROM m_user WHERE `username` = '$MUser->username'";
$res_username = mysql_query($query);

if (mysql_num_rows($res_username) > 0) {
    $error = new Error();
    $error->sendErrorMessage("Пользователь с таким именем уже зарегистрирован", 410);
}

$query = "SELECT * FROM m_user WHERE `email` = '$MUser->email'";
$res_email = mysql_query($query);

if (mysql_num_rows($res_email) > 0) {
    $error = new Error();
    $error->sendErrorMessage("Пользователь с таким e-mail уже зарегистрирован", 410);
}

$code = uniqid();
$current_time = time();

$activated = getParameter("mobile_app_activation_off"); //если отключена активация, то активирован сразу
$query = "INSERT INTO m_user (user_id, username, email, password, register_time, activation_code, activated) VALUES (null, '$MUser->username', '$MUser->email', '$user_data->password', $current_time, '$code', $activated)";
$res_insert = mysql_query($query);

if ($res_insert) {
    $MUser->user_id = mysql_insert_id();

    $message = "New user created: " . iconv('cp1251', 'utf8', $MUser->username) . ", user_id=" . $MUser->user_id . " activation code: " . $code;
    writeLog($log_filename, $message);

    if (getParameter("mobile_app_activation_off") == 0) {

        $email_subject = "Активация пользователя мобильного приложения Живи мини-футболом!";
        $email_headers = 'Content-type: text/html; charset="utf-8"' . "\r\n";
        $email_headers .= 'From: ' . getParameter("email_from") . "\r\n";
        $email_message = "<BODY>
<FONT FACE=Tahoma SIZE=2>
<P ALIGN=left>Здравствуйте.</P>
<P ALIGN=left>Этот email был зарегистрирован в мобильном приложении befutsal. Для активации пользователя пройдите по <A HREF='" . getParameter("url_mobile") . getParameter("mobile_app_email_activation_link") . $code . "'>ссылке</A> или скопируйте ее ниже и вставьте в строку браузера.</P>
<P ALIGN=left>" . getParameter("url_mobile") . getParameter("mobile_app_email_activation_link") . $code . "</P>
<P ALIGN=left><FONT SIZE=1>Если у вас остались вопросы или вы не регистрировались в мобильном приложении befutsal, то мы приносим свои извинения, и просим пройти вас обратиться к администрации проекта <strong>Живи мини-футболом!</strong> письмом на адрес info@befutsal.ru.</FONT></P>
<P ALIGN=left>С уважением,<BR>коллектив сайта Живи мини-футболом!</P>
<P ALIGN=left><FONT SIZE=1>Это письмо сгенерировано роботом, не отвечайте на него!</FONT></P>
</FONT>
</BODY>";

        $sendmail = mail($MUser->email, $email_subject, $email_message, $email_headers);
        $error = new Error();

        if ($sendmail) {
            $message = "Activation code for user " . iconv('cp1251', 'utf8', $MUser->username) . " sent to: " . $MUser->email;
            writeLog($log_filename, $message);
        } else {
            $error->sendErrorMessage("Не удалось отправить код активации на e-mail", 400);
        }

        $error->sendErrorMessage("На указанный email выслано письмо. Для завершения регистрации, пожалуйста, подтвердите свой email.", 200);
    } else {
        $MUser->getFavourites();
        $result = new MUserEnvelope();
        $result->user = $MUser;

        //стартуем сессию
        session_set_cookie_params(getParameter("mobile_app_session_lifetime"), getParameter("mobile_app_session_path"));
        session_start();
        $message = "Session start on register. User_id=" . $MUser->user_id;
        writeLog($log_filename, $message);
        $_SESSION['user_id'] = $MUser->user_id;

        http_response_code(200);
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
} else {
    $error = new Error();
    $error->sendErrorMessage("Произошла непредвиденная ошибка", 400);
}

?>