﻿<?php

$class_active["contact"] = "active";
$page_title = "Контакты";
$description = "Контакты";
include("header.php");

include($add_root . "includings/contacts_federations.php");

print('
   <section class="drawer">
    <section id="contact" class="secondary-page">
      <div class="general">
          <div class="container">
           <div class="content-link col-md-12">
                  <div id="contact_form" class="top-score-title col-md-9 align-center">
');

for ($i = 0;
     $i < sizeof($contacts->contacts);
     $i++) {
    print('
                    <h3><span>' . $contacts->contacts[$i]->title . '</span><span class="point-little">.</span></h3>
                        <div class="col-md-12">
                        ');

    for ($n = 0; $n < sizeof($contacts->contacts[$i]->contact); $n++) {
        print('
                          <p><i class="fa fa-map-marker"></i> ' . $contacts->contacts[$i]->contact[$n]->title . ': <b>' . $contacts->contacts[$i]->contact[$n]->name . '</b></p>
                          <p><i class="fa fa-phone"></i> <a href="tel:' . $contacts->contacts[$i]->contact[$n]->mobtel . '">' . $contacts->contacts[$i]->contact[$n]->mobtel . '</a></p>

        ');
    }
    print('                        </div>  ');
}

print('
                     </div>
                     <div id="info-company" class="top-score-title col-md-3 align-center">
                        <h3>Обратная связь</h3>
                        <div class="col-md-12">
                          <p><i class="fa fa-phone"></i><a href="tel:+79051457451">+7 905 145 7451</a></p>
                          <p><i class="fa fa-envelope-o"></i>info@befutsal.ru </p>
                          <p><i class="fa fa-globe"></i><a href="https://vk.com/befutsal" target="_blank">vk.com/befutsal</a></p>
                        </div>            
                    </div>
                </div>
                </div>
                </div>
                </section>
');

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');

?>
