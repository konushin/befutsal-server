<?php

include("../database_main.php");
include("../includings/functions.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/telegram" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " TELEGRAM NOTIFICATION JOB FOR THIS WEEK BETS STARTED. " . "\n";
fwrite($log, $log_string);

//--------------------ищем игры этой недели-----------------------

$thisweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
$nextweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 6, date("Y")));

//выбираем матчи, где коэфф больше $bet_rate
$bet_rate = 7;

$query = "SELECT DATE_FORMAT(date, '%d.%m') as date, time, bet_0, bet_1, bet_2, team_1, team_2
FROM schedule
WHERE schedule.date BETWEEN '$thisweek' AND '$nextweek'
      AND (bet_0 > $bet_rate OR bet_1 > $bet_rate OR bet_2 > $bet_rate)
ORDER BY date, time";

$log_string = "\n" . $now . " DEBUG: " . $query . "\n";
fwrite($log, $log_string);

$res_matches = mysql_query($query, $db_connection);
$num_matches = mysql_num_rows($res_matches);

$message_header = "<b>Инсайд от <ins>Живи мини-футболом</ins>. На этих матчах можно хорошо заработать в нашем конкурсе прогнозов</b> \n\n";
$message_text = "";

if ($num_matches > 0) {
    for ($n = 0; $n < $num_matches; $n++) {

        $date = mysql_result($res_matches, $n, "date");
        $id_team_1 = mysql_result($res_matches, $n, "team_1");
        $id_team_2 = mysql_result($res_matches, $n, "team_2");
        $res_title_1 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $id_team_1", $db_connection);
        $team_title_1 = iconv("Windows-1251", "UTF-8", mysql_result($res_title_1, 0, "title"));
        $res_title_2 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $id_team_2", $db_connection);
        $team_title_2 = iconv("Windows-1251", "UTF-8", mysql_result($res_title_2, 0, "title"));
        $bet_0 = mysql_result($res_matches, $n, "bet_0");
        $bet_1 = mysql_result($res_matches, $n, "bet_1");
        $bet_2 = mysql_result($res_matches, $n, "bet_2");

        $message_text = $message_text . $date . " <a href='https://befutsal.ru/statistics/match_presentation.php?team1=" . $id_team_1 . "&team2=" . $id_team_2 . "'>" . $team_title_1 . " - " . $team_title_2 . "</a>";

        if ($bet_1 > $bet_rate) {
            $message_text = $message_text . ". П1 х" . $bet_1;
        }
        if ($bet_2 > $bet_rate) {
            $message_text = $message_text . ". П2 х" . $bet_2;
        }
        if ($bet_0 > $bet_rate) {
            $message_text = $message_text . ". Коэффициент на ничью х" . $bet_0;
        }

        $message_text = $message_text . "\n";
    }
}

$message_text = $message_header . $message_text . "\nПолный анонс на <a href='https://befutsal.ru/announcement.php'>сайте</a> и в приложениях <a href='https://itunes.apple.com/ru/app/befutsal/id1273597183?l=ru&ls=1&mt=8'>iOS</a> и <a href='https://play.google.com/store/apps/details?id=ru.befutsal'>Android</a>";

$result = sendTelegram(
    'sendMessage',
    array(
        'chat_id' => getParameter("telegram_channel_id"),
        'text' => $message_text,
        'parse_mode' => "HTML"
    )
);

$log_string = "\n" . $now . " TELEGRAM NOTIFICATION JOB FOR THIS WEEK BETS ENDED. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);

?>