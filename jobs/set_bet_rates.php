<?php

include("../database_main.php");
include("../m/user_structure.php");
include("../includings/functions.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/bets" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " SETTING BET RATES JOB FOR NEAREST MONTH STARTS. " . "\n";
fwrite($log, $log_string);

//--------------------ищем игры в ближайший месяц-----------------------

$period = date("Y-m-d", mktime(0, 0, 0, date("m") + 1, date("d"), date("Y")));
$query = "SELECT id_schedule, team_1, team_2
FROM schedule
WHERE date < '$period'
  AND CONCAT_WS(' ', date, time) > NOW()
ORDER BY id_schedule";

//$log_string = $now . " DEBUG SQL: " . $query . "\n";
//fwrite($log, $log_string);

$res_schedule = mysql_query($query);
$num_schedule = mysql_num_rows($res_schedule);

for ($i = 0; $i < $num_schedule; $i++) {
    //считываем параметры для ставок
    $form_weight = getParameter("bet_form_weight");
    $rate_weight = 1 - $form_weight;
    $max_rate = getParameter("bet_max_rate");

    $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
    $team_1 = mysql_result($res_schedule, $i, "team_1");
    $team_2 = mysql_result($res_schedule, $i, "team_2");

    //-----------определяем рейтинг и текущую форму команд-------------
    $team_1_rate = getTeamRate($team_1, 0);
    $team_2_rate = getTeamRate($team_2, 0);

    //только если у обеих команд есть рейтинг
    if ($team_1_rate != null AND $team_2_rate != null) {
        $team_1_form = getTeamForm($team_1, 5);
        $team_2_form = getTeamForm($team_2, 5);

        $team_1_strength = $rate_weight * $team_1_rate / $max_rate + $form_weight * $team_1_form["performance"];
        $team_2_strength = $rate_weight * $team_2_rate / $max_rate + $form_weight * $team_2_form["performance"];
        $total_strength = $team_1_strength + $team_2_strength;
        $diff_stregth = abs($team_1_strength - $team_2_strength);

        //определяем вероятности
        if ($total_strength != 0) {

            $chance_1 = ($team_1_strength / $total_strength);
            $chance_0 = pow($diff_stregth / $total_strength, 1.1);
            $chance_2 = ($team_2_strength / $total_strength);

            /*
            $chance_2 = 1 / (3 * ($team_1_strength / $team_2_strength + 1));
            $chance_0 = 1 - (($team_1_strength / $team_2_strength + 1) * $chance_2);
            $chance_1 = $team_1_strength * $chance_2 / $team_2_strength;
            */

            $bet_1 = max(1.01, min(12, round(1 / $chance_1, 2)));
            $bet_2 = max(1.01, min(12, round(1 / $chance_2, 2)));
            $bet_0 = max(1.01, min(12, round(1 / $chance_0, 2)));
        } else {
            $bet_1 = 2;
            $bet_0 = 2;
            $bet_2 = 2;
        }
    } else {
        $bet_1 = 2;
        $bet_0 = 2;
        $bet_2 = 2;
    }
    $bet_total = floor(($team_1_form["score"] + $team_2_form["score"]) / 2) + 0.5;
    $bet_total_more = 1.65;
    $bet_total_less = 1.65;

    $query = "UPDATE schedule SET bet_1 = $bet_1, bet_0 = $bet_0, bet_2 = $bet_2, bet_total = $bet_total, bet_total_more = $bet_total_more, bet_total_less = $bet_total_less WHERE id_schedule = $id_schedule";

    //закомментировать весь if ниже, если ничего не нужно писать в лог

    if ($id_schedule == 224707435) {
        $message = "\n Team1=" . $team_1 . ", rate1=" . $team_1_rate . ", form1=" . $team_1_form["performance"] . ", strength1=" . $team_1_strength . ", score1=" . $team_1_form["score"] . ", chance1=" . $chance_1 . ", bet1=" . $bet_1 . ", chance0=" . $chance_0 . ", bet0=" . $bet_0;
        fwrite($log, $message);
        $message = "\n Team2=" . $team_2 . ", rate2=" . $team_2_rate . ", form2=" . $team_2_form["performance"] . ", strength2=" . $team_2_strength . ", score2=" . $team_2_form["score"] . ", chance2=" . $chance_2 . ", bet2=" . $bet_2 . ", chance0=" . $chance_0 . ", bet0=" . $bet_0;
        fwrite($log, $message);

        $message = "\n DEBUG: " . $query;
        fwrite($log, $message);
    }

    $res_update = mysql_query($query);
}

$log_string = "\nMATCHES PROCESSED: " . $num_schedule . "\n";
fwrite($log, $log_string);
$log_string = "\n" . $now . " SETTING BET RATES JOB FOR NEAREST MONTH ENDS. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);

?>