<?php

include("../database_main.php");
include("../includings/functions.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/telegram" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " TELEGRAM NOTIFICATION JOB FOR THIS WEEK GAMES STARTED. " . "\n";
fwrite($log, $log_string);

//--------------------ищем сегодняшние игры-----------------------

$thisweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
$nextweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 6, date("Y")));

//выбираем расписание вышки и первой лиги: rate<5
$query = "SELECT DISTINCT league, title FROM schedule, competition WHERE schedule.date BETWEEN '$thisweek' AND '$nextweek' AND schedule.league = competition.id_competition AND competition.rate < 5 ORDER BY competition.rate ASC, date, time";

$log_string = "\n" . $now . " DEBUG: " . $query . "\n";
fwrite($log, $log_string);

$res_leagues = mysql_query($query, $db_connection);
$num_leagues = mysql_num_rows($res_leagues);

$message_text = "<b>Кроме большого футбола на этой неделе стоит посмотреть:</b> \n";

if ($num_leagues > 0) {
    for ($n = 0; $n < $num_leagues; $n++) {

        $id_league = mysql_result($res_leagues, $n, "league");
        $league_title = iconv("Windows-1251", "UTF-8", mysql_result($res_leagues, $n, "title"));

        $message_text = $message_text . "\n<ins>" . $league_title . "</ins>\n\n";

        $query = "SELECT DATE_FORMAT(date, '%d.%m %a') as date, time, schedule.pitch AS pitch, competition.title, team_1, team_2
FROM schedule, competition
WHERE schedule.league = competition.id_competition
  AND competition.id_competition = $id_league
  AND schedule.date BETWEEN '$thisweek' AND '$nextweek'
ORDER BY date, time";

        $log_string = "\n" . $now . " DEBUG: " . $query . "\n";
        fwrite($log, $log_string);

        $res_schedule = mysql_query($query);
        $num_schedule = mysql_num_rows($res_schedule);

        for ($i = 0; $i < $num_schedule; $i++) {
            $time = substr(mysql_result($res_schedule, $i, "time"), 0, 5);
            $day = (int)substr(mysql_result($res_schedule, $i, "date"), 0, 2);
            $month = substr(mysql_result($res_schedule, $i, "date"), 3, 2);
            $eng_week_day = substr(mysql_result($res_schedule, $i, "date"), 6, 3);
            $date = iconv("Windows-1251", "UTF-8", $day . " " . ruMonth($month) . " (" . ruWeekDay($eng_week_day) . ")");
            $pitch = iconv("Windows-1251", "UTF-8", mysql_result($res_schedule, $i, "pitch"));
            $team_1 = mysql_result($res_schedule, $i, "team_1");
            $team_2 = mysql_result($res_schedule, $i, "team_2");
            $res_team_1 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_1", $db_connection);
            $team_title_1 = iconv("Windows-1251", "UTF-8", mysql_result($res_team_1, 0, "title"));
            $res_team_2 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_2", $db_connection);
            $team_title_2 = iconv("Windows-1251", "UTF-8", mysql_result($res_team_2, 0, "title"));

            $message_text = $message_text . $date . " в " . $time . " сыграют <a href='https://befutsal.ru/statistics/match_presentation.php?team1=" . $team_1 . "&team2=" . $team_2 . "'>" . $team_title_1 . " и " . $team_title_2 . "</a>\n";
        }
    }

    $message_text = $message_text . "\nПолный анонс на <a href='https://befutsal.ru/announcement.php'>сайте</a> и в приложениях <a href='https://itunes.apple.com/ru/app/befutsal/id1273597183?l=ru&ls=1&mt=8'>iOS</a> и <a href='https://play.google.com/store/apps/details?id=ru.befutsal'>Android</a>";

        $result = sendTelegram(
            'sendMessage',
            array(
                'chat_id' => getParameter("telegram_channel_id"),
                'text' => $message_text,
                'parse_mode' => "HTML"
            )
        );

    var_dump($result);
}

$log_string = "\n" . $now . " TELEGRAM NOTIFICATION JOB FOR THIS WEEK GAMES ENDED. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);

?>