<?php

include("../database_main.php");
include("../m/user_structure.php");
include("../m/stats_structure.php");
include("../includings/functions.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/bets" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " PROCESSING BETS JOB STARTS " . "\n";
fwrite($log, $log_string);

//--------------------ищем неразыгранные ставки-----------------------

$query = "SELECT *
FROM bet b, protocol p
WHERE b.bet_played != 1
  AND b.id_schedule = p.id_schedule
ORDER BY b.bet_timestamp ASC";

$res_bets = mysql_query($query);
$num_bets = mysql_num_rows($res_bets);

$message = "\nNUMBER OF BETS TO PROCESS: " . $num_bets;
fwrite($log, $message);

for ($i = 0; $i < $num_bets; $i++) {
    $user_id = mysql_result($res_bets, $i, "user_id");
    $id_schedule = mysql_result($res_bets, $i, "b.id_schedule");
    $id_protocol = mysql_result($res_bets, $i, "p.id_protocol");

    $bet = new Bet();
    $bet->getBetDetails($user_id, $id_schedule);

    //определяем исход матча
    $score_1 = (int)mysql_result($res_bets, $i, "score_end_1");
    $score_2 = (int)mysql_result($res_bets, $i, "score_end_2");
    $score_total = $score_1 + $score_2;
    $bet_result = $score_1 . ":" . $score_2;
    $isForfeited = mysql_result($res_bets, $i, "forfeited");
    $onPenalty = mysql_result($res_bets, $i, "on_penalty");
    $onExtraTime = mysql_result($res_bets, $i, "extratime");
    if ($isForfeited != null) {
        $result = 5;
    } elseif ($score_1 > $score_2) {
        $result = 1;
    } elseif ($score_1 < $score_2) {
        $result = 2;
    } elseif (($score_1 == $score2) OR ($onPenalty == 1) OR ($onExtraTime == 1)) {
        $result = 0;
    } else {
        $result = 5;
    }

    if (($bet->bet_type == $result) OR ($bet->bet_type == 3 AND $score_total > $bet->bet_total) OR ($bet->bet_type == 4 AND $score_total < $bet->bet_total)) {
        $bet_profit = $bet->bet_rate * $bet->bet_amount;
    } elseif ($result == 5) {
        $bet_profit = $bet->bet_amount;
    } else {
        $bet_profit = 0;
    }

    $bet_profit = round($bet_profit, 2);
    $query1 = "UPDATE bet_account SET amount = amount + $bet_profit WHERE user_id = $user_id";
    $query2 = "UPDATE bet SET bet_played = 1, bet_protocol = $id_protocol, bet_result = '$bet_result', bet_profit = $bet_profit WHERE user_id = $user_id AND id_schedule = $id_schedule";
    $res_query1 = mysql_query($query1);
    $res_query2 = mysql_query($query2);
    //$message = "\n DEBUG: " . $query1. "\n". $query2;
    $message = "\n USER: " . $user_id . ", MATCH: " . $id_schedule . ", BET_AMOUNT: " . $bet->bet_amount . ", RATE: " . $bet->bet_rate . ", WIN: " . $bet_profit;
    fwrite($log, $message);
    if (!$res_query1 OR !$res_query2) {
        //если не удалось обновить баланс
        $message = "\n" . $now . " FAILED BET PROFIT: " . $query2;
        fwrite($log, $message);
    } else {
        //если ок, то выбираем device_id этого пользователя и ставим в очередь уведомления push
        $uid_length[0] = 36; //длина uid для OneSignal
        $uid_length[1] = 152; //длина uid Fabric до 2021
        $uid_length[2] = 163; //длина uid Fabric с 2021
        for ($len = 0; $len < sizeof($uid_length); $len++) {
            $query_uids = "SELECT DISTINCT device_id FROM m_uid WHERE user_id = $user_id AND length(m_uid.device_id) = $uid_length[$len]";
            $res_uids = mysql_query($query_uids);
            $uids = array();
            if (mysql_num_rows($res_uids) > 0) {
                for ($n = 0; $n < mysql_num_rows($res_uids); $n++) {
                    $uids[$n] = mysql_result($res_uids, $n, "device_id");
                }
                $uids_ser = serialize($uids);
                $query = "SELECT * FROM schedule WHERE id_schedule = $id_schedule";
                $res_schedule = mysql_query($query);
                $id_team1 = mysql_result($res_schedule, 0, "team_1");
                $id_team2 = mysql_result($res_schedule, 0, "team_2");
                $s_team1 = iconv("Windows-1251", "UTF-8", mysql_result(mysql_query("SELECT title FROM competitor WHERE id_competitor = $id_team1"), 0, "title"));
                $s_team2 = iconv("Windows-1251", "UTF-8", mysql_result(mysql_query("SELECT title FROM competitor WHERE id_competitor = $id_team2"), 0, "title"));

                $congratulations = array("Продолжайте в том же духе!", "Отличный результат!", "Вы хороший прогнозист.", "Сыграем еще?", "Заслуженный результат.", "Браво!", "Вот бы в жизни так!", "Еще один прогноз?", "Можно переходить к игре по-крупному.", "Новые коэффициенты уже готовы.", "Они уже на счету и можно делать новые прогнозы.");
                $miserys = array("Продолжайте совершенствоваться.", "Сыграем еще?", "Попробуйте ещё!", "Может не на тех ставим?", "В следующий раз повезёт больше.", "Так мы быстро проиграем.", "А что если еще попробовать?", "Попробуем другую стратегию?", "Не сдавайтесь!");

                $header = iconv("UTF-8", "Windows-1251", "Живи мини-футболом!");
                if ($bet_profit > 0) {
                    $text = iconv("UTF-8", "Windows-1251", "Ваш прогноз на матч " . $s_team1 . " - " . $s_team2 . " сбылся! Вы заработали " . $bet_profit . " банок. " . $congratulations[array_rand($congratulations, 1)]);
                } else {
                    $text = iconv("UTF-8", "Windows-1251", "Прогноз на матч " . $s_team1 . " - " . $s_team2 . " не сбылся. " . $miserys[array_rand($miserys, 1)]);
                }
                $add_push = addPushJob($uids_ser, $header, $text, null);
            }
        }

    }

}

$log_string = "\n" . $now . " PROCESSING BETS JOB ENDS. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);

?>