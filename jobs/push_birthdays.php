<?php

include("../database_main.php");
include("../m/user_structure.php");
include("../includings/functions.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/push" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " PUSH NOTIFICATION JOB FOR TODAYS BIRTHDAYS STARTED. " . "\n";
fwrite($log, $log_string);

//--------------------ищем сегодняшние дни рождения-----------------------

$res_current_season = mysql_query("SELECT * FROM season ORDER BY id_season DESC LIMIT 0,1", $db_connection);
$current_season = mysql_result($res_current_season, 0, "id_season");
$today = date("m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
$res_birthdays = birthdayList($today, $current_season);
$num_birthdays = mysql_num_rows($res_birthdays);

for ($i = 0; $i < $num_birthdays; $i++) {
    $id_player = mysql_result($res_birthdays, $i, "id_player");
    $name = iconv('windows-1251', 'UTF-8', mysql_result($res_birthdays, $i, "name_1") . " " . mysql_result($res_birthdays, $i, "name_2"));
    $res_application = mysql_query("SELECT *
   FROM application, competitor, competition
   WHERE application.id_player = $id_player
    AND application.id_competitor = competitor.id_competitor
    AND competitor.id_competition = competition.id_competition
    AND competition.season = $current_season
    AND application.active = 1
   ORDER BY application.id_applicant");
    $team_list = "";
    for ($t = 0; $t < mysql_num_rows($res_application); $t++) {
        $team_id = mysql_result($res_application, $t, "application.id_competitor");
        $team_title = iconv('windows-1251', 'UTF-8', mysql_result($res_application, $t, "competitor.title"));
        $team_list = $team_list . " (" . $team_title . ")";
    }

    //-----------Ищем, кто подписан на играющием команды-------------
    //-----для OneSignal------
    $query = "
SELECT d.*
FROM m_favourites AS f, m_uid AS d, competitor AS c 
WHERE f.user_id = d.user_id 
AND c.id_team = f.team_id 
AND c.id_competitor IN (
  SELECT competitor.id_competitor
   FROM application, competitor, competition
   WHERE application.id_player = $id_player
    AND application.id_competitor = competitor.id_competitor
    AND competitor.id_competition = competition.id_competition
    AND competition.season = $current_season
    AND application.active = 1
   ORDER BY application.id_applicant
)
AND length(d.device_id) = 36";

    $res_users = mysql_query($query);
    $num_users = mysql_num_rows($res_users);

    $push = new PushNotification();
    $push->app_id = "dc96628e-70c6-4c13-b0a3-8f95e193b467";
    $push->devices = array();
    $push->header = "Живи мини-футболом!";
    $push->text = $name . $team_list . " сегодня празднует день рождения";
    $devices_array_string = "[";

    //---------Создаем пуш
    for ($n = 0; $n < $num_users; $n++) {
        $user_id = mysql_result($res_users, $n, "user_id");
        $device_id = mysql_result($res_users, $n, "device_id");
        //$push->devices[0] = "31ab027e-6bd9-4e16-b7be-572b95c8e5ce"; //мой HTC U Play
        $push->devices[$n] = $device_id;
        $devices_array_string = $devices_array_string . $push->devices[$n] . ",";
    }

    //отсылаем пуш
    $log_string = "'\r\n" . $now . " Push to devices " . $devices_array_string . "], push-text='" . $push->text . "'\r\n";
    fwrite($log, $log_string);
    $response = $push->sendNotification();

    //-----для Fabric------
    $query = "
SELECT d.*
FROM m_favourites AS f, m_uid AS d, competitor AS c 
WHERE f.user_id = d.user_id 
AND c.id_team = f.team_id 
AND c.id_competitor IN (
  SELECT competitor.id_competitor
   FROM application, competitor, competition
   WHERE application.id_player = $id_player
    AND application.id_competitor = competitor.id_competitor
    AND competitor.id_competition = competition.id_competition
    AND competition.season = $current_season
    AND application.active = 1
   ORDER BY application.id_applicant
)
AND (length(d.device_id) = 152 or length(d.device_id) = 163)";

    $res_users_fabric = mysql_query($query);
    $num_users_fabric = mysql_num_rows($res_users_fabric);

    $pushGCM = new PushNotification();
    $pushGCM->devices = array();
    $pushGCM->header = "Живи мини-футболом!";
    $pushGCM->text = $name . $team_list . " сегодня празднует день рождения";
    $devices_array_string = "[";

    //---------Создаем пуш
    for ($n = 0; $n < $num_users_fabric; $n++) {
        $user_id = mysql_result($res_users_fabric, $n, "user_id");
        $device_id = mysql_result($res_users_fabric, $n, "device_id");
        $pushGCM->devices[$n] = $device_id;
        $devices_array_string = $devices_array_string . $pushGCM->devices[$n] . ",";
    }
    //отсылаем пуш fabric
    $log_string = "'\r\n" . $now . " Push to devices " . $devices_array_string . "], push-text='" . $pushGCM->text . "'\r\n";
    fwrite($log, $log_string);
    $responseGCM = $pushGCM->sendGCM();
    $log_string = "'\r\n" . $now . " Push GCM result: " . json_encode($responseGCM) . "'\r\n";
    fwrite($log, $log_string);
}

$log_string = "\n" . $now . " PUSH NOTIFICATION JOB FOR TODAYS BIRTHDAYS ENDED. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);

?>