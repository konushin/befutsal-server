<?php

//���������� ��������� ���������� ��������
if (ISSET($is_job)) {
    $add_string = "../";
} else {
    $add_string = "";
}

include($add_string . "../database_main.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/ban" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = "BAN UPDATE PROCEDURE STARTED at " . $now . "\n";
fwrite($log, $log_string);

//������� �������� �� ����� ��������������� ������
$query = "DELETE FROM ban WHERE (ban_end < '$now' AND ban_end > '0000-00-00') OR (ban_end = '0000-00-00' AND ban_matches_left = 0)";
$res_update = mysql_query($query, $db_connection);
$affected = mysql_affected_rows();
if ($affected > 0) {
    $log_string = "Bans removed due to date expiration: " . $affected . "\n";
    fwrite($log, $log_string);
}

//���� ���������� ��������������� �� ���������� ���
$query = "SELECT * FROM ban AS b, application AS a
  WHERE b.ban_matches_left > 0
   AND a.id_applicant = b.id_applicant";
$res_bans = mysql_query($query, $db_connection);
if (!$res_bans) {
    $log_string = mysql_errno() . ": " . mysql_error() . "\n";
    fwrite($log, $log_string);
    $log_string = "Query error: \n" . $query . "\n";
    fwrite($log, $log_string);
    print("<SCRIPT LANGUAGE=javascript>errorMessage(2)</SCRIPT>");
} else {
    $num_bans = mysql_num_rows($res_bans);
}

if ($num_bans > 0) {
    for ($i = 0; $i < $num_bans; $i++) {
        $id_applicant = mysql_result($res_bans, $i, "id_applicant");
        $id_player = mysql_result($res_bans, $i, "id_player");
        $ban_start = mysql_result($res_bans, $i, "ban_start");
        $ban_matches_initial = mysql_result($res_bans, $i, "ban_matches_initial");
        $ban_matches_left = mysql_result($res_bans, $i, "ban_matches_left");
        $id_competitor = mysql_result($res_bans, $i, "id_competitor");
        $query = " SELECT COUNT(*) AS scheduled 
	FROM schedule 
	WHERE CONCAT(date) > '$ban_start'
		AND CONCAT(date, ' ', time) < '$now' 
		AND (team_1 = $id_competitor OR team_2 = $id_competitor)";
        $res_scheduled = mysql_query($query, $db_connection);
        $scheduled = mysql_result($res_scheduled, 0, "scheduled");
        if ($ban_matches_initial <= $scheduled) {
            $query = "DELETE FROM ban WHERE id_applicant = $id_applicant";
            if (mysql_query($query, $db_connection)) {
                $log_string = "Ban removed due to expiration: applicant=" . $id_applicant . ", start=" . $ban_start . ", matches_initial=" . $ban_matches_initial . "\n";
                fwrite($log, $log_string);
            }
        } else {
            $ban_matches_left = $ban_matches_initial - $scheduled;
            $query = "UPDATE ban SET ban_matches_left = $ban_matches_left WHERE id_applicant = $id_applicant";
            if (mysql_query($query, $db_connection)) {
                $log_string = "Ban updated: applicant=" . $id_applicant . ", start=" . $ban_start . ", matches_initial=" . $ban_matches_initial . ", matches_left=" . $ban_matches_left . "\n";
                fwrite($log, $log_string);
            }
        }
    }
}

$tomorrow = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
if (!mysql_query("UPDATE settings SET value = '$tomorrow' WHERE parameter = 'ban_upd_next_execute'", $db_connection)) {
    print("<SCRIPT LANGUAGE=javascript>errorMessage(2)</SCRIPT>");
} else {
    $log_string = "BAN UPDATE PROCEDURE FINISHED, NEXT EXECUTION DATE IS SET TO " . $tomorrow . "\n";
    fwrite($log, $log_string);
}

fclose($log);

return "ok";

?>