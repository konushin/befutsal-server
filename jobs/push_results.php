<?php

include("../database_main.php");
include("../m/user_structure.php");
include("../includings/functions.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/push" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " PUSH NOTIFICATION JOB FOR NEW RESULTS STARTS. " . "\n";
fwrite($log, $log_string);

//--------------------ищем сегодняшние игры-----------------------

$query = "SELECT *
FROM job_push
WHERE sent IS NULL";

$res_results = mysql_query($query);
$num_results = mysql_num_rows($res_results);

for ($i = 0; $i < $num_results; $i++) {
    $id_push = mysql_result($res_results, $i, "id_push");
    $header = iconv('windows-1251', 'UTF-8', mysql_result($res_results, $i, "header"));
    $link = iconv('windows-1251', 'UTF-8', mysql_result($res_results, $i, "link"));

    $push = new PushNotification();
    $push->app_id = "dc96628e-70c6-4c13-b0a3-8f95e193b467";
    $push->header = "Живи мини-футболом!";
    $push->text = iconv('windows-1251', 'UTF-8', mysql_result($res_results, $i, "text"));

    $devices = unserialize(mysql_result($res_results, $i, "m_uid"));
    $number_of_devices = sizeof($devices);

    $oneSignal_devices = array();
    $fabric_devices = array();
    for ($n = 0; $n < $number_of_devices; $n++) {
        if (strlen($devices[$n]) == 36) {
            $oneSignal_devices[] = $devices[$n];
        } else {
            $fabric_devices[] = $devices[$n];
        }
    }

    $log_string = "'\r\n" . $now . " Push to oneSignal devices: " . serialize($oneSignal_devices) . "], push-text='" . $push->text . "'\r\n";
    fwrite($log, $log_string);

    $push->devices = $oneSignal_devices;
    $response = $push->sendNotification();

    $log_string = "'\r\n" . $now . " Push to Fabric devices: " . serialize($fabric_devices) . "], push-text='" . $push->text . "'\r\n";
    fwrite($log, $log_string);

    $push->devices = $fabric_devices;
    $response = $push->sendGCM();

    $log_string = "'\r\n" . $now . " Fabric response: " . $response . "]'\r\n";
    fwrite($log, $log_string);

    $time = time();
    $query = "UPDATE job_push SET sent = $time WHERE id_push = $id_push";
    $res_update = mysql_query($query);
}

$log_string = "\n" . $now . " PUSH NOTIFICATION JOB FOR NEW RESULTS ENDS. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);

?>