<?php

include("../database_main.php");
include("../m/user_structure.php");
include("../includings/functions.php");

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/push" . $pre_date . ".log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " PUSH NOTIFICATION JOB FOR NEXT GAMES STARTED. " . "\n";
fwrite($log, $log_string);

//--------------------ищем завтрашние игры-----------------------

$today = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
$query = "SELECT id_schedule, time, schedule.pitch AS pitch, competition.title, team_1, team_2, league
FROM schedule, competition
WHERE date = '$today'
      AND schedule.league = competition.id_competition 
ORDER BY time";

$res_schedule = mysql_query($query);
$num_schedule = mysql_num_rows($res_schedule);

for ($i = 0; $i < $num_schedule; $i++) {
    $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
    $time = substr(mysql_result($res_schedule, $i, "time"), 0, 5);
    $pitch = iconv('windows-1251', 'UTF-8', mysql_result($res_schedule, $i, "pitch"));
    $id_league = mysql_result($res_schedule, $i, "league");
    $league_title = iconv('windows-1251', 'UTF-8', mysql_result($res_schedule, $i, "title"));
    $team_1 = mysql_result($res_schedule, $i, "team_1");
    $team_2 = mysql_result($res_schedule, $i, "team_2");
    $res_team_1 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_1", $db_connection);
    $team_title_1 = iconv('windows-1251', 'UTF-8', mysql_result($res_team_1, 0, "title"));
    $res_team_2 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_2", $db_connection);
    $team_title_2 = iconv('windows-1251', 'UTF-8', mysql_result($res_team_2, 0, "title"));

    //-----------Ищем, кто подписан на играющием команды-------------
    //-----для OneSignal--------------
    $query = "
SELECT d.* 
FROM m_favourites AS f, m_uid AS d, competitor AS c 
WHERE f.user_id = d.user_id 
AND c.id_team = f.team_id 
AND c.id_competitor IN ($team_1, $team_2)
AND length(d.device_id) = 36";
    $res_users = mysql_query($query);
    $num_users = mysql_num_rows($res_users);

    //-----для Fabric------------------
    $query = "
SELECT d.* 
FROM m_favourites AS f, m_uid AS d, competitor AS c 
WHERE f.user_id = d.user_id 
AND c.id_team = f.team_id 
AND c.id_competitor IN ($team_1, $team_2)
AND (length(d.device_id) = 152 or length(d.device_id) = 163)";
    $res_users_fabric = mysql_query($query);
    $num_users_fabric = mysql_num_rows($res_users_fabric);

    //---------Создаем пуш
    $push = new PushNotification();
    $push->app_id = "dc96628e-70c6-4c13-b0a3-8f95e193b467";
    $push->devices = array();
    $push->header = "Живи мини-футболом!";
    $push->text = "Завтра в " . $time . " матч " . $team_title_1 . " - " . $team_title_2 . " (" . $pitch . ")";
    $devices_array_string = "[";
    for ($n = 0; $n < $num_users; $n++) {
        $user_id = mysql_result($res_users, $n, "user_id");
        $device_id = mysql_result($res_users, $n, "device_id");
        $push->devices[$n] = $device_id;
        $devices_array_string = $devices_array_string . $push->devices[$n] . ",";
    }
    //отсылаем пуш
    $log_string = "'\r\n" . $now . " Push to devices " . $devices_array_string . "], push-text='" . $push->text . "'\r\n";
    fwrite($log, $log_string);
    $response = $push->sendNotification();

    //---------Создаем пуш fabric
    $pushGCM = new PushNotification();
    $pushGCM->devices = array();
    $pushGCM->header = "Живи мини-футболом!";
    $pushGCM->text = "Завтра в " . $time . " матч " . $team_title_1 . " - " . $team_title_2 . " (" . $pitch . ")";
    $devices_array_string = "[";
    for ($n = 0; $n < $num_users_fabric; $n++) {
        $user_id = mysql_result($res_users_fabric, $n, "user_id");
        $device_id = mysql_result($res_users_fabric, $n, "device_id");
        $pushGCM->devices[$n] = $device_id;
        $devices_array_string = $devices_array_string . $pushGCM->devices[$n] . ",";
    }
    //отсылаем пуш fabric
    $log_string = "'\r\n" . $now . " Push to devices " . $devices_array_string . "], push-text='" . $pushGCM->text . "'\r\n";
    fwrite($log, $log_string);
    $responseGCM = $pushGCM->sendGCM();
    $log_string = "'\r\n" . $now . " Push GCM result: " . json_encode($responseGCM) . "'\r\n";
    fwrite($log, $log_string);

    //отправляем в телеграм
    define('BOT_TOKEN', getParameter("myBefutsal_bot_token"));
    $subscribers_league = getSubscribers("league", $league);
    $subscribers_team1 = getSubscribers("competitor", $team_1);
    $subscribers_team2 = getSubscribers("competitor", $team_2);
    $subscribers = array_merge($subscribers_league, $subscribers_team1, $subscribers_team2);
    $subscribers = array_values($subscribers);
    $subscribers = array_unique($subscribers);
    $tg_bot = new tg_bot(BOT_TOKEN);
    $tg_text = "Завтра в " . $time . " матч <a href='https://befutsal.ru/match_preview.php?s=" . $id_schedule . "'>" . $team_title_1 . " - " . $team_title_2 . "</a>";
    for ($m = 0; $m < sizeof($subscribers); $m++) {
        $out = "TELEGRAM: " . $tg_bot->send($subscribers[$m], $tg_text, null, false);
        writeLog($log, $out);
    }
}

$log_string = "\n" . $now . " PUSH NOTIFICATION JOB FOR NEXT GAMES ENDED. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);