<?php

include('../database_main.php');
include('../includings/functions.php');

if (!ISSET($logpath)) {
    $logpath = "../logs/rank.log";
}

$now = date("Y-m-d H:i:s");

$log = fopen($logpath, 'a+b');
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);
$log_string = $now . " RANKING PROCESSING JOB STARTS. " . "\n";
fwrite($log, $log_string);

print('<HTML>');

print("<HEAD>");
print("<TITLE>������ ������� ���� - ���� ����-��������!</TITLE>");
print('<LINK HREF="../graphics/style.css" REL="stylesheet" type="text/css">');
print("<META HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=windows-1251'>");
print("<meta name=viewport content='width=device-width, initial-scale=1'>");
print("<META NAME='description' CONTENT='" . $description . "'>");
print("<META NAME='keywords' CONTENT='" . $keywords . "'>");
print("<LINK REL='icon' href='../graphics/favicon.ico' type='image/x-icon'>");
print("<LINK REL='shortcut icon' href='../graphics/favicon.ico' type='image/x-icon'>");
print("</HEAD>");

$res_season = mysql_query("SELECT * FROM season ORDER BY id_season DESC LIMIT 0,6");
$current_season = mysql_result($res_season, 0, "id_season");
$season = mysql_result($res_season, 5, "id_season");

print("��������� ������: >= " . mysql_result($res_season, 5, "title") . " <= " . mysql_result($res_season, 0, "title"));

$log_string = iconv("UTF-8", "Windows-1251", "'\r\n" . $now . " Seasons counted: >= " . mysql_result($res_season, 5, "title") . " <= " . mysql_result($res_season, 0, "title") . "'\r\n");
fwrite($log, $log_string);

//----------------------�������� ������� �� ��������� 5 �������------------------------

$query = "
 SELECT DISTINCT t.id_team
 FROM team AS t, competitor AS cr, competition AS cn
 WHERE
  t.id_team = cr.id_team
  AND cr.id_competition = cn.id_competition
  AND cn.season >= $season AND cn.season <= $current_season
  AND cn.rate IS NOT NULL
";
$res_teams = mysql_query($query);

$num_teams = mysql_num_rows($res_teams);
print("<br>����� ������: " . $num_teams . "<br>");

$log_string = "'\r\n" . $now . " QUERY: " . $query . "'\r\n";
fwrite($log, $log_string);

$log_string = "'\r\n" . $now . " NUM TEAMS: " . $num_teams . "'\r\n";
fwrite($log, $log_string);

//-------------------������� �������--------------------------------

$now_date = date("Y-m-d");

$query = "UPDATE settings SET value = '$now_date' WHERE id_parameter = 1";
$res_update = mysql_query($query);
$log_string = "'\r\n" . $now . " New Ranking date: " . $query . "'\r\n";
fwrite($log, $log_string);

for ($i = 0; $i < $num_teams; $i++) {
    $id_team = mysql_result($res_teams, $i, "id_team");
    $query = "SELECT cr.id_competition, cn.title
	FROM competition AS cn, competitor AS cr 
	WHERE cr.id_team = $id_team
		AND cr.id_competition = cn.id_competition
		AND cn.season = $current_season
		AND cn.rate IS NOT NULL";
    $res_league = mysql_query($query);
    if (mysql_num_rows($res_league) > 0) {
        $id_league = mysql_result($res_league, 0, "id_competition");
        $league_title = mysql_result($res_league, 0, "title");
    } else {
        $id_league = NULL;
        $league_title = NULL;
    }

    $query = "
  SELECT cn.id_competition, cr.id_competitor, r.rate, cn.season
  FROM competition AS cn, competitor AS cr, competition_rate AS r
  WHERE cr.id_team = $id_team
   AND cr.id_competition = cn.id_competition
   AND cn.season >= $season AND cn.season <= $current_season
   AND cn.rate = r.id_competition
 ";
    $res_leagues = mysql_query($query);
    $num_leagues = mysql_num_rows($res_leagues);

    $points = 0;
    $seasons = 0;

    for ($n = 0; $n < $num_leagues; $n++) {
        $league = mysql_result($res_leagues, $n, "id_competition");
        $id_competitor = mysql_result($res_leagues, $n, "id_competitor");
        $rate = mysql_result($res_leagues, $n, "rate");
        $id_season = mysql_result($res_leagues, $n, "season");
        $query = "SELECT * FROM league_table_content WHERE id_league = $league AND id_competitor = $id_competitor";
        $res_pts = mysql_query($query);
        if (mysql_num_rows($res_pts) > 0) {
            $seasons++;
            $season_coefficient = (11 - ($current_season - $id_season)) / 10;
            $goalsfor = 0;
            $goalsfor = mysql_result($res_pts, 0, "goalsfor") * $rate * 0.01;
            $points = $points + (mysql_result($res_pts, 0, "pts") * $rate + $goalsfor) * $season_coefficient;
        } else {
            print("<br>�� ������� ������� ���: " . $query);
            $log_string = "'\r\n" . $now . " Table NOT FOUND: " . $query . "'\r\n";
            fwrite($log, $log_string);
        }
    }

    if ($id_league == NULL) {
        $query = "INSERT INTO ranking(id_team, rate, date) VALUES ($id_team, $points, '$now_date')";
    } else {
        $query = "INSERT INTO ranking(id_team, rate, id_league, league_title, date) VALUES ($id_team, $points, $id_league, '$league_title', '$now_date')";
    }
    $res_insert = mysql_query($query);
    if (!$res_insert) {
        echo("<br>�� �������� ������: " . $query);
        $log_string = iconv("UTF-8", "Windows-1251", "'\r\n" . $now . " Script ERROR: " . $query . "'\r\n");
        fwrite($log, $log_string);
    }
}

//����������� �����
$res_ranking = mysql_query("SELECT * FROM ranking WHERE `date` = '$now_date' ORDER BY rate DESC", $db_connection);
for ($i = 0; $i < (mysql_num_rows($res_ranking)); $i++) {
    $place = $i + 1;
    $id_team = mysql_result($res_ranking, $i, "id_team");
    $query = "UPDATE ranking SET rank = $place WHERE id_team = $id_team AND `date` = '$now_date'";
    $res_update = mysql_query($query);
    if (!$res_update) {
        echo("<br>�� �������� ������: " . $query);
        $log_string = iconv("UTF-8", "Windows-1251", "'\r\n" . $now . " Script ERROR: " . $query . "'\r\n");
        fwrite($log, $log_string);
    }
}

$result = sendTelegram(
    'sendMessage',
    array(
        'chat_id' => getParameter("telegram_channel_id"),
        'text' => html_entity_decode(iconv("Windows-1251", "UTF-8", "����������� ����� <a href='https://befutsal.ru/ranking.php'>������� ���� ����-��������!</a>\n\n #befutsal #����������������!")),
        'parse_mode' => "HTML"
    )
);

if ($result) {
    $log_string = "\n" . $now . " Message successfully sent to Telegram channel \n";
    fwrite($log, $log_string);
} else {
    $log_string = "\n" . $now . " Unable to send to Telegram channel: " . serialize($result) . "\n";
    fwrite($log, $log_string);
}


$log_string = "\n" . $now . " RANKING PROCESSING  ENDS. " . "\n";
fwrite($log, $log_string);
$log_string = "---------------------------------------------------------------------------\n";
fwrite($log, $log_string);

fclose($log);