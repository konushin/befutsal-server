<!--�������������� ��-->
<?php

include('../../database_main.php');
include('../functions.php');

$AdminUser = new AdminUser();
$AdminUser->getInfo("db_menu");

//var_dump($AdminUser);

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../../logs/editor" . $pre_date . ".log";
}

print("<HTML>");

print("<BODY BGCOLOR='44618A'>");

print("<link rel='stylesheet' type='text/css' href='../../graphics/style_editor.css'>");

print("<TABLE align=center width=100% border=0 cellpadding=0 cellspacing=0 style='font-family:Tahoma; font-size: 8pt'>");

print("<tr><td bgcolor=d9d7d0 valign='bottom'><font color=2c476e><img src='../../graphics/edit_category.jpg' border='0'><b>��������� ��������</b>");
print("<tr><td><a class=underline href='category_new.php' target='current_point'>����� ���������</a>");
print("<tr><td><a class=underline href='category_edit.php' target='current_point'>������������� ���������</a>");

print("<tr><td>&nbsp;");
print("<tr><td bgcolor=d9d7d0 valign='bottom'><font color=2c476e><img src='../../graphics/edit_competition.jpg' border='0'><b>�������</b>");
print("<tr><td><a class=underline href='competition_new.php' target='current_point'>����� ������</a>");
print("<tr><td><a class=underline href='competition_rename.php' target='current_point'>�������� �������� �������</a>");
print("<tr><td><a class=underline href='competition_comment.php' target='current_point'>�������� ����������� � �������</a>");

if ($AdminUser->getPermission("delete")) {
    print("<tr><td><a class=underline href='competition_remove.php' target='current_point'>������� ������</a>");
}

print("<tr><td>&nbsp;");
print("<tr><td><a class=underline href='groups_dates_set.php' target='current_point'>������ ���� ����� � ����-���</a>");
print("<tr><td><a class=underline href='groups_select_competition.php' target='current_point'>������� ������ �� ������</a>");
print("<tr><td><a class=underline href='cup_select_competition.php' target='current_point'>������������� ������ ������</a>");

print("<tr><td>&nbsp;");
print("<tr><td bgcolor=d9d7d0 valign='bottom'><font color=2c476e><img src='../../graphics/edit_application.jpg' border='0'><b>������ ������</b>");
print("<tr><td><a class=underline href='competitor_new.php' target='current_point'>������� �������</a>");
print("<tr><td><a class=underline href='competition_batch_1.php' target='current_point'>��������� ������ ����</a>");

if ($AdminUser->getPermission("delete")) {
    print("<tr><td><a class=underline href='competitor_remove.php' target='current_point'>��������� �������</a>");
    print("<tr><td><a class=underline href='competitor_rename.php' target='current_point'>������������� ��������</a>");
}

print("<tr><td><a class=underline href='farmclub_select.php' target='current_point'>����-�����</a>");
print("<tr><td>&nbsp;");
print("<tr><td><a class=underline href='staff_batch_1.php' target='current_point'>����������� ������ ����������</a>");
print("<tr><td><a class=underline href='staff_new_select_player.php' target='current_point'>��������� ���������</a>");

if ($AdminUser->getPermission("delete")) {
    print("<tr><td><a class=underline href='staff_remove.php' target='current_point'>��������� ���������</a>");
}

print("<tr><td><a class=underline href='player_transfer.php' target='current_point'>������� ���������</a>");

print("<tr><td>&nbsp;");
print("<tr><td bgcolor=d9d7d0 valign='bottom'><font color=2c476e><img src='../../graphics/edit_referee.jpg' border='0'><b>�����</b>");
print("<tr><td><a class=underline href='referee_new.php' target='current_point'>����� �����</a>");
print("<tr><td><a class=underline href='referee_rename.php' target='current_point'>�������� ������ � �����</a>");

if ($AdminUser->getPermission("delete")) {
    print("<tr><td><a class=underline href='referee_remove.php' target='current_point'>������� ������ � �����</a>");
}


print("<tr><td>&nbsp;");
print("<tr><td bgcolor=d9d7d0 valign='bottom'><font color=2c476e><img src='../../graphics/edit_player.png' border='0'><b>������ � �������</b>");
print("<tr><td><a class=underline href='player_new.php' target='current_point'>����� ��������</a>");
print("<tr><td><a class=underline href='player_rename.php' target='current_point'>�������� ������ ���������</a>");
print("<tr><td><a class=underline href='player_photo.php' target='current_point'>��������� ���� ���������</a>");

if ($AdminUser->getPermission("delete")) {
    print("<tr><td><a class=underline href='player_remove.php' target='current_point'>������� ������ ���������</a>");
}

print("<tr><td>&nbsp;");
print("<tr><td bgcolor=d9d7d0 valign='bottom'><font color=2c476e><img src='../../graphics/edit_team.jpg' border='0'><b>�������</b>");
print("<tr><td><a class=underline href='team_new.php' target='current_point'>����� �������</a>");
print("<tr><td><a class=underline href='team_rename.php' target='current_point'>�������� ������ �������</a>");
print("<tr><td><a class=underline href='team_emblem.php' target='current_point'>��������� ������� �������</a>");

if ($AdminUser->getPermission("delete")) {
    print("<tr><td><a class=underline href='team_remove.php' target='current_point'>������� ������ �������</a>");
}

print("<tr><td>&nbsp;");
print("<tr><td bgcolor=d9d7d0 valign='bottom'><font color=2c476e><img src='../../graphics/edit_service.png' border='0'><b>��������� �������</b>");
print("<tr><td><a class=underline href='service_staff_protocol_reference.php' target='current_point'>������� ������ ������ �� ��������� (���������)</a>");
print("<tr><td><a class=underline href='service_league_tables_update.php' target='current_point'>�������� ��� ������ (���������)</a>");
print("<tr><td><a class=underline href='service_bans.php' target='current_point'>����������� ����� ��������������� (���������)</a>");

if ($AdminUser->getPermission("history")) {
    print("<tr><td><a class=underline href='service_similar_players.php' target='current_point'>������������� �������</a>");
    print("<tr><td><a class=underline href='service_similar_teams.php' target='current_point'>������������� ������</a>");
    print("<tr><td><a class=underline href='service_similar_referees.php' target='current_point'>������������� �����</a>");
}

if ($AdminUser->getPermission("delete")) {
    print("<tr><td><a class=underline href='service_new_season.php' target='current_point'>������� ����� �����</a>");
}

print("</font></table></BODY></HTML>");

?>