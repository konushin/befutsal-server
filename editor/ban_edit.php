<?php

//ini_set('display_errors', 'On');
//error_reporting(-1);

print("<META HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=windows-1251'>");
print("<script src='../includings/jquery.min.js' type='text/javascript'></script>");

//����������� � ��
include('../database_main.php');
include('../includings/functions.php');
include('functions.php');

$AdminUser = new AdminUser();
$AdminUser->getInfo();

$pre_date = date("Ym");

if (!ISSET($logpath)) {
    $logpath = "../logs/editor" . $pre_date . ".log";
}

$filter = $_GET["filter"];
$label0 = "��������! ������������ ������������ ������ ������ ������ ��� ������� �������������. ��� - ��������� ��� ���������� ���������";
$label1 = "������ - ��������� ���������, � ������� ���� ���� �� ���� ������ ��� ������� ��������";
$label2 = "������� - ��������� ���������, � ������� ���� ������� ��� 3/6/9/12 � �.�. ������ ��������";
$label3 = "���������� - ��������� ���������, � ������� ���� �������� ���������������";
$label4 = "������� - ��������� ��� �������/������������� ������";

switch ($filter) {
    case 0:
        $label = $label0;
        $having_clause = "1 = 1";
        break;
    case 1:
        $label = $label1;
        $having_clause = "bookings > 0 OR sentoffs > 0";
        break;
    case 2:
        $label = $label2;
        $having_clause = "(bookban = 0 AND bookings > 0) OR sentoffs > 0";
        break;
    case 4:
        $label = $label4;
        $having_clause = "1 = 1";
        break;
    default:
        $label = $label3;
        $having_clause = "ban_matches_initial > 0 OR ban_end != '0000-00-00'";
        break;
}

$res_current_season = mysql_query("SELECT * FROM season ORDER BY id_season DESC", $db_connection);
$current_season = mysql_result($res_current_season, 0, "id_season");

//-------�������� �������, � ������� ���� 3-6-9 � �.�. �������������� ��� ��������
if ($filter < 3) {
    $query = "SELECT a.id_applicant, CONCAT(pl.name_2, ' ', pl.name_1) AS playername, cr.title AS teamtitle, cn.title AS leaguetitle, cn.id_competition AS league_id, SUM(s.yellow) AS bookings, SUM(s.yellow)%3 AS bookban, SUM(s.red) AS sentoffs, ban_matches_initial, ban_end, ban_title
FROM application AS a JOIN competitor AS cr JOIN competition AS cn JOIN staff AS s JOIN protocol AS pr JOIN player AS pl
LEFT JOIN ban AS b USING (id_applicant)
WHERE a.id_competitor = cr.id_competitor
 AND cr.id_competition = cn.id_competition
 AND cn.season = $current_season
 AND a.id_applicant = s.id_player
 AND s.id_protocol = pr.id_protocol
 AND pr.league = cn.id_competition
 AND a.id_player = pl.id_player
GROUP BY a.id_applicant
HAVING " . $having_clause . "
ORDER BY leaguetitle ASC, playername ASC, teamtitle ASC";
} elseif ($filter == 4) {
    $query = "SELECT a.id_applicant, CONCAT(pl.name_2, ' ', pl.name_1) AS playername, cr.title AS teamtitle, cn.id_competition AS league_id, cn.title AS leaguetitle, ban_matches_initial, ban_end, ban_title
FROM application AS a JOIN competitor AS cr JOIN competition AS cn JOIN player AS pl
  LEFT JOIN ban AS b USING (id_applicant)
WHERE a.id_competitor = cr.id_competitor
      AND cr.id_competition = cn.id_competition
      AND cn.season = $current_season
      AND a.id_player = pl.id_player
  AND a.goalie = 2
GROUP BY a.id_applicant
ORDER BY leaguetitle ASC, playername ASC, teamtitle ASC";
} else {
    $query = "SELECT a.id_applicant, CONCAT(pl.name_2, ' ', pl.name_1) AS playername, cr.title AS teamtitle, cn.title AS leaguetitle, cn.id_competition AS league_id, ban_matches_initial, ban_end, ban_title
FROM application AS a, competitor AS cr, competition AS cn, player AS pl, ban AS b
WHERE a.id_competitor = cr.id_competitor
 AND cr.id_competition = cn.id_competition
 AND a.id_player = pl.id_player
 AND a.id_applicant = b.id_applicant
GROUP BY a.id_applicant
ORDER BY leaguetitle ASC, playername ASC, teamtitle ASC";
}

$res_bans = mysql_query($query, $db_connection);
$num_bans = mysql_num_rows($res_bans);

print("<table width=100% border=0 cellspacing=0 cellpadding=0 style='BORDER-TOP: #44618A 1px solid; BORDER-BOTTOM: #44618A 1px solid;'>");
print("<tr><td align=center BORDER-BOTTOM: #44618A 1px solid; bgcolor=#A5B6CF><b><font face='Tahoma' color=#44618A>���������������</font></b>");

print("<tr><TD ALIGN='center'>");

print("<br><p align=center><font face=Tahoma size=2pt>������: <a href='ban_edit.php?filter=3' title='" . $label3 . "'>����������</a> | <a href='ban_edit.php?filter=2' title='" . $label2 . "'>�������</a> | <a href='ban_edit.php?filter=1' title='" . $label1 . "'>������</a> | <a href='ban_edit.php?filter=4' title='" . $label4 . "'>�������</a> | <a href='ban_edit.php?filter=0' title='" . $label0 . "'>���</a>");

print("<br><p align=center><font face=Tahoma size=2pt></font>������� ������: " . $label . "</p>");

print("<tr align='center'><td><br>");

print("<form method='POST'>");

print("<table align=center border=1 cellspacing=0 cellpadding=3 style='BORDER-TOP: #44618A 1px solid; BORDER-BOTTOM: #44618A 1px solid; font-family: Tahoma'>");
print("<tr><td align='center'><strong>�������� (�������, ���� &#8593;)<td align='center'><strong>���������<td align='center'><strong>�������<td align='center'><strong>����");
$ban_expire_values = explode(",", getParameter("ban_expire_values"));

$color = FALSE;
for ($i = 0; $i < $num_bans; $i++) {
    $league_id = mysql_result($res_bans, $i, "league_id");

    $res_categories = mysql_query("SELECT * FROM competition_by_category WHERE id_competition = $league_id");
    for ($cat = 0; $cat < mysql_num_rows($res_categories); $cat++) {
        $category_id = mysql_result($res_categories, $cat, "id_category");
        if ($AdminUser->getPermission("category", $category_id)) {
            $color = !$color;
            if ($color) {
                $bgcolor = "#FFFFFF";
            } else {
                $bgcolor = "#A5B6CF";
            }
            $name = mysql_result($res_bans, $i, "playername");
            $id_applicant = mysql_result($res_bans, $i, "id_applicant");
            $team_title = cutString(mysql_result($res_bans, $i, "teamtitle"), 20);
            $league_title = cutString(mysql_result($res_bans, $i, "leaguetitle"), 30);

            if ($filter != 3) {
                $bookings = mysql_result($res_bans, $i, "bookings");
                $sentoffs = mysql_result($res_bans, $i, "sentoffs");
            }
            $ban_matches_initial = mysql_result($res_bans, $i, "ban_matches_initial");
            $ban_title = mysql_result($res_bans, $i, "ban_title");
            $ban_end = mysql_result($res_bans, $i, "ban_end");
            print("<tr bgcolor=" . $bgcolor . ">
    <td align=left><font size=2>" . $name . " (" . $team_title . ", " . $league_title . ")</font>
    <td align=center><font size=2><img src='../graphics/yellow_card.gif' title='������ ��������'>x<font size=3>" . $bookings . "&nbsp&nbsp&nbsp<img src='../graphics/red_card.gif' title='������� ��������'><font size=2>x<font size=3>" . $sentoffs . "
    <td align=left>");
            print("� <select id='year" . $id_applicant . "' style='font-size: 7pt; width: 35pt;'>");
            $a_date = getdate();
            for ($d = $a_date["year"] - 1; $d <= $a_date["year"] + 1; $d++) {
                if ($d == $a_date["year"]) {
                    print ("<option value=" . $d . " SELECTED>" . $d);
                } else {
                    print ("<option value=" . $d . ">" . $d);
                }
            }
            print("</select><select id='month" . $id_applicant . "' style='font-size: 7pt; width: 27pt;'>");
            for ($d = 1; $d <= 12; $d++) {
                if ($d == $a_date["mon"]) {
                    print ("<option value=" . $d . " SELECTED>" . $d);
                } else {
                    print ("<option value=" . $d . ">" . $d);
                }
            }
            print("</select><select id='day" . $id_applicant . "' style='font-size: 7pt; width: 27pt;'>");
            for ($d = 1; $d <= 31; $d++) {
                if ($d == $a_date["mday"]) {
                    print ("<option value=" . $d . " SELECTED>" . $d);
                } else {
                    print ("<option value=" . $d . ">" . $d);
                }
            }
            print("</select>&nbsp;");

            for ($n = 0; $n < sizeof($ban_expire_values); $n++) {
                $postfix = $ban_expire_values[$n] . " ����";
                if ($ban_expire_values[$n] > 1 AND $ban_expire_values[$n] <= 4) {
                    $postfix = $ban_expire_values[$n] . " ����";
                } elseif ($ban_expire_values[$n] >= 5 AND $ban_expire_values[$n] < 50) {
                    $postfix = $ban_expire_values[$n] . " ���";
                } elseif ($ban_expire_values[$n] == 50) {
                    $postfix = "�������";
                } elseif ($ban_expire_values[$n] == 100) {
                    $postfix = "1 ���";
                } elseif ($ban_expire_values[$n] == 200) {
                    $postfix = "2 ����";
                } elseif ($ban_expire_values[$n] == 1000) {
                    $postfix = "���";
                }
                print("<button class='ban" . $id_applicant . "_" . $ban_expire_values[$n] . "' style='font-size: 7pt;'>" . $postfix . "</button>&nbsp;");
                print("<script language='javascript' type='text/javascript'>
      $('.ban" . $id_applicant . "_" . $ban_expire_values[$n] . "').click( function(e) {
	e.preventDefault();
        $.ajax({
	  type: 'POST',
	  url: 'ban_submit.php',
	  data: {
		'id_applicant': '" . $id_applicant . "',
		'expire': '" . $ban_expire_values[$n] . "',
		'ban_title': $('#ban" . $id_applicant . "_title').val(),
		'ban_label': '" . $postfix . "',
		'ban_start_year': $('#year" . $id_applicant . "').val(),
		'ban_start_month': $('#month" . $id_applicant . "').val(),
		'ban_start_day': $('#day" . $id_applicant . "').val()
	  },
          success: function(data){
            $('.results" . $id_applicant . "').html(data);
          }
        });

      });
      </script>");
            }

            print("
    	<button class='ban" . $id_applicant . "_0' style='font-size: 7pt; background: #929ea8;'>������</button></a>
    	<script language='javascript' type='text/javascript'>
	$('.ban" . $id_applicant . "_0').click( function(e) {
	e.preventDefault();
        $.ajax({
	  type: 'POST',
	  url: 'ban_submit.php',
	  data: {
		'id_applicant': '" . $id_applicant . "',
		'expire': '" . $ban_expire_values[$n] . "',
		'ban_title': $('#ban" . $id_applicant . "_title').val(),
		'ban_label': '" . $postfix . "'
	  },
          success: function(data){
            $('.results" . $id_applicant . "').html(data);
          }
        });
      });
	</script>
 ");

            print("<input type='text' size='20' style='font-size: 7pt;' id='ban" . $id_applicant . "_title' value='" . $ban_title . "'></input>");

            if ($ban_matches_initial > 0) {
                $current_ban = $ban_matches_initial . " ���";
            } elseif ($ban_end > "0000-00-00" and $ban_end < "3000-01-01") {
                $current_ban = $ban_end;
            } elseif ($ban_end >= "3000-01-01") {
                $current_ban = $ban_end;
            } else {
                $current_ban = "---";
            }

            print("<td align=left><div class='results" . $id_applicant . "'><font size=2>" . $current_ban . "</div>");
        }
    }


}

print("</table>");
print("</form>");

print("<br></table>");

mysql_close($db_connection);
?>