<?php

/**
 * Created by PhpStorm.
 * User: Konushin
 * Date: 07.01.2022
 * Time: 15:03
 */
class AdminUser {

    public $user_id;
    public $username;
    public $super;

    public function getInfo($source_page = 'Undefined page') {
        $pre_date = date("Ym");
        if (!isset($logpath)) {
            $logpath = "../../logs/editor" . $pre_date . ".log";
        }

        $remote_user = $_SERVER['REMOTE_USER'];
        $query = "SELECT * FROM admin_users WHERE username = '$remote_user'";
        $res_user = mysql_query($query);
        if (mysql_num_rows($res_user) == 0) {
            $this->super = TRUE;
            $this->username = $remote_user;
            $message = "INFO: " . $source_page . ". User " . $this->username . " is superadmin";
            writeLog($logpath, $message);
        } else {
            $this->user_id = mysql_result($res_user, 0, "user_id");
            $this->username = mysql_result($res_user, 0, "username");

            $res_super = mysql_num_rows(mysql_query("SELECT * FROM admin_permissions WHERE user_id = $this->user_id"));
            if ($res_super == 0) {
                $this->super = TRUE;
            } else {
                $this->super = FALSE;
            }

            $message = "INFO: " . $source_page . ". User " . $this->username . " is admin";
            writeLog($logpath, $message);
        }

        return $this;
    }

    public function getPermission($permission_group, $group_id = null) {
        $pre_date = date("Ym");
        if (!isset($logpath)) {
            $logpath = "../../logs/editor" . $pre_date . ".log";
        }

        if (!isset($this->user_id)) {
            $this->getInfo("functions");
        }

        //если суперадмин, сразу даем права
        if ($this->super) {
            return TRUE;
        }

        if (isset($group_id)) {
            $query = "SELECT * FROM admin_permissions WHERE user_id = $this->user_id AND permission_group = '$permission_group' AND group_id in ($group_id)";
        } else {
            $query = "SELECT * FROM admin_permissions WHERE user_id = $this->user_id AND permission_group = '$permission_group'";
        }

        //$message = "DEBUG: User ".$this->username.", permission query=" . $query;
        //writeLog($logpath, $message);

        $res_permission = mysql_query($query);
        $num_permissions = mysql_num_rows($res_permission);

        if ($num_permissions != 1) {
            // если ограничений на группу не найдено
            $message = "DEBUG: User ".$this->username." has no permissions for " . $permission_group . " on " . $group_id;
            writeLog($logpath, $message);
            return FALSE;
        } else {
            $allow = mysql_result($res_permission, 0, "permission_allow");
            if ($allow == 1) {
                //если права есть
                $message = "DEBUG: User ".$this->username." has permissions for " . $permission_group . " on " . $group_id;
                writeLog($logpath, $message);
                return TRUE;
            } else {
                //если прав нет
                $message = "DEBUG: User ".$this->username." has no permissions for " . $permission_group . " on " . $group_id;
                writeLog($logpath, $message);
                return FALSE;
            }
        }
    }

}

/**
 * Логирование
 * @param string $filename Имя файла
 * @param string $log_string Логируемая строка
 */
if (!function_exists("writeLog")) {

    function writeLog($filename, $log_string) {
        $log = fopen($filename, 'a+');
        $time = date("Y-m-d H:i:s");
        $log_string = $time . ": " . $log_string . "\n";
        fwrite($log, $log_string);
        fclose($log);
    }

}
?>