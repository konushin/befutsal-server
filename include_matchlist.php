<?php

/**
 * Created by PhpStorm.
 * User: Konushin
 * Date: 23.11.2021
 * Time: 18:56
 */
if ($id_protocol > 0) {
    $is_forfeited = mysql_result($res_schedule, $i, "forfeited");

    if ($is_forfeited == NULL) {
        $score_end_1 = mysql_result($res_schedule, $i, "score_end_1");
        $score_end_2 = mysql_result($res_schedule, $i, "score_end_2");
    } else {
        if (mysql_result($res_schedule, $i, "score_end_1") == 0) {
            $score_end_1 = "-";
        } elseif (mysql_result($res_schedule, $i, "score_end_1") > 0) {
            $score_end_1 = "+";
        }

        if (mysql_result($res_schedule, $i, "score_end_2") == 0) {
            $score_end_2 = "-";
        } elseif (mysql_result($res_schedule, $i, "score_end_2") > 0) {
            $score_end_2 = "+";
        }
    }


    if (!isset($group_label)) {
        $group_label = null;
    }

    $href = "window.location.href='match.php?p=" . $id_protocol . "'";

    $result = '<tr onclick="' . $href . '; return false"><td class="team1">' . cutString($competitor_home->title, 30) . '&nbsp;<img src="/emblems/' . $competitor_home->team_details->emblem_path . '" style="max-height:25px; max-width:25px">&nbsp;</a></td><td class="score"><strong>' . $score_end_1 . '</strong></td><td class="score"><strong>' . $score_end_2 . '</strong></td><td  class="team2">&nbsp;<img src="/emblems/' . $competitor_away->team_details->emblem_path . '"  style="max-height:25px; max-width:25px">&nbsp;' . cutString($competitor_away->title, 30) . '</a></td><td class="only-desktop" style="width: 30%;"><i class="fa fa-map-marker"></i>&nbsp;' . $pitch . ' ' . $group_label . '</td></tr>';
} else {
    $href = "window.location.href='match_preview.php?s=" . $id_schedule . "'";

    $result = '<tr onclick="' . $href . '; return false"><td class="team1">' . cutString($competitor_home->title, 30) . '&nbsp;<img src="/emblems/' . $competitor_home->team_details->emblem_path . '" style="max-height:25px; max-width:25px">&nbsp;</td><td colspan="2" style="width: 10%;"><i class="fa fa-calendar-o"></i> ' . $time . '</td><td  class="team2">&nbsp;<img src="/emblems/' . $competitor_away->team_details->emblem_path . '"  style="max-height:25px; max-width:25px">&nbsp;' . cutString($competitor_away->title, 30) . '</a></td><td class="only-desktop" style="width: 30%;"><i class="fa fa-map-marker"></i>&nbsp;' . $pitch . ' ' . $group_label . '</td></tr>';
}

print($result);
?>