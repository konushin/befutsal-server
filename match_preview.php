﻿<?php
$class_active["season"] = "active";
$page_title = "Превью матча";
include("header.php");

/** Отображение состава в превью матча
 * @param bool $home TRUE если отображаем хояев, FALSE если отображаем гостей
 * @return bool Выводит на экран список игроков
 */
function displayStaff($home) {
    global $best_home_player, $best_away_player, $detailedProtocol, $res_staff_home, $res_staff_away; //видимость всех переменных внутри функции

    if ($home) {
        $staff = $detailedProtocol->players_home;
        $res_staff = $res_staff_home;
        $best_player = $best_home_player;
    } else {
        $staff = $detailedProtocol->players_away;
        $res_staff = $res_staff_away;
        $best_player = $best_away_player;
    }

    for ($i = 0; $i < sizeof($staff); $i++) {
        if ($staff[$i]->number == null) {
            $staff[$i]->number = "&nbsp;";
        }
        print('<p>' . $staff[$i]->number . '</p>');
    }

    print('                                            </div>
                                                   <div class="col-md-7 single-match-data">
                                                     <p class="nm-player">Игрок</p>
');

    for ($i = 0; $i < sizeof($staff); $i++) {
        print('<p style="text-align: left; padding-left: 20px;"><a href="player.php?p=' . mysql_result($res_staff, $i, "id_applicant") . '">');

        print($staff[$i]->name_1 . ' ' . $staff[$i]->name_2 . '</a>');

        if ($staff[$i]->goalie == 1) {
            print('&nbsp;&#129508;'); //перчатки
        }

        if (mysql_result($res_staff, $i, "id_applicant") == $best_player) {
            print("&nbsp;&#11088;"); //бриллиант лучший игрок
        }

        print('</p>');
    }

    print('                                            </div>
                                                   <div class="col-md-4 single-match-data">
                                                     <p class="nm-player">Статус</p>
');

    for ($i = 0; $i < sizeof($staff); $i++) {
        print('<p>');

        if (checkPlayerBan($staff[$i]->id_player)) {
            print('<img src="/graphics/red_card.gif" height=12px title="Дисквалифицирован">');
        } elseif ($staff[$i]->played > 0) {
            print('&#9989;'); //готов
        } else {
            print('<img src="img/question.png" height=12px title="Не выходил на поле в текущем турнире">');
        }

        if (isset($_GET["z"])) {
            print($staff[$i]->season_performance);
        }

        print('</p>');
    }

    return TRUE;
}

$id_schedule = $_GET["s"];

$res_schedule = mysql_query("SELECT * FROM schedule WHERE id_schedule = $id_schedule");

$schedule = new Schedule();
$schedule->id_schedule = mysql_result($res_schedule, 0, "id_schedule");
$schedule->league = mysql_result($res_schedule, 0, "league");
$schedule->team_1 = new TeamCompetitor();
$schedule->team_1->getTeamCompetitor(mysql_result($res_schedule, 0, "team_1"));
$schedule->team_2 = new TeamCompetitor();
$schedule->team_2->getTeamCompetitor(mysql_result($res_schedule, 0, "team_2"));
$schedule->date = mysql_result($res_schedule, 0, "date");
$schedule->time = mysql_result($res_schedule, 0, "time");
$schedule->pitch = mysql_result($res_schedule, 0, "pitch");
$schedule->bet = new BetRate();
$schedule->bet->bet_1 = mysql_result($res_schedule, 0, "bet_1");
$schedule->bet->bet_2 = mysql_result($res_schedule, 0, "bet_2");
$time = substr($schedule->time, 0, 5);
$year = substr($schedule->date, 0, 4);
$month = substr($schedule->date, 5, 2);
$day = (int) substr($schedule->date, 8, 2);

$league = new Tournament();
$league->getCompetitionInfo($schedule->league);

$id_competitor = $schedule->team_1->id_competitor;

$staff_home_query = "SELECT *
FROM application AS a
WHERE a.id_competitor = $id_competitor AND a.active = 1
ORDER BY a.goalie DESC, IF(a.number IS NULL,1,0), a.number";

$id_competitor = $schedule->team_2->id_competitor;

$staff_away_query = "SELECT *
FROM application AS a
WHERE a.id_competitor = $id_competitor AND a.active = 1
ORDER BY a.goalie DESC, IF(a.number IS NULL,1,0), a.number";

$res_staff_home = mysql_query($staff_home_query);
$res_staff_away = mysql_query($staff_away_query);

//формируем объект
$detailedProtocol = new DetailedProtocol();
$detailedProtocol->protocol = $id_schedule;
$detailedProtocol->players_home = array();
$player_list = new PlayerList();
$player_list->getPlayers($schedule->team_1->id_competitor);
$detailedProtocol->players_home = $player_list->players;
for ($i = 0; $i < mysql_num_rows($res_staff_home); $i++) {
    $id_applicant = mysql_result($res_staff_home, $i, "id_applicant");
    $query = "SELECT * 
              FROM player AS p, application AS a
              WHERE a.id_applicant = $id_applicant
                  AND a.id_player = p.id_player";
    $res_player = mysql_query($query);
    $detailedProtocol->players_home[$i]->id_player = mysql_result($res_player, 0, "p.id_player");
    $detailedProtocol->players_home[$i]->name_1 = mysql_result($res_player, 0, "p.name_1");
    $detailedProtocol->players_home[$i]->name_2 = mysql_result($res_player, 0, "p.name_2");
    $detailedProtocol->players_home[$i]->season_performance = playerPerformanceInCompetition($id_applicant);
}
$players_performance = array();
$players_performance = playersPerformanceInCompetitor($schedule->team_1->id_competitor);
reset($players_performance);
$best_home_player = key($players_performance);

$detailedProtocol->players_away = array();
$player_list = new PlayerList();
$player_list->getPlayers($schedule->team_2->id_competitor);
$detailedProtocol->players_away = $player_list->players;
for ($i = 0; $i < mysql_num_rows($res_staff_away); $i++) {
    $id_applicant = mysql_result($res_staff_away, $i, "id_applicant");
    $query = "SELECT * 
              FROM player AS p, application AS a
              WHERE a.id_applicant = $id_applicant
                  AND a.id_player = p.id_player";
    $res_player = mysql_query($query);
    $detailedProtocol->players_away[$i]->id_player = mysql_result($res_player, 0, "p.id_player");
    $detailedProtocol->players_away[$i]->name_1 = mysql_result($res_player, 0, "p.name_1");
    $detailedProtocol->players_away[$i]->name_2 = mysql_result($res_player, 0, "p.name_2");
    $detailedProtocol->players_away[$i]->season_performance = playerPerformanceInCompetition($id_applicant);
}
$players_performance = array();
$players_performance = playersPerformanceInCompetitor($schedule->team_2->id_competitor);
reset($players_performance);
$best_away_player = key($players_performance);

print('
   <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
                <div class="effect-cover">
                    <h3 class="txt-advert animated">' . $schedule->team_1->title . ' - ' . $schedule->team_2->title . '</h3>
                    <p class="txt-advert-sub">' . $league->category[0]->title . '. ' . $league->title . '</p>
                </div>
           </div>
    
    <section id="single-match-pl" class="container secondary-page">
      <div class="general general-results players">   
           <div class="top-score-title right-score col-md-9">
                <div class="top-score-title player-vs">
                   <h3>' . $schedule->pitch . ', <span>' . $day . '.' . $month . '.' . $year . ' ' . $time . '</span><span class="point-little">.</span></h3>
                        <div class="main">
                            <div class="tabs standard">
                                <div class="tab-content">
                                 <div class="effect-color">
                                    <div class="tab active">
                                            <div class="col-md-12 player-score-bg"></div>
                                            <div class="col-md-12 player-photo">
                                                <img class="img-face img-mm1" src="/emblems/' . $schedule->team_1->team_details->emblem_path . '" alt="" />
                                                <img class="img-face img-mm2" src="/emblems/' . $schedule->team_2->team_details->emblem_path . '" alt="" />
                                            </div>
                                              <div class="col-md-12 play-vs">
                                                <div class="result-name">
                                                      <span>' . $schedule->team_1->title . '</span> 
                                                      <span class="txt-vs">vs</span> 
                                                      <span>' . $schedule->team_2->title . '</span>
                                                 </div>
                                              </div>
                                              <div class="clear"></div>
                                              <div class="col-md-12 play-vs">
                                                <table id="result-table">
                                                      <tr><td class="fpt">П1 x' . $schedule->bet->bet_1 . '</td><td class="fpt">П2 x' . $schedule->bet->bet_2 . '</td></tr>');

print('
                                                 </table>
                                              </div>
                                    </div>   
                                   </div>
                                                                   
                                </div>                                
                            </div>');

//-----------------------------форма команд-----------------------------------------------

$form_depth = 5;

$team_1_form = getTeamForm($schedule->team_1->id_competitor, $form_depth);
$team_2_form = getTeamForm($schedule->team_2->id_competitor, $form_depth);

$team_1_form["performance"] = $team_1_form["performance"] * 100;
$team_2_form["performance"] = $team_2_form["performance"] * 100;

print('
                <div class="col-md-12 atp-single-player skill-content">
                     <div class="exp-skill">
                            <p class="exp-title-pp">ФОРМА СОПЕРНИКОВ</p>
                            <div class="skills-pp">
                            <div class="skillbar-pp clearfix" data-percent="' . $team_1_form["performance"] . '%">
                                <div class="skillbar-title-pp"><span>' . $schedule->team_1->shortname . ' ' . round($team_1_form["performance"], 0) . '%</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">
                                    <div id="form" class="match-tbs-title">
                                          <table class="match-tbs-title">
                                            <tr style="height: 10pt; margin-top: 1px;">');

for ($s = 0; $s < strlen($team_1_form["results"]); $s++) {
    $result = substr($team_1_form["results"], $s, 1);
    switch ($result) {
        case "W":
            $tdclass = "win";
            $label = "В";
            break;
        case "D":
            $tdclass = "draw";
            $label = "Н";
            break;
        case "L":
            $tdclass = "lost";
            $label = "П";
            break;
    }

    print('<td class = "' . $tdclass . '">' . $label . '</td>');
}

print('                                 </tr >                             
                                        </table><br>
                                    </div>
                                </div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $team_2_form["performance"] . '%">
                                <div class="skillbar-title-pp"><span>' . $schedule->team_2->shortname . ' ' . round($team_2_form["performance"], 0) . '%</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">
                                    <div id="form" class="match-tbs-title">
                                          <table class="match-tbs-title">
                                            <tr style="height: 10pt; margin-top: 1px;">');

for ($s = 0; $s < strlen($team_2_form["results"]); $s++) {
    $result = substr($team_2_form["results"], $s, 1);
    switch ($result) {
        case "W":
            $tdclass = "win";
            $label = "В";
            break;
        case "D":
            $tdclass = "draw";
            $label = "Н";
            break;
        case "L":
            $tdclass = "lost";
            $label = "П";
            break;
    }

    print('<td class = "' . $tdclass . '">' . $label . '</td>');
}

print('                                     </tr >                             
                                            </table>
                                    </div>
                                    </div>
                                </div>
                            </div>                            
                    </div>
                </div>');

//-----------------------------статистика встреч между собой-------------------------------

$contest_id_team1 = $schedule->team_1->team_details->id_team;
$contest_id_team2 = $schedule->team_2->team_details->id_team;

//----------победы1
$query = "SELECT * FROM protocol
   WHERE (team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)
     AND score_end_1 > score_end_2)
    OR (team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)
     AND score_end_1 < score_end_2)
     AND forfeited is null";
$res_won = mysql_query($query);
if ($res_won) {
    $won = mysql_num_rows($res_won);
} else {
    $won = 0;
}

//----------ничьи
$query = "SELECT * FROM protocol
   WHERE ((team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
      AND team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2))
     OR (team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
      AND team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)))
     AND score_end_1 = score_end_2
     AND forfeited is null";
$res_drew = mysql_query($query);
if ($res_drew) {
    $drew = mysql_num_rows($res_drew);
} else {
    $drew = 0;
}

//----------победы2
$query = "SELECT * FROM protocol
   WHERE (team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)
     AND team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND score_end_1 > score_end_2)
    OR (team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)
     AND team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND score_end_1 < score_end_2)
     AND forfeited is null";
$res_lost = mysql_query($query);
if ($res_lost) {
    $lost = mysql_num_rows($res_lost);
} else {
    $lost = 0;
}

//----------технические поражения
$query = "SELECT * FROM protocol
   WHERE ((team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = id_team = $contest_id_team2)
     AND team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = id_team = $contest_id_team1))
    OR (team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = id_team = $contest_id_team2)
     AND team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = id_team = $contest_id_team1)))
     AND forfeited = 1";
$res_forfeited = mysql_query($query);
if ($res_forfeited) {
    $forfeited = mysql_num_rows($res_forfeited);
} else {
    $forfeited = 0;
}

//----------забито1
$res_for_home = mysql_query("SELECT CAST(SUM(score_end_1) AS UNSIGNED) AS sc_home FROM protocol WHERE team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)");
$res_for_away = mysql_query("SELECT CAST(SUM(score_end_2) AS UNSIGNED) AS sc_away FROM protocol WHERE team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)");
$goalsfor = (int) mysql_result($res_for_home, 0, "sc_home") + (int) mysql_result($res_for_away, 0, "sc_away");

//----------забито2
$res_against_home = mysql_query("SELECT CAST(SUM(score_end_2) AS UNSIGNED) as ms_home FROM protocol WHERE team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)");
$res_against_away = mysql_query("SELECT CAST(SUM(score_end_1) AS UNSIGNED) as ms_away FROM protocol WHERE team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
     AND team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)");
$goalsagainst = (int) mysql_result($res_against_home, 0, "ms_home") + (int) mysql_result($res_against_away, 0, "ms_away");

$contest_played = $won + $drew + $lost;
$contest_won_percentage = round($won / ($won + $lost) * 100);
$contest_drew_percentage = round($drew / $contest_played * 100);
$contest_goal_percentage = round($goalsfor / ($goalsfor + $goalsagainst) * 100);

print('
                <div class="col-md-12 atp-single-player skill-content">
                     <div class="exp-skill">
                            <p class="exp-title-pp">ИСТОРИЯ ВСТРЕЧ</p>
                            <div class="skills-pp">
                            <div class="skillbar-pp clearfix" data-percent="' . $contest_won_percentage . '%">
                                <div class="skillbar-title-pp"><span>Победы. ' . $schedule->team_1->shortname . ': ' . $won . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $schedule->team_2->shortname . ': ' . $lost . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $contest_drew_percentage . '%">
                                <div class="skillbar-title-pp"><span>Ничьих: ' . $drew . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $contest_drew_percentage . '%</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $contest_goal_percentage . '%">
                                <div class="skillbar-title-pp"><span>Мячей. ' . $schedule->team_1->shortname . ': ' . $goalsfor . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $schedule->team_2->shortname . ': ' . $goalsagainst . '</div>
                            </div>
                        </div>
                    </div>
                    <div>');

//TODO последний раз Х выигрывал...

$query = "SELECT * FROM protocol, competition, season
WHERE ((protocol.team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)
  AND protocol.team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2))
 OR (protocol.team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team2)
  AND protocol.team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $contest_id_team1)))
 AND protocol.league = competition.id_competition
 AND competition.season = season.id_season
 ORDER BY season.id_season DESC";
$res_protocols = mysql_query($query);
$num_protocols = mysql_num_rows($res_protocols);

print('                         <div id="results" class="tab active">
                                  <table class="match-tbs">
                                    <tr><td class="match-tbs-title" colspan="6">Все матчи команд</td></tr>');

for ($i = 0; $i < $num_protocols; $i++) {
    $protocol = new Protocol();
    $protocol->getProtocolData($res_protocols, $i);
    $competition = new Tournament();
    $competition->getCompetitionInfo($protocol->league);

    if ($protocol->team_1_emblem == "") {
        $protocol->team_1_emblem = "emblems/emblem_default.PNG";
    }

    if ($protocol->team_2_emblem == "") {
        $protocol->team_2_emblem = "emblems/emblem_default.PNG";
    }

    $protocol_date = (int) substr($protocol->dt_protocol, 8, 2) . " " . iconv("windows-1251", "utf-8", substr(ruMonth(substr($protocol->dt_protocol, 5, 2)), 0, 3)) . " '" . substr($protocol->dt_protocol, 2, 2);

    $href = "window.location.href='match.php?p=" . $protocol->id_protocol . "'";

    $result = '<tr onclick="' . $href . '; return false"><td class="team1">' . cutString(iconv('utf-8', 'windows-1251', $protocol->team_1), 30) . '&nbsp;<img src="' . $protocol->team_1_emblem . '" style="max-height:25px; max-width:25px">&nbsp;</a></td><td class="score"><strong>' . $protocol->score_end_1 . '</strong></td><td class="score"><strong>' . $protocol->score_end_2 . '</strong></td><td  class="team2">&nbsp;<img src="' . $protocol->team_2_emblem . '"  style="max-height:25px; max-width:25px">&nbsp;' . cutString(iconv('utf-8', 'windows-1251', $protocol->team_2), 30) . '</a></td><td class="only-desktop" style="width: 15%;"><i class="fa fa-calendar"></i>&nbsp;' . $protocol_date . '</td><td class="only-desktop" style="width: 30%;"><i class="fa fa-map-marker"></i> ' . cutString(iconv('utf-8', 'windows-1251', $protocol->league), 50) . '</td></tr>';

    print($result);
}
print("</table>
</div>");

print('
                    </div>
             </div>
');

//СОСТАВЫ

print('
                            <div class="tabs standard single-pl">
                                <div class="tab-content single-match">                                
                                    <div id="tab1" class="tab active">
                                       <h3 class="tab-match-title">ПРЕДПОЛАГАЕМЫЕ СОСТАВЫ</h3>
                                        <div class="tabs standard">
                                            <ul class="tab-links-match">
                                                <li class="active"><a class="first-tabs" href="#tab1a">' . $schedule->team_1->title . '</a></li>
                                                <li><a href="#tab2a">' . $schedule->team_2->title . '</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab1a" class="tab active">
                                                   <div class="col-md-1 single-match-data">
                                                      <p class="nm-player">#</p>');

displayStaff(TRUE); // отображаем состав хозяев

print('
                                                </div>
                                                </div>
                                                <div id="tab2a" class="tab">
                                                   <div class="col-md-1 single-match-data">
                                                      <p class="nm-player">#</p>');

displayStaff(FALSE); // отображаем состав гостей

print('
                                                  </div>
                                                </div>
                                            </div>
                                 
                                        </div>     
                                    </div>
                                </div>
                                 
                            </div>');

print('
                            </div>
                   </div>
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>
<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>   

<!--PERCENTAGE-->
<script>
    $(function () {
        "use strict";
        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").width(0);
        });

        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").animate({
                width: $(this).attr("data-percent")
            }, 2000);
        });
    });
</script>
');
