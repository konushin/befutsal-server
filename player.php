﻿<?php

$class_active["season"] = "active";
include("header.php");

$id_applicant = $_GET["p"];

$player = new PlayerApplication();
$player->getPlayerApplication($id_applicant);

$id_competition = $player->team_details->competition_details->competition_id;

$res_last_date = mysql_query("SELECT DATE_FORMAT(MAX(dt_protocol), '%Y-%m-%e') AS last FROM protocol, competition WHERE protocol.league = $id_competition");
$last_date = mysql_result($res_last_date, 0, "last");
if ($last_date == null) {
    $last_date = $now;
}
$plr_age = $last_date - $player->player_personal->birthdate;

if ($player->player_personal->birthdate > '1900-01-01') {
    $s_year = mb_substr($player->player_personal->birthdate, 0, 4);
    $s_month = iconv('windows-1251', 'utf-8', ruMonth(mb_substr($player->player_personal->birthdate, 5, 2)));
    $s_day = mb_substr($player->player_personal->birthdate, 8, 2);
    $birthdate = $s_day . " " . $s_month . " " . $s_year;
} else {
    $birthdate = "Не указана.";
}

if ($player->player_personal->photo_path == null) {
    $player->player_personal->photo_path = getParameter("default_photo");
}

if ($player->player_personal->goalie == 1) {
    $position = "Вратарь";
} else {
    $position = "Полевой игрок";
}

if ($player->number == null) {
    $player->number = "?";
}

//смотрим, переходил ли игрок в другие команды этой же лиги в сезоне
$query = "SELECT * FROM application, competitor
 WHERE application.id_player = " . $player->player_personal->id_player . "
  AND application.id_competitor = competitor.id_competitor
  AND competitor.id_competition = " . $player->team_details->competition_details->competition_id;
$res_transfers = mysql_query($query);
$num_transfers = mysql_num_rows($res_transfers);

$played = 0;
$scored = 0;
$booked = 0;
$sentoff = 0;
$missed = 0;
$cleansheets = 0;

for ($i = 0; $i < $num_transfers; $i++) {
    $id_prev_applicant = mysql_result($res_transfers, $i, "id_applicant");
    $res_prev_staff = mysql_query("SELECT COUNT(*) AS played, SUM(scored) AS scored, SUM(yellow) AS booked, SUM(red) AS sentoff FROM staff WHERE id_player = $id_prev_applicant");
    $played = $played + (int)mysql_result($res_prev_staff, 0, "played");
    $scored = $scored + (int)mysql_result($res_prev_staff, 0, "scored");
    $booked = $booked + (int)mysql_result($res_prev_staff, 0, "booked");
    $sentoff = $sentoff + (int)mysql_result($res_prev_staff, 0, "sentoff");
    $id_competitor = $player->team_details->id_competitor;

    $res_scheduled = mysql_query("SELECT COUNT(*) AS scheduled FROM schedule WHERE team_1 = $id_competitor OR team_2 = $id_competitor");
    $scheduled = mysql_result($res_scheduled, 0, "scheduled");

    $query = "SELECT * FROM staff WHERE id_player = $id_applicant";
    $res_staff = mysql_query($query);
    for ($m = 0; $m < mysql_num_rows($res_staff); $m++) {
        $id_protocol = mysql_result($res_staff, $m, "id_protocol");
        $res_missed1 = mysql_query("SELECT SUM(score_end_2) AS missed FROM protocol WHERE id_protocol = $id_protocol AND team_1 = $id_competitor");
        $res_missed2 = mysql_query("SELECT SUM(score_end_1) AS missed FROM protocol WHERE id_protocol = $id_protocol AND team_2 = $id_competitor");
        $missed += -((int)mysql_result($res_missed1, 0, "missed") + (int)mysql_result($res_missed2, 0, "missed"));
        if ((int)mysql_result($res_missed1, 0, "missed") + (int)mysql_result($res_missed2, 0, "missed") == 0) {
            $cleansheets++;
        }
    }

    $played_percentage = round(($played / $scheduled) * 100);
    $scored_percentage = min(100, round(($scored / $scheduled) * 100));
    $booked_percentage = round(($booked / $scheduled) * 100);
    $sentoff_percentage = round(($sentoff / $scheduled) * 100);
    $missed_percentage = round(($missed / $scheduled) * 100);
    $cleansheets_percentage = round(($cleansheets / $scheduled) * 100);
}

$id_player = $player->player_personal->id_player;
$query = "SELECT DISTINCT competitor.id_team FROM application, competitor, team WHERE application.id_competitor = competitor.id_competitor AND application.id_applicant IN (SELECT id_applicant FROM application WHERE id_player = $id_player) ORDER BY competitor.id_competitor ASC";
$res_history = mysql_query($query);

for ($i = 0; $i < mysql_num_rows($res_history); $i++) {
    $id_team_history[$i] = mysql_result($res_history, $i, "id_team");
    $query = "SELECT * FROM team WHERE id_team = $id_team_history[$i]";
    $team_title_history[$i] = mysql_result(mysql_query($query), 0, "title");
}

print('<section class="drawer">
    <div class="col-md-12 size-img back-img-match">
        <div class="effect-cover">
                <h3 class="txt-advert animated">' . $player->team_details->title . '</h3>
                <p class="txt-advert-sub">' . $player->team_details->competition_details->title . ' (' . $player->team_details->competition_details->season_details->title . ')</p>
            </div>
    </div>
    
    <section id="single_player" class="container secondary-page">
      <div class="general general-results players">
           <div class="top-score-title right-score col-md-9">
                <h3><span>' . $player->player_personal->name_1 . '</span> ' . $player->player_personal->name_2 . '<span class="point-little">.</span></h3>
                <div class="col-md-12 atp-single-player">
                  <img class="img-djoko" src="/photos/' . $player->player_personal->photo_path . '" alt="" />
                  <div class="col-md-2 data-player">
                     <p>Родился:</p>
                     <p>Возраст:</p>
                     <p>Позиция:</p>
                  </div>
                  <div class="col-md-3 data-player-2">
                     <p>' . $birthdate . '</p>
                     <p>' . $plr_age . '</p>
                     <p>' . $position . '</p>
                  </div>
                  <div class="col-md-2 rank-player">
                     <div class="content-rank"><p class="rank-data">' . $player->number . '</p></div>
                     <p class="rank-title">Игровой номер</p>
                  </div>
                </div>
                <div class="col-md-12 atp-single-player skill-content">
                     <div class="exp-skill">
                            <p class="exp-title-pp">Статистика сезона</p>
                            <div class="skills-pp">
                            <div class="skillbar-pp clearfix" data-percent="' . $played_percentage . '%">
                                <div class="skillbar-title-pp"><span>Игр </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $played . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $scored_percentage . '%">
                                <div class="skillbar-title-pp"><span>Забито  </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $scored . '</div>
                            </div>');

if ($player->goalie == 1) {
    print('                 <div class="skillbar-pp clearfix" data-percent="' . $missed_percentage . '%">
                                <div class="skillbar-title-pp"><span>Пропущено  </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $missed . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $cleansheets_percentage . '%">
                                <div class="skillbar-title-pp"><span>Матчей на ноль  </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $cleansheets . '</div>
                            </div>
    ');
}

print('
                            <div class="skillbar-pp clearfix" data-percent="' . $booked_percentage . '%">
                                <div class="skillbar-title-pp"><span>Предупреждений</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $booked . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $sentoff_percentage . '%">
                                <div class="skillbar-title-pp"><span>Удалений</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $sentoff . '</div>
                            </div>
                        </div>
                    </div>
             </div>
               <div class="col-md-12 atp-single-player skill-content">
                      <div class="ppl-desc">
                        <p class="exp-title-pp">КАРЬЕРА</p>');

$played_total = 0;
$scored_total = 0;
$booked_total = 0;
$sentoff_total = 0;
$missed_total = 0;
$cleansheets_total = 0;

for ($i = 0; $i < sizeof($team_title_history); $i++) {
    $team = new TeamInfo();
    $team->getTeamInfo($id_team_history[$i]);

    print('
            <!-- одна команда начало div -->
            <div>
                        <p><span><img src="/emblems/' . $team->emblem_path . '" height="25px" style="max-width: 30px"></span>&nbsp;' . $team_title_history[$i] . '</p>');

    $query = "SELECT * FROM application, competitor, competition, season WHERE competitor.id_competition = competition.id_competition AND competition.season = season.id_season AND application.id_player = $id_player AND application.id_competitor = competitor.id_competitor AND competitor.id_team = $id_team_history[$i] ORDER BY season.id_season";
    $res_seasons = mysql_query($query);

    print('
                    <div class="col-md-12 home-page">
                        <table class="tab-career">
                           <tr class="top-career-table"><td class="career-position">СЕЗОН</td><td class="only-desktop">ТУРНИР</td><td class="tab-career-numbers">&#128085;</td><td class="tab-career-numbers">&#9917;</td><td class="tab-career-numbers"><img src="/graphics/yellow_card.gif" style="height: 12px; width: 7px"></td><td class="tab-career-numbers"><img src="/graphics/red_card.gif" style="height: 12px; width: 7px"></td></tr>');

    $played_in_team = 0;
    $scored_in_team = 0;
    $booked_in_team = 0;
    $sentoff_in_team = 0;
    $missed_in_team = 0;
    $cleansheets_in_team = 0;

    for ($n = 0; $n < mysql_num_rows($res_seasons); $n++) {
        $season_id = mysql_result($res_seasons, $n, "season.id_season");
        $season_title = mysql_result($res_seasons, $n, "season.title");
        $competition_id = mysql_result($res_seasons, $n, "competition.id_competition");
        $competition_title = mysql_result($res_seasons, $n, "competition.title");
        $competitor_id = mysql_result($res_seasons, $n, "competitor.id_competitor");
        $applicant_id = mysql_result($res_seasons, $n, "application.id_applicant");
        $query = "SELECT COUNT(staff.id_player) AS played, SUM(scored) AS scored, SUM(yellow) AS booked, SUM(red) AS sentoff FROM staff WHERE id_player = $applicant_id";
        $res_performance = mysql_query($query);
        $played = (int)mysql_result($res_performance, 0, "played");
        $scored = (int)mysql_result($res_performance, 0, "scored");
        $booked = (int)mysql_result($res_performance, 0, "booked");
        $sentoff = (int)mysql_result($res_performance, 0, "sentoff");

        $missed = 0;
        $cleansheets = 0;

        $query = "SELECT * FROM staff WHERE id_player = $applicant_id";
        $res_staff = mysql_query($query);
        for ($m = 0; $m < mysql_num_rows($res_staff); $m++) {
            $id_protocol = mysql_result($res_staff, $m, "id_protocol");
            $res_missed1 = mysql_query("SELECT SUM(score_end_2) AS missed FROM protocol WHERE id_protocol = $id_protocol AND team_1 = $competitor_id");
            $res_missed2 = mysql_query("SELECT SUM(score_end_1) AS missed FROM protocol WHERE id_protocol = $id_protocol AND team_2 = $competitor_id");
            $missed += -((int)mysql_result($res_missed1, 0, "missed") + (int)mysql_result($res_missed2, 0, "missed"));
            if ((int)mysql_result($res_missed1, 0, "missed") + (int)mysql_result($res_missed2, 0, "missed") == 0) {
                $cleansheets++;
            }
        }

        $played_in_team += $played;
        $scored_in_team += $scored;
        $booked_in_team += $booked;
        $sentoff_in_team += $sentoff;
        $missed_in_team += $missed;
        $cleansheets_in_team += $cleansheets;

        print('
                           <tr><td class="career-position">' . $season_title . '</td><td class="only-desktop">' . $competition_title . '</td><td  class="tab-career-numbers">' . $played . '</td><td class="tab-career-numbers">' . $scored . '</td><td  class="tab-career-numbers">' . $booked . '</td><td  class="tab-career-numbers">' . $sentoff . '</td></tr>');
    }

    if (mysql_num_rows($res_seasons) > 1 ){
        print('
                           <tr><td class="career-position">ВСЕГО</td><td class="only-desktop">&nbsp;</td><td  class="tab-career-numbers">' . $played_in_team . '</td><td class="tab-career-numbers">' . $scored_in_team . '</td><td  class="tab-career-numbers">' . $booked_in_team . '</td><td  class="tab-career-numbers">' . $sentoff_in_team . '</td></tr>');
    }

    $played_total += $played_in_team;
    $scored_total += $scored_in_team;
    $booked_total += $booked_in_team;
    $sentoff_total += $sentoff_in_team;
    $missed_total += $missed_in_team;
    $cleansheets_total += $cleansheets_in_team;

    print('
                        </table>
             <br> </div> <!-- col-md-12 home-page -->
          </div> <!-- одна команда конец div -->
');
}

print('
                                  </div> <!-- ppl-desc -->                                  
                    </div> <!-- col-md-12 atp-single-player skill-content -->');

$scored_total_percentage = min(100, round(($scored_total / $played_total) * 100));
$booked_total_percentage = round(($booked_total / $played_total) * 100);
$sentoff_total_percentage = round(($sentoff_total / $played_total) * 100);
$missed_total_percentage = min(100, round((-$missed_total / $played_total) * 100));
$cleansheets_total_percentage = round(($cleansheets_total / $played_total) * 100);
$played_total_percentage = max(100, $missed_total_percentage, $scored_total_percentage);

print('
                <div class="col-md-12 atp-single-player skill-content">
                     <div class="exp-skill">
                            <p class="exp-title-pp">Статистика карьеры</p>
                            <div class="skills-pp">
                            <div class="skillbar-pp clearfix" data-percent="' . $played_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Игр </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $played_total . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $scored_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Забито  </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $scored_total . '</div>
                            </div>');

if ($player->goalie == 1) {
    print('                 <div class="skillbar-pp clearfix" data-percent="' . $missed_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Пропущено  </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $missed_total . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $cleansheets_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Матчей на ноль  </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $cleansheets_total . '</div>
                            </div>
    ');
}

print('
                            <div class="skillbar-pp clearfix" data-percent="' . $booked_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Предупреждений</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $booked_total . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $sentoff_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Удалений</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $sentoff_total . '</div>
                            </div>
                        </div>
                    </div>');
              
print('</div>
           </div><!--Close Top Match-->

');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>

<!--PERCENTAGE-->
<script>
    $(function () {
        "use strict";
        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").width(0);
        });

        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").animate({
                width: $(this).attr("data-percent")
            }, 2000);
        });
    });
</script>
');

?>