﻿<?php

$page_title = "Вход в личный кабинет";
$description = "Регистрация или вход в личный кабинет befutsal";
include("header.php");

print('
    <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
             <div class="effect-cover">
                    <h3 class="txt-advert animated">Живи мини-футболом!</h3>
                    <p class="txt-advert-sub animated">Ваши персональные интересы и предпочтения</p>
                 </div>
             </div>
      
    <section id="login" class="container secondary-page">  
      <div class="general general-results players">
           <div class="top-score-title right-score col-md-6">
                <h3>Добро<span class="point-int"> пожаловать !</span></h3>
                <div class="col-md-12 login-page login-w-page">
                   <p class="logiin-w-title">Регистрация бесплатна</p>
                   <p>Мы не рассылаем новости и спам. Регистрация нужна лишь для того, чтобы мы точно знали, что на сайт вошли именно вы, а не кто-то другой. <a href="privacy_policy_ru.php">Ознакомиться с политикой конфиденциальности.</a></p>
                   <h3><img class="ball-tennis" src="graphics/bola.png" alt=""/>Ваши предпочтения</h3>
                   <p>Вы сможете выбирать любимые команды и игроков, а сайт и мобильное приложение будут подстраиваться под вас.</p>
                   <h3><img class="ball-tennis" src="graphics/bola.png" alt=""/>Единый логин</h3>
                   <p>Вы можете входить по единому логину как на сайт, так и в мобильное приложение. Если вы уже зарегистрированы в мобильном приложении BEFUTSAL, то просто войдите!</p>
                </div>
           </div><!--Close welcome-->
           <!-- LOGIN BOX -->
           <div class="top-score-title right-score col-md-6">
               <h3>Вход<span> для пользователей</span><span class="point-int"></span></h3>
                <div class="col-md-12 login-page">   
                    <form method="post" class="login-form" action="authenticate.php?z">
                        <div class="email">
                            <label for="email">* Логин/E-mail:</label><div class="clear"></div>
                            <input id="email" name="email" type="text" placeholder="example@domain.com" required=""/>
                        </div>
                        <div class="password">
                            <label for="pwd">* Пароль:</label><div class="clear"></div>
                            <input id="password" name="password" type="password" placeholder="********" required=""/>
                        </div>
                        <div id="login-submit">
                            <button>Войти</button> 
                        </div>');

if (isset($_GET["login_err"])) {
    print('
                        <div class="label">
                            <p class="result" style="color: #000000">' . $_GET["login_err"] . '</p>
                        </div>');
}

print('
                    </form>
                </div>
                
           </div><!--Close Login-->
           <!-- REGISTER BOX -->

           <div class="top-score-title right-score col-md-12">
            <h3 id="register">Зарегистрируйтесь <span>сейчас</span><span class="point-int"> !</span></h3>
                <div class="col-md-12 login-page">       
                    <form id="register_form" method="post" class="register-form" action="register.php">         
                        <div class="email">
                            <label for="email">* E-mail:</label><div class="clear"></div>
                            <input id="email" name="email" type="text" placeholder="example@domain.com" required=""/>
                        </div>
                        <div class="name">
                            <label for="name">* Имя:</label><div class="clear"></div>
                            <input id="name" name="name" type="text" minlength="3" maxlength="16" placeholder="Серхио Родригес" title="От 3 до 16 символов" required=""/>
                        </div>
                        <div class="name">
                            <label for="password">* Пароль:</label><div class="clear"></div>
                            <input id="password" name="password" type="password" minlength="4" maxlength="16" placeholder="********" required=""/>
                        </div>
                        <div id="register-submit">
                            <button>Отправить</button> 
                        </div>');

if (isset($_GET["reg_err"])) {
    print('
                        <div class="label">
                            <p class="result" style="color: #000000">' . $_GET["reg_err"] . '</p>
                        </div>');
}

print('
                        
                       </form>
                       <div class="ctn-img">
                            <img src="images/team_bgr.jpg" />
                       </div><!--Close Images-->
                       <div class="clear"></div>
                </div>
                
           </div><!--Close REgistration-->
          </div> 
        </section>');

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');

?>