<?php
/**
 * Created by PhpStorm.
 * User: Konushin
 * Date: 23.11.2021
 * Time: 18:34
 */

if ($num_bans > 0) {
    print('
                                <div id="bans" class="tab active">
                                  <table class="match-tbs">
                                    <tr><td class="match-tbs-title" colspan="5">Дисквалифицированные игроки</td></tr>
                                    <tr class="match-sets"><td>ИГРОК</td><td>КОМАНДА</td><td>Матчей осталось</td></tr>');

    for ($i = 0; $i < $num_bans; $i++) {
        $id_applicant = mysql_result($res_bans, $i, "id_applicant");
        $name_1 = mysql_result($res_bans, $i, "name_1");
        $name_2 = mysql_result($res_bans, $i, "name_2");
        $player_photo = mysql_result($res_bans, $i, "photo_path");
        if ($player_photo == "") {
            $player_photo = "photo_default.PNG";
        }
        $id_team = mysql_result($res_bans, $i, "id_team");
        $team_title = mysql_result($res_bans, $i, "teamtitle");
        $ban_end = mysql_result($res_bans, $i, "ban_end");
        if ($ban_end == '0000-00-00' or $ban_end == null) {
            $ban_matches_initial = mysql_result($res_bans, $i, "ban_matches_initial");
            $ban_matches_left = mysql_result($res_bans, $i, "ban_matches_left");
            if ($ban_matches_initial != $ban_matches_left) {
                $label = $ban_matches_left . " из " . $ban_matches_initial;
            } else {
                $label = $ban_matches_initial;
            }
        } elseif ($ban_end > getCDCDate()) {
            $label = "до КДК";
        } else {
            $label = "до " . date("d.m.Y", strtotime($ban_end));
        }

        print('
                                  <tr><td style="text-align: left; padding-left: 2px;"><a href="player.php?p=' . $id_applicant . '"><img src="/photos/' . $player_photo . '" height="25px"> ' . $name_1 . ' ' . $name_2 . '</a></td><td style="text-align: left; padding-left: 2px;"><a href="competitor.php?c=' . $id_team . '">' . $team_title . '</a></td><td>' . $label . '</td></tr>');
    }
}

print('

                                    </table>
                                </div>
                            </div>
');