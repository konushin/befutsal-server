﻿<?php

$class_active["bans"] = "active";
$page_title = "Дисквалификации";
$description = "Дисциплинарные наказания, решения КДК, сроки дисквалификаций";
include("header.php");

$now = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$ban_upd_next_execute = date("Y-m-d", strtotime(getParameter('ban_upd_next_execute')));

if ($ban_upd_next_execute <= $now) {
    include("jobs/ban_update.php");
}

print('
   <section class="drawer">
    <div class="col-md-12 size-img back-img-match">
        <div class="effect-cover">
            <h3 class="txt-advert animated">Дисквалификации</h3>
            <p class="txt-advert-sub">Решения КДК</p>
        </div>
    </div>
    
    <section id="summary" class="container secondary-page">
      <div class="general general-results">
           <div id="rrResult" class="top-score-title right-score total-reslts col-md-9">
                <h3>Текущие <span>дисквалификации</span><span class="point-little">.</span></h3>
                <div class="main">
                        <div class="tabs animated-slide-2">

                            <div class="tab-content">
                                <div id="results" class="tab active">
                                <table class="tab-score">
                                  <tr class="top-scrore-table"><td class="score-position">ИГРОК</td><td>СРОК</td><td class="only-desktop">РЕШЕНИЕ</td></tr>');

$res_bans = getBans($now, 0);
$num_bans = mysql_num_rows($res_bans);

for ($i = 0; $i < $num_bans; $i++) {
    $id_applicant = mysql_result($res_bans, $i, "id_applicant");
    $name = mysql_result($res_bans, $i, "playername");
    $id_team = mysql_result($res_bans, $i, "id_team");
    $team_title = cutString(mysql_result($res_bans, $i, "teamtitle"), 40);
    $id_league = mysql_result($res_bans, $i, "id_league");
    $tournament = new Tournament();
    $tournament->getCompetitionInfo($id_league);
    $league_title = cutString($tournament->title, 40);
    $ban_title = mysql_result($res_bans, $i, "ban_title");
    $ban_left = mysql_result($res_bans, $i, "ban_matches_left");
    $ban_start = mysql_result($res_bans, $i, "ban_start");
    $ban_end = mysql_result($res_bans, $i, "ban_end");
    if ($ban_end == '0000-00-00' OR $ban_end == null) {
        if ($ban_left == 1) {
            $add_label = " матч";
        } elseif ($ban_left > 1 AND $ban_left <= 4) {
            $add_label = " матча";
        } elseif ($ban_left > 4) {
            $add_label = " матчей";
        } else {
            $add_label = "---";
        }
        $ban_period = $ban_left . $add_label;
    } elseif ($ban_end > getCDCDate()) {
        $ban_period = "до КДК";
    } else {
        $ban_period = date("d.m.Y", strtotime($ban_end));
    }

    print('
                                  <tr><td><a href="player.php?p=' . $id_applicant . '">' . $name . '</a> <span class="newrecord"><a href="competitor.php?c=' . $id_team . '">' . $team_title . '</a>, <a href="' . $tournament->link . '">' . $league_title . '</a></span></td><td>' . $ban_period . '</td><td class="only-desktop" style="font-weight: 400;">' . $ban_title . '</td></tr>');
}

print('
                                 </table>
                                </div>
                            </div>
                            <div class="score-view-all"></div>
                        </div>
                    </div>
           </div><!--Close Top Match-->
');


include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>
<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>

 <script src="js/custom.js" type="text/javascript"></script>   
');

?>