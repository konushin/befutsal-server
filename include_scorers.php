<?php

print('
                                <div id="topscorers" class="tab active">
                                   <table class="match-tbs">
                                         <tr><td class="match-tbs-title" colspan="5">Лучшие бомбардиры</td></tr>
                                         <tr class="match-sets"><td>#</td><td></td><td></td><td class="fpt">&#9917;</td><td class="fpt">&#128085;</td></tr>
');

$minimum = min($n_scorers, 10);
for ($i = 0; $i < $minimum; $i++) {
    $place = $i + 1;
    $player = new PlayerApplication();
    $id_applicant = mysql_result($res_scorers, $i, "id_applicant");
    $player->getPlayerApplication($id_applicant);
    if ($player->player_personal->photo_path == "") {
        $player->player_personal->photo_path = "photo_default.PNG";
    }
    $played = mysql_result($res_scorers, $i, "played");
    $scored = mysql_result($res_scorers, $i, "scored");

    print('
                                        <tr><td class="fpt">' . $place . '</td><td><a href="player.php?p=' . $id_applicant . '"><img src="/photos/' . $player->player_personal->photo_path . '" height="25px"></a></td><td class="fpt" style="text-align: left;"><a href="player.php?p=' . $id_applicant . '">' . $player->player_personal->name_1 . ' ' . $player->player_personal->name_2 . ' (' . $player->team_details->shortname . ')</a></td><td class="fpt">' . $scored . '</td><td class="fpt">' . $played . '</td></tr>
');
}

print("                                 </table>");