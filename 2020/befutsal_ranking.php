<?php

header('HTTP/1.1 200 OK');
header('Location: https://befutsal.ru/new/ranking.php');
exit();

$root = true;
$page_title = "������ ����";
$description = "���� ����-��������! ������ ����";
include("includings/header.php");

$rank_date = getParameter("befutsal_ranking_date");

$query = "SELECT DISTINCT r.date AS prev_date FROM ranking AS r ORDER BY prev_date DESC";
$res_dates = mysql_query($query, $db_connection);
$prev_date = mysql_result($res_dates, 1, "prev_date");

$s_year = substr($rank_date, 0, 4);
$n_month = substr($rank_date, 5, 2);
$s_day = (int)substr($rank_date, 8, 2);

print('
<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0">
 <tr>');

print('
  <td height="6" background="' . $add_string . 'graphics/h_bg_1.gif" bgcolor="#28C638"><img src="' . $add_string . 'graphics/0.gif" width="1" height="6" border="0" title=""></td>
 </tr>
 <tr>
  <td height="1" bgcolor="#FFFFFF"><img src="' . $add_string . 'graphics/0.gif" width="1" height="1" border="0" title=""></td>
 </tr>');

print('
 <tr>
  <td height="30" bgcolor="ffffff" style="background-image:url(../graphics/h_bg_2.gif);background-repeat:repeat-x;background-position:top"><img src="../graphics/0.gif" width="1" height="30" border="0" title=""></td>
 </tr>
</table>

<!----------------------- End of Header ---------------------------->

<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff">
 <tr>');

include("includings/main menu.php");

print('
<!------------------------ Central Part.Right Part ---------------------------->

  <td valign="top" style="padding:7px 20px 10px 20px">
   <div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#5BC1D7">
   <tr>
    <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_left.gif" width="14" height="35" border="0" title=""></td>
    <td valign="top" background="../graphics/cp_blue_bg.gif" bgcolor="#5BC1D7" style="padding-left:20px;padding-top:4px;" class="cp_header">' . $description . ' �� ' . $s_day . ' ' . ruMonth($n_month) . ' ' . $s_year . '</td>
    <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_right.gif" width="14" height="35" border="0" title=""></td>
   </tr>
  </table>
  </div>
  <div align="center" style="padding-top:10px; padding-bottom: 10px">� ������� ������� ����� � ���������� ��������.</div>
');

//----------------------�������� �������------------------------

$query = "SELECT r.id_team, t.title, r.rate, r.id_league, r.league_title, r.rank 
FROM team AS t, ranking AS r 
WHERE r.id_team = t.id_team AND r.date = '$rank_date' AND r.rank IS NOT NULL 
ORDER BY r.rank ASC";
$res_ranking = mysql_query($query, $db_connection);
$num_teams = mysql_num_rows($res_ranking);

print('
  <div align="center">
     <table cellpadding="0" cellspacing="0" border="0">
      <tr>
       <td width="10"><img src="../graphics/green_tab2_left.gif" width="10" height="29" border="0" title=""></td>
     <td background="../graphics/green_tab2_bg.gif" bgcolor="#0FC020" valign="top">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
       <tr>
        <td align="center" class="w_text_b" width="60">#</td>
        <td align="center" class="w_text_b" width="180">�������</td>
        <td align="center" class="w_text_b" width="180">��������� �  ������� ������</td>
        <td align="center" class="w_text_b" width="60">������</td>
        <td align="center" class="w_text_b" width="100">������������</td>
       </tr>
      </table>
     </td>
     <td width="10"><img src="../graphics/green_tab2_right.gif" width="10" height="29" border="0" title=""></td>
      </tr>');

$color = FALSE;

for ($i = 0; $i < $num_teams; $i++) {
    $color = !$color;
    if ($color) {
        $img = "grey_tab2";
        $bgcolor = "#F7F7F7";
    } else {
        $img = "green_tab";
        $bgcolor = "#E5FEE7";
    }

    $rank = mysql_result($res_ranking, $i, "rank");
    $id_team = mysql_result($res_ranking, $i, "id_team");
    $team_details = new TeamInfo();
    $team_details->getTeamInfo($id_team);
    $query = "SELECT rank FROM ranking WHERE id_team = $id_team AND `date` = '$prev_date'";
    $res_prev_rank = mysql_query($query, $db_connection);
    if (mysql_num_rows($res_prev_rank) > 0) {
        $prev_rank = mysql_result($res_prev_rank, 0, "rank");
    } else {
        $prev_rank = "-";
    }
    $team_title = mysql_result($res_ranking, $i, "title");
    $rate = mysql_result($res_ranking, $i, "rate");
    $id_league = mysql_result($res_ranking, $i, "id_league");
    $league_title = mysql_result($res_ranking, $i, "league_title");
    if ($id_league != NULL) {
        $league_print = '<a href="statistics/league_presentation.php?league=' . $id_league . '" class="b_link3">' . $league_title . '</a>';
    } else {
        $league_print = "�� ��������";
    }

    print('
      <tr>
       <td colspan="3"><img src="../graphics/0.gif" width="1" height="1" border="0" title=""></td>
      </tr>
      <tr>
       <td width="10"><img src="../graphics/' . $img . '_left.gif" width="10" height="29" border="0" title=""></td>
     <td background="../graphics/' . $img . '_bg.gif" bgcolor="' . $bgcolor . '" valign="top">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" height="28">
       <tr>
        <td align="center" class="bl_text_b" width="60">' . $rank . ' (' . $prev_rank . ')</td>
        <td style="padding-left:10px;" width="180"><a href="statistics/history_team.php?id=' . $id_team . '" class="b_link3">' . $team_title . '</a></td>
        <td style="padding-left:10px;" width="180">' . $league_print . '</td>
        <td align="center" class="bl_text_b" width="60">' . $rate . '</td>
        <td align="center" class="bl_text_b" width="10"><img src="' . $team_details->stars_path . '" border="0" height="15px" align="middle" title="' . $team_details->followers . ' �����������"></td>
       </tr>
      </table>
     </td>
     <td width="10"><img src="../graphics/' . $img . '_right.gif" width="10" height="29" border="0" title=""></td>
      </tr>');
}


print('
     </table>
       </div><br><br>
  </td>
 </tr>
</table>  ');

require("includings/big footer.php");

?>