﻿<?php
$class_active["index"] = "active";
$page_title = "Главная";
$description = "Уникальный портал статистики футзала и футбола Владимира и Владимирской области";
include("header.php");

print('
          <!--SECTION CONTAINER SLIDER-->
    <section id="summary-slider">
           <div class="general">
           <div class="content-result content-result-news col-md-12">
             <div id="textslide" class="effect-backcolor">
                <div class="container">
                    <div class="col-md-12 slide-txt">
                        <p class="sub-result aft-little welcome linetheme-left">Живи</p>
                        <p class="sub-result aft-little linetheme-right">мини-</p>
                        <p class="sub-result aft-little linetheme-right">футболом<span class="point-big">!</span></p>
                    </div>
                </div>
             </div>
           </div>
');

$id_schedule = matchOfWeek();
if ($id_schedule > 0) {
    $res_match = mysql_query("SELECT * FROM schedule WHERE id_schedule = $id_schedule");
    $id_team_1 = mysql_result($res_match, 0, "team_1");
    $id_team_2 = mysql_result($res_match, 0, "team_2");
    $top_team_1_details = new TeamCompetitor();
    $top_team_1_details->getTeamCompetitor($id_team_1);
    $top_team_2_details = new TeamCompetitor();
    $top_team_2_details->getTeamCompetitor($id_team_2);
    $top_match_date["day"] = mb_substr(mysql_result($res_match, 0, "date"), 8, 2);
    $top_match_date["mon"] = iconv('windows-1251', 'utf-8', ruMonth(mb_substr(mysql_result($res_match, 0, "date"), 5, 2)));
    $top_match_time = mb_substr(mysql_result($res_match, 0, "time"), 0, 5);
    $top_match_pitch = mysql_result($res_match, 0, "pitch");

    print('
           <a href="match_preview.php?s=' . $id_schedule . '"><div id="slidematch" class="col-xs-12 col-md-12">
                  <div class="content-match-team-wrapper">
                     <span class="gdlr-left">' . $top_team_1_details->title . '&nbsp;<img class="" style="max-height: 70px; max-width: 70px;" src="/emblems/' . $top_team_1_details->team_details->emblem_path . '" alt=""/></span>
                     <span class="gdlr-upcoming-match-versus">VS</span>
                     <span class="gdlr-right"><img class="" style="max-height: 70px; max-width: 70px;" src="/emblems/' . $top_team_2_details->team_details->emblem_path . '" alt=""/>&nbsp;' . $top_team_2_details->title . '</span>
                  </div>
                  <div class="content-match-team-time">
                     <span class="gdlr-left">' . (int) $top_match_date["day"] . ' ' . $top_match_date["mon"] . ' - ' . $top_match_time . '</span>
                     <span class="gdlr-right">' . $top_match_pitch . '</span>
                  </div>
               </div>
         </div></a>
    </section>
');
}


print('          <!-- SECTION NEWS SLIDER -->
     <section class="news_slide-over-color">
          <div class="news_slide-over"></div>
           <div class="container">
             <div class="col-xs-12 col-md-12 top-slide-info">             
');

$get_count = 4;

if (!isset($wall)) {
    $wall = getVKWall($get_count);
}

for ($i = 0; $i < $get_count; $i++) {

    $header_len = 200;
    $text_len = 500;
    if (isset($wall[$i]->copy_history)) {
        $text = $wall[$i]->copy_history[0]->text;
        if (isset($wall[$i]->copy_history[0]->attachments)) {
            switch ($wall[$i]->copy_history[0]->attachments[0]->type) {
                case "photo":
                    $picture_url = $wall[$i]->copy_history[0]->attachments[0]->photo->sizes[4]->url;
                    $icon = '';
                    break;
                case "video":
                    $picture_url = $wall[$i]->copy_history[0]->attachments[0]->video->image[2]->url;
                    $icon = '<i class="fa fa-video-camera"></i>';
                    break;
            }
        } else {
            $picture_url = "/graphics/news_624x428.jpg";
        }
    } else {
        $text = $wall[$i]->text;
        if (isset($wall[$i]->attachments)) {
            switch ($wall[$i]->attachments[0]->type) {
                case "photo":
                    $picture_url = $wall[$i]->attachments[0]->photo->sizes[4]->url;
                    $icon = '';
                    break;
                case "video":
                    $picture_url = $wall[$i]->attachments[0]->video->image[2]->url;
                    $icon = '<i class="fa fa-video-camera"></i>';
                    break;
            }
        } else {
            $picture_url = "/graphics/news_624x428.jpg";
        }
    }
    $strings = explode(PHP_EOL, $text);
    if (sizeof($strings) == 0 or strlen($strings[0]) < 2) {
        $strings[0] = "Новость";
    } elseif (sizeof($strings) == 1) {
        $strings[1] = "Читать далее";
    }
    if (sizeof($strings) > 0 AND strlen($strings[1]) < 2) {
        $strings[1] = $strings[2];
    }
    $header = cutString($strings[0], $header_len);
    $text = cutString($strings[1], $text_len);
    $news_date = array();
    $news_date["day"] = date("d", $wall[$i]->date);
    $news_date["month"] = iconv('windows-1251', 'utf-8', ruMonth(date("m", $wall[$i]->date)));
    $news_date["year"] = date("Y", $wall[$i]->date);
    $news_date["time"] = date("H:i", $wall[$i]->date);

    if (isset($wall[$i]->is_pinned)) {
        $exclusive = "Важно";
    } else {
        $exclusive = "";
    }

    print('

              <div class="col-xs-6 col-md-6">
                <div class="col-md-4 slide-cont-img"><a href="https://vk.com/wall-37375522_' . $wall[$i]->id . '" target="_blank"><img class="scale_image" src="' . $picture_url . '" alt=""/>' . $icon . '</a></div>
                <div class="event_date dd-date">' . $news_date["day"] . ' ' . $news_date["month"] . ' ' . $news_date["year"] . ' ' . $news_date["time"] . ' <div class="post_theme">' . $exclusive . '</div></div><h4><a href="https://vk.com/wall-37375522_' . $wall[$i]->id . '" target="_blank">' . $header . '</a></h4>
                <p><a href="https://vk.com/wall-37375522_' . $wall[$i]->id . '" target="_blank">' . $text . '</a></p>
              </div>
    ');
}

print('
             </div>
           </div>
     </section>

     <section id="parallaxTraining">
        <div class="black-shad">
        <div class="container">
            <div class="col-md-12">
                <div class="txt-training">
                  <p>быстрые</p>
                  <h2>разделы</h2>
                  <a href="ranking.php">Рейтинг</a><a href="bans.php">КДК</a><a href="contact.php">Контакты</a>
                </div>
            </div>
        </div>
      </div>
     </section>
          <!--SECTION Match TOP SCORE-->
     <section id="atp-match">
           <div class="container">
           <div id="people-top" class="top-match col-xs-12 col-md-12">
              <h3 class="news-title n-match">свежие <span>результаты</span><span class="point-little">.</span></h3>
              <p class="subtitle">Самый интересный матч прошедшего тура и лидеры чемпионата</p>
');

$id_protocol = resultOfWeek();
if ($id_protocol > 0) {
    $res_match = mysql_query("SELECT * FROM protocol, schedule, competition WHERE id_protocol = $id_protocol AND protocol.id_schedule = schedule.id_schedule AND competition.id_competition = protocol.league");
    $id_team_1 = mysql_result($res_match, 0, "protocol.team_1");
    $id_team_2 = mysql_result($res_match, 0, "protocol.team_2");
    $team1 = new TeamCompetitor();
    $team2 = new TeamCompetitor();
    $team1->getTeamCompetitor($id_team_1);
    $team2->getTeamCompetitor($id_team_2);
    $match_date["day"] = mb_substr(mysql_result($res_match, 0, "dt_protocol"), 8, 2);
    $match_date["mon"] = iconv('windows-1251', 'utf-8', ruMonth(mb_substr(mysql_result($res_match, 0, "dt_protocol"), 5, 2)));
    $match_time = mb_substr(mysql_result($res_match, 0, "dt_protocol"), 11, 5);
    $match_pitch = mysql_result($res_match, 0, "pitch");
    $match_tour = mysql_result($res_match, 0, "tour");
    $onpenalty = mysql_result($res_match, 0, "on_penalty");
    $extratime = mysql_result($res_match, 0, "extratime");

    if ($onpenalty == 1) {
        $add_label = "<br><font size='1pt'>по пен.</font>";
    }

    if ($extratime == 1) {
        $add_label = "<br><font size='1pt'>доп. вр.</font>";
    }

    print('
            <!--SECTION TOP MATCH-->
              <div class="next-match-co col-xs-8 col-md-8">
                 <div id="nextmatch-content" class="experience">
                   <div class="col-xs-12 atphead"><div class="match-sing-title"><img src="images/sub-ball.png" alt="" />' . $team1->competition_details->category[0]->title . '. ' . $team1->competition_details->title . '</div></div>
                   <div class="col-xs-4 pht-1 pht-left">
                         <div class="img-face-home">
                            <img src="/emblems/' . $team1->team_details->emblem_path . '" alt="" />
                            <p class="name-mc">' . $team1->title . '</p>
                         </div>                      
                  </div>
                  <div class="col-xs-4 pl-point ">
                      <p class="col-xs-12 name-mc-title">' . $match_tour . ' тур</p>
                      <div class="col-xs-4 nm-result">
                            <p class="nr1 ris1"> ' . mysql_result($res_match, 0, "score_end_1") . ' </p>
                            <p class="nr2"> (' . mysql_result($res_match, 0, "score_half_1") . ') </p>
                      </div>
                      <div class="col-xs-4 nm-result-vs">
                            <p class="nrvs"> VS ' . $add_label . '</p>
                      </div>
                      <div class="col-xs-4 nm-result">
                            <p class="nr1 ris2"> ' . mysql_result($res_match, 0, "score_end_2") . ' </p>
                            <p class="nr2"> (' . mysql_result($res_match, 0, "score_half_2") . ') </p>
                      </div>

                  </div>
                   <div class="col-xs-4 pht-1 pht-right">
                        <div class="img-face-home">
                            <img src="/emblems/' . $team2->team_details->emblem_path . '" alt="" />
                            <p class="name-mc">' . $team2->title . '</p>
                        </div>

                  </div>
                  <div class="col-xs-12 atphead"><div class="match-sing-title inf-bottom">
                        <p><i class="fa fa-calendar-o"></i>' . $match_date["day"] . ' ' . $match_date["mon"] . ' - ' . $match_time . '<i class="fa fa-map-marker"></i> ' . $match_pitch . '</p>
                   </div></div>
                 </div>
              </div>
');
}

$res_top_league = mysql_query("SELECT * FROM competition, competition_rate WHERE season = $current_season->id_season AND competition.rate = competition_rate.id_competition ORDER BY competition_rate.rate DESC LIMIT 0, 1");
$top_league_id = mysql_result($res_top_league, 0, "id_competition");
$top_league_title = mysql_result($res_top_league, 0, "title");

$rows_to_show = 4;

print('
              <div class="col-md-4 home-page">
                   <div class="main">
                                <div class="tabs animated-slide-2">
                                 <div class="result-filter">
                                    <ul class="tab-links">
                                        <li class="active"><a href="#tab_league">' . $top_league_title . '</a></li>
                                        <li><a href="#tab_rank">Индекс силы</a></li>
                                        <li><a href="#tab_scorers">Лучшие игроки</a></li>
                                    </ul>
                                    </div>
                                    <div class="tab-content-point">
                                        <div id="tab_league" class="tab active">
                                        <table class="tab-score">
                                          <tr class="top-scrore-table"><td class="score-position">#</td><td>КОМАНДА</td><td>МЯЧИ</td><td>ОЧКИ</td></tr>
');

//топ-лига

$tour_date = "9999-12-31 23:59:59";

$league_table = getLeagueTable($top_league_id, $tour_date);

for ($i = 0; $i < min(mysql_num_rows($league_table), $rows_to_show); $i++) {
    $place = $i + 1;
    $team = mysql_result($league_table, $i, "name");
    if (strlen($team) > 30) {
        $s_team = substr($team, 0, 27) . "...";
    } else {
        $s_team = $team;
    }
    $id_team = mysql_result($league_table, $i, "id_competitor");
    $competitor = new TeamInfo();
    $competitor->getTeamInfoByCompetitor($id_team);
    $goalsf = mysql_result($league_table, $i, "goalsfor");
    $goalsa = mysql_result($league_table, $i, "goalsagainst");
    $goalsd = $goalsf - $goalsa;
    if ($goalsd > 0) {
        $goalsd = "+" . $goalsd;
    }
    $points = mysql_result($league_table, $i, "pts");

    print('
                                          <tr><td class="score-position">' . $place . '.</td><td><a href="competitor.php?c=' . $id_team . '"><img src="/emblems/' . $competitor->emblem_path . '" style="max-height: 25px; max-width: 25px;">&nbsp;' . $team . '</a></td><td>' . $goalsd . '</td><td>' . $points . '</td></tr>
    ');
}

print('
                                         </table>
                                        </div>
                                        <div id="tab_rank" class="tab">
                                        <table class="tab-score">
                                        <tr class="top-scrore-table"><td class="score-position">#</td><td>КОМАНДА</td><td>ИНДЕКС</td></tr>
');

//топ рейтинг

$rank_date = getParameter("befutsal_ranking_date");

$query = "SELECT DISTINCT r.date AS prev_date FROM ranking AS r ORDER BY prev_date DESC";
$res_dates = mysql_query($query);
$prev_date = mysql_result($res_dates, 1, "prev_date");

$query = "SELECT r.id_team, t.title, t.emblem_path, r.rate, r.id_league, r.league_title, r.rank FROM team AS t, ranking AS r WHERE r.id_team = t.id_team AND r.date = '$rank_date' AND r.league_title <> 'Чемпионат мира 2022' ORDER BY r.rank ASC LIMIT 0, " . $rows_to_show;
$res_ranking = mysql_query($query);
$num_teams = mysql_num_rows($res_ranking);

for ($i = 0; $i < min($num_teams, $rows_to_show); $i++) {
    $place = $i + 1;
    print('
                                          <tr><td class="score-position">' . $place . '.</td><td><a href="competitor.php?c=' . mysql_result($res_ranking, $i, "r.id_team") . '"><img src="/emblems/' . mysql_result($res_ranking, $i, "t.emblem_path") . '" style="max-height: 25px; max-width: 25px;">&nbsp;' . mysql_result($res_ranking, $i, "t.title") . '</a></td><td>' . mysql_result($res_ranking, $i, "r.rate") . '</td></tr>
');
}

print('
                                         </table>
                                        </div>
                                        <div id="tab_scorers" class="tab">
                                        <table class="tab-score">
                                        <tr class="top-scrore-table"><td class="score-position">#</td><td>ИГРОК</td><td>ИГР</td><td>ГОЛОВ</td></tr>
');

//топ-игроки

$limit = "LIMIT 0, " . $rows_to_show;
$res_scorers = topScorers($top_league_id, $limit);

for ($i = 0; $i < min(mysql_num_rows($res_scorers), $rows_to_show); $i++) {
    $place = $i + 1;
    print('
                                          <tr><td class="score-position">' . $place . '</td><td><a href="player.php?p=' . mysql_result($res_scorers, $i, "id_applicant") . '">' . mysql_result($res_scorers, $i, "name_1") . ' ' . mysql_result($res_scorers, $i, "name_2") . '</a></td><td>' . mysql_result($res_scorers, $i, "played") . '</td><td>' . mysql_result($res_scorers, $i, "scored") . '</td></tr>
');
}

print('
                                         </table>
                                        </div>
                                    </div>
                                    <div class="score-view-all"><a class="pl-point-button" href="results.php">Матч-центр</a></div>
                                </div>
                            </div>
                </div>
             </div><!--Close Top Match-->
           </div>
     </section>
');

print('
           <!--SECTION NEXT MATCH-->
     <section id="next-match">
           <div  class="container">
              <div class="next-match-news top-match col-xs-12 col-md-12">
              <h3 class="news-title n-match">Самое <span>интересное</span><span class="point-little">.</span></h3>
              <p class="subtitle">Обратите внимание на интересные события.</p>
                <a href="match_preview.php?s=' . $id_schedule . '"><div class="other-match col-md-4">              
    ');

if ($id_schedule > 0) {
    print('                       
                    <div class="score-next-time">
                        <div class="circle-ico"><p>ТОП</p></div>
                    </div>
                    <div id="getting-started"></div>
                    <div class="col-xs-5 col-md-5 match-team">
                        <img class="" style="max-height: 70px; max-width: 70px;" src="/emblems/' . $top_team_1_details->team_details->emblem_path . '" alt=""/>
                        <p>' . $top_team_1_details->title . '</p>
                    </div>
                    <div class="col-xs-2 col-md-2 match-team-vs">
                        <span class="txt-vs"> - vs - </span>
                    </div>
                    <div class="col-xs-5 col-md-5 match-team">
                        <img class="" style="max-height: 70px; max-width: 70px;" src="/emblems/' . $top_team_2_details->team_details->emblem_path . '" alt=""/>
                        <p>' . $top_team_2_details->title . '</p>
                    </div>
                    <div class="next-match-place">
                        <p class="sub-result">' . $top_team_1_details->competition_details->title . '</p>
                        <p class="dd-news-date">' . $top_match_date["day"] . ' ' . $top_match_date["mon"] . ' - ' . $top_match_time . ' ' . $top_match_pitch . '</p>
                    </div>
                </div></a>
                <div class="other-match col-md-4">
            ');
}


$today = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
$now = date("H:i", mktime(date("H"), date("i"), 0, 0, 0, 0));
$res_schedule = mysql_query("SELECT id_schedule, league, `date`, `time`, schedule.pitch AS pitch, title, team_1, team_2, bet_1, bet_2 FROM schedule, competition WHERE ((`date` = '$today' AND time > '$now') OR `date` > '$today') AND league = id_competition ORDER BY `date`, `time`, schedule.pitch, league LIMIT 0, 10");
$num_schedule = mysql_num_rows($res_schedule);

for ($i = 0; $i < min($num_schedule, 4); $i++) {
    $day = substr(mysql_result($res_schedule, $i, "date"), 8, 2);
    $s_month = iconv('windows-1251', 'utf-8', ruMonth(substr(mysql_result($res_schedule, $i, "date"), 5, 2)));
    $year = substr(mysql_result($res_schedule, $i, "date"), 0, 4);
    $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
    $league = mysql_result($res_schedule, $i, "league");
    $time = mysql_result($res_schedule, $i, "time");
    $pitch = mysql_result($res_schedule, $i, "pitch");
    $league_title = mysql_result($res_schedule, $i, "title");
    $team_1 = mysql_result($res_schedule, $i, "team_1");
    $team_2 = mysql_result($res_schedule, $i, "team_2");
    $bet_1 = mysql_result($res_schedule, $i, "bet_1");
    $bet_2 = mysql_result($res_schedule, $i, "bet_2");
    $team_1_details = new TeamCompetitor();
    $team_1_details->getTeamCompetitor($team_1);
    $team_2_details = new TeamCompetitor();
    $team_2_details->getTeamCompetitor($team_2);

    print('
                    <a href="match_preview.php?s=' . $id_schedule . '"><div class="match-team-list">
                        
                        <span>' . $team_1_details->title . '&nbsp;<img class="" src="/emblems/' . $team_1_details->team_details->emblem_path . '" alt="" height="25"/></span>
                        <span class="txt-vs"> - vs - </span>
                        <span><img class="" style="max-height: 70px; max-width: 70px;" src="/emblems/' . $team_2_details->team_details->emblem_path . '" alt="" height="25"/>&nbsp;' . $team_2_details->title . '</span>
                        
                        <p>' . $day . ' ' . mb_substr($s_month, 0, 6) . ' ' . substr($time, 0, 5) . ' ' . $pitch . '</p>
                    </div></a>          
    ');
}

$week_ago = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 10, date("Y")));
$res_transfers = mysql_query("SELECT * FROM transfer WHERE `date` >= '$week_ago' ORDER BY id_transfer LIMIT 0,4");
$num_transfers = mysql_num_rows($res_transfers);

print('
                    <a href="results.php"><div class="team-view-all">
                       <p>Все матчи</p>
                    </div></a>
                </div>');

if ($num_transfers > 0) {

    $transfer_list = new TransferList();
    $transfer_list->getTransfers($current_season->id_season);

    print('<div class="other-match col-md-4 other-last">');

    for ($i = 0; $i < $num_transfers; $i++) {

        print('
                    <div class="match-team-list">
                        
                        <span><strong>' . $transfer_list->transfers[$i]->player->name_1 . ' ' . $transfer_list->transfers[$i]->player->name_2 . '</strong></span>
                        <span class="txt-vs">перешёл в </span>
                        <span><img class="" style="max-height: 70px; max-width: 70px;" src="/emblems/' . $transfer_list->transfers[$i]->new_team->team_details->emblem_path . '" alt="" height="25"/>&nbsp;#' . $transfer_list->transfers[$i]->new_team->shortname . '</span>
                        
                        <p>' . $transfer_list->transfers[$i]->date . '</p>
                    </div>            
    ');
    }

    print('         <a href="transfers.php?s=' . $current_season->id_season . '"><div class="team-view-all">
                       <p>Все трансферы</p>
                    </div></a>
                </div>');
} else {
    print('
                <div class="other-match col-md-4 other-last">
                   <a href="contact.php"><img src="images/adwertise.jpg" alt="" /></a>
                </div>');
}

print('
                
            </div>
           </div>
     </section>
');

print('

          <!-- SECTION SUBSCRIPTIONS-->
     <section class="bbtxt-content-subscription">
           <div class="container">
                    <div class="col-xs-12 bbtxt-box">
                        <h4><span class="middle-txt">Живи</span> <span class="point-big">Мини-</span>футболом!</h4>
                        <p class="subin">С нами удобно везде.</p>
                        <div class="subscription-content">
                            <div class="col-md-4">
                                <div class="subscription-tennis">
                                     
                                     <h3>подпишись<span> ВКонтакте</span></h3>
                                     
                                     <p class="desc-subscription">Последние новости от сообщества, трансляции, фотоотчеты, обсуждения.</p>
                                     <a class="pl-point-button" href="htpps://vk.com/befutsal">Подписаться</a>
                                 </div>
                             </div>
                             <div class="col-md-4">
                                <div class="subscription-tennis">
                                   
                                  <h3>скачай<span> приложение</span></h3>
                                  
                                  <p class="desc-subscription">Сообщим о результатах, напомним о днях рождения, конкурсы прогнозов.</p>
                                  ' . getParameter("mobile_app_ios_badge_new") . ' ' . getParameter("mobile_app_android_badge_new") . '
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="subscription-tennis">
                                  
                                  <h3>твоё<span> избранное</span></h3>
                                  
                                  <p class="desc-subscription">Создай учетную запись и настрой отображение сайта и приложения под себя.</p>
                                  <a class="pl-point-button" href="login.php">Войти</a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
     </section>
');

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');
