<?php

$root = true;
$page_title = "BEFUTSAL.RU Privacy Policy";
$description = "Privacy Policy";
$keywords = "��������, ����-������, ������, vladimir, futsal, ������, ��������, contacts, �������, email";
include("includings/header.php");

print('
 <tr>
  <td height="6" background="' .$add_string. 'graphics/h_bg_1.gif" bgcolor="#28C638"><img src="' .$add_string. 'graphics/0.gif" width="1" height="6" border="0" title=""></td>
 </tr>
 <tr>
  <td height="1" bgcolor="#FFFFFF"><img src="' .$add_string. 'graphics/0.gif" width="1" height="1" border="0" title=""></td>
 </tr>
 <tr>
  <td height="30" style="background-image:url(' .$add_string. 'graphics/h_bg_2.gif);background-repeat:repeat-x;background-position:top"><img src="' .$add_string. 'graphics/0.gif" width="1" height="30" border="0" title=""></td>
 </tr>
</table>

<!----------------------- End of Header ---------------------------->

<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff">
 <tr>');

include("includings/main menu.php");

include('database_main.php');

print('
<!------------------------ Central Part.Right Part ---------------------------->

  <td valign="top">
   <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
');

print('
<!------------------------ Central Part.Right Part.Central Part ---------------------------->

	 <td valign="top" style="padding:7px 20px 10px 20px">
	 <div>
      <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#5BC1D7">
	   <tr>
	    <td valign="top" width="14" height="35"><img src="graphics/cp_blue_left.gif" width="14" height="35" border="0" title=""></td>
		<td valign="top" background="graphics/cp_blue_bg.gif" bgcolor="#5BC1D7" style="padding-left:20px;padding-top:4px;" class="cp_header">'.$page_title.'</td>
		<td valign="top" width="14" height="35"><img src="graphics/cp_blue_right.gif" width="14" height="35" border="0" title=""></td>
	   </tr>
	  </table>
	  </div><br>');
	  
 print('
	   <table width="100%" cellpadding="0" cellspacing="0" border="0">
	    <tr>
	     <td valign="top" colspan="3" style="padding-left:10px; font-size:11px;">
	     <p align="left"><a href="privacy_policy_ru.php">�������</a></p>
		 <p align="justify">This privacy policy governs your use of the software application BEFUTSAL (�Application�) for mobile devices that was created by <strong>Knowledge Department OOO</strong>. The Application is a mobile client for system of futsal statistics (league tables, staffs, protocols, schedules, etc.) located at <a href="http://befutsal.ru">www.befutsal.ru</a>.</p>
<h3>What information does the Application obtain and how is it used?</h3>
<h2>User Provided Information</h2> 
<p align="justify">The Application obtains the information you provide when you download and register the Application. Registration is required, it is used to identify you and store your settings on the server. You will not be able to use the Application if you do not register with us.</p>
 
<p align="justify">When you register with us and use the Application, you generally provide (a) your name, email address, age, user name, password and other registration information; (b) information you provide us when you contact us for help.</p>

<p align="justify">We may also use the information you provided us to contact your from time to time to provide you with important information, required notices and marketing promotions.</p>

<h2>Automatically Collected Information</h2> 
 
<p align="justify">In addition, the Application may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile devices unique device ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browsers you use, and information about the way you use the Application.</p>
 
<h2>Does the Application collect precise real time location information of the device?</h2>
<p align="justify">This Application does not collect precise information about the location of your mobile device.</p> 
 
<h2>Do third parties see and/or have access to information obtained by the Application?</h2>

<p align="justify">Yes. We will share your information with third parties only in the ways that are described in this privacy statement.</p>
<p align="justify">We may disclose User Provided and Automatically Collected Information:</p>
<ul>
<li>as required by law, such as to comply with a subpoena, or similar legal process;</li>
<li>when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request;</li>
<li>with our trusted services providers who work on our behalf, do not have an independent use of the information we disclose to them, and have agreed to adhere to the rules set forth in this privacy statement;</li>
<li>if <strong>Knowledge Department OOO</strong> is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our Web site of any change in ownership or uses of this information, as well as any choices you may have regarding this information;</li>
<li>to advertisers and third party advertising networks and analytics companies as described in the section below.</li>
</ul>
</p>
 
<h2>Automatic Data Collection and Advertising</h2>

<p align="justify">We may work with analytics companies to help us understand how the Application is being used, such as the frequency and duration of usage. We work with advertisers and third party advertising networks, who need to know how you interact with advertising provided in the Application which helps us keep the cost of the Application low. Advertisers and advertising networks use some of the information collected by the Application, including, but not limited to, the unique identification ID of your mobile device and your mobile telephone number. To protect the anonymity of this information, we use an encryption technology to help ensure that these third parties can�t identify you personally. These third parties may also obtain anonymous information about other applications you�ve downloaded to your mobile device, the mobile websites you visit, your non-precise location information (e.g., your zip code), and other non- precise location information in order to help analyze and serve anonymous targeted advertising on the Application and elsewhere. We may also share encrypted versions of information you have provided in order to enable our partners to append other available information about you for analysis or advertising related use. </p>

<p align="justify">If you�d like to opt-out from third party use of this type of information to help serve targeted advertising, please visit the section entitled �Opt-out� below.</p>
 
<h2>What are my opt-out rights?</h2>

<p align="justify">There are multiple opt-out options for users of this Application:</p>

<p align="justify">Opt-out of all information collection by uninstalling the Application: You can stop all collection of information by the Application easily by uninstalling the Application. You may use the standard uninstall processes as may be available as part of your mobile device or via the mobile application marketplace or network. You can also request to opt-out via email, at <a href="mailto:support@befutsal.ru">support@befutsal.ru</a>.</p>
  
<h2>Data Retention Policy, Managing Your Information</h2>

<p align="justify">We will retain User Provided data for as long as you use the Application and for a reasonable time thereafter. We will retain Automatically Collected information for up to 24 months and thereafter may store it in aggregate. If you�d like us to delete User Provided Data that you have provided via the Application, please contact us at <a href="mailto:support@befutsal.ru">support@befutsal.ru</a> and we will respond in a reasonable time. Please note that some or all of the User Provided Data may be required in order for the Application to function properly.</p>
 
<h2>Children</h2>

<p align="justify">We do not use the Application to knowingly solicit data from or market to children under the age of 3. If a parent or guardian becomes aware that his or her child has provided us with information without their consent, he or she should contact us at <a href="mailto:support@befutsal.ru">support@befutsal.ru</a>. We will delete such information from our files within a reasonable time.</p>
 
<h2>Security</h2>

<p align="justify">We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic, and procedural safeguards to protect information we process and maintain. For example, we limit access to this information to authorized employees and contractors who need to know that information in order to operate, develop or improve our Application. Please be aware that, although we endeavor provide reasonable security for information we process and maintain, no security system can prevent all potential security breaches.</p>
 
<h2>Changes</h2>

<p align="justify">This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes to our Privacy Policy by posting the new_design Privacy Policy here and informing you via email or text message. You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes.</p>
 
<h2>Your Consent</h2>

<p align="justify">By using the Application, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us. "Processing,� means using cookies on a computer/hand held device or using or touching information in any way, including, but not limited to, collecting, storing, deleting, using, combining and disclosing information, all of which activities will take place in the Russian Federation. If you reside outside the Russian Federation your information may be transferred, processed and stored there under Russian Federation privacy standards. </p>
 
<h2>Contact us</h2>
<p align="justify">If you have any questions regarding privacy while using the Application, or have questions about our practices, please contact us via email at <a href="mailto:support@befutsal.ru">support@befutsal.ru</a>.</p>
		 </td>
	    </tr>
	   </table>
');

print('

<!------------------------ Central Part.Right Part.Central Part.End ---------------------------->

<!------------------------ Central Part.Right Part.Right Part ---------------------------->
');

//������������, ����������

require("includings/birthdays.php");

require("includings/top matches.php");

require("includings/latest comments.php");

print('</table>');

print('
<!------------------------ Central Part.Right Part.Right Part.End ---------------------------->
');

print('
	</tr>
   </table>

  </td>

<!------------------------ Central Part.Right Part.End ---------------------------->

 </tr>

</table>');


require("includings/big footer.php");

?>