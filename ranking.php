﻿<?php

$class_active["ranking"] = "active";
$page_title = "Индекс силы";
$description = "Индекс силы и рейтинг команд";
include("header.php");

$rank_date = getParameter("befutsal_ranking_date");

$query = "SELECT DISTINCT r.date AS prev_date FROM ranking AS r ORDER BY prev_date DESC";
$res_dates = mysql_query($query);
$prev_date = mysql_result($res_dates, 1, "prev_date");

$s_year = substr($rank_date, 0, 4);
$n_month = substr($rank_date, 5, 2);
$s_day = (int)substr($rank_date, 8, 2);
$print_date = iconv('windows-1251', 'utf-8', $s_day . ' ' . ruMonth($n_month) . ' ' . $s_year);

print('
   <section class="drawer">
    <div class="col-md-12 size-img back-img-match">
        <div class="effect-cover">
            <h3 class="txt-advert animated">Рейтинг Живи мини-футболом!</h3>
            <p class="txt-advert-sub">Индекс силы команд</p>
        </div>
    </div>
    
    <section id="summary" class="container secondary-page">
      <div class="general general-results">
           <div id="rrResult" class="top-score-title right-score total-reslts col-md-9">
                <h3>Рейтинг <span>команд</span><span class="point-little">.</span></h3>
                <div class="cat-con-desc">
                <img src="images/cup1.jpg" alt="" /><p class="news-title-right">Индекс силы</p>
                <p class="txt-right">Индекс силы команд, участвующих в турнирах города Владимира.</p>
                <p class="txt-right">Индекс рассчитывается ежемесячно на основе показателей выступлений команды за последние 5 сезонов.</p>
                </div>
                <div class="main">
                        <div class="tabs animated-slide-2">
                            <ul class="tab-links">
                                <li class="active"><a href="#tab1111">' . $print_date . '</a></li>
                                <li></li>
                                <li></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab1111" class="tab active">
                                <table class="tab-score">
                                  <tr class="top-scrore-table"><td class="score-position">#</td><td>Команда</td><td>Турнир</td><td>Индекс</td></tr>');

$query = "SELECT r.id_team, t.title, r.rate, r.id_league, r.league_title, r.rank, t.emblem_path FROM team AS t, ranking AS r WHERE r.id_team = t.id_team AND r.date = '$rank_date' AND r.league_title <> 'Чемпионат мира 2022' ORDER BY r.rank ASC";
$res_ranking = mysql_query($query);
$num_teams = mysql_num_rows($res_ranking);

for ($i = 0; $i < $num_teams; $i++) {

    $rank = mysql_result($res_ranking, $i, "rank");
    $id_team = mysql_result($res_ranking, $i, "id_team");
    $emblem_path = mysql_result($res_ranking, $i, "emblem_path");
    if ($emblem_path == null) {
        $emblem_path = "emblem_default.PNG";
    }
    $query = "SELECT rank FROM ranking WHERE id_team = $id_team AND `date` = '$prev_date'";
    $res_prev_rank = mysql_query($query);
    if (mysql_num_rows($res_prev_rank) > 0) {
        $prev_rank = mysql_result($res_prev_rank, 0, "rank");
    } else {
        $prev_rank = "-";
    }
    $team_title = mysql_result($res_ranking, $i, "title");
    $rate = mysql_result($res_ranking, $i, "rate");
    $id_league = mysql_result($res_ranking, $i, "id_league");
    $league_title = mysql_result($res_ranking, $i, "league_title");
    if ($id_league != NULL) {
        $league_print = '<a href="league.php?l=' . $id_league . '" class="b_link3">' . $league_title . '</a>';
    } else {
        $league_print = "Не заявлена";
    }

    print('
                                  <tr><td>' . $rank . ' (' . $prev_rank . ')</td><td><img src="/emblems/' . $emblem_path . '" height="25px"> ' . $team_title . '</td><td><a href="competition.php?c=' . $id_league . '">' . $league_print . '</a></td><td>' . $rate . '</td></tr>');
}

print('
                                 </table>
                                </div>
                            </div>
                            <div class="score-view-all"></div>
                        </div>
                    </div>
           </div><!--Close Top Match-->
');


include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>
<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>

 <script src="js/custom.js" type="text/javascript"></script>   
');
