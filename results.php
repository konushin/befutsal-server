﻿<?php
$class_active["results"] = "active";
$page_title = "Матч-центр";
$description = "Матч-центр, расписание, результаты, прогнозы";
include("header.php");

print('
   <section class="drawer">
    <div class="col-md-12 size-img back-img-match">
        <div class="effect-cover">
            <h3 class="txt-advert animated">Матч-центр</h3>
            <p class="txt-advert-sub">Расписание и результаты</p>
        </div>
    </div>
    
    <section id="summary" class="container secondary-page">
      <div class="general general-results">
           <div id="rrResult" class="top-score-title right-score total-reslts col-md-9">
                <h3>Матч-<span>центр</span><span class="point-little">.</span></h3>
                <div class="main">
                        <div class="tabs animated-slide-2">
                            <ul class="tab-links">
                                <li class="active"><a href="#results">Результаты</a></li>
                                <li><a href="#schedule">Расписание</a></li>
                                <li><a href="#bets">Прогнозы</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="results" class="tab active">
                                <table class="tab-score">
                                  <tr class="top-scrore-table"><td class="score-position">ДАТА</td><td>МАТЧ</td><td>СЧЕТ</td></tr>');

$weekago = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
$today = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));

$res_leagues = mysql_query("SELECT DISTINCT league FROM protocol WHERE dt_protocol BETWEEN '$weekago' AND '$today' ORDER BY dt_protocol DESC");
$num_leagues = mysql_num_rows($res_leagues);

//РЕЗУЛЬТАТЫ
for ($n = 0; $n < $num_leagues; $n++) {
    $league = mysql_result($res_leagues, $n, "league");
    $tournament = new Tournament();
    $tournament->getCompetitionInfo($league);
    $res_latest = mysql_query("SELECT DATE_FORMAT(MAX(dt_protocol), '%Y-%m-%e') AS latest FROM protocol WHERE league = $league");
    $latest = mysql_result($res_latest, 0, "latest");
    $res_for_print = mysql_query("SELECT DATE_FORMAT(MAX(dt_protocol), '%d.%m') AS latest FROM protocol WHERE league = $league");
    $date = mysql_result($res_for_print, 0, "latest");
    $day = (int) substr($date, 0, 2);
    $s_month = substr(ruMonth(substr($date, 3, 2)), 0, 3);
    $date = $day . " " . $s_month;
    $date = iconv('windows-1251', 'utf-8', $date);

    $res_results = mysql_query("SELECT * FROM protocol WHERE league = $league AND DATE_FORMAT(dt_protocol, '%Y-%m-%e') = '$latest'");
    $n_results = mysql_num_rows($res_results);

    $top_result = resultOfWeek();

    for ($i = 0; $i < $n_results; $i++) {
        $id_protocol = mysql_result($res_results, $i, "id_protocol");
        if ($id_protocol == $top_result) {
            $top_label = '<span class="newrecord">Топ-матч</span>';
        } else {
            $top_label = "";
        }
        $team_1 = mysql_result($res_results, $i, "team_1");
        $team_2 = mysql_result($res_results, $i, "team_2");
        $score_half_1 = mysql_result($res_results, $i, "score_half_1");
        $score_half_2 = mysql_result($res_results, $i, "score_half_2");
        $score_end_1 = mysql_result($res_results, $i, "score_end_1");
        $score_end_2 = mysql_result($res_results, $i, "score_end_2");
        $res_team_1 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_1");
        $res_team_2 = mysql_query("SELECT * FROM competitor WHERE id_competitor = $team_2");
        $s_team_1 = mysql_result($res_team_1, 0, "title");
        $s_team_2 = mysql_result($res_team_2, 0, "title");

        print('
                                  <tr><td><i class="fa fa-calendar-o"></i> ' . $date . '</td><td><a href="competitor.php?c=' . $team_1 . '">' . $s_team_1 . '</a> - <a href="competitor.php?c=' . $team_2 . '">' . $s_team_2 . '</a>' . $top_label . '</td><td><a href="match.php?p=' . $id_protocol . '">' . $score_end_1 . ':' . $score_end_2 . ' (' . $score_half_1 . ':' . $score_half_2 . ')</a></td></tr>');
    }
}

print('
                                 </table>
                                </div>
                                <div id="schedule" class="tab">');

//РАСПИСАНИЕ
print('
                                <table class="tab-score">
                                  <tr class="top-scrore-table"><td class="score-position" width="20%">ВРЕМЯ</td><td>МАТЧ</td><td>ПОЛЕ</td></tr>');

$thisweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
$nextweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 6, date("Y")));

$res_leagues = mysql_query("SELECT DISTINCT league, date FROM schedule WHERE date BETWEEN '$thisweek' AND '$nextweek' ORDER BY date, time");
$num_leagues = mysql_num_rows($res_leagues);

for ($n = 0; $n < $num_leagues; $n++) {
    $league = mysql_result($res_leagues, $n, "league");
    $tournament = new Tournament();
    $tournament->getCompetitionInfo($league);
    $date = mysql_result($res_leagues, $n, "date");
    $now = getdate();
    $year = $now["year"];
    $month = $now["mon"];
    $day = $now["mday"];
    $now_date = $year . '-' . $month . '-' . $day;
    $res_schedule = mysql_query("select DATE_FORMAT(date, '%d.%m %a') as date, team_1, team_2, time, id_schedule, pitch from schedule where league = $league and date = '$date' order by date, time");
    $n_schedule = mysql_num_rows($res_schedule);

    $top_schedule = matchOfWeek();

    for ($i = 0; $i < $n_schedule; $i++) {
        $team_1 = mysql_result($res_schedule, $i, "team_1");
        $team_2 = mysql_result($res_schedule, $i, "team_2");
        $time = mysql_result($res_schedule, $i, "time");
        $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
        if ($id_schedule == $top_schedule) {
            $top_label = '<span class="newrecord">Топ-матч</span>';
        } else {
            $top_label = "";
        }
        $pitch = mysql_result($res_schedule, $i, "pitch");
        $date = mysql_result($res_schedule, $i, "date");
        $day = (int) substr($date, 0, 2);
        $n_month = substr($date, 3, 2);
        $eng_week_day = substr($date, 6, 3);
        $date = $day . " " . substr(ruMonth($n_month), 0, 3) . ", " . ruWeekDay($eng_week_day) . ' ' . substr($time, 0, 5);
        $date = iconv('windows-1251', 'utf-8', $date);
        $res_team_1 = mysql_query("select * from competitor where id_competitor = $team_1");
        $res_team_2 = mysql_query("select * from competitor where id_competitor = $team_2");
        $s_team_1 = mysql_result($res_team_1, 0, "title");
        $s_team_2 = mysql_result($res_team_2, 0, "title");

        print('
                                  <tr><td class="score-position"><i class="fa fa-calendar-o"></i> ' . $date . '</td><td><p style="font-size: 6pt; background-color: #808B96; display: none;">' . cutString($tournament->title, 50) . '</p><a href="match_preview.php?s=' . $id_schedule . '">' . $s_team_1 . ' - ' . $s_team_2 . $top_label . '</a></td><td><i class="fa fa-map-marker"></i> ' . $pitch . '</td></tr>');
    }
}

print('
                                 </table>
                                </div>
                                <div id="bets" class="tab">');

//ПРОГНОЗЫ
print('
                                <table class="tab-score">
                                  <tr class="top-scrore-table"><td class="score-position">ВРЕМЯ</td><td>МАТЧ</td><td>П1 - Н - П2</td></tr>');

$thisweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
$nextweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 6, date("Y")));

$res_leagues = mysql_query("SELECT DISTINCT league, date FROM schedule WHERE date BETWEEN '$thisweek' AND '$nextweek' ORDER BY date, time");
$num_leagues = mysql_num_rows($res_leagues);

for ($n = 0; $n < $num_leagues; $n++) {
    $league = mysql_result($res_leagues, $n, "league");
    $tournament = new Tournament();
    $tournament->getCompetitionInfo($league);
    $date = mysql_result($res_leagues, $n, "date");
    $now = getdate();
    $year = $now["year"];
    $month = $now["mon"];
    $day = $now["mday"];
    $now_date = $year . '-' . $month . '-' . $day;
    $res_schedule = mysql_query("select DATE_FORMAT(date, '%d.%m %a') as date, team_1, team_2, time, id_schedule, bet_1, bet_0, bet_2 from schedule where league = $league and date = '$date' and (schedule.bet_1 is not null and schedule.bet_1 <> 2) order by date, time");
    $n_schedule = mysql_num_rows($res_schedule);

    for ($i = 0; $i < $n_schedule; $i++) {
        $team_1 = mysql_result($res_schedule, $i, "team_1");
        $team_2 = mysql_result($res_schedule, $i, "team_2");
        $time = mysql_result($res_schedule, $i, "time");
        $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
        $date = mysql_result($res_schedule, $i, "date");
        $day = (int) substr($date, 0, 2);
        $n_month = substr($date, 3, 2);
        $eng_week_day = substr($date, 6, 3);
        $date = $day . " " . ruMonth($n_month) . ", " . ruWeekDay($eng_week_day) . ' ' . substr($time, 0, 5);
        $date = iconv('windows-1251', 'utf-8', $date);
        $res_team_1 = mysql_query("select * from competitor where id_competitor = $team_1");
        $res_team_2 = mysql_query("select * from competitor where id_competitor = $team_2");
        $s_team_1 = mysql_result($res_team_1, 0, "title");
        $s_team_2 = mysql_result($res_team_2, 0, "title");
        $bet_1 = mysql_result($res_schedule, $i, "bet_1");
        $bet_0 = mysql_result($res_schedule, $i, "bet_0");
        $bet_2 = mysql_result($res_schedule, $i, "bet_2");

        print('
                                  <tr><td class="score-position"><i class="fa fa-calendar-o"></i> ' . $date . '</td><td><a href="match_preview.php?s=' . $id_schedule . '">' . $s_team_1 . ' - ' . $s_team_2 . '</a></td><td>' . $bet_1 . ' - ' . $bet_0 . ' - ' . $bet_2 . '</td></tr>');
    }
}

print('
                                 </table>
                                </div>
                            </div>
                            <div class="score-view-all"></div>
                        </div>
                    </div>
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>
<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>

 <script src="js/custom.js" type="text/javascript"></script>   
');
?>