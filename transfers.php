﻿<?php
$class_active["transfers"] = "active";
$page_title = "Переходы игроков";
$description = "Информация о переходах игроков между командами";
include("header.php");

$transfer_list = new TransferList();
$transfer_list->getTransfers($season->id_season);

print('
   <section class="drawer">
    <div class="col-md-12 size-img back-img-match">
        <div class="effect-cover">
            <h3 class="txt-advert animated">Трансферы</h3>
            <p class="txt-advert-sub">' . $season->title . '</p>
        </div>
    </div>
    
    <section id="summary" class="container secondary-page">
      <div class="general general-results">
           <div id="rrResult" class="top-score-title right-score total-reslts col-md-9">
                <h3>Переходы <span>игроков</span><span class="point-little">.</span></h3>
                <div class="main">
                        <div class="tabs animated-slide-2">

                                <table class="match-tbs">
');

for ($i = 0; $i < sizeof($transfer_list->transfers); $i++) {

    $id_player = $transfer_list->transfers[$i]->player->id_player;
    $id_competitor = $transfer_list->transfers[$i]->new_team->id_competitor;
    $res_player = mysql_query("SELECT *
	 FROM application
	 WHERE application.id_player = $id_player
             AND application.id_competitor = $id_competitor");
    $id_applicant = mysql_result($res_player, 0, "id_applicant");
    if ($transfer_list->transfers[$i]->player->photo_path == '') {
            $transfer_list->transfers[$i]->player->photo_path = "images/player/face.jpg";
        } else {
            $transfer_list->transfers[$i]->player->photo_path = "/photos/" . $transfer_list->transfers[$i]->player->photo_path;
        }
    
    $href = "window.location.href='player.php?p=" . $id_applicant ."'";
    
    if ($transfer_list->transfers[$i]->player->goalie == 1) {
        $goalie_label = " (GK)";
    } else {
        $goalie_label = "";
    }
        
    print('<tr onclick="' . $href . '; return false" class="match-sets"><td colspan="3" style="text-align: left;"><img style="max-height: 50px; margin: 5px;" src="' . $transfer_list->transfers[$i]->player->photo_path . '">' . $transfer_list->transfers[$i]->player->name_1 . ' ' . $transfer_list->transfers[$i]->player->name_2 . $goalie_label . '</td><td colspan="2" style="width:35%; text-align: right; white-space: nowrap; overflow: hidden; padding-right: 5px;">#' . $transfer_list->transfers[$i]->old_team->shortname . ' &#10145;
 #' . $transfer_list->transfers[$i]->new_team->shortname . '</td></tr>');
    
    print('<tr class="only-desktop"><td style="width: 25%; text-align: right; white-space: nowrap; overflow: hidden;">' . $transfer_list->transfers[$i]->old_team->title . '&nbsp;<img src="/emblems/' . $transfer_list->transfers[$i]->old_team->team_details->emblem_path . '" style="max-height:25px; max-width:25px" alt="эмблема">&nbsp;</td><td colspan="2" style="width: 10%;"><img src="images/control_right.png" style="max-height: 25px;"></td><td style="width: 25%; text-align: left; white-space: nowrap; overflow: hidden;">&nbsp;<img src="/emblems/' . $transfer_list->transfers[$i]->new_team->team_details->emblem_path . '"  style="max-height:25px; max-width:25px" alt="эмблема">&nbsp;' . $transfer_list->transfers[$i]->new_team->title . '</a></td><td class="only-desktop" style="width: 30%;"><i class="fa fa-calendar-o"></i> ' . $transfer_list->transfers[$i]->date . '</td></tr>');
}

print('
                                 </table> 
                            </div>
                            <div class="score-view-all"></div>
                        </div>
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>
<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>

 <script src="js/custom.js" type="text/javascript"></script>   
');