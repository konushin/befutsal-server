﻿<?php

$class_active["season"] = "active";
include("header.php");

$id_competitor = (int)$_GET['c'];

$competitor = new TeamCompetitor();
$competitor->getTeamCompetitor($id_competitor);

//информация о команде
$team = new TeamInfo();
$team->getTeamInfoByCompetitor($id_competitor);

if ($team->founded == NULL) {
    $team->founded = 'Неизвестно';
}

//информация о матчах
$res_matches = mysql_query("SELECT * FROM protocol WHERE team_1 = $id_competitor OR team_2 = $id_competitor ORDER BY dt_protocol");
$n_matches = mysql_num_rows($res_matches);

//--------------------информация о выступлении команды-------------------------------

//----------победы
$res_won = mysql_query("SELECT * FROM protocol WHERE (team_1 = $id_competitor AND score_end_1 > score_end_2) OR (team_2 = $id_competitor AND score_end_1 < score_end_2)");
$won = mysql_num_rows($res_won);

//----------ничьи
$res_drew = mysql_query("SELECT * FROM protocol WHERE (team_1 = $id_competitor OR team_2 = $id_competitor) AND score_end_1 = score_end_2 AND forfeited IS NULL");
$drew = mysql_num_rows($res_drew);

//----------поражения
$res_lost = mysql_query("SELECT * FROM protocol WHERE (team_1 = $id_competitor AND score_end_1 < score_end_2) OR (team_2 = $id_competitor AND score_end_1 > score_end_2)");
$lost = mysql_num_rows($res_lost);

//----------технические поражения
$res_forfeited = mysql_query("SELECT * FROM protocol WHERE (team_1 = $id_competitor OR team_2 = $id_competitor) AND forfeited = 1");
$forfeited = mysql_num_rows($res_forfeited);
$lost_forfeited = $lost + $forfeited;

//----------игр и очков
$played = $won + $drew + $lost + $forfeited;
$points = $won * 3 + $drew;

//----------забито
$res_for_home = mysql_query("SELECT SUM(score_end_1) AS sc_home FROM protocol WHERE team_1 = $id_competitor");
$res_for_away = mysql_query("SELECT SUM(score_end_2) AS sc_away FROM protocol WHERE team_2 = $id_competitor");
$goalsfor = (int)mysql_result($res_for_home, 0, "sc_home") + (int)mysql_result($res_for_away, 0, "sc_away");

//----------пропущено
$res_against_home = mysql_query("SELECT SUM(score_end_2) as ms_home FROM protocol WHERE team_1 = $id_competitor");
$res_against_away = mysql_query("SELECT SUM(score_end_1) as ms_away FROM protocol WHERE team_2 = $id_competitor");
$goalsagainst = (int)mysql_result($res_against_home, 0, "ms_home") + (int)mysql_result($res_against_away, 0, "ms_away") + $forfeited * 5;

if ($team->tshirt_home != NULL) {
    $tshirt_home = new Tshirt();
    $tshirt_home->getTshirtDetails($team->tshirt_home);
    $tshirt_home_toprint = '<img src="/graphics/' . $tshirt_home->picture . '">';
} else {
    $tshirt_home_toprint = null;
}

if ($team->tshirt_away != NULL) {
    $tshirt_away = new Tshirt();
    $tshirt_away->getTshirtDetails($team->tshirt_away);
    $tshirt_away_toprint = '<img src="/graphics/' . $tshirt_away->picture . '">';
} else {
    $tshirt_away_toprint = null;
}

print('
    <section class="drawer">
        <div class="col-md-12 size-img back-img-match">
            <div class="effect-cover">
                <h3 class="txt-advert animated">' . $competitor->title . '</h3>
                <p class="txt-advert-sub">' . $competitor->competition_details->category[0]->title . '. ' . $competitor->competition_details->title . '</p>
            </div>
        </div>
    
    <section id="single_player" class="container secondary-page">
      <div class="general general-results players">
           <div class="top-score-title right-score col-md-9">
                <h3>Выступление <span>в турнире</span><span class="point-little">.</span></h3>
                <div class="col-md-12 atp-single-player">
                  <img class="img-djoko" src="/emblems/' . $team->emblem_path . '" alt="Эмблема" />
                  <div class="col-md-2 data-player">
                     <p>Основана:</p>
                     <p></p>
                  </div>
                  <div class="col-md-3 data-player-2">
                     <p>' . $team->founded . '</p>
                     <p>' . $tshirt_home_toprint . ' ' . $tshirt_away_toprint . '</p>
                  </div>
');

if ($team->rank > 0) {
    print('
                  <div class="col-md-2 rank-player">
                     <div class="content-rank"><p class="rank-data">' . $team->rank . '</p></div>
                     <p class="rank-title">в рейтинге befutsal</p>
                  </div>
                                      
');
}

print('
                </div>
');

if ($played > 0) {
    $won_percentage = round(($won / $played) * 100);
    $drew_percentage = round(($drew / $played) * 100);
    $lost_percentage = round(($lost / $played) * 100);
    $pts_percentage = round(($points / ($played * $competitor->competition_details->win_points)) * 100);
    $goals_percentage = round(($goalsfor / ($goalsfor + $goalsagainst)) * 100);

    $res_scheduled = mysql_query("SELECT COUNT(*) AS scheduled FROM schedule WHERE team_1 = $id_competitor OR team_2 = $id_competitor");
    $scheduled = mysql_result($res_scheduled, 0, "scheduled");
    $played_percentage = round(($played / $scheduled) * 100);

    print('
                <div class="col-md-12 atp-single-player skill-content">
                     <div class="exp-skill">
                            <p class="exp-title-pp">ПОКАЗАТЕЛИ</p>
                            <div class="skills-pp">
                            <div class="skillbar-pp clearfix" data-percent="' . $played_percentage . '%">
                                <div class="skillbar-title-pp"><span>Матчей: ' . $played . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $played_percentage . '% из календаря</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $won_percentage . '%">
                                <div class="skillbar-title-pp"><span>Побед: ' . $won . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $won_percentage . '%</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $drew_percentage . '%">
                                <div class="skillbar-title-pp"><span>Ничьих: ' . $drew . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $drew_percentage . '%</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $lost_percentage . '%">
                                <div class="skillbar-title-pp"><span>Поражений: ' . $lost . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $lost_percentage . '%</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $pts_percentage . '%">
                                <div class="skillbar-title-pp"><span>Очков: ' . $points . '</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $pts_percentage . '%</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $goals_percentage . '%">
                                <div class="skillbar-title-pp"><span>Мячей: ' . $goalsfor . ' забито</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $goalsagainst . ' пропущено</div>
                            </div>
                        </div>
                    </div>
             </div>
');
}

if ($competitor->team_details->comments == null) {
    $competitor->team_details->comments = "История пишется... <br>Но ты можешь помочь - присылай ее на info@befutsal.ru или в <a href='https://vk.com/befutsal' target='_blank'>группу ВК</a>.";
}

print('
               <div class="col-md-12 atp-single-player skill-content" style="z-index: 1000;">
                      <div class="ppl-desc">
                        <p class="exp-title-pp">О КОМАНДЕ</p>
                        <p>' . $competitor->team_details->comments . '</p>
                        <br><a href="club.php?t=' . $team->id_team . '" class="ca-more" style="padding-right: 20px;"><i class="fa fa-angle-double-right"></i>Профиль клуба</a>
                    </div>
            </div>
');

$res_plrs = mysql_query("SELECT player.*, application.*, ban.ban_start
 FROM player, application
 LEFT JOIN ban USING (id_applicant)
 WHERE player.id_player = application.id_player
  AND application.id_competitor = $competitor->id_competitor
  AND application.active = 1
 ORDER BY application.goalie DESC, IF(number IS NULL,1,0), number, name_2, name_1");
$n_plrs = mysql_num_rows($res_plrs);

if ($n_plrs > 0) {
    print('
           <div class="top-score-title right-score col-md-12">
                <h3>СОСТАВ <span>команды</span><span class="point-little">.</span></h3>
');

    for ($i = 0; $i < $n_plrs; $i++) {
        $id_player = mysql_result($res_plrs, $i, "id_player");
        $id_applicant = mysql_result($res_plrs, $i, "id_applicant");
        $name_1 = mysql_result($res_plrs, $i, "name_1");
        $name_2 = mysql_result($res_plrs, $i, "name_2");
        $photo_path = mysql_result($res_plrs, $i, "photo_path");
        if ($photo_path == '') {
            $photo_path = "images/player/face.jpg";
        } else {
            $photo_path = "/photos/" . $photo_path;
        }
        $goalie = mysql_result($res_plrs, $i, "goalie");
        if ($goalie == 1) {
            $goalie_label = "(GK)";
        } else {
            $goalie_label = "";
        }

        $query = "SELECT COUNT(staff.id_player) AS played, SUM(scored) AS scored, SUM(yellow) AS booked, SUM(red) AS sentoff FROM staff WHERE id_player = $id_applicant";
        $res_performance = mysql_query($query);
        $played = (int)mysql_result($res_performance, 0, "played");
        $scored = (int)mysql_result($res_performance, 0, "scored");
        $booked = (int)mysql_result($res_performance, 0, "booked");
        $sentoff = (int)mysql_result($res_performance, 0, "sentoff");

        if ($played > 0) {
            $played_label = "&#128085;" . $played;
        } else {
            $played_label = "";
        }

        if ($scored > 0) {
            $scored_label = "&#9917;" . $scored;
        } else {
            $scored_label = "";
        }

        if ($booked > 0) {
            $booked_label = "&#129000;" . $booked;
        } else {
            $booked_label = "";
        }

        print('
                <div class="col-md-3 atp-player">
                  <a href="player.php?p=' . $id_applicant . '"><img src="' . $photo_path . '" alt="" width="148" height="198" />
                  <p>' . $name_1 . '<br>' . $name_2 . ' ' . $goalie_label . '<br>' . $played_label . ' ' . $scored_label . ' ' . $booked_label . '</p></a>
                </div>');
    }
}

$query = "SELECT DATE_FORMAT(date, '%d.%m %a') as s_date, s.*, p.*
FROM schedule s
 LEFT JOIN protocol p ON s.id_schedule = p.id_schedule 
WHERE s.team_1 = $id_competitor OR s.team_2 = $id_competitor
ORDER BY s.date, s.time";
$res_schedule = mysql_query($query);
$n_schedule = mysql_num_rows($res_schedule);

if ($n_schedule > 0) {
    print('
           <div class="col-md-12 atp-single-player skill-content">
                <h3><i class="fa fa-calendar-o"></i>&nbsp;РАСПИСАНИЕ <span>матчей</span><span class="point-little">.</span></h3>
                
                <div id="fixtures" class="tab active">
                     <table class="match-tbs">
                          <tr><td class="match-tbs-title" colspan="5">' . $competitor->competition_details->title . '</td></tr>
');
    for ($i = 0; $i < $n_schedule; $i++) {
        $team_1 = mysql_result($res_schedule, $i, "s.team_1");
        $team_2 = mysql_result($res_schedule, $i, "s.team_2");

        $competitor_home = new TeamCompetitor();
        $competitor_home->getTeamCompetitor($team_1);

        $competitor_away = new TeamCompetitor();
        $competitor_away->getTeamCompetitor($team_2);

        $time = mysql_result($res_schedule, $i, "s.time");
        $tour = mysql_result($res_schedule, $i, "s.tour");
        $id_schedule = mysql_result($res_schedule, $i, "s.id_schedule");
        $pitch = mysql_result($res_schedule, $i, "s.pitch");
        $date = mysql_result($res_schedule, $i, "s_date");
        $day = (int)substr($date, 0, 2);
        $n_month = substr($date, 3, 2);
        $eng_week_day = substr($date, 6, 3);
        $date = $day . " " . ruMonth($n_month) . ", " . ruWeekDay($eng_week_day);
        $time = substr($time, 0, 5);
        $date = iconv('windows-1251', 'utf-8', $date);

        $id_protocol = mysql_result($res_schedule, $i, "p.id_protocol");
        if ($id_protocol != null) {
            $score_end_1 = mysql_result($res_schedule, $i, "score_end_1");
            $score_end_2 = mysql_result($res_schedule, $i, "score_end_2");

            $href = "window.location.href='match.php?p=" . $id_protocol . "'";

            $result = '<tr onclick="' . $href . '; return false"><td class="team1">' . cutString($competitor_home->title, 30) . '&nbsp;<img src="/emblems/' . $competitor_home->team_details->emblem_path . '" style="max-height:25px; max-width:25px" alt="эмблема">&nbsp;</a></td><td  class="score"><strong>' . $score_end_1 . '</strong></td><td class="score"><strong>' . $score_end_2 . '</strong></td><td class="team2">&nbsp;<img src="/emblems/' . $competitor_away->team_details->emblem_path . '"  style="max-height:25px; max-width:25px" alt="эмблема">&nbsp;' . cutString($competitor_away->title, 30) . '</a></td><td class="only-desktop" style="width: 30%;"><i class="fa fa-map-marker"></i>&nbsp;' . $pitch . '</td></tr>';
        } else {
            $href = "window.location.href='match_preview.php?s=" . $id_schedule . "'";

            $result = '<tr onclick="' . $href . '; return false"><td style="width: 25%; text-align: right; white-space: nowrap; overflow: hidden;">' . cutString($competitor_home->title, 30) . '&nbsp;<img src="/emblems/' . $competitor_home->team_details->emblem_path . '" style="max-height:25px; max-width:25px" alt="эмблема">&nbsp;</td><td colspan="2" style="width: 10%;"><i class="fa fa-calendar-o"></i> ' . $time . '</td><td style="width: 25%; text-align: left; white-space: nowrap; overflow: hidden;">&nbsp;<img src="/emblems/' . $competitor_away->team_details->emblem_path . '"  style="max-height:25px; max-width:25px" alt="эмблема">&nbsp;' . cutString($competitor_away->title, 30) . '</a></td><td class="only-desktop" style="width: 30%;"><i class="fa fa-map-marker"></i>&nbsp;' . $pitch . '</td></tr>';
        }

        print('                          <tr class="match-sets"><td colspan="5" style="text-align: left;">' . $date . '</td></tr>');
        print($result);

        //print('<div class="col-md-6"><i class="fa fa-calendar-o"></i> ' . $date . '&nbsp;<i class="fa fa-map-marker"></i> ' . $pitch . '</div><div class="col-md-5">' . $result . '</div><br><br>');
    }
    print('</table>');
    print('</div>');
}

print('</div>
            </div>
');

print('
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>

<!--PERCENTAGE-->
<script>
    $(function () {
        "use strict";
        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").width(0);
        });

        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").animate({
                width: $(this).attr("data-percent")
            }, 2000);
        });
    });
</script>
');