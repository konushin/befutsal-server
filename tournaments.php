﻿<?php

$class_active["history"] = "active";
$page_title = "Все сезоны";
$description = "Статистика турниров с 1985 года";
include("header.php");

//$res_seasons = $mysqli->query("SELECT * FROM season ORDER BY id_season DESC");
$res_seasons = mysql_query("SELECT * FROM season ORDER BY id_season DESC");

print('
    <section id="summary" class="container secondary-page">
      <div class="general general-results tournaments">
           
           <div id="c-calend" class="top-score-title right-score col-md-9">
                <h3><span>Все турниры</span> 1985 - ' . date("Y") . '<span class="point-little">.</span></h3>
');

for ($i = 0; $i < mysql_num_rows($res_seasons); $i++) {

    $season = new SeasonInfo();
    $season->id_season = mysql_result($res_seasons, $i, "id_season");
    $season->getSeasonInfo($season->id_season);
    $section_id = $i + 1;
    if ($section_id == 1) {
        $winner_header = "Лидер";
        $winner_class_style = "t4";
    } else {
        $winner_header = "Победитель";
        $winner_class_style = "t5";
    }

    print('
                <div class="accordion" id="section' . $section_id . '"><i class="fa fa-calendar-o"></i>' . $season->title . '<span></span></div>
                    <div class="acc-content">
                            <div class="col-md-1 acc-title"></div>
                            <div class="col-md-5 acc-title">Турнир</div>
                            <div class="col-md-2 acc-title">Команд</div>
                            <div class="col-md-4 acc-title">' . $winner_header . '</div>
                            <div class="acc-footer"></div>  
    ');

    $query = "SELECT C.id_competition, C.rate FROM competition C
          LEFT JOIN competition_by_category CBC
            ON CBC.id_competition = C.id_competition
        WHERE C.season = $season->id_season
         ORDER BY IFNULL(C.rate, 100), CBC.id_category ASC, C.title ASC";

    $res_leagues = mysql_query($query);
    for ($n = 0; $n < mysql_num_rows($res_leagues); $n++) {

        $competition = new Tournament();
        $competition->getCompetitionInfo(mysql_result($res_leagues, $n, "id_competition"));
        if ($competition->category[0]->title == null) {
            $competition->category[0]->picture = "category_default.png";
        } else {
            $competition->title = $competition->category[0]->title . ". " . $competition->title;
        }
        if ($competition->groups_end != null) {
            $tour_date = $competition->groups_end;
        } else {
            $tour_date = "9999-12-31 23:59:59";
        } 
        $res_table = getLeagueTable($competition->competition_id, $tour_date);
        $num_teams = mysql_num_rows($res_table);
        if ($num_teams > 0) {
            $winner = mysql_result($res_table, 0, "name");
            $winner_id = mysql_result($res_table, 0, "id_competitor");

            print('
                            
                            <div class="col-md-1 timg"><img src="graphics/' . $competition->category[0]->picture . '" alt="" /></div>
                            <div class="col-md-5 t1"><p><a href="' . $competition->link . '">' . $competition->title . '</a></p> </div>
                            <div class="col-md-2 t3"><p>' . $num_teams . '</p></div>
                            <div class="col-md-4 ' . $winner_class_style . '"><p><a href="">' . $winner . '</a></p></div>
                            <div class="acc-footer"></div>    
            ');
        }
    }
    
    print('</div>');

}

print('

           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>

<script src="js/jquery.accordion.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                "use strict";
                $(\'.accordion\').accordion({ defaultOpen: \'section1\' }); //some_id section1 in demo
            });
        });
    </script>
');