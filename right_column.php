<?php
/**
 * Created by PhpStorm.
 * User: Konushin
 * Date: 01.11.2020
 * Time: 22:54
 */

$wall = getVKWall(3);

print('
           <div class="col-md-3 right-column">
           <div class="top-score-title col-md-12 right-title">
                <h3>Последние новости</h3>');

for ($i = 0; $i < count($wall); $i++) {
    $header_len = 200;
    if (isset($wall[$i]->copy_history)) {
        $text = $wall[$i]->copy_history[0]->text;
    } else {
        $text = $wall[$i]->text;
    }
    $header = cutString($text, $header_len);
    $news_date = array();
    $news_date["day"] = date("d", $wall[$i]->date);
    $news_date["month"] = iconv('windows-1251', 'utf-8', ruMonth(date("m", $wall[$i]->date)));
    $news_date["year"] = date("Y", $wall[$i]->date);
    $news_date["time"] = date("H:i", $wall[$i]->date);

    print('

                <div class="right-content">
                    <p class="news-title-right">' . $news_date["day"] . ' ' . $news_date["month"] . ' ' . $news_date["year"] . '</p>
                    <p class="txt-right">' . nl2br($header) . '</p>
                    <a href="https://vk.com/wall-37375522_' . $wall[$i]->id . '" class="ca-more" target="_blank"><i class="fa fa-angle-double-right"></i>читать...</a>
                </div>
                ');


    //echo "<p><b>" . ($i + 1) . "</b>. <i>" . iconv('utf-8', 'windows-1251', $wall[$i]->text) . "</i><br /><span>" . date("Y-m-d H:i:s", $wall[$i]->date) . "</span>"; // Выводим записи
    //echo "<br /><span>https://vk.com/wall-{$group_id}_{$wall[$i]->id}</span></p><br>";
}

$vk_photos = getVKPhotos(9);

print('
          </div>
          <div class="top-score-title col-md-12">
            <a href="https://vk.com/reaktorwear" target="_blank"><img src="/graphics/banner_reaktor_1000x433.png" alt="" /></a>
          </div>
          <div class="top-score-title col-md-12 right-title">
                <h3>Фото</h3> 
                <ul class="right-last-photo">');

//var_dump($vk_photos[0]);

for ($i = 0; $i < sizeof($vk_photos); $i++) {

    $max_size = sizeof($vk_photos[$i]->sizes)-1;
    $picture_thumb = $vk_photos[$i]->sizes[3]->url;
    $picture_full = $vk_photos[$i]->sizes[$max_size]->url;

    print('
                        <li>
                            <div class="jm-item second">
							    <div class="jm-item-wrapper">
								    <div class="jm-item-image">
									    <img src="' . $picture_thumb . '" alt="" />
									    <div class="jm-item-description">
                                            <div class="jm-item-button">
                                                <a href="' . $picture_full . '" target="_blank"><i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
								    </div>	
							    </div>
						    </div>
                        </li>');
}


print('
                </ul>
          </div>
          </div>
           
       </div>
    </section>
');

?>