﻿<?php

if ($_GET["l"] == 224668385) {
    $class_active["wc22"] = "active";
} else {
    $class_active["season"] = "active";
}
$page_title = $league->title;
$description = "Статистика турнира " . $league->title;
include("header.php");

$id_league = $_GET["l"];

$league = new Tournament();
$league->getCompetitionInfo($id_league);
$league->getGroupsDates();
$group_date = $league->groups_end;
$playoff_date = $league->playoff_start;

//----формируем уникальное имя таблицы
$res_create = mysql_query("CREATE TEMPORARY TABLE temp_played_games (id_schedule INT, playoff INT, id_protocol INT, tour INT, tour_title VARCHAR(255), team_1 INT, team_2 INT, score_end_1 INT, score_end_2 INT, forfeited INT, on_penalty INT)");

//информация о матчах
$query_schedule = "SELECT * FROM schedule WHERE league = $id_league ORDER BY `date`, `time`";
$res_schedule = mysql_query($query_schedule);
$n_scheduled = mysql_num_rows($res_schedule);

for ($i = 0; $i < $n_scheduled; $i++) {
    $n_schedule = mysql_result($res_schedule, $i, "id_schedule");
    $tour = mysql_result($res_schedule, $i, "tour");
    $team_1 = mysql_result($res_schedule, $i, "team_1");
    $team_2 = mysql_result($res_schedule, $i, "team_2");
    $query_round = "SELECT * FROM cup_round WHERE id_competition = $id_league AND id_tour = $tour";
    $res_round = mysql_query($query_round);
    $tour_title = mysql_result($res_round, 0, "title");
    $res_protocol = mysql_query("SELECT * FROM protocol WHERE id_schedule = $n_schedule");
    $forfeited = null;
    $schedule_datetime = mysql_result($res_schedule, $i, "date") . " " . mysql_result($res_schedule, $i, "time");
    if ($schedule_datetime > $league->groups_end) {
        $playoff = 1;
    } else {
        $playoff = 0;
    }
    if (mysql_num_rows($res_protocol) > 0) {
        $protocol = new Protocol();
        $protocol->getProtocolData($res_protocol, 0);
        $query = "INSERT INTO temp_played_games VALUES ($n_schedule, $playoff, $protocol->id_protocol, $tour, '$tour_title', $team_1, $team_2, $protocol->score_end_1, $protocol->score_end_2, "; //начало SQL-запроса
        if ($protocol->forfeited == 1) {
            $query = $query . "1, NULL)";
        } else {
            if ($protocol->on_penalty == 1) {
                $query = $query . "NULL, 1)";
            } else {
                $query = $query . "NULL, NULL)";
            }
        }

    } else {
        $query = "INSERT INTO temp_played_games VALUES ($n_schedule, $playoff, 0, $tour, '$tour_title', $team_1, $team_2, 0, 0, null, null)";
    }
    //попытка записать во временную таблицу
    $res_ins = mysql_query($query);
}

print('
  <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
                <div class="effect-cover">
                    <h3 class="txt-advert animated">' . $league->category[0]->title . '<span class="point-little">.</span></h3>
                </div>
           </div>
    
    <section id="allmatch" class="container secondary-page">
      <div class="general general-results">
           <div class="top-score-title right-score col-md-9">
                <h3>' . $league->title . '</span></h3>
                <div class="main">
                        <div class="tabs animated-slide-2 matches-tbs">
                            <ul class="tab-links-matches">');

$query = "SELECT DISTINCT (`group`) FROM tournament_competitor WHERE id_competition = $league->competition_id ORDER BY `group`";
$res_groups_list = mysql_query($query);

$GroupsList = array();

for ($i = 0; $i < mysql_num_rows($res_groups_list); $i++) {
    $group_title = mysql_result($res_groups_list, $i, "group");

    $TeamsInGroup = array();
    array_push($TeamsInGroup, $group_title);

    $query = "SELECT * FROM tournament_competitor WHERE `group` = '$group_title' and id_competition = $league->competition_id";
    $res_group_teams = mysql_query($query);
    for ($n = 0; $n < mysql_num_rows($res_group_teams); $n++) {
        $competitor_id = mysql_result($res_group_teams, $n, "id_competitor");
        array_push($TeamsInGroup, $competitor_id);
    }

    array_push($GroupsList, $TeamsInGroup);

    print('<li><a href="#group' . $i . '">' . $group_title . '</a></li>');
}

print('<li><a href="#groups_schedule">Расписание</a></li>');

$res_tours = mysql_query("SELECT DISTINCT tour_title FROM temp_played_games WHERE playoff=1 ORDER BY tour");
$n_tours = mysql_num_rows($res_tours);
$tours = array();

for ($i = 0; $i < $n_tours; $i++) {
    $tour_title = mysql_result($res_tours, $i, "tour_title");
    if ($i == 0) {
        print('<li class="active">');
    } else {
        print('<li>');
    }
    print('<li><a href="#tour' . $i . '">' . $tour_title . '</a></li>');
}

$res_scorers = topScorers($id_league, "LIMIT 0, 10");
$n_scorers = mysql_num_rows($res_scorers);
if ($n_scorers > 0) {
    print('
                                <li><a href="#topscorers">Бомбардиры</a></li>');
}

$res_goalies = mysql_query("SELECT * FROM application, competitor, staff WHERE application.id_competitor = competitor.id_competitor AND competitor.id_competition = $id_league AND application.goalie = 1 AND application.active = 1 AND staff.id_player = application.id_applicant");
$n_goalies = mysql_num_rows($res_goalies);
if ($n_goalies > 0) {
    print('
                                <li><a href="#goalies">Вратари</a></li>');
}

$now_date = date("Y-m-d");
$res_bans = getBans($now_date, $id_league);
$num_bans = mysql_num_rows($res_bans);
if ($num_bans > 0) {
    print('
                                <li><a href="#bans">Дисквалификации</a></li>');
}

print('
                            </ul>
                            <div class="tab-content">
');

//ОТОБРАЖАЕМ ГРУППЫ
for ($i = 0; $i < count($GroupsList); $i++) {
    $group_title = mysql_result($res_groups_list, $i, "group");

    print('
                                <div id="group' . $i . '" class="tab active">
                                   <table class="match-tbs">
                                         <tr><td class="match-tbs-title" colspan="9">' . $group_title . '</td></tr>
                                         <tr class="match-sets"><td>#</td><td></td><td></td><td class="fpt">Очки</td><td class="fpt">Игры</td><td class="fpt">В</td><td class="fpt">Н</td><td class="fpt">П</td><td class="only-desktop">Мячи</td></tr>
');


    //получаем таблицу лиги на момент окончания группового раунда
    $res_table = getLeagueTable($league->competition_id, $group_date);
    $group_table = new TournamentTable();
    $group_table->table_items = array();
    $place = 0;

    //перебираем таблицу, находим в ней команды из группы, формируем таблицу по очкам
    for ($p = 0; $p < mysql_num_rows($res_table); $p++) {
        $table_team_id = mysql_result($res_table, $p, "id_competitor");

        for ($j = 1; $j < count($GroupsList[$i]); $j++) {
            $ident_team = (int)$GroupsList[$i][$j];

            if ($table_team_id == $ident_team) {

                $place++;
                $team = mysql_result($res_table, $p, "name");
                if (strlen($team) > 20) {
                    $s_team = substr($team, 0, 17) . "...";
                } else {
                    $s_team = $team;
                }

                $TableItem = new TableItem();

                $TableItem->team_id = $ident_team;
                $TableItem->position = $place;
                $TableItem->title = $team;
                $competitor = new TeamInfo();
                $competitor->getTeamInfoByCompetitor($ident_team);
                $TableItem->emblem_path = $competitor->emblem_path;
                $TableItem->pld = mysql_result($res_table, $p, "pld");
                $TableItem->won = mysql_result($res_table, $p, "won");
                $TableItem->drew = mysql_result($res_table, $p, "drew");
                $TableItem->lost = mysql_result($res_table, $p, "lost");
                $TableItem->scored = mysql_result($res_table, $p, "goalsfor");
                $TableItem->missed = mysql_result($res_table, $p, "goalsagainst");
                $TableItem->pts = mysql_result($res_table, $p, "pts");

                $group_table->table_items[$place] = $TableItem;
            }
        }
    }

    //выводим на экран
    for ($j = 1; $j <= count($group_table->table_items); $j++) {

        $table_item = new TableItem();
        $table_item = $group_table->table_items[$j];

        $goalsd = $table_item->scored - $table_item->missed;
        if ($goalsd > 0) {
            $goalsd = "+" . $goalsd;
        }

        $href = "window.location.href='competitor.php?c=" . $table_item->team_id . "'";

        print('
                                        <tr onclick="' . $href . '; return false"><td class="fpt">' . $j . '</td><td><img src="/emblems/' . $table_item->emblem_path . '" style="max-height: 25px;"></td><td class="fpt" style="text-align: left; padding-left: 2px;">' . $table_item->title . '</td><td class="fpt"><b>' . $table_item->pts . '</b></td><td class="fpt">' . $table_item->pld . '</td><td class="fpt">' . $table_item->won . '</td><td class="fpt">' . $table_item->drew . '</td><td class="fpt">' . $table_item->lost . '</td><td class="only-desktop">' . $table_item->scored . '-' . $table_item->missed . ' (' . $goalsd . ')</td></tr>
');
    }

    print('
                                     </table>
                                </div>');
}

//РАСПИСАНИЕ ГРУППОВОГО РАУНДА

$res_schedule = mysql_query("SELECT * FROM temp_played_games WHERE playoff=0 ORDER BY id_schedule");
$n_schedule = mysql_num_rows($res_schedule);

print('
                                <div id="groups_schedule" class="tab active">
                                  <table class="match-tbs">
                                    <tr><td class="match-tbs-title" colspan="5">Расписание группового раунда</td></tr>');

$prev_date = 0;

for ($i = 0; $i < $n_schedule; $i++) {
    $team_1 = mysql_result($res_schedule, $i, "team_1");
    $team_2 = mysql_result($res_schedule, $i, "team_2");

    $competitor_home = new TeamCompetitor();
    $competitor_home->getTeamCompetitor($team_1);

    $competitor_away = new TeamCompetitor();
    $competitor_away->getTeamCompetitor($team_2);

    $id_protocol = mysql_result($res_schedule, $i, "id_protocol");
    $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
    $tour = mysql_result($res_schedule, $i, "tour_title");
    $query = "SELECT DATE_FORMAT(date, '%d.%m %a') as s_date, s.*
                  FROM schedule s
                  WHERE s.id_schedule = $id_schedule";
    $res_match = mysql_query($query);
    $time = substr(mysql_result($res_match, 0, "s.time"), 0, 5);
    $date = mysql_result($res_match, 0, "s_date");
    $day = (int)substr($date, 0, 2);
    $n_month = substr($date, 3, 2);
    $eng_week_day = substr($date, 6, 3);
    $date = $day . " " . ruMonth($n_month) . ", " . ruWeekDay($eng_week_day);
    $date = iconv('windows-1251', 'utf-8', $date);
    $pitch = mysql_result($res_match, 0, "s.pitch");
    $forfeited = mysql_result($res_schedule, $i, "forfeited");
    $on_penalty = mysql_result($res_schedule, $i, "on_penalty");

    $group_label = "<span style='background: #EAECEE; padding-left: 4px; padding-right: 4px; text-align: left;'>" . mysql_result(mysql_query("SELECT * FROM tournament_competitor WHERE id_competitor = $competitor_home->id_competitor"), 0, "group") . "</span><br>";

    if ($date != $prev_date) {
        print('                                    <tr class="match-sets"><td colspan="5" style="text-align: left;">' . $date . '</td></tr>');
    }

    include("include_matchlist.php"); //перед вызовом должен быть задан $id_protocol

    $prev_date = $date;

    unset($group_label);
}

print('
                                     </table>
                                </div>');

//ПЛЕЙОФФ
for ($n = 0; $n < $n_tours; $n++) {
    $tour = mysql_result($res_tours, $n, "tour_title");
    $res_schedule = mysql_query("SELECT * FROM temp_played_games WHERE tour_title='$tour' ORDER BY id_schedule");
    $n_schedule = mysql_num_rows($res_schedule);

    print('
                                <div id="tour' . $n . '" class="tab active">
                                  <table class="match-tbs">
                                    <tr><td class="match-tbs-title" colspan="7">' . $tour . '</td></tr>');

    $prev_date = 0;

    for ($i = 0; $i < $n_schedule; $i++) {
        $team_1 = mysql_result($res_schedule, $i, "team_1");
        $team_2 = mysql_result($res_schedule, $i, "team_2");

        $competitor_home = new TeamCompetitor();
        $competitor_home->getTeamCompetitor($team_1);

        $competitor_away = new TeamCompetitor();
        $competitor_away->getTeamCompetitor($team_2);

        $id_protocol = mysql_result($res_schedule, $i, "id_protocol");
        $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
        $query = "SELECT DATE_FORMAT(date, '%d.%m %a') as s_date, s.*
                  FROM schedule s
                  WHERE s.id_schedule = $id_schedule";
        $res_match = mysql_query($query);
        $time = substr(mysql_result($res_match, 0, "s.time"), 0, 5);
        $date = mysql_result($res_match, 0, "s_date");
        $day = (int)substr($date, 0, 2);
        $n_month = substr($date, 3, 2);
        $eng_week_day = substr($date, 6, 3);
        $date = $day . " " . ruMonth($n_month) . ", " . ruWeekDay($eng_week_day);
        $date = iconv('windows-1251', 'utf-8', $date);
        $pitch = mysql_result($res_match, 0, "s.pitch");
        $forfeited = mysql_result($res_schedule, $i, "forfeited");
        $on_penalty = mysql_result($res_schedule, $i, "on_penalty");

        if ($date != $prev_date) {
            print('                                    <tr class="match-sets"><td colspan="7" style="text-align: left;">' . $date . '</td></tr>');
        }

        include("include_matchlist.php"); //перед вызовом должен быть задан $id_protocol

        $prev_date = $date;
    }

    print('
                                     </table>
                                </div>');
}

//БОМБАРДИРЫ
if ($n_scorers > 0) {
    include("include_scorers.php");

    print ("</div>");
}

//ВРАТАРИ
if ($n_goalies > 0) {
    include("include_goalies.php");

    print ("</div>");
}

//ДИСКВАЛИФИКАЦИИ
//перед вызовом должен быть определен $res_bans (см. в блоке меню)

    include("include_bans.php");




print('


                    </div>
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include("footer.php");

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');