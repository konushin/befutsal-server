﻿<?php
$class_active["season"] = "active";
$page_title = "Протокол матча";
$description = "Протокол матча, составы, голы, наказания";
include("header.php");

/** Отображение состава в превью матча
 * @param bool $home TRUE если отображаем хояев, FALSE если отображаем гостей
 * @return bool Выводит на экран список игроков
 */
function displayStaff($home) {
    global $detailedProtocol, $res_staff_home, $res_staff_away; //видимость всех переменных внутри функции

    if ($home) {
        $staff = $detailedProtocol->players_home;
        $res_staff = $res_staff_home;
    } else {
        $staff = $detailedProtocol->players_away;
        $res_staff = $res_staff_away;
    }

    for ($i = 0; $i < sizeof($staff); $i++) {
        if ($staff[$i]->number == null) {
            $staff[$i]->number = "&nbsp;";
        }
        print('<p>' . $staff[$i]->number . '</p>');
    }

    print('                                            </div>
                                                   <div class="col-md-7 single-match-data">
                                                     <p class="nm-player">Игрок</p>
');

    for ($i = 0; $i < sizeof($staff); $i++) {
        print('<p style="text-align: left; padding-left: 20px;"><a href="player.php?p=' . mysql_result($res_staff, $i, "id_applicant") . '">');

        print($staff[$i]->name_1 . ' ' . $staff[$i]->name_2 . '</a>');

        if ($staff[$i]->goalie == 1) {
            print('&nbsp;&#129508;'); //перчатки
        }

        if ($staff[$i]->captain == 1) {
            print('C'); //капитан
        }

        print('</p>');
    }

    print('                                            </div>
                                                   <div class="col-md-4 single-match-data">
                                                     <p class="nm-player">Статистика</p>
');

    for ($i = 0; $i < sizeof($staff); $i++) {
        print('<p>');

        for ($n = 0; $n < $staff[$i]->scored_in_match; $n++) {
            print('&#9917;');
        }


        if ($staff[$i]->owngoal > 0) {
            if ($staff[$i]->owngoal > 1) {
                print($staff[$i]->owngoal);
            }
            print('<img src="../graphics/owngoal.png" height="14px" title="Автогол">');
        } else {
            print('&nbsp;');
        }

        if ($staff[$i]->booked > 0) {
            print(' <img src="/graphics/yellow_card.gif" height=12px>');
        } else {
            print('&nbsp;');
        }

        if ($staff[$i]->sentoff > 0) {
            print(' <img src="/graphics/red_card.gif" height=12px>');
        } else {
            print('&nbsp;');
        }

        print('</p>');
    }

    return TRUE;
}

$id_protocol = $_GET["p"];

$res_protocol = mysql_query("SELECT * FROM protocol WHERE id_protocol = $id_protocol");

$protocol = new Protocol();
$protocol->getProtocolData($res_protocol, 0);
$protocol->team_1 = mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $protocol->id_team_1"), 0, "title");
if ($protocol->team_1_emblem == null) {
    $protocol->team_1_emblem = "emblems/default_emblem.jpg";
}
$protocol->team_2 = mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $protocol->id_team_2"), 0, "title");
if ($protocol->team_2_emblem == null) {
    $protocol->team_2_emblem = "emblems/default_emblem.jpg";
}
$protocol->ref_1 = mysql_result($res_protocol, 0, "ref_1");
$protocol->ref_2 = mysql_result($res_protocol, 0, "ref_2");
if ($protocol->ref_1 > 0) {
    $protocol->ref_1 = mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $protocol->ref_1"), 0, "name_1") . " " . mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $protocol->ref_1"), 0, "name_2");
} else {
    $protocol->ref_1 = null;
}
if ($protocol->ref_2 > 0) {
    $protocol->ref_2 = mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $protocol->ref_2"), 0, "name_1") . " " . mysql_result(mysql_query("SELECT * FROM referee WHERE id_referee = $protocol->ref_2"), 0, "name_2");
} else {
    $protocol->ref_2 = null;
}
$time = substr($protocol->dt_protocol, 11, 5);
$year = substr($protocol->dt_protocol, 0, 4);
$month = substr($protocol->dt_protocol, 5, 2);
$day = (int) substr($protocol->dt_protocol, 8, 2);

$res_schedule = mysql_query("SELECT * FROM protocol, schedule WHERE id_protocol = $id_protocol AND schedule.id_schedule = protocol.id_schedule");
$schedule = new Schedule();
$schedule->id_schedule = mysql_result($res_schedule, 0, "schedule.id_schedule");
$schedule->pitch = mysql_result($res_schedule, 0, "schedule.pitch");
$schedule->league = mysql_result($res_schedule, 0, "schedule.league");
$schedule->team_1 = new TeamCompetitor();
$schedule->team_1->getTeamCompetitor(mysql_result($res_schedule, 0, "schedule.team_1"));
$schedule->team_2 = new TeamCompetitor();
$schedule->team_2->getTeamCompetitor(mysql_result($res_schedule, 0, "schedule.team_2"));

$league = new Tournament();
$league->getCompetitionInfo($schedule->league);

$staff_home_query = "SELECT *
FROM staff AS s, application AS a
WHERE s.id_protocol = $protocol->id_protocol
      AND a.id_competitor = $protocol->id_team_1
      AND s.id_player = a.id_applicant
ORDER BY a.goalie DESC, IF(a.number IS NULL,1,0), a.number";

$staff_away_query = "SELECT *
FROM staff AS s, application AS a
WHERE s.id_protocol = $protocol->id_protocol
      AND a.id_competitor = $protocol->id_team_2
      AND s.id_player = a.id_applicant
ORDER BY a.goalie DESC, IF(a.number IS NULL,1,0), a.number";

$res_staff_home = mysql_query($staff_home_query);
$res_staff_away = mysql_query($staff_away_query);

//формируем объект
$detailedProtocol = new DetailedProtocol();
$detailedProtocol->protocol = $protocol;
$detailedProtocol->players_home = array();
$player_list = new PlayerList();
$player_list->getPlayers($protocol->id_team_1);
$detailedProtocol->players_home = $player_list->players;
$detailedProtocol->players_home = $detailedProtocol->getPlayerInGame($res_staff_home);
for ($i = 0; $i < mysql_num_rows($res_staff_home); $i++) {
    $id_applicant = mysql_result($res_staff_home, $i, "id_applicant");
    $query = "SELECT * 
              FROM player AS p, application AS a
              WHERE a.id_applicant = $id_applicant
                  AND a.id_player = p.id_player";
    $res_player = mysql_query($query);
    $detailedProtocol->players_home[$i]->name_1 = mysql_result($res_player, 0, "p.name_1");
    $detailedProtocol->players_home[$i]->name_2 = mysql_result($res_player, 0, "p.name_2");
    $detailedProtocol->players_home[$i]->season_performance = playerPerformanceInCompetition($id_applicant);
}

$detailedProtocol->players_away = array();
$player_list = new PlayerList();
$player_list->getPlayers($protocol->id_team_2);
$detailedProtocol->players_away = $player_list->players;
$detailedProtocol->players_away = $detailedProtocol->getPlayerInGame($res_staff_away);
for ($i = 0; $i < mysql_num_rows($res_staff_away); $i++) {
    $id_applicant = mysql_result($res_staff_away, $i, "id_applicant");
    $query = "SELECT * 
              FROM player AS p, application AS a
              WHERE a.id_applicant = $id_applicant
                  AND a.id_player = p.id_player";
    $res_player = mysql_query($query);
    $detailedProtocol->players_away[$i]->name_1 = mysql_result($res_player, 0, "p.name_1");
    $detailedProtocol->players_away[$i]->name_2 = mysql_result($res_player, 0, "p.name_2");
    $detailedProtocol->players_away[$i]->season_performance = playerPerformanceInCompetition($id_applicant);
}

if ($protocol->forfeited != NULL) {
    if ($protocol->score_end_1 == 0) {
        $protocol->score_end_1 = "-";
    } elseif ($protocol->score_end_1 > 0) {
        $protocol->score_end_1 = "+";
    }

    if ($protocol->score_end_2 == 0) {
        $protocol->score_end_2 = "-";
    } elseif ($protocol->score_end_2 > 0) {
        $protocol->score_end_2 = "+";
    }

    $home_result = $protocol->score_end_1;
    $away_result = $protocol->score_end_2;
} else {
    $home_result = $protocol->score_end_1 . ' (' . $protocol->score_half_1 . ')';
    $away_result = $protocol->score_end_2 . ' (' . $protocol->score_half_2 . ')';
}

$match_score = '<td class="fpt">' . $home_result . '</td>';
if ($protocol->extratime == 1) {
    $match_score = $match_score . '<td class="fpt">доп.<br>время</td>';
}
if ($protocol->on_penalty == 1) {
    $match_score = $match_score . '<td class="fpt">по<br>пенальти</td>';
}
$match_score = $match_score . '<td class="fpt">' . $away_result . '</td>';

print('
   <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
                <div class="effect-cover">
                    <h3 class="txt-advert animated">' . $protocol->team_1 . ' - ' . $protocol->team_2 . '</h3>
                    <p class="txt-advert-sub">' . $league->category[0]->title . '. ' . $league->title . '</p>
                </div>
           </div>
    
    <section id="single-match-pl" class="container secondary-page">
      <div class="general general-results players">   
           <div class="top-score-title right-score col-md-9">
                <div class="top-score-title player-vs">
                   <h3>' . $schedule->pitch . ', <span>' . $day . '.' . $month . '.' . $year . ' ' . $time . '</span><span class="point-little">.</span></h3>
                        <div class="main">
                            <div class="tabs standard">
                                <div class="tab-content">
                                 <div class="effect-color">
                                    <div class="tab active">
                                            <div class="col-md-12 player-score-bg"></div>
                                            <div class="col-md-12 player-photo">
                                                <img class="img-face img-mm1" src="/emblems/' . $schedule->team_1->team_details->emblem_path . '" alt="" style="max-width: 75px; max-height: 75px"/>
                                                <img class="img-face img-mm2" src="/emblems/' . $schedule->team_2->team_details->emblem_path . '" alt=""  style="max-width: 75px; max-height: 75px"/>
                                            </div>
                                              <div class="col-md-12 play-vs">
                                                <div class="result-name">
                                                      <span>' . $protocol->team_1 . '</span> 
                                                      <span class="txt-vs">vs</span> 
                                                      <span>' . $protocol->team_2 . '</span>
                                                 </div>
                                              </div>
                                              <div class="clear"></div>
                                              <div class="col-md-12 play-vs">
                                                <table id="result-table">
                                                      <tr>' . $match_score . '</tr>');

print('
                                                 </table>
                                              </div>
                                    </div>   
                                   </div>
                                                                   
                                </div>                                
                            </div>

                            <div class="tabs standard single-pl">');

if (trim($protocol->ref_1) != "") {
    print('
                                <ul class="tab-links-match tb-set">
                                    <li class="active">Арбитры: </li>
                                    <li><a class="first-tabs">' . $protocol->ref_1 . '</a></li>
                                    <li><a>' . $protocol->ref_2 . '</a></li>
                                </ul>');
}

print('
                                <div class="tab-content single-match">                                
                                    <div id="tab1" class="tab active">
                                       <h3 class="tab-match-title">СТАТИСТИКА МАТЧА</h3>
                                        <div class="tabs standard">
                                            <ul class="tab-links-match">
                                                <li class="active"><a class="first-tabs" href="#tab1a">' . $protocol->team_1 . '</a></li>
                                                <li><a href="#tab2a">' . $protocol->team_2 . '</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab1a" class="tab active">
                                                   <div class="col-md-1 single-match-data">
                                                      <p class="nm-player">#</p>');

displayStaff(TRUE); //отображаем состав хозяев

print('
                                                </div>
                                                </div>
                                                <div id="tab2a" class="tab">
                                                   <div class="col-md-1 single-match-data">
                                                      <p class="nm-player">#</p>');

displayStaff(FALSE); //отображаем состав гостей

print('
                                                  </div>
                                                </div>
                                            </div>
                                 
                                        </div>     
                                    </div>
                                </div>
                                 
                            </div>
                            </div> 
                   </div>
           </div><!--Close Top Match-->
');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>
<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>

 <script src="js/custom.js" type="text/javascript"></script>   
');
