﻿<?php

$page_title = "Регистрация в befutsal";
$description = "Регистрация в befutsal";
include("header.php");

$post_username = mysql_real_escape_string(trim($_POST["name"]));
$post_email = mysql_real_escape_string(trim($_POST["email"]));
$post_password = md5(mysql_real_escape_string(trim($_POST["password"])));

$allowed = TRUE;

if (strlen($post_username) < 3 OR strlen($post_username) > 16) {
    $message = "ERROR: username=" . $post_username . ", length=" . strlen($post_username);
    writeLog($log_filename, $message);
    $allowed = FALSE;
    $error_msg = "Имя пользователя должно содержать от 3 до 16 символов";
    echo '<script>location.replace("login.php?reg_err=' . $error_msg . '#register");</script>';
    exit;
}

if (!filter_var($post_email, FILTER_VALIDATE_EMAIL, FILTER_VALIDATE_DOMAIN)) {
    $message = "ERROR: username=" . $post_username . ", email=" . $post_email;
    writeLog($log_filename, $message);
    $allowed = FALSE;
    $error_msg = "Некорректный формат e-mail";
    echo '<script>location.replace("login.php?reg_err=' . $error_msg . '#register");</script>';
    exit;
}

if ($post_password == "d41d8cd98f00b204e9800998ecf8427e") {
    $message = "ERROR: username=" . $post_username . ", password empty=" . $post_password;
    writeLog($log_filename, $message);
    $allowed = FALSE;
    $error_msg = "Пароль не может быть пустым";
    echo '<script>location.replace("login.php?reg_err=' . $error_msg . '#register");</script>';
    exit;
}


$query = "SELECT * FROM m_user WHERE `username` = '$post_username'";
$res_username = mysql_query($query);

if (mysql_num_rows($res_username) > 0) {
    $message = "ERROR: Trying to register existing username " . $post_username;
    writeLog($log_filename, $message);
    $allowed = FALSE;
    $error_msg = "Пользователь с таким именем уже зарегистрирован";
    echo '<script>location.replace("login.php?reg_err=' . $error_msg . '#register");</script>';
    exit;
}

$query = "SELECT * FROM m_user WHERE `email` = '$post_email'";
$res_email = mysql_query($query);

if (mysql_num_rows($res_email) > 0) {
    $message = "ERROR: Trying to register existing email " . $post_email;
    writeLog($log_filename, $message);
    $allowed = FALSE;
    $error_msg = "Пользователь с таким e-mail уже зарегистрирован";
    echo '<script>location.replace("login.php?reg_err=' . $error_msg . '#register");</script>';
    exit;
}

$MUser = new MUser();
$MUser->username = $post_username;
$MUser->email = $post_email;
$password = $post_password;

if ($allowed) {
    $code = uniqid();
    $current_time = time();

    $activated = getParameter("web_activation_off"); //если отключена активация, то активирован сразу
    $query = "INSERT INTO m_user (user_id, username, email, password, register_time, activation_code, activated) VALUES (null, '$MUser->username', '$MUser->email', '$password', $current_time, '$code', $activated)";
    $res_insert = mysql_query($query);

    if ($res_insert) {
        $MUser->user_id = mysql_insert_id();

        $message = "New user created via web: " . $MUser->username . ", user_id=" . $MUser->user_id . " activation code: " . $code;
        writeLog($log_filename, $message);

        if (getParameter("web_activation_off") == 0) {

            $email_subject = "Активация пользователя Живи мини-футболом!";
            $email_headers = 'Content-type: text/html; charset="utf-8"' . "\r\n";
            $email_headers .= 'From: ' . getParameter("email_from") . "\r\n";
            $email_message = "<BODY>
<FONT FACE=Tahoma SIZE=2>
<P ALIGN=left>Здравствуйте.</P>
<P ALIGN=left>Этот email был зарегистрирован на портале befutsal. Для активации пользователя пройдите по <A HREF='" . getParameter("url_mobile") . getParameter("mobile_app_email_activation_link") . $code . "'>ссылке</A> или скопируйте ее ниже и вставьте в строку браузера.</P>
<P ALIGN=left>" . getParameter("url_mobile") . getParameter("mobile_app_email_activation_link") . $code . "</P>
<P ALIGN=left><FONT SIZE=1>Если у вас остались вопросы или вы не регистрировались на портале befutsal, то мы приносим свои извинения, и просим пройти вас обратиться к администрации проекта <strong>Живи мини-футболом!</strong> письмом на адрес info@befutsal.ru.</FONT></P>
<P ALIGN=left>С уважением,<BR>коллектив сайта Живи мини-футболом!</P>
<P ALIGN=left><FONT SIZE=1>Это письмо сгенерировано роботом, не отвечайте на него!</FONT></P>
</FONT>
</BODY>";

            $sendmail = mail($MUser->email, $email_subject, $email_message, $email_headers);

            if ($sendmail) {
                $message = "Activation code for user " . $MUser->username . " sent to: " . $MUser->email;
                writeLog($log_filename, $message);
            } else {
                $message = "ERROR: Activation code for user " . $MUser->username . " was not sent: " . $MUser->email;
                writeLog($log_filename, $message);
            }
        }
    }
}

print('
    <section class="drawer">
            <div class="col-md-12 size-img back-img-match">
             <div class="effect-cover">
                    <h3 class="txt-advert animated">Живи мини-футболом!</h3>
                    <p class="txt-advert-sub animated">Ваши персональные интересы и предпочтения</p>
                 </div>
             </div>
      
    <section id="login" class="container secondary-page">  
      <div class="general general-results players">
      
      <!-- REGISTER BOX -->

           <div class="top-score-title right-score col-md-12">
            <h3>Остался <span>один шаг</span><span class="point-int"> !</span></h3>
                <div class="col-md-12 login-page login-w-page">
                   <p class="logiin-w-title">Мы хотим быть уверены, что под вашим именем не зарегистрировались недоброжелатели,</p>
                   <p>поэтому отправили письмо со ссылкой для подтверждения на указанный вами адрес: ' . $MUser->email . '</p>
                   <h3><img class="ball-tennis" src="graphics/bola.png" alt=""/>Подтвердите ваш email</h3>
                   <p>Просто пройдите по ссылке из письма. Если письмо не приходит, проверьте, что указали корректный email, и проверьте папку со спамом.</p>
                   <p>Если и после этого не получилось, пишите нам на <a href="mailto:support@befutsal.ru">support@befutsal.ru</a>, обязательно поможем.</p>
                </div>
                
           </div><!--Close REgistration-->
          </div> 
        </section>');

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>
');

?>