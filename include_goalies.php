<?php

print('
                                <div id="goalies" class="tab active">
                                   <table class="match-tbs">
                                         <tr><td class="match-tbs-title" colspan="6">Вратари</td></tr>
                                         <tr class="match-sets"><td>#</td><td></td><td></td><td class="fpt">&#128085;</td><td class="fpt">&#9917;</td><td class="fpt"><img src="graphics/cleansheet.png" border="0" title="На ноль"></td></tr>
');

//----формируем уникальное имя таблицы
$res_create = mysql_query("CREATE TEMPORARY TABLE temp_goalie (id_applicant INT NOT NULL UNIQUE, played INT(3), missed INT(3), avg_missed DOUBLE(5,1), clean_sheet INT(3))");

//выбираем инфо о вратарях
$query = "SELECT application.id_applicant, name_1, name_2, birthdate, application.id_competitor, title, transfer.*
FROM player, competitor, application
LEFT JOIN transfer ON transfer.id_player = application.id_player
WHERE competitor.id_competition = $id_league
 AND competitor.id_competitor = application.id_competitor
 AND application.id_player = player.id_player
 AND application.goalie = 1
 AND application.active = 1";

$res_all_players = mysql_query($query);
$n_all_players = mysql_num_rows($res_all_players);
for ($i = 0; $i < $n_all_players; $i++) {
    $id_team = mysql_result($res_all_players, $i, "id_competitor");
    $id_applicant = mysql_result($res_all_players, $i, "id_applicant");
    $id_player = mysql_result($res_all_players, $i, "id_player");
    $id_transfer = mysql_result($res_all_players, $i, "id_transfer");
    if ($id_transfer != null) {
        $old_team = mysql_result($res_all_players, $i, "transfer.old_team");
        $new_team = mysql_result($res_all_players, $i, "transfer.new_team");
        $res_old_league = mysql_query("SELECT * FROM competitor WHERE id_competitor = $old_team");
        $old_league = mysql_result($res_old_league, 0, "id_competition");
        //$res_new_league = mysql_query("SELECT * FROM competitor WHERE id_competitor = $new_team", $db_connection);
        //$new_league = mysql_result($res_new_league, 0, "id_competition");
        if ($old_league == $id_league) {
            $old_player_query = "SELECT * FROM application WHERE id_competitor = $old_team AND id_player = $id_player";
            $res_old_player = mysql_query($old_player_query);
            $old_player_id = mysql_result($res_old_player, 0, "id_applicant");
            $query = "SELECT * FROM staff WHERE id_player IN ($id_applicant, $old_player_id)";
        } else {
            $query = "SELECT * FROM staff WHERE id_player = $id_applicant";
        }
    } else {
        $query = "SELECT * FROM staff WHERE id_player = $id_applicant";
        $old_team = 0;
    }
    $res_played = mysql_query($query);
    $played = mysql_num_rows($res_played);
    $clean_sheets = 0;
    if ($played != null) {
        $missed = 0;
        $avg_missed = 0;
        for ($n = 0; $n < $played; $n++) {
            $id_protocol = mysql_result($res_played, $n, "id_protocol");
            $res_missed1 = mysql_query("SELECT SUM(score_end_2) AS missed FROM protocol WHERE id_protocol = $id_protocol AND team_1 IN ($id_team, $old_team)");
            $res_missed2 = mysql_query("SELECT SUM(score_end_1) AS missed FROM protocol WHERE id_protocol = $id_protocol AND team_2 IN ($id_team, $old_team)");
            $missed += (int)mysql_result($res_missed1, 0, "missed") + (int)mysql_result($res_missed2, 0, "missed");
            if ((int)mysql_result($res_missed1, 0, "missed") + (int)mysql_result($res_missed2, 0, "missed") == 0) {
                $clean_sheets++;
            }
        }
        $avg_missed = $missed / $played;
    } else {
        $played = 0;
        $missed = 0;
        $avg_missed = 0;
    }

    $insert_query = "INSERT INTO temp_goalie VALUES ($id_applicant, $played, $missed, $avg_missed, $clean_sheets)";

    $res_ins = mysql_query($insert_query);
}

$res_inserted = mysql_query("SELECT COUNT(*) AS cnt FROM temp_goalie");

$res_goalies = mysql_query("SELECT * FROM temp_goalie WHERE played > 0 ORDER BY avg_missed, played DESC, clean_sheet, missed LIMIT 0,10");

$n_goalies = mysql_num_rows($res_goalies);

for ($i = 0; $i < $n_goalies; $i++) {
    $place = $i + 1;
    $player = new PlayerApplication();
    $id_applicant = mysql_result($res_goalies, $i, "id_applicant");
    $player->getPlayerApplication($id_applicant);
    if ($player->player_personal->photo_path == "") {
        $player->player_personal->photo_path = "photo_default.PNG";
    }
    $played = mysql_result($res_goalies, $i, "played");
    $missed = mysql_result($res_goalies, $i, "missed");
    $avg_missed = mysql_result($res_goalies, $i, "avg_missed");
    $clean_sheets = mysql_result($res_goalies, $i, "clean_sheet");

    print('
                                        <tr><td class="fpt">' . $place . '</td><td><a href="player.php?p=' . $id_applicant . '"><img src="/photos/' . $player->player_personal->photo_path . '" height="25px"></a></td><td class="fpt" style="text-align: left;"><a href="player.php?p=' . $id_applicant . '">' . $player->player_personal->name_1 . ' ' . $player->player_personal->name_2 . ' (' . $player->team_details->shortname . ')</a></td><td class="fpt">' . $played . '</td><td class="fpt">' . $missed . ' (' . $avg_missed . ')</td><td class="fpt">' . $clean_sheets . '</td></tr>
');
}

print("                                 </table>");