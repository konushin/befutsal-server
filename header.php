<?php

//дебаг опция
if (isset($_GET["z"])) {
    ini_set('display_errors', 'On');
    error_reporting(-1);
} else {
    ini_set('display_errors', 'Off');
}

session_start();
$headers = apache_request_headers();

$now = date("Y-m-d H:i:s");
$pre_date = date("Ym");
$log_filename = "logs/web" . $pre_date . ".log";

if (!isset($class_active["index"])) {
    $class_active["index"] = "";
}

if (!isset($class_active["season"])) {
    $class_active["season"] = "";
}

if (!isset($class_active["history"])) {
    $class_active["history"] = "";
}

if (!isset($class_active["contact"])) {
    $class_active["contact"] = "";
}

if (!isset($class_active["results"])) {
    $class_active["results"] = "";
}

if (!isset($class_active["ranking"])) {
    $class_active["ranking"] = "";
}

if (!isset($class_active["docs"])) {
    $class_active["docs"] = "";
}

if (!isset($class_active["search"])) {
    $class_active["search"] = "";
}

if (!isset($class_active["old_version"])) {
    $class_active["old_version"] = "";
}

if (!isset($class_active["bans"])) {
    $class_active["bans"] = "";
}

if (!isset($class_active["transfers"])) {
    $class_active["transfers"] = "";
}

if (!isset($class_active["wc22"])) {
    $class_active["wc22"] = "";
}

include('includings/functions.php');
include('m/stats_structure.php');
include('m/user_structure.php');

if (empty($_SESSION["user_id"])) {
    if (isset($_GET["z"])) {
        if (isset($_COOKIE["PHPSESSID"])) {
            $message = "DEBUG: Empty user_id, request url=" . $_SERVER['REQUEST_URI'] . " ,PHPSESSID=" . $_COOKIE["PHPSESSID"];
        } else {
            $message = "DEBUG: Empty SESSION attribute user_id.";
        }
        writeLog($log_filename, $message);
    }
    $is_active = FALSE;
} else {
    $logged_user_id = $_SESSION["user_id"];
    $is_active = TRUE;
}

$db_connection = mysql_connect("localhost", "befutsal_sprhst", "eCgSiv9u");
mysql_select_db("befutsal_main");

$current_season = new Season;
$current_season->getCurrentSeason();

if (isset($_GET["l"])) {
    $league_details = new Tournament;
    $league_details->getCompetitionInfo($_GET["l"]);
    $page_title = $league_details->title . " (" . $league_details->season_details->title . ")";
    $description = "Статистика турнира " . $league_details->title . " (" . $league_details->season_details->title . ")";
} elseif (stripos($_SERVER["REQUEST_URI"], "competitor.php")) {
    $team_details = new TeamCompetitor;
    $team_details->getTeamCompetitor($_GET["c"]);
    $page_title = $team_details->title . ". " . $team_details->competition_details->title . " (" . $team_details->competition_details->season_details->title . ")";
    $description = "Показатели выступления команды " . $team_details->title . ". " . $team_details->competition_details->title . " (" . $team_details->competition_details->season_details->title . ")";
} elseif (stripos($_SERVER["REQUEST_URI"], "club.php")) {
    $team_details = new TeamInfo;
    $team_details->getTeamInfo($_GET["t"]);
    $page_title = $team_details->title;
    $description = "История выступлений команды " . $team_details->title;
} elseif (stripos($_SERVER["REQUEST_URI"], "player.php")) {
    $player_details = new PlayerApplication;
    $player_details->getPlayerApplication($_GET["p"]);
    $page_title = $player_details->player_personal->name_1 . " " . $player_details->player_personal->name_2 . ", " . $player_details->team_details->title . " (" . $player_details->team_details->competition_details->title . ", " . $player_details->team_details->competition_details->season_details->title . ")";
    $description = "Статистика игрока в сезоне " . $player->player_personal->name_1 . " " . $player->player_personal->name_2 . ", " . $player_details->team_details->title . " (" . $player_details->team_details->competition_details->title . ", " . $player_details->team_details->competition_details->season_details->title . ")";
} elseif (stripos($_SERVER["REQUEST_URI"], "match_preview.php")) {
    $id_schedule = $_GET["s"];
    $res_schedule = mysql_query("SELECT * FROM schedule WHERE id_schedule = $id_schedule", $db_connection);
    $schedule = new Schedule;
    $schedule->date = mysql_result($res_schedule, 0, "date");
    $schedule->time = mysql_result($res_schedule, 0, "time");
    $schedule->team_1 = new TeamCompetitor;
    $schedule->team_1->getTeamCompetitor(mysql_result($res_schedule, 0, "team_1"));
    $schedule->team_2 = new TeamCompetitor;
    $schedule->team_2->getTeamCompetitor(mysql_result($res_schedule, 0, "team_2"));
    $page_title = $schedule->team_1->title . " vs " . $schedule->team_2->title . " (" . substr($schedule->date, 8, 2) . " " . iconv("Windows-1251", "UTF-8", ruMonth(substr($schedule->date, 5, 2))) . " " . substr($schedule->date, 0, 4) . " " . substr($schedule->time, 0, 5) . ", " . $schedule->team_1->competition_details->title . ")";
    $description = "Превью матча " . $schedule->team_1->title . " vs " . $schedule->team_2->title . " (" . substr($schedule->date, 8, 2) . " " . iconv("Windows-1251", "UTF-8", ruMonth(substr($schedule->date, 5, 2))) . " " . substr($schedule->date, 0, 4) . " " . substr($schedule->time, 0, 5) . ", " . $schedule->team_1->competition_details->title . ")";
} elseif (stripos($_SERVER["REQUEST_URI"], "transfers.php")) {
    if (ISSET($_GET["s"])) {
        $id_season = (int)$_GET["s"];
    } else {
        $id_season = $current_season->id_season;
    }
    $season = new SeasonInfo();
    $season->getSeasonInfo($id_season);
    $page_title = "Трансферы " . $season->title;
    $description = "Переходы игроков в сезоне " . $season->title;
} elseif (stripos($_SERVER["REQUEST_URI"], "match.php")) {
    $id_protocol = $_GET["p"];
    $res_protocol = mysql_query("SELECT * FROM protocol WHERE id_protocol = $id_protocol", $db_connection);
    $protocol = new Protocol;
    $protocol->dt_protocol = mysql_result($res_protocol, 0, "dt_protocol");
    $protocol->getProtocolData($res_protocol, 0);
    $protocol->team_1 = mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $protocol->id_team_1"), 0, "title");
    $protocol->team_2 = mysql_result(mysql_query("SELECT * FROM competitor WHERE id_competitor = $protocol->id_team_2"), 0, "title");
    $page_title = $protocol->team_1 . " vs " . $protocol->team_2 . " (" . substr($protocol->dt_protocol, 8, 2) . " " . iconv("Windows-1251", "UTF-8", ruMonth(substr($protocol->dt_protocol, 5, 2))) . " " . substr($protocol->dt_protocol, 0, 4) . ", " . iconv('UTF-8', 'windows-1251', $protocol->league) . ")";
    $description = "Протокол матча " . $protocol->team_1 . " vs " . $protocol->team_2 . " (" . substr($protocol->dt_protocol, 8, 2) . " " . iconv("Windows-1251", "UTF-8", ruMonth(substr($protocol->dt_protocol, 5, 2))) . " " . substr($protocol->dt_protocol, 0, 4) . ", " . iconv('UTF-8', 'windows-1251', $protocol->league) . ")";
}

print('
<!DOCTYPE html>

<html xmlns="https://www.w3.org/1999/xhtml">
<head>
    <title>' . $page_title . ' - Живи мини-футболом!</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />

<meta property="og:image" content="https://befutsal.ru/graphics/placeholder_1200x350.jpg" />

<!-- яндекс-метрика и ссылка на itunes -->
<meta name="apple-itunes-app" content="app-id=1273597183">
<meta name="yandex-verification" content="6efe9abaaa19a1a9" />

<meta name="author" content="Андрей Конушин" />
<meta name="keywords" content="клубы, события, футбол, футзал, некоммерчекий, мини-футбол, прогнозы на матч, спорт, гол, игрок, матч, расписание, турнир, кубок, чемпионат, плейофф" />
<meta name="description" content="' . $description . '"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300" rel="stylesheet" type="text/css"/>
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700" rel="stylesheet" type="text/css"/>-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,100,300,200,500,600,700,800,900" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css">

    <link href="css/fonts/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--Clients-->
    <link href="css/own/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="css/own/owl.theme.css" rel="stylesheet" type="text/css" />


    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.jscrollpane.css" rel="stylesheet" type="text/css" />
    
    <link href="css/minislide/flexslider.css" rel="stylesheet" type="text/css" />
    <link href="css/component.css" rel="stylesheet" type="text/css" />
    <link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <link href="css/style_dir.css?version=9" rel="stylesheet" type="text/css" />
    
    <link rel="icon" type="image/svg+xml" href="favicon.svg" />
    <link href="css/responsive.css?version=9" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />

</head>
<body>');

include('includings/analytics.php');

//$message = "DEBUG: session user_id = " . $logged_user_id . ", is_active=" . $is_active;
//writeLog($log_filename, $message);

if (isset($_GET["z"])) {
    if ($is_active) {
        $query = "SELECT * FROM m_user WHERE user_id = $logged_user_id";
        $res_user = mysql_query($query);

        $MUser = new MUser();
        $MUser->user_id = $logged_user_id;
        $MUser->username = mysql_result($res_user, 0, "username");
        $MUser->email = mysql_result($res_user, 0, "email");
        $MUser->user_player = mysql_result($res_user, 0, "user_player");
        $MUser->avatar = mysql_result($res_user, 0, "avatar");
        $MUser->getFavourites();

        $login_label = '<a href="errorpage.php" title="Личный кабинет"><img src="graphics/user_icon.gif" border="0" style="max-height: 10px; max-width: 10px;"> ' . $MUser->username . '</a>';
    } else {
        $login_label = '<a href="login.php">Войти</a>';
    }

    print('
<!--SECTION TOP LOGIN-->

     <section class="content-top-login">
           <div class="container">
      <div class="col-md-12">
           <div class="box-support"> 
             <p class="support-info"><i class="fa fa-envelope-o"></i> info@befutsal.ru</p>
          </div>
           <div class="box-login"> 
             ' . $login_label . '
          </div>
        </div>
      </div>
     </section>');
}

print('

      <!--SECTION MENU -->
     <section class="container box-logo">
        <header>
           <div class="content-logo col-md-12">
          <div class="logo"> 
            <a href="https://befutsal.ru"><img src="img/logo.png" alt="" /></a>
          </div>
          
          <div class="bt-menu"><a href="#" class="menu"><span>&equiv;</span> Меню</a></div>

         <div class="box-menu">
            
            <nav id="cbp-hrmenu" class="cbp-hrmenu">
					    <ul id="menu">    
                            <li><a class="lnk-menu ' . $class_active["results"] . '" href="results.php">Матч-центр</a></li>
                            <li>
								<a class="lnk-menu ' . $class_active["season"] . '" href="#">Чемпионат</a>
                                <div class="cbp-hrsub sub-little">
                                  <div class="cbp-hrsub-inner"> 
                                      <div class="content-sub-menu">
								        <ul class="menu-pages">');

$res_top_leagues = mysql_query("SELECT * FROM competition WHERE season = $current_season->id_season AND rate IS NOT NULL ORDER BY rate ASC");
for ($i = 0; $i < mysql_num_rows($res_top_leagues); $i++) {
    $id_league = mysql_result($res_top_leagues, $i, "id_competition");
    $tournament = new Tournament();
    $tournament->getCompetitionInfo($id_league);
    print('<li><a href="' . $tournament->link . '"><span>' . $tournament->title . '</span></a></li>');
}
print('<li><a href="transfers.php?s=' . $current_season->id_season . '"><span>+ Трансферы</span></a></li>');
print('
								          </ul>
                                        </div>
                                    </div>
                                </div>
							</li>
						<li><a class="lnk-menu ' . $class_active["history"] . '" href="tournaments.php">Все турниры</a></li>
						<li><a class="lnk-menu ' . $class_active["bans"] . '" href="bans.php">КДК</a></li>
						<li><a class="lnk-menu ' . $class_active["docs"] . '" href="docs.php">Документы</a></li>
                                                <li><a class="lnk-menu ' . $class_active["contact"] . '" href="contact.php">Контакты</a></li>
                                                <li><a class="lnk-menu ' . $class_active["search"] . '" href="search.php"><img src="img/search.png" style="max-height: 20px; "></a></li>
					</ul>
				</nav>
              </div>
			</div>
	    </header>
     </section>');
?>