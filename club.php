﻿<?php

$class_active["season"] = "active";
include("header.php");

$id_team = $_GET["t"];

$team = new TeamInfo;
$team->getTeamInfo($id_team);

if ($team->founded == NULL) {
    $team->founded = 'Неизвестно';
}

if ($team->tshirt_home != NULL) {
    $tshirt_home = new Tshirt();
    $tshirt_home->getTshirtDetails($team->tshirt_home);
    $tshirt_home_toprint = '<img src="/graphics/' . $tshirt_home->picture . '" title="Домашная форма">';
} else {
    $tshirt_home_toprint = null;
}

if ($team->tshirt_away != NULL) {
    $tshirt_away = new Tshirt();
    $tshirt_away->getTshirtDetails($team->tshirt_away);
    $tshirt_away_toprint = '<img src="/graphics/' . $tshirt_away->picture . '" title="Гостевая форма">';
} else {
    $tshirt_away_toprint = null;
}

$max_team_rank = getTeamRank($id_team, 1);
if ($max_team_rank == 0) {
    $max_team_rank = "-";
}

$query = "SELECT *
 FROM competitor, competition, season
 WHERE id_team = $id_team
  AND competitor.id_competition = competition.id_competition
  AND season.id_season = competition.season
 ORDER BY competition.season
 DESC, competition.id_competition";
$res_history = mysql_query($query);

$num_history = mysql_num_rows($res_history);

$t_played = 0;
$t_won = 0;
$t_drew = 0;
$t_lost = 0;
$t_scored = 0;
$t_missed = 0;
$competitor = array();

for ($i = 0; $i < $num_history; $i++) {
    $id_competitor = mysql_result($res_history, $i, "competitor.id_competitor");
    $competitor[$i] = new TeamCompetitor;
    $competitor[$i]->getTeamCompetitor($id_competitor);
    $historical_titles[$i] = $competitor[$i]->title;

    $league = $competitor[$i]->competition_details->competition_id;
    $tour_date = "9999-12-31 23:59:59";
    $res_table = getLeagueTable($league, $tour_date);
    $num_teams = mysql_num_rows($res_table);
    $res_team_performance = mysql_query("SELECT * FROM league_table_content WHERE id_league = $league AND id_competitor = $id_competitor");

    //----------победы
    $won[$i] = mysql_result($res_team_performance, 0, "won");

    //----------ничьи
    $drew[$i] = mysql_result($res_team_performance, 0, "drew");

    //----------поражения
    $lost[$i] = mysql_result($res_team_performance, 0, "lost");

    //----------игр и очков
    $played[$i] = mysql_result($res_team_performance, 0, "pld");

    $points[$i] = mysql_result($res_team_performance, 0, "pts");

    //----------забито
    $scored[$i] = mysql_result($res_team_performance, 0, "goalsfor");

    //----------пропущен
    $missed[$i] = mysql_result($res_team_performance, 0, "goalsagainst");

    //----------место
    $place[$i] = 0;

    $difference[$i] = $scored[$i] - $missed[$i];
    if ($difference[$i] > 0) {
        $difference[$i] = "+" . $difference[$i];
    }

    if ($played[$i] != 0) {
        $percentage = ($points[$i] / $played[$i] / $competitor[$i]->competition_details->win_points * 100);
        if ($percentage == 100) {
            $points_percentage[$i] = "100%";
        } else {
            $points_percentage[$i] = substr($percentage, 0, 2) . "%";
        }
    } else {
        $points_percentage[$i] = "-";
    }

    $t_played = $t_played + (int)$played[$i];
    $t_won = $t_won + (int)$won[$i];
    $t_drew = $t_drew + (int)$drew[$i];
    $t_lost = $t_lost + (int)$lost[$i];
    $t_scored = $t_scored + (int)$scored[$i];
    $t_missed = $t_missed + (int)$missed[$i];

    //если это кубок
    if ($competitor[$i]->competition_details->is_cup > 0) {
        $query_round = "SELECT *, LOWER(r.title) AS round_title FROM schedule s, cup_round r WHERE s.tour = r.id_tour AND r.id_competition = $league AND (s.team_1 = $id_competitor OR s.team_2 = $id_competitor) ORDER BY r.id_tour DESC LIMIT 0,1";
        $res_round = mysql_query($query_round);
        $round_title = mysql_result($res_round, 0, "r.title");
        $round_title_lc = mysql_result($res_round, 0, "round_title");
        $place[$i] = "<img src='graphics/cup.png' style='max-height:16px; max-width:16px; align:center; border:0;' title='" . $round_title . "'>";
        if (strpos($round_title_lc, "финал") === FALSE and strpos($round_title_lc, "матч") === FALSE) {
            $round_title = "Групповой этап";
            $place[$i] = "<img src='img/group_white.png' style='max-height:16px; max-width:16px; align:center; border:0;' title='" . $round_title . "'>";
        }

    } //если это чемпионат
    else {
        for ($n = 0; $n < $num_teams; $n++) {
            $place[$i]++;
            if ($id_competitor == mysql_result($res_table, $n, "id_competitor")) {
                break;
            }
        }
    }
}

$played_total_percentage = 100;
$scored_total_percentage = $t_scored / max($t_scored, $t_missed) * 100;
$missed_total_percentage = $t_missed / max($t_scored, $t_missed) * 100;
$won_total_percentage = $t_won / $t_played * 100;
$drew_total_percentage = $t_drew / $t_played * 100;
$lost_total_percentage = $t_lost / $t_played * 100;

$historical_titles = array_unique($historical_titles);
foreach ($historical_titles as $unique_title) {
    $historical_titles_to_print = $historical_titles_to_print . $unique_title . ", ";
}

$historical_titles_to_print = mb_strcut($historical_titles_to_print, 0, strlen($historical_titles_to_print) - 2);

print('<section class="drawer">
    <div class="col-md-12 size-img back-img-match">
        <div class="effect-cover">
                <h3 class="txt-advert animated">' . $team->title . '</h3>
                <p class="txt-advert-sub">' . $historical_titles_to_print . '</p>
            </div>
    </div>
    
    <section id="single_player" class="container secondary-page">
      <div class="general general-results players">
           <div class="top-score-title right-score col-md-9">
                <h3><span>Профиль</span> клуба<span class="point-little">.</span></h3>
                <div class="col-md-12 atp-single-player">
                  <img class="img-djoko" src="/emblems/' . $team->emblem_path . '" alt="Эмблема" />
                  <div class="col-md-2 data-player">
                     <p>Основана:</p>
                     <p></p>
                  </div>
                  <div class="col-md-3 data-player-2">
                     <p>' . $team->founded . '</p>
                     <p>' . $tshirt_home_toprint . ' ' . $tshirt_away_toprint . '</p>
                  </div>
                  <div class="col-md-2 rank-player">
                     <div class="content-rank"><p class="rank-data">' . $max_team_rank . '</p></div>
                     <p class="rank-title">лучшее место в рейтинге befutsal</p>
                  </div>
                </div>
                <div class="col-md-12 atp-single-player skill-content">
                     <div class="exp-skill">
                            <p class="exp-title-pp">Общая статистика выступлений</p>
                            <div class="skills-pp">
                            <div class="skillbar-pp clearfix" data-percent="' . $played_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Игр </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $t_played . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $won_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Побед </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $t_won . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $drew_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Ничьих </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $t_drew . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $lost_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Поражений </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $t_lost . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $scored_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Забито  </span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $t_scored . '</div>
                            </div>
                            <div class="skillbar-pp clearfix" data-percent="' . $missed_total_percentage . '%">
                                <div class="skillbar-title-pp"><span>Пропущено</span></div>
                                <div class="skillbar-bar-pp"></div>
                                <div class="skill-bar-percent-pp">' . $t_missed . '</div>
                            </div>
                        </div>
                    </div>
             </div>
               <div class="col-md-12 atp-single-player skill-content">
                      <div class="ppl-desc">
                        <p class="exp-title-pp">ВЫСТУПЛЕНИЯ</p>');

print('
                    <div class="col-md-12 home-page">
                        <table class="tab-career">
                           <tr class="top-career-table"><td class="tab-career-numbers">ТУРНИР</td><td class="only-desktop">И</td><td class="only-desktop" style="text-align: center;">В</td><td class="only-desktop" style="text-align: center;">Н</td><td class="only-desktop" style="text-align: center;">П</td><td class="only-desktop" style="text-align: center;">МЯЧИ</td><td class="only-desktop" style="text-align: center;">ОЧКИ</td><td class="tab-career-numbers">МЕСТО</td></tr>');

for ($i = 0; $i < sizeof($competitor); $i++) {

    $href = "window.location.href='" . $competitor[$i]->competition_details->link . "'";

    print('
                           <tr onclick="' . $href . '; return false"><td class="tab-career-numbers" style="text-align: left;">' . $competitor[$i]->competition_details->season_details->title . '. ' . cutString($competitor[$i]->competition_details->title, 40) . '</td><td class="only-desktop">' . $played[$i] . '</td><td class="only-desktop" style="text-align: center;">' . $won[$i] . '</td><td class="only-desktop" style="text-align: center;">' . $drew[$i] . '</td><td class="only-desktop" style="text-align: center;">' . $lost[$i] . '</td><td class="only-desktop" style="text-align: center;">' . $scored[$i] . '-' . $missed[$i] . '</td><td class="only-desktop" style="text-align: center;">' . $points[$i] . '<span>(' . $points_percentage[$i] . ')</span></td><td  class="tab-career-numbers">' . $place[$i] . '</td></tr>');
}

print('
                           <tr class="only-desktop"><td class="tab-career-numbers" style="text-align: right;">ВСЕГО</td><td class="only-desktop">' . $t_played . '</td><td class="only-desktop" style="text-align: center;">' . $t_won . '</td><td class="only-desktop" style="text-align: center;">' . $t_drew . '</td><td class="only-desktop" style="text-align: center;">' . $t_lost . '</td><td class="only-desktop" style="text-align: center;">' . $t_scored . '-' . $t_missed . '</td><td class="only-desktop">&nbsp;</td><td  class="tab-career-numbers">&nbsp;</td></tr>');

print('
                        </table>
                                  </div> <!-- ppl-desc -->                                  
                    </div> <!-- col-md-12 atp-single-player skill-content -->');

print('</div>
           </div><!--Close Top Match-->

');

include("right_column.php");

include("sponsors.php");

include('footer.php');

print('
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="js/jquery.transit.min.js" type="text/javascript"></script>

<!--MENU-->
<script src="js/menu/modernizr.custom.js" type="text/javascript"></script>
<script src="js/menu/cbpHorizontalMenu.js" type="text/javascript"></script>
<!--END MENU-->

<!-- Button Anchor Top-->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script src="js/jquery.bxslider.js" type="text/javascript"></script>

<!--PERCENTAGE-->
<script>
    $(function () {
        "use strict";
        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").width(0);
        });

        $(".skillbar-pp").each(function () {
            $(this).find(".skillbar-bar-pp").animate({
                width: $(this).attr("data-percent")
            }, 2000);
        });
    });
</script>
');

?>