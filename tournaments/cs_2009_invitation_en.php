<?php

$root = false;
$page_title = "Christmas Stars 2009";
$description = "Invitation for participation in Christmas Stars 2009";
include("../includings/header.php");

print('
 <tr>
  <td height="6" background="' .$add_string. 'graphics/h_bg_1.gif" bgcolor="#28C638"><img src="' .$add_string. 'graphics/0.gif" width="1" height="6" border="0" title=""></td>
 </tr>
 <tr>
  <td height="1" bgcolor="#FFFFFF"><img src="' .$add_string. 'graphics/0.gif" width="1" height="1" border="0" title=""></td>
 </tr>
 <tr>
  <td height="30" style="background-image:url(' .$add_string. 'graphics/h_bg_2.gif);background-repeat:repeat-x;background-position:top"><img src="' .$add_string. 'graphics/0.gif" width="1" height="30" border="0" title=""></td>
 </tr>
</table>

<!----------------------- End of Header ---------------------------->

<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff">
 <tr>');

include("../includings/main menu.php");

include('../database_main.php');

print('
<!------------------------ Central Part.Right Part ---------------------------->

  <td valign="top">
   <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
');

print('
<!------------------------ Central Part.Right Part.Central Part ---------------------------->

	 <td valign="top" style="padding:7px 20px 10px 20px">
	 <div>
      <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#5BC1D7">
	   <tr>
	    <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_left.gif" width="14" height="35" border="0" title=""></td>
		<td valign="top" background="../graphics/cp_blue_bg.gif" bgcolor="#5BC1D7" style="padding-left:20px;padding-top:4px;" class="cp_header">Christmas Stars 2009</td>
		<td valign="top" width="14" height="35"><img src="../graphics/cp_blue_right.gif" width="14" height="35" border="0" title=""></td>
	   </tr>
	  </table>
	  </div>
	  <br><br>
	<div style="padding-left:34px; padding-right:34px">
  <table width="90%" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td valign="top">
     Contact person: Paul Bondarev<br>
     President of Vladimir linguistic Association<br>
     Sacco-Vantsetti street, 64-4<br>
     600017 Vladimir Russia<br>
     Contacts: phone number/fax: 7(4922) 23-83-48<br>
     E-mail: lingua.vla@gmail.com
    </td>
    <td align="right"><a href="christmas_stars_2009.php" title="Christmas Stars 2009"><img src="../graphics/christmas_stars_2009_logo.gif" border="0" align="right" hspace="15" vspace="15"><br clear="all">
   <tr>
    <td colspan="2" align="left">
        <p align=justify>Dear Futsal-players,</p>
   <p align=justify>Every year Vladimir Linguistic Association "LINGUA" and Vladimir regional football federation organize traditional futsal tournament "Christmas Stars" in Vladimir, Russia.</p>
   <p align=justify>The annual tournament is held on Christmas weekend.  12 teams from Vladimir, Moscow,Yaroslavl, Nizhni Novgorod and other cities participate and strive to win this prestigious tournament. </p>
   <p align=justify>The 15th tournament "Christmas Stars" will be organized in 2009. This 5-day event will take place on the Christmas weekend, starting on the 3d of January and finishing on the 7-th of January.
   </p>
   <p align="justify">If you are interested in our tournament call or E-mail us.<br>
   We hope to meet and welcome you in Vladimir.
   </p>
   <p align="right">With kind regards,<br>
   President of Vladimir Linguistic Association<br>
   Paul Bondarev
   </p>
    </td>
   </tr>
  </table>
  <p align="center"></p>
 <br>
</div>
	</td>');

print('

<!------------------------ Central Part.Right Part.Central Part.End --------------------------->
');

print('
	</tr>
   </table>

  </td>

<!------------------------ Central Part.Right Part.End ---------------------------->

 </tr>

</table>');


require("../includings/big footer.php");

mysql_close($db_connection);

?>