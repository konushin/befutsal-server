<?php

$root = false;
$page_title = "Christmas Stars 2010";
$description = "���������� � ������� �������������� ������ 2010";
include("../includings/header.php");

print('
 <tr>
  <td height="6" background="' .$add_string. 'graphics/h_bg_1.gif" bgcolor="#28C638"><img src="' .$add_string. 'graphics/0.gif" width="1" height="6" border="0" title=""></td>
 </tr>
 <tr>
  <td height="1" bgcolor="#FFFFFF"><img src="' .$add_string. 'graphics/0.gif" width="1" height="1" border="0" title=""></td>
 </tr>
 <tr>
  <td height="30" style="background-image:url(' .$add_string. 'graphics/h_bg_2.gif);background-repeat:repeat-x;background-position:top"><img src="' .$add_string. 'graphics/0.gif" width="1" height="30" border="0" title=""></td>
 </tr>
</table>

<!----------------------- End of Header ---------------------------->

<!------------------------ Central Part ---------------------------->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="ffffff">
 <tr>');

include("../includings/main menu.php");

include('../database_main.php');

print('
<!------------------------ Central Part.Right Part ---------------------------->

  <td valign="top">
   <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
');

print('
<!------------------------ Central Part.Right Part.Central Part ---------------------------->

	 <td valign="top" style="padding:7px 20px 10px 20px">
	 <div>
      <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#5BC1D7">
	   <tr>
	    <td valign="top" width="14" height="35"><img src="../graphics/cp_blue_left.gif" width="14" height="35" border="0" title=""></td>
		<td valign="top" background="../graphics/cp_blue_bg.gif" bgcolor="#5BC1D7" style="padding-left:20px;padding-top:4px;" class="cp_header">Christmas Stars 2010</td>
		<td valign="top" width="14" height="35"><img src="../graphics/cp_blue_right.gif" width="14" height="35" border="0" title=""></td>
	   </tr>
	  </table>
	  </div>
	  <br><br>
	<div style="padding-left:34px; padding-right:34px">
  <table width="90%" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td valign="top">
     <a href="cs_2010_sponsorship.php">����������� � �����������</a><br>
     <a href="cs_2010_history.php">������� �������</a><br>
     <a href="cs_2010_application_ru.doc" target="_blank">������ (�������)</a><br>
     <a href="../documents/cs_state_2010.htm" target="_blank">���������</a> (<a href="../documents/cs_state_2010.doc" target="_blank">MS Word</a> | <a href="../documents/cs_state_2010.rar" target="_blank">RAR</a>)<br>
     <a href="../documents/cs_disc_code_2010.htm" target="_blank">�������������� ������</a> (<a href="../documents/cs_disc_code_2010.doc" target="_blank">MS Word</a> | <a href="../documents/cs_disc_code_2010.rar" target="_blank">RAR</a>)<br>
     <a href="../statistics/league_presentation.php?league=224667610">������ ����������</a><br>
     <a href="christmas_stars_2010_groups.php">��������� ���������</a><br>
    </td>
    <td align="right"><a href="christmas_stars_2010.php" title="Christmas Stars 2010"><img src="../graphics/christmas_stars_2009_logo.gif" border="0" align="right" hspace="15" vspace="15"><br clear="all">
    </tr>');

print('
   <tr>
    <td colspan="2" align="left"><font class="news_date">�������-��������� �������:</font>
     <br><a href="../statistics/teams.php?id=224667761" title="������� �� �������� �������">����� (��������)</a>
     <br><a href="../statistics/teams.php?id=224667755" title="������� �� �������� �������">������� (��������)</a>
     <br><a href="../statistics/teams.php?id=224667757" title="������� �� �������� �������">����� (�������)</a>
     <br><a href="../statistics/teams.php?id=224667760" title="������� �� �������� �������">�������-������ (��������)</a>
     <br><a href="../statistics/teams.php?id=224667759" title="������� �� �������� �������">������ (��������)</a>
     <br><a href="../statistics/teams.php?id=224667758" title="������� �� �������� �������">Sun Petrol (��������)</a>
     <br><a href="../statistics/teams.php?id=224667762" title="������� �� �������� �������">��� ���� (��������)</a>
    </td>
   </tr>
   <tr>
    <td colspan="2" align="left"><br><br>
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <td align="center"><font class="news_date">������ �</font>
        <br>����� (��������)
        <br>Sun Petrol (��������)
        <br>����� (�������)
        <br>������� (��������)
       </td>
       <td align="center"><font class="news_date">������ �</font>
        <br>������ (��������)
        <br>�������-������ (��������)
        <br>��� ���� (��������)
        <br>4-� �������, ���� ����������
       </td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td colspan="2" align="left"><font class="news_date"><br>����� �������:</font>
     <br>������� ������ �������� (������������� ���������), �. ��������
     <br>�������� ���� (��������������� ���������), �. ��������
     <br>����� �������� (��������������� ���������), �. ��������
     <br>������ ������� (��������������� ���������), �. ��������
     <br>������ ������ (��������������� ���������), �. ��������
     <br>
     <br>������� ����� ������� - ������� ������� �������� - ��������� ��������� ������� ������������ �������
     <br>������������ ����������� - �������� ����� �����������
    </td>
   </tr>
   <tr>
    <td colspan="2" align="left"><font class="news_date"><br><a href="../statistics/results.php?league=224667610">���������� ������</a>:</font>
					<table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <td align="left" colspan="3"><b>3 ������ 2010 ����, �����������</b></td>
      </tr>
      <tr>
       <td align="left">1</td>
       <td align="left">14:00</td>
       <td align="left">������ - </td>
      </tr>
      <tr>
       <td align="left">2</td>
       <td align="left">15:00</td>
       <td align="left">�������-������ - �������</td>
      </tr>
      <tr>
       <td align="left">3</td>
       <td align="left">16:00</td>
       <td align="left">����� - �����</td>
      </tr>
      <tr>
       <td align="left">4</td>
       <td align="left">17:00</td>
       <td align="left">Sun Petrol - �������</td>
      </tr>
      <tr>
       <td align="left" colspan="3"><b>4 ������ 2010 ����, �����������</b></td>
      </tr>
      <tr>
       <td align="left">5</td>
       <td align="left">14:00</td>
       <td align="left">������� - �����</td>
      </tr>
      <tr>
       <td align="left">6</td>
       <td align="left">15:00</td>
       <td align="left">����� - Sun Petrol</td>
      </tr>
      <tr>
       <td align="left">7</td>
       <td align="left">16:00</td>
       <td align="left">������� -</td>
      </tr>
      <tr>
       <td align="left">8</td>
       <td align="left">17:00</td>
       <td align="left">������ � �������-������</td>
      </tr>
      <tr>
       <td align="left" colspan="3"><b>5 ������ 2010 ����, �������</b></td>
      </tr>
      <tr>
       <td align="left">9</td>
       <td align="left">14:00</td>
       <td align="left">����� - Sun Petrol</td>
      </tr>
      <tr>
       <td align="left">10</td>
       <td align="left">15:00</td>
       <td align="left">������� - �����</td>
      </tr>
      <tr>
       <td align="left">11</td>
       <td align="left">16:00</td>
       <td align="left">������� -</td>
      </tr>
      <tr>
       <td align="left">12</td>
       <td align="left">17:00</td>
       <td align="left">������� - ������</td>
      </tr>
      <tr>
       <td align="left" colspan="3"><b>6 ������ 2010 ����, �����</b></td>
      </tr>
      <tr>
       <td align="left">13</td>
       <td align="left">11:00</td>
       <td align="left">���������. 1� - 2�</td>
      </tr>
      <tr>
       <td align="left">14</td>
       <td align="left">12:00</td>
       <td align="left">���������. 2� - 1�</td>
      </tr>
      <tr>
       <td align="left">15</td>
       <td align="left">13:00</td>
       <td align="left">���� �� 7-8 �����</td>
      </tr>
      <tr>
       <td align="left">16</td>
       <td align="left">14:00</td>
       <td align="left">���� �� 5-6 �����</td>
      </tr>
      <tr>
       <td align="left">17</td>
       <td align="left">15:00</td>
       <td align="left">���� �� 3 �����</td>
      </tr>
      <tr>
       <td align="left">18</td>
       <td align="left">16:00</td>
       <td align="left">�����</td>
      </tr>
      <tr>
       <td align="left">&nbsp;</td>
       <td align="left">17:00</td>
       <td align="left">�����������</td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
</div>
	</td>');

print('<tr>
    <td colspan="2" align="center"><br><br><font class="news_date">������� �� ������������!</font><br>Please follow the updates</td>
   </tr>
  </table>
  <p align="center"></p>
 <br>
</div>
	</td>');

print('

<!------------------------ Central Part.Right Part.Central Part.End --------------------------->
');

print('
	</tr>
   </table>

  </td>

<!------------------------ Central Part.Right Part.End ---------------------------->

 </tr>

</table>');


require("../includings/big footer.php");

?>