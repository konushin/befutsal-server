<?php

//Открываем соединение с MySQL
$db_connection = mysql_connect("localhost", "befutsal_rstqb", "11MQPn6Y");

//Выбираем БД
mysql_select_db("befutsal_rstqb");

mysql_query("SET NAMES 'utf8_unicode_ci'");

//токен бота RSTQB_FAQ_Bot
define('BOT_TOKEN', '5530535949:AAHVTgGLXcW2FaOylNUPlAAY_-0BxFmK_kE');

$pre_date = date("Ymd");
$disable_web_page_preview = true; //по умолчанию отключаем превью в чате

$bot_commands = array(
    'start' => '/start',
    'help' => '/help',
    'faq' => '/faq',
    'register' => '/register'
);

if (!ISSET($log_filename)) {
    $log_filename = "/logs/rstqb_bot_" . $pre_date . ".log";
}

class rstqb_faq_bot {

    public $token = ''; //Создаём публичную переменную для токена, который нужно отправлять каждый раз при использовании апи тг

    public function __construct($token) {
        $this->token = $token; //Забиваем в переменную токен при конструкте класса
    }

    public function send($chat_id, $message, $reply_markup, $disable_web_page_preview) {
        //Заполняем массив $data инфой, которую мы через api отправим до телеграмма
        $data = array(
            'chat_id' => $chat_id,
            'text' => $message,
            'parse_mode' => 'HTML',
            'reply_markup' => $reply_markup,
            'disable_web_page_preview' => $disable_web_page_preview
        );

        $out = $this->request('sendMessage', $data);

        return $out;
    }

    public function request($method, $data = array()) {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot' . $this->token . '/' . $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST'); //Отправляем через POST
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //Сами данные отправляемые

        $out = json_decode(curl_exec($curl), true); //Получаем результат выполнения, который сразу расшифровываем из JSON'a в массив для удобства

        curl_close($curl); //Закрываем курл

        return $out; //Отправляем ответ в виде массива
    }

}

class message {
    public $message_id;
    public $tg_message_id;
    public $chat_id;
    public $text;
    public $timestamp;
}

$body = file_get_contents('php://input'); //Получаем в $body json строку
$update = json_decode($body, true); //Разбираем json запрос на массив в переменную $update

$tg_bot = new rstqb_faq_bot(BOT_TOKEN);

$new_message = new message();

$new_message->text = $update['message']['text']; //Получаем текст сообщения, которое нам пришло.
$new_message->chat_id = $update['message']['chat']['id']; //Сразу и id получим, которому нужно отправлять всё это назад
$chat_first_name = $update['message']['chat']['first_name']; //Имя пользователя
$new_message->tg_message_id = $update['message']['message_id']; //id конкретного сообщения в телеге
$callback_query = $update['callback_query'];
$callback_data = $callback_query['data']; //Получаем callback после нажатия на кнопку
$callback_chat_id = $callback_query['message']['chat']['id'];

$new_message->timestamp = date("Y-m-d H:i:s");

$log_string = "REQUEST\nChatId=" . $new_message->chat_id . "\nTGMessageId=" . $new_message->tg_message_id . "\nChatFirstName=" . $chat_first_name . "\nMessage=" . $new_message->message_text . "\nCallbackData=" . $callback_data . "\nCallbackChatId=" . $callback_chat_id . "\nLogQuery=" . $query_log;
writeLog($log_filename, $log_string);

//СМОТРИМ ПОСЛЕДНЮЮ КОМАНДУ ОТ ПОЛЬЗОВАТЕЛЯ--------------------------------------------------
//анализируем жалобу пользователя
if ($callback_data != NULL) {
    $id_league = $callback_data;
    $new_message->chat_id = $callback_chat_id;

//обрабатываем первую команду
} else {
    switch ($new_message->text) {
        case $bot_commands['start']:
            $response = "Здравствуй, " . $chat_first_name . "! Это помощник RSTQB. Начни с команды " . $bot_commands['help'] . " для знакомства с моими возможностями.";
            break;

        case $bot_commands['help']:
            $response = "Просто выбери одну из моих доступных команд и следуй подсказкам:\n";
            break;

        case $bot_commands['faq']:
            $query_topics = "SELECT * FROM topics ORDER BY topic_id ASC";
            $res_topics = mysql_query($query_topics);

            $keyboard = array();
            $row = 0;

            for ($i = 0; $i < mysql_num_rows($res_topics); $i++) {
                $topic_id = mysql_result($res_topics, $i, "topic_id");
                $question = mysql_result($res_topics, $i, "question");
                $answer = mysql_result($res_topics, $i, "answer");

                $key = [
                    'text' => $question,
                    'callback_data' => $topic_id
                ]; //Кнопка

                $keyboard[$row][] = $key;

                $row++;
            }

            $replyMarkup = [
                'inline_keyboard' => $keyboard,
                'resize_keyboard' => false,
                'one_time_keyboard' => true
            ];

            $encodedMarkup = json_encode($replyMarkup);

            $response = "Выбери тему";

            break;

        default:
            $response = "😵‍💫 Такой команды я еще не знаю";
    }
}

if ($new_message->chat_id == 385073919) { //если из моего чата, то добавляем дебаг инфо
    $response = $response
            . "\n\nDEBUG INFO\nChatId=" . $new_message->chat_id
            . "\nChatState=" . $last_message->chat_state
            . "\nMessageId=" . $last_message->message_id
            . "\nTG-MessageId=" . $new_message->tg_message_id
            . "\nChatFirstName=" . $chat_first_name
            . "\nLastCommand=" . $last_message->command
            . "\nLastText=" . $last_message->message_text
            . "\nMessage=" . $new_message->message_text
            . "\nCallbackData=" . $callback_data
            . "\nCallbackChatId=" . $callback_chat_id
            . "\nLogQuery=" . $query_log;
}

$response_from_tg = $tg_bot->send($new_message->chat_id, $response, $encodedMarkup, $disable_web_page_preview);

$log_string = "RESPONSE: " . json_encode($response_from_tg);
writeLog($log_filename, $log_string);

exit('ok'); //Обязательно возвращаем "ok", чтобы телеграмм не подумал, что запрос не дошёл