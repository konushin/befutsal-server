<?php

//callback_types:
//LEAG - информация о лиге
//SBLG - подписаться на уведомления по лиге
//SBTM - подписаться на уведомления по команде
//LTMS - список команд лиги
//TEAM - команда (id_team)
//UNST - отписаться от команды
//UNSL - отписаться от лиги

include("../database_main.php");
include("functions.php");

//токен бота mybefutsal
define('BOT_TOKEN', getParameter("myBefutsal_bot_token"));

$pre_date = date("Ym");
$url_base = "https://befutsal.ru/";
$disable_web_page_preview = true; //по умолчанию отключаем превью в чате

$bot_commands = array(
    'start' => '/start',
    'help' => '/help',
    'contribute' => '/contribute',
    'birthdays' => '/birthdays',
    'search' => '/search',
    'table' => '/table',
    'favourites' => '/favourites'
);

if (!ISSET($log_filename)) {
    $log_filename = "../logs/mybefutsal_bot_" . $pre_date . ".log";
}

$query_season = "SELECT * FROM season ORDER BY id_season DESC LIMIT 1";
$current_season = mysql_result(mysql_query($query_season), 0, "id_season");

class tg_message {

    public $message_id;
    public $chat_id;
    public $chat_state;
    public $tg_message_id;
    public $command;
    public $message_text;
    public $timestamp;

    public function setChatState($state) {
        $query_state = "UPDATE bot_mybefutsal SET chat_state = '$state' WHERE message_id =" . $this->message_id;

        $res_state = mysql_query($query_state);

        if ($res_state) {
            return true;
        } else {
            return false;
        }
    }

}

$body = file_get_contents('php://input'); //Получаем в $body json строку
$update = json_decode($body, true); //Разбираем json запрос на массив в переменную $update

$tg_bot = new tg_bot(BOT_TOKEN);

$new_message = new tg_message();

$new_message->message_text = $update['message']['text']; //Получаем текст сообщения, которое нам пришло.
$new_message->chat_id = $update['message']['chat']['id']; //Сразу и id получим, которому нужно отправлять всё это назад
$chat_first_name = $update['message']['chat']['first_name']; //Имя пользователя
$new_message->tg_message_id = $update['message']['message_id']; //id конкретного сообщения в телеге
$callback_query = $update['callback_query'];
$callback_data = $callback_query['data']; //Получаем callback после нажатия на кнопку
$callback_chat_id = $callback_query['message']['chat']['id'];

$new_message->timestamp = date("Y-m-d H:i:s");

$query_chat_state = "SELECT * FROM bot_mybefutsal WHERE chat_id = $new_message->chat_id ORDER BY `timestamp` DESC LIMIT 1";
$res_chat_state = mysql_query($query_chat_state);

$last_message = new tg_message();

if (mysql_num_rows($res_chat_state) == 1) {
    $last_message->command = mysql_result($res_chat_state, 0, "command");
    $last_message->chat_state = mysql_result($res_chat_state, 0, "chat_state");
    $last_message->message_text = mysql_result($res_chat_state, 0, "message_text");
    $last_message->message_id = mysql_result($res_chat_state, 0, "message_id");
    $last_message->tg_message_id = mysql_result($res_chat_state, 0, "tg_message_id");
} else {
    $last_message->command = null;
    $last_message->chat_state = null;
    $last_message->message_text = "stub";
}

if (($last_message->command == $bot_commands["contribute"] OR $last_message->command == $bot_commands["search"]) AND $last_message->message_text == NULL) {
    $query_log = "UPDATE bot_mybefutsal SET message_text = '$new_message->message_text' WHERE message_id = $last_message->message_id";
} else {
    $query_log = "INSERT INTO bot_mybefutsal (message_id, chat_id, tg_message_id, command, timestamp) VALUES (null, $new_message->chat_id, $new_message->tg_message_id, '$new_message->message_text', '$new_message->timestamp')";
}
$res_log = mysql_query($query_log);

$log_string = "REQUEST\nChatId=" . $new_message->chat_id . "\nTGMessageId=" . $new_message->tg_message_id . "\nChatFirstName=" . $chat_first_name . "\nMessage=" . $new_message->message_text . "\nCallbackData=" . $callback_data . "\nCallbackChatId=" . $callback_chat_id . "\nLogQuery=" . $query_log;
writeLog($log_filename, $log_string);

//СМОТРИМ ПОСЛЕДНЮЮ КОМАНДУ ОТ ПОЛЬЗОВАТЕЛЯ--------------------------------------------------
//анализируем жалобу пользователя
if ($last_message->command == $bot_commands["contribute"] AND $last_message->message_text == NULL) {

    $response = "Спасибо, я все записал и передам хозяину. А пока можем попробовать другую команду, жми " . $bot_commands['help'];
    $new_state = "init";
    $admin_chat_id = getParameter("telegram_bot_admin_chat_id");
    $response_from_tg = $tg_bot->send($admin_chat_id, "$chat_first_name ($new_message->chat_id) написал обратную связь по befutsal:\n" . $new_message->message_text, $encodedMarkup);

//анализируем строку поиска
} elseif ($last_message->command == $bot_commands["search"] AND $last_message->message_text == NULL) {

    $search_string = mysql_real_escape_string($new_message->message_text);

    //удаляем пробелы
    $search_normalized = str_replace(' ', '', $search_string);
    $search_normalized = iconv("UTF-8", "Windows-1251", $search_normalized);

    if ($search_normalized == '') {
        $response = "🤔 Я не смог понять твой запрос, давай попробуем еще раз, жми " . $bot_commands['search'];
    } else {
        //ищем игроков
        $query_player = "SELECT * FROM player WHERE (LOCATE(LCASE('" . $search_normalized . "'), LCASE(REPLACE(REPLACE(name_1, ' ', ''), '-', '')))<>0) or (LOCATE(LCASE('" . $search_normalized . "'), LCASE(REPLACE(REPLACE(name_2, ' ', ''), '-', '')))<>0) ORDER BY name_2, name_1";
        $res_player = mysql_query($query_player);
        $num_player = mysql_num_rows($res_player);

        //ищем команды
        $query_team = "SELECT DISTINCT id_team FROM competitor WHERE LOCATE(LCASE('" . $search_normalized . "'), LCASE(REPLACE(REPLACE(title, ' ', ''), '-', '')))<>0 ORDER BY title";
        $res_team = mysql_query($query_team);
        $num_teams = mysql_num_rows($res_team);

        //ищем турниры
        $query_league = "SELECT DISTINCT c.id_competition, c.title, c.season, s.title FROM competition AS c, season AS s WHERE LOCATE(LCASE('" . $search_normalized . "'), LCASE(REPLACE(REPLACE(c.title, ' ', ''), '-', '')))<>0 AND c.season = s.id_season ORDER BY c.title";
        $res_league = mysql_query($query_league);
        $num_league = mysql_num_rows($res_league);

        $num_search = $num_player + $num_team + $num_league;

        if ($num_search == 0) {
            $response = "👻 я ничего не смог найти, давай попробуем еще раз, жми " . $bot_commands['search'];
        } elseif ($num_search > 50) {
            $response = "👀 Я нашёл чересчур много всего, давай попробуем уточнить запрос, жми " . $bot_commands['search'];
        } else {
            $response = $response . "Вот, что я нашёл:\n";

            //выводим игроков
            for ($i = 0; $i < $num_player; $i++) {
                $player_id = mysql_result($res_player, $i, "id_player");
                $player = new PlayerPersonal();
                $player->getPlayerPersonal($player_id);
                $id_applicant = mysql_result(mysql_query("SELECT * FROM application WHERE id_player = $player_id ORDER BY id_applicant DESC LIMIT 1"), 0, "id_applicant");
                $response = $response . "<a href='" . $url_base . "player.php?p=" . $id_applicant . "'>👤 " . iconv("Windows-1251", "UTF-8", $player->name_2) . " " . iconv("Windows-1251", "UTF-8", $player->name_1) . "</a>\n";
            }

            //выводим команды
            for ($i = 0; $i < $num_teams; $i++) {
                $id_team = mysql_result($res_team, $i, "id_team");
                $team = new TeamInfo();
                $team->getTeamInfo($id_team);
                $res_competitor = mysql_query("SELECT * FROM competitor WHERE id_team = $id_team ORDER BY id_competitor DESC");
                $id_competitor = mysql_result($res_competitor, 0, "id_competitor");
                $response = $response . "<a href='" . $url_base . "club.php?t=" . $id_team . "'>👥 " . iconv("Windows-1251", "UTF-8", $team->title) . "</a>\n";
            }

            //выводим турниры
            for ($i = 0; $i < $num_league; $i++) {
                $id_competition = mysql_result($res_league, $i, "c.id_competition");
                $tournament = new Tournament();
                $tournament->getCompetitionInfo($id_competition);
                $response = $response . "<a href='" . $url_base . $tournament->link . "'>🏆 " . iconv("Windows-1251", "UTF-8", $tournament->title) . " (" . iconv("Windows-1251", "UTF-8", $tournament->season_details->title) . ")</a>\n";
            }
        }
    }

//анализируем callback_data
} elseif ($callback_data != NULL) {

    $callback_type = substr($callback_data, 0, 4);
    $callback_value = substr($callback_data, 5);

    switch ($callback_type) {
        case "LEAG":
            $id_league = $callback_value;
            $new_message->chat_id = $callback_chat_id;

            $tournament = new Tournament();
            $tournament->getCompetitionInfo($id_league);

            $response = "<strong>" . iconv("Windows-1251", "UTF-8", $tournament->title) . "</strong>";

            $today = date("YYYY-mm-dd");
            $table = getLeagueTable($id_league, $today);
            for ($i = 0; $i < mysql_num_rows($table); $i++) {
                $place = $i + 1;
                $team_title = iconv("Windows-1251", "UTF-8", mysql_result($table, $i, "name"));
                $team_pts = mysql_result($table, $i, "pts");

                $response = $response . "\n" . $place . ". " . $team_title . " - " . $team_pts . " очков";
            }

            $response = $response . "\n\n<a href='" . $url_base . $tournament->link . "'>Смотреть полную статистику 🌐</a>";

            $disable_web_page_preview = false;

            $button_url = $url_base . "league.php?l=" . $id_league;

            $keyboard = array();

            $key = [
                'text' => "📈 Перейти к команде",
                'callback_data' => "LTMS_" . $id_league
            ]; //Кнопка

            $keyboard[][] = $key;

            $key = [
                'text' => "🔔 Подписаться на новости турнира",
                'callback_data' => "SBLG_" . $id_league
            ]; //Кнопка

            $keyboard[][] = $key;

            $key = [
                'text' => "🌐 Перейти на страницу турнира",
                'url' => $button_url
            ]; //Кнопка

            $keyboard[][] = $key;

            $replyMarkup = [
                'inline_keyboard' => $keyboard,
                'resize_keyboard' => false,
                'one_time_keyboard' => true
            ];

            $encodedMarkup = json_encode($replyMarkup);

            break;
        case "SBLG":
            $id_league = $callback_value;
            $new_message->chat_id = $callback_chat_id;

            $tournament = new Tournament();
            $tournament->getCompetitionInfo($id_league);

            $query_subscribe = "INSERT INTO m_favourites (tg_chat_id, league_id) VALUES ($new_message->chat_id, $id_league) ON DUPLICATE KEY UPDATE league_id = $id_league";
            $res_subscribe = mysql_query($query_subscribe);

            if ($res_subscribe) {
                $response = "Договорились 🤝 Я буду присылать новости по событиям турнира <a href='" . $url_base . $tournament->link . "'><strong>" . iconv("Windows-1251", "UTF-8", $tournament->title) . "</strong></a>.";
            } else {
                $response = "😞 Упс... что-то пошло не так. Передай следующую информацию в мою поддержку через команду " . $bot_commands["contribute"] . ":\n\n" . $query_subscribe;
            }

            break;

        case "SBTM":
            $id_team = $callback_value;
            $new_message->chat_id = $callback_chat_id;

            $res_team = mysql_query("SELECT * FROM team WHERE id_team = $id_team");

            if (mysql_num_rows($res_team) == 1) {
                $query_subscribe = "INSERT INTO m_favourites (tg_chat_id, team_id) VALUES ($new_message->chat_id, $id_team) ON DUPLICATE KEY UPDATE team_id = $id_team";
                $res_subscribe = mysql_query($query_subscribe);

                if ($res_subscribe) {
                    $response = "Договорились 🤝 Я буду присылать новости по команде <a href='" . $url_base . "club.php?t=" . $id_team . "'><strong>" . iconv("Windows-1251", "UTF-8", mysql_result($res_team, 0, "title")) . "</strong></a>.";
                } else {
                    $response = "😞 Упс... что-то пошло не так. Передай следующую информацию в мою поддержку через команду " . $bot_commands["contribute"] . ":\n\n" . $query_subscribe;
                }
            } else {
                $response = "😞 Упс... что-то пошло не так. Передай следующую информацию в мою поддержку через команду " . $bot_commands["contribute"] . ":\n\n" . $query_subscribe;
            }

            break;
        case "LTMS":
            $id_league = $callback_value;
            $new_message->chat_id = $callback_chat_id;

            $query_list_teams = "SELECT * FROM competitor WHERE id_competition = $id_league";
            $res_list_teams = mysql_query($query_list_teams);

            $row = 0;

            for ($i = 0; $i < mysql_num_rows($res_list_teams); $i++) {
                $id_team = mysql_result($res_list_teams, $i, "id_team");
                $team_title = "🔔 ".iconv("Windows-1251", "UTF-8", mysql_result($res_list_teams, $i, "title"));

                $key = [
                    'text' => $team_title,
                    'callback_data' => "SBTM_" . $id_team
                ]; //Кнопка

                $keyboard[$row][] = $key;

                $row++;
            }

            $replyMarkup = [
                'inline_keyboard' => $keyboard,
                'resize_keyboard' => false,
                'one_time_keyboard' => true
            ];

            $encodedMarkup = json_encode($replyMarkup);

            $response = "Выбери команду, за которой хочешь следить:";
            
            break;

        case "UNSL":
            $id_league = $callback_value;
            $new_message->chat_id = $callback_chat_id;

            $tournament = new Tournament();
            $tournament->getCompetitionInfo($id_league);

            $query_unsubscribe = "DELETE FROM m_favourites WHERE tg_chat_id = $callback_chat_id AND league_id = $id_league";
            $res_unsubscribe = mysql_query($query_unsubscribe);

            if ($res_unsubscribe) {
                $response = "Отписал тебя от уведомлений по событиям турнира <a href='" . $url_base . $tournament->link . "'><strong>" . iconv("Windows-1251", "UTF-8", $tournament->title) . "</strong></a>.";
            } else {
                $response = "😞 Упс... что-то пошло не так. Передай следующую информацию в мою поддержку через команду " . $bot_commands["contribute"] . ":\n\n" . $query_unsubscribe;
            }

            break;

        case "UNST":
            $id_team = $callback_value;
            $new_message->chat_id = $callback_chat_id;

            $team = new TeamInfo();
            $team->getTeamInfo($id_team);

            $query_unsubscribe = "DELETE FROM m_favourites WHERE tg_chat_id = $callback_chat_id AND team_id = $id_team";
            $res_unsubscribe = mysql_query($query_unsubscribe);

            if ($res_unsubscribe) {
                $response = "Отписал тебя от новостей по команде <a href='" . $url_base . "club.php?t=" . $id_team . "'><strong>" . iconv("Windows-1251", "UTF-8", $team->title) . "</strong></a>.";
            } else {
                $response = "😞 Упс... что-то пошло не так. Передай следующую информацию в мою поддержку через команду " . $bot_commands["contribute"] . ":\n\n" . $query_unsubscribe;
            }

            break;

        default:
            break;
    }



    $new_state = "init";

//обрабатываем первую команду
} else {
    switch ($new_message->message_text) {
        case $bot_commands['start']:
            $response = "Приветствую тебя, " . $chat_first_name . "! Добро пожаловать в мир «Живи мини-футболом!» Начни с команды " . $bot_commands['help'] . " для знакомства с моими возможностями.";
            $new_state = "init";
            break;

        case $bot_commands['help']:
            $response = "Меня зовут <strong>Мой befutsal</strong>, я твой личный помощник в мире «Живи мини-футболом!» Просто выбери одну из моих доступных команд и следуй подсказкам:\n"
                    . $bot_commands['start'] . " - начать работу с ботом (то есть со мной).\n"
                    . $bot_commands['help'] . " - выводит эту справку по использованию бота (то есть меня).\n"
                    . $bot_commands['contribute'] . " - предложить свою фичу или сообщить о проблеме.\n"
                    . $bot_commands['birthdays'] . " - узнать, у кого сегодня день рождения.\n"
                    . $bot_commands['table'] . " - таблица турнира.\n"
                    . $bot_commands['search'] . " - поиск в моей базе данных.\n"
                    . $bot_commands['favourites'] . " - твое избранное.";
            $new_state = "init";
            break;

        case $bot_commands['birthdays']:
            $today = date("m-d");
            $season = $current_season - 10;
            $res_birthdays = birthdayList($today, $season);

            $log_string = "DEBUG: " . mysql_num_rows($res_birthdays);
            //writeLog($log_filename, $log_string);

            if (mysql_num_rows($res_birthdays) > 0) {
                $response = "Сегодня свой день рождения празднуют:";

                for ($i = 0; $i < mysql_num_rows($res_birthdays); $i++) {
                    $id_player = mysql_result($res_birthdays, $i, "id_player");
                    $id_applicant = mysql_result(mysql_query("SELECT * FROM application WHERE id_player = $id_player ORDER BY id_applicant DESC LIMIT 1"), 0, "id_applicant");
                    $name_1 = iconv("Windows-1251", "UTF-8", mysql_result($res_birthdays, $i, "name_1"));
                    $name_2 = iconv("Windows-1251", "UTF-8", mysql_result($res_birthdays, $i, "name_2"));
                    $response = $response . "\n👤 <a href='" . $url_base . "player.php?p=" . $id_applicant . "'>" . $name_1 . " " . $name_2 . "</a>";
                }
            } else {
                $response = "По моим данным сегодня ни у кого нет дня рождения 🙄";
            }
            $new_state = "init";
            break;

        case $bot_commands['contribute']:
            $response = "Опиши проблему или своё предложение по улучшению, я буду ждать и передам всю информацию хозяину:";
            $new_state = "contribute_input";
            break;

        case $bot_commands['search']:
            $response = "Напиши фамилию, название команды или турнира, а я попробую найти то, что есть в моей базе:";
            $new_state = "search_input";
            break;

        case $bot_commands['table']:
            $query_competitions = "SELECT * FROM competition WHERE season = $current_season AND is_cup = 0";
            $res_leagues = mysql_query($query_competitions);

            $keyboard = array();
            $row = 0;

            for ($i = 0; $i < mysql_num_rows($res_leagues); $i++) {
                $league_id = mysql_result($res_leagues, $i, "id_competition");
                $button_url = $url_base . "league.php?l=" . $league_id;
                $league_title_win1251 = mysql_result($res_leagues, $i, "title");
                $league_title = iconv("Windows-1251", "UTF-8", $league_title_win1251);

                $key = [
                    'text' => $league_title,
                    'callback_data' => "LEAG_" . $league_id
                ]; //Кнопка

                $keyboard[$row][] = $key;

                $row++;
            }

            $replyMarkup = [
                'inline_keyboard' => $keyboard,
                'resize_keyboard' => false,
                'one_time_keyboard' => true
            ];

            $encodedMarkup = json_encode($replyMarkup);

            $response = "Выбери турнир:";
            $new_state = "table_select";

            break;

        case $bot_commands['favourites']:
            $query_favourites = "SELECT * FROM m_favourites WHERE tg_chat_id = $new_message->chat_id ORDER BY league_id, team_id";
            $res_favourites = mysql_query($query_favourites);

            $num_favourites = mysql_num_rows($res_favourites);

            if ($num_favourites > 0) {
                $response = "Твое избранное:\n";

                $keyboard = array();

                for ($i = 0; $i < $num_favourites; $i++) {
                    $league_id = mysql_result($res_favourites, $i, "league_id");
                    $team_id = mysql_result($res_favourites, $i, "team_id");

                    if ($team_id != NULL) {
                        $fav_team = new TeamInfo();
                        $fav_team->getTeamInfo($team_id);
                        $response = $response . "\n👥 " . iconv("Windows-1251", "UTF-8", $fav_team->title);
                        $key_title = "🚫 " . iconv("Windows-1251", "UTF-8", $fav_team->title);
                        $key_callback = "UNST_" . $team_id;
                    }

                    if ($league_id != NULL) {
                        $fav_league = new Tournament();
                        $fav_league->getCompetitionInfo($league_id);
                        $response = $response . "\n🏆 " . iconv("Windows-1251", "UTF-8", $fav_league->title);
                        $key_title = "🚫 " . iconv("Windows-1251", "UTF-8", $fav_league->title);
                        $key_callback = "UNSL_" . $league_id;
                    }

                    $key = [
                        'text' => $key_title,
                        'callback_data' => $key_callback
                    ]; //Кнопка

                    $keyboard[$i][] = $key;
                }

                $response = $response . "\n\nЧтобы удалить из избранного, просто нажми ниже на нужную кнопку:";

                $replyMarkup = [
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => false,
                    'one_time_keyboard' => true
                ];

                $encodedMarkup = json_encode($replyMarkup);
            } else {
                $response = "😕 Ты еще не добавил ни одной команды или турнира в избранное. Попробуй команду " . $bot_commands["table"] . " и следуй моим подсказкам.";
            }

            $new_state = "favourites";
            break;

        default:
            $response = "😵‍💫 Такой команды я еще не знаю. Но ты можешь предложить моему хозяину добавить то, чего тебе не хватило, для этого используй команду " . $bot_commands['contribute'];
            $new_state = "init";
    }
}

//влключить дебаг
/*
  if ($new_message->chat_id == 385073919) { //если из моего чата, то добавляем дебаг инфо
  $response = $response
  . "\n\nDEBUG INFO\nChatId=" . $new_message->chat_id
  . "\nChatState=" . $last_message->chat_state
  . "\nMessageId=" . $last_message->message_id
  . "\nTG-MessageId=" . $new_message->tg_message_id
  . "\nLastCommand=" . $last_message->command
  . "\nLastText=" . $last_message->message_text
  . "\nMessage=" . $new_message->message_text
  . "\nCallbackData=" . $callback_data
  . "\nCallbackChatId=" . $callback_chat_id
  . "\nLogQuery=" . $query_log;
  }
 */

$response_from_tg = $tg_bot->send($new_message->chat_id, $response, $encodedMarkup, $disable_web_page_preview);

if (!$last_message->setChatState($new_state)) {
    $log_string = "UNABLE TO UPDATE STATE: " . $new_state;
} else {
    $log_string = "STATE UPDATE TO: " . $new_state;
}
//writeLog($log_filename, $log_string);

$log_string = "RESPONSE: " . json_encode($response_from_tg);
writeLog($log_filename, $log_string);

exit('ok'); //Обязательно возвращаем "ok", чтобы телеграмм не подумал, что запрос не дошёл