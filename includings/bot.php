<?php

include("functions.php");

$pre_date = date("Ym");

if (!ISSET($log_filename)) {
    $log_filename = "../logs/telegram" . $pre_date . ".log";
}

$data = file_get_contents('php://input');
$data = json_decode($data);

$log_string = "\n" . $now . " Request from VK: " . serialize($data) . "\n";
//writeLog($log_filename, $log_string);
//общение с ВК
$confirmation_token = '4fb3f12c';

switch ($data->type) {
    case 'confirmation':
        $log_string = "\n" . $now . " Confirmation response: " . $confirmation_token . "\n";
        //writeLog($log_filename, $log_string);
        echo $confirmation_token;
        break;

    case 'wall_post_new':
        if ($data->object->post_type == "post" AND $data->object->text != "") {
            $message_text = $data->object->text;
            $link = "https://vk.com/wall" . $data->object->owner_id . "_" . $data->object->id;
            $signer_id = $data->object->signer_id;

            if ($signer_id > 0) {
                $message_to_telegram = $link;
            } else {
                $message_to_telegram = $message_text . "\n<a href='" . $link . "'>Подробнее >></a>" . "\n\n #befutsal #живиминифутболом!";
            }

            $log_string = "\r" . $now . " Message text: " . $message_to_telegram . "\n";
            writeLog($log_filename, $log_string);

            $channel_id = getParameter("telegram_channel_id");

            $log_string = "\n" . $now . " Channel: " . $channel_id . "\n";
            //writeLog($log_filename, $log_string);

            $channel_id = "-1001464189306";

            $params = array(
                'chat_id' => $channel_id,
                'text' => html_entity_decode($message_to_telegram),
                'parse_mode' => "HTML"
            );

            $log_string = "\n" . $now . " Params: " . serialize($params) . "\n";
            //writeLog($log_filename, $log_string);

            $result = sendTelegram(
                    'sendMessage',
                    $params
            );

            $log_string = "\n" . $now . " Result: " . serialize($result) . "\n";
            //writeLog($log_filename, $log_string);
            //Возвращаем "ok" серверу Callback API
            echo('ok');
        } else {
            //Возвращаем "ok" серверу Callback API
            echo('ok');
        }

        break;
}