<?php

$federation = new ContactFederation();
$federation->title = "Владимирская Городская Федерация футбола, пляжного футбола, мини-футбола и футзала";
$federation->contact = array();

$person = new ContactPerson();
$person->name = "Чинкин Николай Николаевич";
$person->title = "Президент";
$person->worktel = "54-53-89";
$person->mobtel = "8(910)176-00-14";
$person->hometel = "38-67-44";

$federation->contact[0] = $person;

$person = new ContactPerson();
$person->name = "Воробьева Ольга Николаевна";
$person->title = "Бухгалтер";
$person->worktel = "21-96-43";
$person->mobtel = "8(906)561-50-86";

$federation->contact[1] = $person;

$person = new ContactPerson();
$person->name = "Чинкин Николай Николаевич";
$person->title = "Главный судья";
$person->worktel = "54-53-89";
$person->mobtel = "8(910)176-00-14";
$person->hometel = "38-67-44";

$federation->contact[2] = $person;

$person = new ContactPerson();
$person->name = " Гаченков Алексей Владимирович";
$person->title = "Ответственный секретарь";
$person->worktel = "42-09-61";
$person->mobtel = "8(904)592-50-87";

$federation->contact[3] = $person;

//оборачиваем контакты
$contacts = new Contacts();
$contacts->contacts = array();
$contacts->contacts[0] = $federation;

//вторая федерация
$federation = new ContactFederation();
$federation->title = "Владимирская Областная федерация мини-футбола";
$federation->contact = array();

$person = new ContactPerson();
$person->name = "Касаткин Константин Александрович";
$person->title = "Президент";
$person->mobtel = "8(905)140-04-73";

$federation->contact[0] = $person;

//оборачиваем контакты
$contacts->contacts[1] = $federation;

?>