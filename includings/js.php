<?php

//общий механизм js-сообщений об ошибках
print("<script language='javascript'>
    function errorMessage(errorCode) {
        switch(errorCode) {
            case 1: errorString = 'Не удалось создать таблицу в базе данных.'; break;
            case 2: errorString = 'Невозможно выполнить операцию на сервере.'; break;
        }
        errorString = errorString + ' Если эта ошибка повторится, сообщите, пожалуйста, об этом администратору сайта. Сейчас Вы будете возвращены на предыдущую страницу.';
        alert(errorString);
        history.back(-1);
    }
</script>");

//----скрипт для скрытого меню
print('<script>
    function collapseElement(id) {
        if ( document.getElementById(id).style.display != "none" ) {
            document.getElementById(id).style.display = "none";
        }
        else {
            document.getElementById(id).style.display = "";
        }
    }
</script>');

?>