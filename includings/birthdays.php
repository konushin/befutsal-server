<?php

$today = date("m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
$tomorrow = date("m-d", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
$after_tomorrow = date("m-d", mktime(0, 0, 0, date("m"), date("d") + 2, date("Y")));

$past_season = $current_season - 5;

$query = "SELECT COUNT(id_player) AS num_players FROM player WHERE 
(DATE_FORMAT(birthdate, '%m-%d') = '$today') OR (DATE_FORMAT(birthdate, 
'%m-%d') = '$tomorrow') OR (DATE_FORMAT(birthdate, '%m-%d') = 
'$after_tomorrow') AND died = 0";

$res_birthdays = mysql_query($query, $db_connection);
$num_birthdays = mysql_result($res_birthdays, 0, "num_players");

if ($num_birthdays != 0) {
    print('

	 <td valign="top" width="203">
	  <div style="padding-left: 15px;">
	  '.getParameter("mobile_app_ios_badge").'
	  </div>
	  <div style="padding-left: 5px;">
	  '.getParameter("mobile_app_android_badge").'
	  </div>
      <div>
      <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr>
	    <td width="20"><img src="graphics/p_right_01.gif" width="20" height="14" border="0" alt=""></td>
	    <td width="183" height="14" valign="top"><img src="graphics/p_right_02.gif" width="183" height="14" border="0" alt=""></td>
	   </tr>
	   <tr>
	    <td valign="top" height="20"><img src="graphics/p_right_03.gif" width="20" height="20" border="0" alt=""></td>
	    <td background="graphics/p_bg_green.gif" bgcolor="#1BAC29" class="w_text_b" style="padding-left:15px;">Поздравления</td>
	   </tr>
	   <tr>
	    <td width="20" height="13"><img src="graphics/p_right_04.gif" width="20" height="13" border="0" alt=""></td>
	    <td valign="top" width="183" height="13"><img src="graphics/p_right_05.gif" width="183" height="13" border="0" alt=""></td>
	   </tr>
      </table>
	  </div>
	 <div align="center">
      <table width="170" cellpadding="0" cellspacing="0" border="0">
	');
}

//--------------------------------сегодня---------------------------------------------
$res_birthdays = birthdayList($today, $past_season);
$num_birthdays = mysql_num_rows($res_birthdays);

if ($num_birthdays > 0) {
    print('    <tr>
	    <td class="o_text" style="padding-left:10px;padding-bottom:3px;" valign="bottom" height="25">Сегодня</td>
	   </tr>
	   <tr>
	    <td height="1" background="graphics/dot_bg.gif"><img src="graphics/0.gif" width="1" height="1" border="0" alt=""></td>
	   </tr>
	   <tr>
	    <td valign="top">
 ');
    for ($i = 0; $i < $num_birthdays; $i++) {
        $id_player = mysql_result($res_birthdays, $i, "id_player");
        $name = mysql_result($res_birthdays, $i, "name_1") . " " . mysql_result($res_birthdays, $i, "name_2");
        $res_application = mysql_query("SELECT *
   FROM application, competitor, competition
   WHERE application.id_player = $id_player
    AND application.id_competitor = competitor.id_competitor
    AND competitor.id_competition = competition.id_competition
    AND competition.season = $current_season
    AND application.active = 1
   ORDER BY application.id_applicant");
        $team_link = "";
        for ($t = 0; $t < mysql_num_rows($res_application); $t++) {
            $team_id = mysql_result($res_application, $t, "application.id_competitor");
            $team_title = mysql_result($res_application, $t, "competitor.title");
            $team_link = $team_link . " [<a href='statistics/teams.php?id=" . $team_id . "'>" . $team_title . "</a>]";
        }
        print('<div style="padding:3 5 0 10"><a href="statistics/history_player.php?id=' . $id_player . '">' . $name . '</a>' . $team_link . '</div>');
    }
    print('
		</td>
	   </tr>
 ');
}

//---------------------------------завтра---------------------------------------------
$res_birthdays = birthdayList($tomorrow, $past_season);
$num_birthdays = mysql_num_rows($res_birthdays);

if ($num_birthdays > 0) {
    print('    <tr>
	    <td class="o_text" style="padding-left:10px;padding-bottom:3px;" valign="bottom" height="25">Завтра</td>
	   </tr>
	   <tr>
	    <td height="1" background="graphics/dot_bg.gif"><img src="graphics/0.gif" width="1" height="1" border="0" alt=""></td>
	   </tr>
	   <tr>
	    <td valign="top">
 ');
    for ($i = 0; $i < $num_birthdays; $i++) {
        $id_player = mysql_result($res_birthdays, $i, "id_player");
        $name = mysql_result($res_birthdays, $i, "name_1") . " " . mysql_result($res_birthdays, $i, "name_2");
        $res_application = mysql_query("SELECT *
   FROM application, competitor, competition
   WHERE application.id_player = $id_player
    AND application.id_competitor = competitor.id_competitor
    AND competitor.id_competition = competition.id_competition
    AND competition.season = $current_season
    AND application.active = 1
   ORDER BY application.id_applicant");
        $team_link = "";
        for ($t = 0; $t < mysql_num_rows($res_application); $t++) {
            $team_id = mysql_result($res_application, $t, "application.id_competitor");
            $team_title = mysql_result($res_application, $t, "competitor.title");
            $team_link = $team_link . " [<a href='statistics/teams.php?id=" . $team_id . "'>" . $team_title . "</a>]";
        }
        print('<div style="padding:3 5 0 10"><a href="statistics/history_player.php?id=' . $id_player . '">' . $name . '</a>' . $team_link . '</div>');
    }
    print('
		</td>
	   </tr>
 ');
}

//---------------------------------послезавтра---------------------------------------------
$res_birthdays = birthdayList($after_tomorrow, $past_season);
$num_birthdays = mysql_num_rows($res_birthdays);

if ($num_birthdays > 0) {
    print('    <tr>
	    <td class="o_text" style="padding-left:10px;padding-bottom:3px;" valign="bottom" height="25">Послезавтра</td>
	   </tr>
	   <tr>
	    <td height="1" background="graphics/dot_bg.gif"><img src="graphics/0.gif" width="1" height="1" border="0" alt=""></td>
	   </tr>
	   <tr>
	    <td valign="top">
 ');
    for ($i = 0; $i < $num_birthdays; $i++) {
        $id_player = mysql_result($res_birthdays, $i, "id_player");
        $name = mysql_result($res_birthdays, $i, "name_1") . " " . mysql_result($res_birthdays, $i, "name_2");
        $res_application = mysql_query("SELECT *
   FROM application, competitor, competition
   WHERE application.id_player = $id_player
    AND application.id_competitor = competitor.id_competitor
    AND competitor.id_competition = competition.id_competition
    AND competition.season = $current_season
    AND application.active = 1
   ORDER BY application.id_applicant");
        $team_link = "";
        for ($t = 0; $t < mysql_num_rows($res_application); $t++) {
            $team_id = mysql_result($res_application, $t, "application.id_competitor");
            $team_title = mysql_result($res_application, $t, "competitor.title");
            $team_link = $team_link . " [<a href='statistics/teams.php?id=" . $team_id . "'>" . $team_title . "</a>]";
        }
        print('<div style="padding:3 5 0 10"><a href="statistics/history_player.php?id=' . $id_player . '">' . $name . '</a>' . $team_link . '</div>');
    }
    print('
		</td>
	   </tr>
 ');

    print('
   </table>
	 </div>');
}

?>
