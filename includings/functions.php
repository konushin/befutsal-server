<?php

class TournamentTable {

    public $table_items;

}

class TeamInfo {

    public $id_team;
    public $title;
    public $founded;
    public $emblem_path;
    public $comments;
    public $existence;
    public $photo_path;
    public $website;
    public $tshirt_home;
    public $tshirt_away;
    public $rank;
    public $stars_path;
    public $followers;

    /**
     * ��������� ���������� � �������
     * @param $id_team int id �������
     * @return $this TeamInfo
     */
    public function getTeamInfo($id_team) {
        $query = "SELECT * FROM team WHERE id_team = $id_team";
        $res_team = mysql_query($query);
        if (mysql_num_rows($res_team) == 1) {
            $this->id_team = $id_team;
            $this->comments = nl2br(mysql_result($res_team, 0, "comments"));
            $this->emblem_path = mysql_result($res_team, 0, "emblem_path");
            if ($this->emblem_path == null) {
                $this->emblem_path = getParameter("default_emblem");
                ;
            }
            $this->existence = mysql_result($res_team, 0, "existence");
            $this->founded = mysql_result($res_team, 0, "founded");
            $this->photo_path = mysql_result($res_team, 0, "photo_path");
            $this->title = mysql_result($res_team, 0, "title");
            $this->tshirt_away = mysql_result($res_team, 0, "tshirt_away");
            $this->tshirt_home = mysql_result($res_team, 0, "tshirt_home");
            $this->website = mysql_result($res_team, 0, "website");
            $this->rank = getTeamRank($id_team, 0);

            //�������, � �������� ������������� ������� � ���������
            $query = "SELECT COUNT(DISTINCT user_id) AS num_users
                      FROM m_favourites
                      WHERE team_id = $id_team";
            $res_favourites = mysql_query($query);
            $this->followers = mysql_result($res_favourites, 0, "num_users");
            $max_stars = getTopFavouriteTeam();
            $diff = $this->followers / $max_stars;
            if ($diff > 0.84) {
                $this->stars_path = "../graphics/star6.png";
            } elseif ($diff > 0.70) {
                $this->stars_path = "../graphics/star5.png";
            } elseif ($diff > 0.56) {
                $this->stars_path = "../graphics/star4.png";
            } elseif ($diff > 0.42) {
                $this->stars_path = "../graphics/star3.png";
            } elseif ($diff > 0.28) {
                $this->stars_path = "../graphics/star2.png";
            } elseif ($diff > 0.14) {
                $this->stars_path = "../graphics/star1.png";
            } else {
                $this->stars_path = "../graphics/star0.png";
            }
        }
        return $this;
    }

    /**
     * ��������� ���������� � ������� �� ������
     * @param $id_competitor int id ������ �������
     * @return $this TeamInfo
     */
    public function getTeamInfoByCompetitor($id_competitor) {
        $query = "SELECT * FROM competitor WHERE id_competitor = $id_competitor";
        $res_competitor = mysql_query($query);
        if (mysql_num_rows($res_competitor) == 1) {
            $id_team = mysql_result($res_competitor, 0, "id_team");
            $this->getTeamInfo($id_team);
        }
        return $this;
    }

}

class Tshirt {

    public $id_tshirt;
    public $picture;
    public $description;
    public $order;

    /**
     * ��������� ���������� � �����
     * @param $id_tshirt int id �����
     * @return $this
     */
    public function getTshirtDetails($id_tshirt) {
        $res_tshirt = mysql_query("SELECT * FROM `t-shirt` WHERE id_tshirt = $id_tshirt");
        if (mysql_num_rows($res_tshirt) == 1) {
            $this->id_tshirt = $id_tshirt;
            $this->picture = mysql_result($res_tshirt, 0, "picture");
            $this->description = mysql_result($res_tshirt, 0, "description");
            $this->order = mysql_result($res_tshirt, 0, "order");
        }
        return $this;
    }

}

class TeamCompetitor {

    public $id_competitor;
    public $team_details;
    public $competition_details;
    public $title;
    public $shortname;
    public $farmclub_of;
    public $farmclub;

    /**
     * ��������� ���������� � ������ �������
     * @param $id_competitor int id ������ �������
     * @return $this
     */
    public function getTeamCompetitor($id_competitor) {
        $query = "SELECT * FROM competitor WHERE id_competitor = $id_competitor";
        $res_competitor = mysql_query($query);
        if (mysql_num_rows($res_competitor) == 1) {
            $this->id_competitor = $id_competitor;
            $id_competition = mysql_result($res_competitor, 0, "id_competition");
            $this->title = mysql_result($res_competitor, 0, "title");
            $this->shortname = mysql_result($res_competitor, 0, "shortname");
            if ($this->shortname == '') {
                $this->shortname = mb_strtoupper(substr(strtr($this->title, [' ' => '', '-' => '']), 0, 6), 'UTF-8');
            }
            $this->competition_details = new Tournament();
            $this->competition_details->getCompetitionInfo($id_competition);
            $this->team_details = new TeamInfo();
            $this->team_details->getTeamInfoByCompetitor($this->id_competitor);
        }
        return $this;
    }

    /**
     * ��������� ���������� � ����-����� ��� ������� �������
     * ������������� farmclub - ���� � ���� ������� ���� ����-����, ��� farmclub_of - ���� ��� ������� ���� ����-����
     */
    public function getFarmclubInfo() {
        $query = "SELECT farmclub_id FROM competitor WHERE id_competitor = $this->id_competitor";
        $res_farmclub = mysql_query($query);
        if (mysql_num_rows($res_farmclub) == 1) {
            $this->farmclub = mysql_result($res_farmclub, 0, "farmclub_id");
        }

        $query = "SELECT id_competitor FROM competitor WHERE farmclub_id = $this->id_competitor";
        $res_farmclub = mysql_query($query);
        if (mysql_num_rows($res_farmclub) == 1) {
            $this->farmclub_of = mysql_result($res_farmclub, 0, "id_competitor");
        }
    }

    /**
     * ������ �������� �������� �������
     * @return $avg_age
     */
    public function getAverageAge() {
        $date = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
        ;
        $query = "SELECT SUBSTRING(AVG(EXTRACT(YEAR FROM DATE_SUB('$date', INTERVAL DATE_FORMAT(player.birthdate, '%Y') YEAR))), 1, 4) AS age
FROM player, application
WHERE application.id_player = player.id_player
      AND application.id_competitor = $this->id_competitor
      AND player.birthdate > '1900-01-01'
      AND application.active = 1
GROUP BY application.id_competitor";
        $res_query = mysql_query($query);
        if (mysql_num_rows($res_query) == 1) {
            $avg_age = mysql_result($res_query, 0, "age");
        } else {
            $avg_age = null;
        }
        return $avg_age;
    }

}

class PlayerPersonal {

    public $id_player;
    public $name_1;
    public $name_2;
    public $birthdate;
    public $address;
    public $photo_path;
    public $goalie;
    public $died;

    /**
     * ��������� ���������� �� ������
     * @param $id_player int id ������
     * @return $this PlayerPersonal ���������� �� ������
     */
    public function getPlayerPersonal($id_player) {
        $query = "SELECT * FROM player WHERE id_player = $id_player";
        $res_player = mysql_query($query);
        if (mysql_num_rows($res_player) == 1) {
            $this->id_player = $id_player;
            $this->name_1 = mysql_result($res_player, 0, "name_1");
            $this->name_2 = mysql_result($res_player, 0, "name_2");
            $this->birthdate = mysql_result($res_player, 0, "birthdate");
            $this->address = mysql_result($res_player, 0, "address");
            $this->photo_path = mysql_result($res_player, 0, "photo_path");
            $this->goalie = mysql_result($res_player, 0, "goalie");
            $this->died = mysql_result($res_player, 0, "died");
        }
        return $this;
    }

    /**
     * ��������� ���������� �� ������ �� id ������
     * @param $id_applicant
     * @return $this
     */
    public function getPlayerPersonalByApplicant($id_applicant) {
        $query = "SELECT * FROM application WHERE id_applicant = $id_applicant";
        $res_applicant = mysql_query($query);
        if (mysql_num_rows($res_applicant) == 1) {
            $id_player = mysql_result($res_applicant, 0, "id_player");
            $this->getPlayerPersonal($id_player);
        }
        return $this;
    }

}

class PlayerApplication {

    public $id_applicant;
    public $player_personal;
    public $team_details;
    public $represent;
    public $goalie;
    public $active;
    public $number;

    /**
     * ��������� ������ ������
     * @param int $id_applicant id ������
     * @return $this PlayerApplication
     */
    public function getPlayerApplication($id_applicant) {
        $query = "SELECT * FROM application WHERE id_applicant = $id_applicant";
        $res_application = mysql_query($query);
        if (mysql_num_rows($res_application) == 1) {
            $this->id_applicant = $id_applicant;
            $this->goalie = mysql_result($res_application, 0, "goalie");
            $this->represent = mysql_result($res_application, 0, "represent");
            $this->active = mysql_result($res_application, 0, "active");
            $this->number = mysql_result($res_application, 0, "number");
            $this->player_personal = new PlayerPersonal();
            $this->player_personal->getPlayerPersonalByApplicant($this->id_applicant);
            $this->team_details = new TeamCompetitor();
            $id_competitor = mysql_result($res_application, 0, "id_competitor");
            $this->team_details->getTeamCompetitor($id_competitor);
        }
        return $this;
    }

}

class Tournament {

    public $competition_id;
    public $season_details;
    public $title;
    public $comments;
    public $promoted;
    public $relegated;
    public $sort_order;
    public $page;
    public $pitch;
    public $matches_in_tour;
    public $forum_link;
    public $is_cup;
    public $rate;
    public $category;
    public $exception_date;
    public $exception_sign;
    public $win_points;
//��������� ������ � ���������� �������
    public $groups_end;
    public $playoff_start;
    public $playoff_end;
    public $link;

    /**
     * @param $competition_id int id �������
     * @return $this Tournament ���������� � �������
     */
    public function getCompetitionInfo($competition_id) {
        $query = "SELECT * FROM competition LEFT JOIN competition_by_category
 ON competition_by_category.id_competition = competition.id_competition WHERE competition.id_competition = $competition_id";
        $res_competition = mysql_query($query);
        if ($res_competition) {
            $this->competition_id = $competition_id;
            $this->season_details = new SeasonInfo();
            $this->season_details->getSeasonByCompetition($this->competition_id);
            $this->comments = mysql_result($res_competition, 0, "comments");
            $this->forum_link = mysql_result($res_competition, 0, "forum_link");
            $this->is_cup = mysql_result($res_competition, 0, "is_cup");
            $this->matches_in_tour = mysql_result($res_competition, 0, "matches_in_tour");
            $this->page = mysql_result($res_competition, 0, "page");
            if ($this->page == NULL and $this->is_cup == 0) {
                $this->page = "statistics/league_presentation.php?league=" . $competition_id;
            } elseif ($this->page == NULL and $this->is_cup == 1) {
                $this->page = "statistics/cup_presentation.php?league=" . $competition_id;
            } elseif ($this->page == NULL and $this->is_cup == 2) {
                $this->page = "statistics/tournament.php?league=" . $competition_id;
            }
            $this->pitch = mysql_result($res_competition, 0, "pitch");
            $this->promoted = mysql_result($res_competition, 0, "promoted");
            $this->relegated = mysql_result($res_competition, 0, "relegated");
            $this->rate = mysql_result($res_competition, 0, "rate");
            $this->sort_order = mysql_result($res_competition, 0, "sort_order");
            $this->title = mysql_result($res_competition, 0, "title");
            $this->win_points = mysql_result($res_competition, 0, "win_pts");
            $this->category = array();
            for ($i = 0; $i < mysql_num_rows($res_competition); $i++) {
                $category_id = mysql_result($res_competition, $i, "competition_by_category.id_category");
                $this->category[$i] = new CompetitionCategory();
                $this->category[$i]->getCategoryInfo($category_id);
            }
            if ($this->is_cup == 1) {
                $this->link = "cup.php?l=" . $this->competition_id;
            } elseif ($this->is_cup == 2) {
                $this->link = "tournament.php?l=" . $this->competition_id;
            } else {
                $this->link = "league.php?l=" . $this->competition_id;
            }
        }
        return $this;
    }

    /**
     * @return $this Tournament ���������� � ����� ���������� ������ � �������
     */
    public function getGroupsDates() {
        $query = "SELECT * FROM tournament WHERE id_competition = $this->competition_id";

        $res_dates = mysql_query($query);
        if (mysql_num_rows($res_dates) > 0) {
            $this->groups_end = mysql_result($res_dates, 0, "groups_end");
            $this->playoff_start = mysql_result($res_dates, 0, "playoff_start");
            $this->playoff_end = mysql_result($res_dates, 0, "playoff_end");
        }
        return $this;
    }

}

class CompetitionCategory {

    public $category_id;
    public $title;
    public $location;
    public $federation;
    public $order;
    public $picture;

    /**
     * @param $category_id int id ���������
     * @return $this CompetitionCategory ���������� � ��������� �������
     */
    public function getCategoryInfo($category_id) {
        if ($category_id == null) {
            $query = "SELECT * FROM competition_category WHERE id_category IS NULL";
        } else {
            $query = "SELECT * FROM competition_category WHERE id_category = $category_id";
        }

        $res_category = mysql_query($query);
        if (mysql_num_rows($res_category) == 1) {
            $this->category_id = $category_id;
            $this->title = mysql_result($res_category, 0, "title");
            $this->federation = mysql_result($res_category, 0, "federation");
            $this->location = mysql_result($res_category, 0, "location");
            $this->order = mysql_result($res_category, 0, "order");
            $this->picture = mysql_result($res_category, 0, "picture");
        }
        return $this;
    }

}

class SeasonInfo {

    public $id_season;
    public $title;

    /**
     * ��������� ���������� � ������
     * @param $id_season int id ������
     * @return $this Season
     */
    public function getSeasonInfo($id_season) {
        $query = "SELECT * FROM season WHERE id_season = $id_season";
        $res_season = mysql_query($query);
        if (mysql_num_rows($res_season) == 1) {
            $this->id_season = $id_season;
            $this->title = mysql_result($res_season, 0, "title");
        }
        return $this;
    }

    public function getSeasonByCompetition($id_competition) {
        $query = "SELECT * FROM competition WHERE id_competition = $id_competition";
        $res_competition = mysql_query($query);
        if (mysql_num_rows($res_competition) == 1) {
            $id_season = mysql_result($res_competition, 0, "season");
            $this->getSeasonInfo($id_season);
        }
        return $this;
    }

}

class Transfer {

    public $id_transfer;
    public $player;
    public $old_team;
    public $new_team;
    public $date;

}

class TransferList {

    public $transfers;

    /**
     * 
     * @param int $id_season - id ������
     * @return array ������ ����������
     */
    function getTransfers($id_season) {

        $res_transfers = mysql_query("SELECT *
 FROM transfer, competitor, competition
 WHERE old_team = competitor.id_competitor
  AND competitor.id_competition = competition.id_competition
  AND competition.season = $id_season
 ORDER BY date DESC, new_team, old_team");
        $num_transfers = mysql_num_rows($res_transfers);

        $transfers = array();

        for ($i = 0; $i < $num_transfers; $i++) {
            $transfer = new Transfer();

            $transfer->id_transfer = mysql_result($res_transfers, $i, "id_transfer");
            $old_team = mysql_result($res_transfers, $i, "old_team");
            $new_team = mysql_result($res_transfers, $i, "new_team");
            $id_player = mysql_result($res_transfers, $i, "id_player");

            $transfer->old_team = new TeamCompetitor();
            $transfer->old_team->getTeamCompetitor($old_team);
            $transfer->new_team = new TeamCompetitor();
            $transfer->new_team->getTeamCompetitor($new_team);
            $transfer->date = mysql_result($res_transfers, $i, "date");
            $day = (int) substr($transfer->date, 8, 2);
            $n_month = substr($transfer->date, 5, 2);
            $n_year = substr($transfer->date, 0, 4);
            $date = $day . " " . ruMonth($n_month) . " " . $n_year;
            $transfer->date = iconv('windows-1251', 'utf-8', $date);

            $transfer->player = new PlayerPersonal();
            $transfer->player->getPlayerPersonal($id_player);

            if ($transfer->player->id_player != null) {
                $this->transfers[] = $transfer;
            }
        }

        return $this->transfers;
    }

}

class tg_bot {

    public $token = ''; //������ ��������� ���������� ��� ������, ������� ����� ���������� ������ ��� ��� ������������� ��� ��

    /**
     * 
     * @param int $token ����� ����
     * @return object tg_bot
     */
    public function __construct($token) {
        $this->token = $token; //�������� � ���������� ����� ��� ���������� ������
    }

    /**
     * ���������� ��������� � ���
     * @param int $chat_id Id ���� ��� �������� ���������
     * @param string $message ����� ������������� ���������
     * @param object $reply_markup https://core.telegram.org/bots/api#replykeyboardmarkup
     * @param boolean $disable_web_page_preview ��������� ���-������
     * @return object Message ����� https://core.telegram.org/bots/api#message
     */
    public function send($chat_id, $message, $reply_markup, $disable_web_page_preview) {
        //��������� ������ $data �����, ������� �� ����� api �������� �� ����������
        $data = array(
            'chat_id' => $chat_id,
            'text' => $message,
            'parse_mode' => 'HTML',
            'reply_markup' => $reply_markup,
            'disable_web_page_preview' => $disable_web_page_preview
        );

        $out = $this->request('sendMessage', $data);

        return $out;
    }

    /**
     * ���������� ��������� ������
     * @param string $method ����� api telegram
     * @param array $data ������
     * @return object Out �����
     */
    public function request($method, $data = array()) {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot' . $this->token . '/' . $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST'); //���������� ����� POST
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //���� ������ ������������

        $out = json_decode(curl_exec($curl), true); //�������� ��������� ����������, ������� ����� �������������� �� JSON'a � ������ ��� ��������

        curl_close($curl); //��������� ����

        return $out; //���������� ����� � ���� �������
    }

}

/**
 * �������� ��������� ������
 * @param string $string - �������� ������
 * @param int $maxlen - ���������� �������� ����� ���������
 * @return string - �������� ������
 */
function cutString($string, $maxlen) {
    $len = (mb_strlen($string) > $maxlen) ? mb_strripos(mb_substr($string, 0, $maxlen), ' ') : $maxlen;
    $cutStr = mb_substr($string, 0, $len);
    return (mb_strlen($string) > $maxlen) ? $cutStr . '...' : $cutStr;
}

/**
 * ������� ������ �� �������
 * @param int $n_month ���������� ����� ������
 * @return string ������ � �������
 */
function ruMonth($n_month) {
    switch ($n_month) {
        case 1:
            $s_month = "������";
            break;
        case 2:
            $s_month = "�������";
            break;
        case 3:
            $s_month = "�����";
            break;
        case 4:
            $s_month = "������";
            break;
        case 5:
            $s_month = "���";
            break;
        case 6:
            $s_month = "����";
            break;
        case 7:
            $s_month = "����";
            break;
        case 8:
            $s_month = "�������";
            break;
        case 9:
            $s_month = "��������";
            break;
        case 10:
            $s_month = "�������";
            break;
        case 11:
            $s_month = "������";
            break;
        case 12:
            $s_month = "�������";
            break;
    }
    return $s_month;
}

/**
 * ������� ��� ������ �� �������
 * @param int $eng_week_day ���� ������ �� ����������
 * @return string ���� ������ �� �������
 */
function ruWeekDay($eng_week_day) {
    switch ($eng_week_day) {
        case 'Mon':
            $rus_week_day = "�����������";
            break;
        case 'Tue':
            $rus_week_day = "�������";
            break;
        case 'Wed':
            $rus_week_day = "�����";
            break;
        case 'Thu':
            $rus_week_day = "�������";
            break;
        case 'Fri':
            $rus_week_day = "�������";
            break;
        case 'Sat':
            $rus_week_day = "�������";
            break;
        case 'Sun':
            $rus_week_day = "�����������";
            break;
    }
    return $rus_week_day;
}

/**
 * ��������� ����� ������� � ��������
 * @param int $id_team - id �������
 * @param bool $max : 0-�������, 1-������ �� ��� �������
 * @return int ����� ������� � ��������
 */
function getTeamRank($id_team, $max) {
    if ($max == 0) {
        $rank_date = getParameter("befutsal_ranking_date");
        $query = "SELECT * FROM ranking WHERE id_team = $id_team AND `date` = '$rank_date' ORDER BY `date` DESC LIMIT 1";
    } else {
        $query = "SELECT * FROM ranking WHERE id_team = $id_team ORDER BY rank ASC LIMIT 1";
    }
    $res_rank = mysql_query($query);
    if (mysql_num_rows($res_rank) > 0) {
        $rank = mysql_result($res_rank, 0, "rank");
    } else {
        $rank = 0;
    }
    return $rank;
}

/**
 * ��������� �������� �������
 * @param int $id_team - id �������-���������
 * @param bool $max : 0-�������, 1-������ �� ��� �������
 * @return float|null ������� ������� ��� null, ���� �� ��� � ��������
 */
function getTeamRate($id_team, $max) {
    //�������� ������������ id �������
    $query = "SELECT * FROM competitor WHERE id_competitor = $id_team";
    $res_team = mysql_query($query);
    $team_historical = mysql_result($res_team, 0, "id_team");

    if ($max == 0) {
        $rank_date = getParameter("befutsal_ranking_date");
        $query = "SELECT * FROM ranking WHERE id_team = $team_historical AND `date` = '$rank_date' ORDER BY `date` DESC LIMIT 1";
    } else {
        $query = "SELECT * FROM ranking WHERE id_team = $team_historical ORDER BY rank ASC LIMIT 1";
    }
    $res_rate = mysql_query($query);
    if (mysql_num_rows($res_rate) > 0) {
        $rate = mysql_result($res_rate, 0, "rate");
    } else {
        $rate = null;
    }
    return $rate;
}

/**
 * ������� "�����" �������
 * @param int $id_team �������-���������
 * @param int $limit ���������� ��������� ���
 * @return array PERFORMANCE=���� ��������� ����� � ��������� �����, SCORE=���������� ����� � ����� � �������� �������, � �������, RESULTS=���������� ��� � ��������������� ������� � ������� W/D/L
 */
function getTeamForm($id_team, $limit) {
    $team_form = array();

    $query = "SELECT * FROM competitor WHERE id_competitor = $id_team";
    $res_team = mysql_query($query);
    $team_historical = mysql_result($res_team, 0, "id_team");
    $id_competition = mysql_result($res_team, 0, "id_competition");

    //������� ����� �� ������
    $res_competition = mysql_query("SELECT * FROM competition WHERE id_competition = $id_competition");
    $win_points = (int) mysql_result($res_competition, 0, "win_pts");

    //����� ��������� ���
    $query = "SELECT * FROM protocol WHERE team_1 IN (SELECT id_competitor FROM competitor WHERE id_team = $team_historical) OR team_2 IN (SELECT id_competitor FROM competitor WHERE id_team = $team_historical) AND forfeited IS NULL ORDER BY dt_protocol DESC LIMIT $limit";
    $res_games = mysql_query($query);

    //������������ �����
    $form = 0;
    $score = (int) 0;
    $results = "";
    for ($i = 0; $i < mysql_num_rows($res_games); $i++) {
        $t1 = mysql_result($res_games, $i, "team_1");
        $t2 = mysql_result($res_games, $i, "team_2");
        $s1 = (int) mysql_result($res_games, $i, "score_end_1");
        $s2 = (int) mysql_result($res_games, $i, "score_end_2");
        $et = (int) mysql_result($res_games, $i, "extratime");
        $pen = (int) mysql_result($res_games, $i, "on_penalty");

        $homeaway = null;

        //��������� ���������� ������/�������
        $query = "SELECT * FROM competitor WHERE id_competitor = $t1";
        $res_team1 = mysql_query($query);
        $team1_historical = mysql_result($res_team1, 0, "id_team");
        if ($team1_historical == $team_historical) {
            $homeaway = "home";
        }

        $query = "SELECT * FROM competitor WHERE id_competitor = $t2";
        $res_team2 = mysql_query($query);
        $team2_historical = mysql_result($res_team2, 0, "id_team");
        if ($team2_historical == $team_historical) {
            $homeaway = "away";
        }

        if (($homeaway == "home" AND $s1 > $s2 AND $et != 1 AND $pen != 1) OR ($homeaway == "away" AND $s1 < $s2 AND $et != 1 AND $pen != 1)) {
            $form = $form + $win_points;
            $results = "W" . $results;
        } elseif ($homeaway AND $s1 == $s2 OR $et == 1 OR $pen == 1) {
            $form = $form + 1;
            $results = "D" . $results;
        } else {
            $results = "L" . $results;
        }
        $score = $score + $s1 + $s2;
    }

    if (mysql_num_rows($res_games) > 0) {
        $team_form["performance"] = $form / (mysql_num_rows($res_games) * $win_points);
        $team_form["score"] = $score / mysql_num_rows($res_games);
        $team_form["results"] = $results;
    } else {
        $team_form["performance"] = 0;
        $team_form["score"] = 0;
        $team_form["results"] = "";
    }

    return $team_form;
}

/**
 * ��������� ������ ����������� ����
 * @param int $league ����
 * @param string $limit ���������� ����������� ('LIMIT 0,10')
 * @return resource ������ �����������
 */
function topScorers($league, $limit) {
    $query = "SELECT application.id_competitor AS id_team, application.id_applicant, name_1, name_2, photo_path, title, application.goalie, COUNT(staff.id_player) AS played, SUM(staff.scored) AS scored, transfer.id_transfer
FROM player, competitor, staff, application
  LEFT JOIN transfer ON application.id_applicant = transfer.id_player
WHERE competitor.id_competitor = application.id_competitor
      AND application.id_player = player.id_player
      AND staff.id_player = application.id_applicant
      AND competitor.id_competition = $league
      AND application.active = 1
      AND staff.scored > 0
      AND (application.id_competitor = transfer.new_team OR transfer.id_transfer IS NULL)
GROUP BY application.id_applicant
ORDER BY scored DESC, played DESC, name_2, name_1 " . $limit;
    $res_scorers = mysql_query($query);

    return $res_scorers;
}

/**
 * ��������� ������ ����������� �������
 * @param int $team_id id ������� � �������
 * @param string $limit ���������� ����������� ('LIMIT 0,10')
 * @return resource ������ �����������
 */
function topScorersByTeam($team_id, $limit) {
    $query = "SELECT application.id_competitor AS id_team, application.id_applicant, name_1, name_2, title, application.goalie, application.number, COUNT(staff.id_player) AS played, SUM(staff.scored) AS scored
 FROM player, competitor, application, staff
 WHERE competitor.id_competitor = application.id_competitor
  AND application.id_player = player.id_player
  AND staff.id_player = application.id_applicant
  AND competitor.id_competitor = $team_id
  AND application.active = 1
 GROUP BY application.id_applicant
 ORDER BY scored DESC, played DESC, name_2, name_1 " . $limit;
    $res_scorers = mysql_query($query);
    return $res_scorers;
}

/**
 * ��������� �������� ��������� �� ����� �� ������� settings
 * @param string $parameter_name ��� ��������� � �������
 * @return null|string �������� ���������
 */
function getParameter($parameter_name) {
    $query = "SELECT * FROM settings WHERE parameter = '$parameter_name'";
    $res_parameter = mysql_query($query);
    if (mysql_num_rows($res_parameter) > 0) {
        $parameter_value = mysql_result($res_parameter, 0, "value");
        return $parameter_value;
    } else {
        return null;
    }
}

/**
 * �����������
 * @param string $filename ��� �����
 * @param string $log_string ���������� ������
 */
function writeLog($filename, $log_string) {
    $log = fopen($filename, 'a+');
    $time = date("Y-m-d H:i:s");
    $log_string = $time . ": " . $log_string . "\n";
    fwrite($log, $log_string);
    fclose($log);
}

/**
 * ��������� ������ ���������������
 * @param string $date ����
 * @param int $competition ����
 * @return resource ������ ���������������
 */
function getBans($date, $competition = 0) {
    $res_current_season = mysql_query("SELECT * FROM season ORDER BY id_season DESC LIMIT 0,1");
    $current_season = mysql_result($res_current_season, 0, "id_season");

    if ($competition > 0) {
        $add_string = " AND cn.id_competition = $competition ";
    } else {
        $add_string = "";
    }

    $query = "SELECT a.id_applicant, a.goalie, pl.name_1 AS name_1, pl.name_2 AS name_2, CONCAT(pl.name_2, ' ', pl.name_1) AS playername, pl.photo_path AS photo_path, cr.id_competitor AS id_team, cr.title AS teamtitle, cn.id_competition AS id_league, cn.title AS leaguetitle, b.*
	FROM application AS a, competitor AS cr, competition AS cn, player AS pl, ban AS b
	WHERE a.id_competitor = cr.id_competitor
	 AND cr.id_competition = cn.id_competition"
            . $add_string .
            " AND a.id_player = pl.id_player
	 AND (b.ban_matches_left > 0 OR b.ban_end >= '$date')
	 AND b.id_applicant = a.id_applicant
	GROUP BY a.id_applicant
	ORDER BY ban_end ASC, ban_matches_left ASC, id_league ASC, playername ASC, teamtitle ASC";

    $res_bans = mysql_query($query);
    return $res_bans;
}

/**
 * ��������� ���� ��������������� �� ���
 * @return null|string
 */
function getCDCDate() {
    $now = getdate();
    $CDCDate = date("Y-m-d", mktime(0, 0, 0, $now["mon"], $now["mday"], $now["year"] + 9));
    return $CDCDate;
}

/**
 * ��������� ������ �� ��������� ���� � PDF
 * @param int $league
 * @return null|string
 */
function getLeagueCalendar($league) {
    $calendar_file = '../calendar/' . $league . ".pdf";
    if (file_exists($calendar_file)) {
        $calendar_download = " | �������� <a href='" . $calendar_file . "' target='_blank' title='������� �������� ��������� � ���� PDF-�����'> <img src='../graphics/docs_icon.gif' border=0> PDF</a>";
    } else {
        $calendar_download = null;
    }
    return $calendar_download;
}

/**
 * �������� ������������ ������� ����
 * @param int $league
 * @return int|string
 */
function isTableActual($league) {
    $now = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
    $res_query = mysql_query("SELECT * FROM league_table_status WHERE league = $league");
    if (mysql_num_rows($res_query) > 0) {
        $actual = mysql_result($res_query, 0, "actual");
    } else {
        $res_query = mysql_query("INSERT INTO league_table_status VALUES ($league, 0, '$now')");
        $actual = 0;
    }
    return $actual;
}

/**
 * �������� �������
 * @param int $league ����
 * @param string $tour_date ����
 */
function computeTable($league, $tour_date) {
    //---exception_date ������������ �� ������ ��� ����, �� � ��� id ����, ���������� ������ ����� �������� � ������� �����������, � ������ include

    $res_teams = mysql_query("SELECT * FROM competitor WHERE id_competition=$league ORDER BY id_competitor");
    $n_teams = mysql_num_rows($res_teams);

    $res_competition = mysql_query("SELECT * FROM competition WHERE id_competition = $league");
    $exception_date = mysql_result($res_competition, 0, "exception_date");
    $exception_sign = mysql_result($res_competition, 0, "exception_sign");
    $win_points = mysql_result($res_competition, 0, "win_pts");

    //------------------------------������� ��� �� ���������---------------------
    if (isTableActual($league) == 0) {
        $res_update = mysql_query("UPDATE league_table_status SET actual = 0 WHERE league = $league");
        if (!mysql_query("DELETE FROM league_table_content WHERE id_league = $league")) {
            mysql_close();
            //print("<SCRIPT LANGUAGE=javascript>errorMessage(2)</SCRIPT>");
        }

        for ($i = 0; $i < $n_teams; $i++) {
            $ident_team = mysql_result($res_teams, $i, "id_competitor");
            $s_team = mysql_result($res_teams, $i, "title");
            $id_league = mysql_result($res_teams, $i, "id_competition");
            $id_team = mysql_result($res_teams, $i, "id_team");

            $done = FALSE;

            if ($exception_date) {
                $won_to_add = 0;
                $drew_to_add = 0;
                $lost_to_add = 0;
                $goals_for_to_add = 0;
                $goals_against_to_add = 0;

                if ($exception_sign == "after") {
                    //----------������
                    $res_won = mysql_query("SELECT * FROM protocol WHERE ((dt_protocol > '$exception_date') AND (team_1 = $ident_team AND score_end_1 > score_end_2) OR (dt_protocol > '$exception_date') AND (team_2 = $ident_team AND score_end_1 < score_end_2)) AND (dt_protocol <= '$tour_date')");
                    //---------�����
                    $res_drew = mysql_query("SELECT * FROM protocol WHERE (league = $league AND dt_protocol > '$exception_date' AND (team_1 = $ident_team OR team_2 = $ident_team) AND score_end_1 = score_end_2) AND (dt_protocol <= '$tour_date') AND forfeited IS NULL");
                    //--------���������
                    $res_lost = mysql_query("SELECT * FROM protocol WHERE (dt_protocol > '$exception_date' AND (team_1 = $ident_team AND score_end_1 < score_end_2) OR dt_protocol > '$exception_date' AND (team_2 = $ident_team AND score_end_1 > score_end_2)) AND (dt_protocol <= '$tour_date')");
                    //--------����������� ���������
                    $res_forfeited = mysql_query("SELECT * FROM protocol WHERE (league = $league AND dt_protocol > '$exception_date' AND (team_1 = $ident_team OR team_2 = $ident_team)) AND (dt_protocol <= '$tour_date') AND forfeited = 1");
                    //--------������
                    $res_for_home = mysql_query("SELECT SUM(score_end_1) AS sc_home FROM protocol WHERE (dt_protocol > '$exception_date' AND team_1 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    $res_for_away = mysql_query("SELECT SUM(score_end_2) AS sc_away FROM protocol WHERE (dt_protocol > '$exception_date' AND team_2 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    //----------���������
                    $res_against_home = mysql_query("SELECT SUM(score_end_2) AS ms_home FROM protocol WHERE (dt_protocol > '$exception_date' AND team_1 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    $res_against_away = mysql_query("SELECT SUM(score_end_1) AS ms_away FROM protocol WHERE (dt_protocol > '$exception_date' AND team_2 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    $done = TRUE;
                } elseif ($exception_sign == "before") {
                    //----------������
                    $res_won = mysql_query("SELECT * FROM protocol WHERE ((dt_protocol < '$exception_date') AND (team_1 = $ident_team AND score_end_1 > score_end_2) OR (dt_protocol < '$exception_date') AND (team_2 = $ident_team AND score_end_1 < score_end_2)) AND (dt_protocol <= '$tour_date')");
                    //---------�����
                    $res_drew = mysql_query("SELECT * FROM protocol WHERE (league = $league AND dt_protocol < '$exception_date' AND (team_1 = $ident_team OR team_2 = $ident_team) AND score_end_1 = score_end_2) AND (dt_protocol <= '$tour_date') AND forfeited IS NULL");
                    //--------���������
                    $res_lost = mysql_query("SELECT * FROM protocol WHERE (dt_protocol < '$exception_date' AND (team_1 = $ident_team AND score_end_1 < score_end_2) OR dt_protocol < '$exception_date' AND (team_2 = $ident_team AND score_end_1 > score_end_2)) AND (dt_protocol <= '$tour_date')");
                    //--------����������� ���������
                    $res_forfeited = mysql_query("SELECT * FROM protocol WHERE (league = $league AND dt_protocol < '$exception_date' AND (team_1 = $ident_team OR team_2 = $ident_team)) AND (dt_protocol <= '$tour_date') AND forfeited = 1");
                    //--------������
                    $res_for_home = mysql_query("SELECT SUM(score_end_1) AS sc_home FROM protocol WHERE (dt_protocol < '$exception_date' AND team_1 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    $res_for_away = mysql_query("SELECT SUM(score_end_2) AS sc_away FROM protocol WHERE (dt_protocol < '$exception_date' AND team_2 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    //----------���������
                    $res_against_home = mysql_query("SELECT SUM(score_end_2) AS ms_home FROM protocol WHERE (dt_protocol < '$exception_date' AND team_1 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    $res_against_away = mysql_query("SELECT SUM(score_end_1) AS ms_away FROM protocol WHERE (dt_protocol < '$exception_date' AND team_2 = $ident_team) AND (dt_protocol <= '$tour_date')");
                    $done = TRUE;
                } elseif ($exception_sign == "include") {
                    //----������������ ������� - ������� ��� �����������[ ��� ������ ���� ���������
                    $query = "SELECT * FROM competitor WHERE id_team = $id_team AND id_competition in ($exception_date)";
                    $id_included_competitor = mysql_result(mysql_query($query), 0, "id_competitor");
                    $query = "SELECT * FROM league_table_content WHERE id_league in ($exception_date) AND id_competitor = $id_included_competitor";
                    $res_previous_results = mysql_query($query);
                    $won_to_add = mysql_result($res_previous_results, 0, "won");
                    $drew_to_add = mysql_result($res_previous_results, 0, "drew");
                    $lost_to_add = mysql_result($res_previous_results, 0, "lost");
                    $goals_for_to_add = mysql_result($res_previous_results, 0, "goalsfor");
                    $goals_against_to_add = mysql_result($res_previous_results, 0, "goalsagainst");
                    $done = FALSE;
                }
            } else {
                $won_to_add = 0;
                $drew_to_add = 0;
                $lost_to_add = 0;
                $goals_for_to_add = 0;
                $goals_against_to_add = 0;
            }

            if (!$done) {
                //----------������
                $res_won = mysql_query("SELECT * FROM protocol WHERE ((team_1 = $ident_team AND score_end_1 > score_end_2) OR (team_2 = $ident_team AND score_end_1 < score_end_2)) AND (dt_protocol <= '$tour_date')");
                //---------�����
                $res_drew = mysql_query("SELECT * FROM protocol WHERE ((team_1 = $ident_team OR team_2 = $ident_team) AND score_end_1 = score_end_2) AND (dt_protocol <= '$tour_date') AND forfeited IS NULL");
                //--------���������
                $res_lost = mysql_query("SELECT * FROM protocol WHERE ((team_1 = $ident_team AND score_end_1 < score_end_2) OR (team_2 = $ident_team AND score_end_1 > score_end_2)) AND (dt_protocol <= '$tour_date')");
                //--------����������� ���������
                $res_forfeited = mysql_query("SELECT * FROM protocol WHERE ((team_1 = $ident_team OR team_2 = $ident_team)) AND (dt_protocol <= '$tour_date') AND forfeited = 1");
                //--------������
                $res_for_home = mysql_query("SELECT SUM(score_end_1) AS sc_home FROM protocol WHERE (team_1 = $ident_team) AND (dt_protocol <= '$tour_date')");
                $res_for_away = mysql_query("SELECT SUM(score_end_2) AS sc_away FROM protocol WHERE (team_2 = $ident_team) AND (dt_protocol <= '$tour_date')");
                //----------���������
                $res_against_home = mysql_query("SELECT SUM(score_end_2) AS ms_home FROM protocol WHERE (team_1 = $ident_team) AND (dt_protocol <= '$tour_date')");
                $res_against_away = mysql_query("SELECT SUM(score_end_1) AS ms_away FROM protocol WHERE (team_2 = $ident_team) AND (dt_protocol <= '$tour_date')");
            }

            $won = mysql_num_rows($res_won) + $won_to_add;
            $drew = mysql_num_rows($res_drew) + $drew_to_add;
            $forfeited = mysql_num_rows($res_forfeited);
            $lost = mysql_num_rows($res_lost) + $forfeited + $lost_to_add;
            //----------��� � �����
            $played = $won + $drew + $lost;
            $points = $won * $win_points + $drew;
            //----------������
            $goalsfor = (int) mysql_result($res_for_home, 0, "sc_home") + (int) mysql_result($res_for_away, 0, "sc_away") + $goals_for_to_add;
            //----------���������
            $goalsagainst = (int) mysql_result($res_against_home, 0, "ms_home") + (int) mysql_result($res_against_away, 0, "ms_away") + $forfeited * 5 + $goals_against_to_add;

            $query = "INSERT INTO league_table_content VALUES ($league, $ident_team, '$s_team', $played, $won, $drew, $lost, $goalsfor, $goalsagainst, $points, 0, 0)";
            $res_ins = mysql_query($query);
        }

        //-------------------��������� ������ ������---------------
        $query = "SELECT * FROM league_table_content WHERE id_league = $league ORDER BY pts DESC";
        $res_table = mysql_query($query);
        for ($i = 0; $i < $n_teams; $i++) {
            $ident_team = mysql_result($res_table, $i, "id_competitor");
            $pts = mysql_result($res_table, $i, "pts");
            //---������� ������� � ����� �� ����������� �����
            $res_duplicate = mysql_query("SELECT * FROM league_table_content WHERE id_league = $league AND pts = $pts AND id_competitor <> $ident_team");
            $num_duplicates = mysql_num_rows($res_duplicate);
            for ($n = 0; $n < $num_duplicates; $n++) {
                $result = 0;
                $home_result = 0;
                $away_result = 0;
                $rival = mysql_result($res_duplicate, $n, "id_competitor");
                $res_home = mysql_query("SELECT * FROM protocol WHERE (team_1 = $ident_team AND team_2 = $rival) AND (dt_protocol <= '$tour_date')");
                $res_away = mysql_query("SELECT * FROM protocol WHERE (team_2 = $ident_team AND team_1 = $rival) AND (dt_protocol <= '$tour_date')");
                if (mysql_num_rows($res_home) > 0) {
                    $home_result = mysql_result($res_home, 0, "score_end_1") - mysql_result($res_home, 0, "score_end_2");
                }
                if (mysql_num_rows($res_away) > 0) {
                    $away_result = mysql_result($res_away, 0, "score_end_2") - mysql_result($res_away, 0, "score_end_1");
                }
                $pts_adv = (int) 0;
                if ($home_result > 0) {
                    $pts_adv = (int) $pts_adv + (int) $win_points;
                } elseif ($home_result == 0) {
                    $pts_adv = $pts_adv + 1;
                }
                if ($away_result > 0) {
                    $pts_adv = (int) $pts_adv + (int) $win_points;
                } elseif ($away_result == 0) {
                    $pts_adv = $pts_adv + 1;
                }
                $res_update = mysql_query("UPDATE league_table_content SET pts_advantage = pts_advantage + $pts_adv WHERE id_competitor = $ident_team");
                $goals_adv = 0;
                $goals_adv = $home_result + $away_result;
                $res_update = mysql_query("UPDATE league_table_content SET goals_advantage = goals_advantage + $goals_adv WHERE id_competitor = $ident_team");
            }
        }
        $now = date("Y-m-d H:i:s");
        $res_update = mysql_query("UPDATE league_table_status SET actual = 1, last_update = '$now' WHERE league = $league");
        //---------------------------------������� ��� ���������-----------------
    } else {
        //--------������ �� ���� ������
    }
}

/**
 * ��������� ������� ����
 * @param int $league
 * @param string $tour_date
 * @return resource
 */
function getLeagueTable($league, $tour_date) {

    //�������� ������� ��������� �������
    computeTable($league, $tour_date);

    $res_sort = mysql_query("SELECT sort_order FROM competition WHERE id_competition = $league");
    $sort_order = mysql_result($res_sort, 0, "sort_order");
    $query = "SELECT DISTINCT pld FROM league_table_content WHERE id_league = $league";
    $res_finished = mysql_query($query);
    $is_finished = mysql_num_rows($res_finished);
    if ($is_finished > 0) {
        $matches_played = mysql_result($res_finished, 0, "pld");
    } else {
        $matches_played = 0;
    }
    $num_teams = mysql_query("SELECT * FROM competitor WHERE id_competition=$league ORDER BY id_competitor");
    $n_teams = mysql_num_rows($num_teams);
    if ($is_finished == 1 AND ($matches_played == $n_teams - 1 OR $matches_played == ($n_teams - 1) * 2)) {
        if ($sort_order == null) {
            $sort_order = "pts DESC, pts_advantage DESC, goals_advantage DESC, won DESC, goalsfor-goalsagainst DESC, goalsfor DESC, pld ASC, name";
        }
    } else {
        if ($sort_order == null) {
            $sort_order = "pts DESC, pts_advantage DESC, goals_advantage DESC, won DESC, goalsfor-goalsagainst DESC, goalsfor DESC, pld ASC, name";
        }
    }

    $query = "SELECT * FROM league_table_content WHERE id_league = $league ORDER BY " . $sort_order;

    $res_table = mysql_query($query);
    return $res_table;
}

/**
 * ��������� ������ ���� ��������
 * @param $date string ���� � ������� m-d
 * @param $season int ���� ����� �� ��� ������� �� �� ���� ������� ����� ���������� ������, �� �� �� ����������� (���������� �� "������" �������).
 * @return null|resource
 */
function birthdayList($date, $season) {
    $query = "SELECT *
FROM player AS p
WHERE DATE_FORMAT(p.birthdate, '%m-%d') = '$date'
  AND p.died = 0
  AND p.id_player IN (SELECT id_player
                      FROM application
                      WHERE id_competitor IN (SELECT id_competitor
                                              FROM competitor
                                              WHERE id_competition IN (SELECT id_competition
                                                                       FROM competition
                                                                       WHERE season >= $season)))
ORDER BY p.name_2, p.name_1";

    $list_birthdays = mysql_query($query);
    if ($list_birthdays) {
        return $list_birthdays;
    } else {
        return null;
    }
}

/**
 * �������� ������� ��������������� � ������
 * @param $id_player int id ������
 * @return int id ��������� ������ � ���������������� ��� 0 - ���� �� �������
 */
function checkPlayerBan($id_player) {
    $query = "SELECT b.id_applicant FROM ban AS b, application AS a WHERE a.id_applicant = b.id_applicant AND a.id_player = $id_player";
    $res_bans = mysql_query($query);
    if (mysql_num_rows($res_bans) > 0) {
        $banned = mysql_result($res_bans, 0, "id_applicant");
    } else {
        $banned = 0;
    }
    return $banned;
}

/**
 * ������ � ������� ��� ����������� �������� ����
 * @param $uid string UID � OneSignal
 * @param $header string ����� ���������
 * @param $text string ����� ���������
 * @param $link string ������
 */
function addPushJob($uid, $header, $text, $link) {
    $current_time = time();
    $query = "INSERT INTO job_push (id_push, m_uid, header, text, link, created) VALUES (null, '$uid', '$header', '$text', '$link', $current_time)";
    $res_ins = mysql_query($query);
    if ($res_ins) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * ���������� ������� �� �����
 * @param $x
 * @param $y
 * @return bool
 */
function table_sort($x, $y) {
    return ($x['points'] < $y['points']);
}

/**
 * ����� ���� ������
 * @param $competition_id - id �������, ���� �����, �� ���������� ���
 * @return bool
 */
function table_cache_reset($competition_id) {
    if (isset($competition_id)) {
        $res_reset = mysql_query("UPDATE league_table_status SET actual = 0 WHERE league = $competition_id");
    } else {
        $res_reset = mysql_query("UPDATE league_table_status SET actual = 0");
    }

    return $res_reset;
}

/**
 * �������� ��������� � Telegram
 * @param $method string ��. ������������ API
 * @param $response array ����������
 * @return bool
 */
function sendTelegram($method, $response) {
    //����� �������� webhook, ��������� - https://api.telegram.org/bot935868081:AAHQJqv1gYGtpspJpgqozCNKSRlG8KLiQoI/setWebhook?url=https://befutsal.ru/demo/includings/bot.php
    //������ chat_id - https://api.telegram.org/bot935868081:AAHQJqv1gYGtpspJpgqozCNKSRlG8KLiQoI/getChat?chat_id=@befutsal_test
    //-1001464189306 - befutsal
    //-1001497462890 - befutsal_test

    define('TOKEN', '935868081:AAHQJqv1gYGtpspJpgqozCNKSRlG8KLiQoI');

    $ch = curl_init('https://api.telegram.org/bot' . TOKEN . '/' . $method);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $res = curl_exec($ch);
    curl_close($ch);

    return $res;
}

/**
 * ��������� ������ ������ �� ����� ��
 * @param $count int ���������� �������
 * @return array ������ �������
 */
function getVKWall($count) {
    // ID ������ ���������� ���������
    $wall_id = getParameter("vk_wall_id");
    $token = getParameter("vk_wall_token");
    $api_version = getParameter("vk_api_version");

    // �������� ����������, ��������� ��� ������ ����.
    $api = file_get_contents("http://api.vk.com/method/wall.get?owner_id={$wall_id}&count={$count}&access_token={$token}&v={$api_version}");

    // ����������� JSON-������ � ������
    $wall = json_decode($api);

    // �������� ������
    $wall = $wall->response->items;

    return $wall;
}

function getVKPhotos($count) {

    // ID ������ ���������� ���������
    $wall_id = getParameter("vk_wall_id");
    $token = getParameter("vk_wall_token");
    $api_version = getParameter("vk_api_version");

    //�������� ��������� ������
    //$query = file_get_contents("https://api.vk.com/method/photos.getAlbums?owner_id={$wall_id}&count=1&access_token={$token}&rev=1&v=5.77");
    //$result = json_decode($query);
    //�������� ���� �� �������
    $url = "https://api.vk.com/method/photos.get?owner_id={$wall_id}&album_id=286898526&count={$count}&access_token={$token}&rev=1&v={$api_version}";
    $query = file_get_contents($url);
    $result = json_decode($query);

    $result = $result->response->items;

    return $result;
}

/**
 * ��������� �������� ����� ������ �� ������ �������������
 * @return int id ���������� �������� ����� ��� 0, ���� ������ ���
 */
function matchOfWeek() {
    $thisweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
    $nextweek = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));

    $res_schedule = mysql_query("SELECT * FROM schedule WHERE `date` BETWEEN '$thisweek' AND '$nextweek' ORDER BY date, time");
    $n_schedule = mysql_num_rows($res_schedule);
    $top_rate = 0;
    $id_schedule = 0;

    for ($i = 0; $i < $n_schedule; $i++) {
        $id_team1 = mysql_result($res_schedule, $i, "team_1");
        $id_team2 = mysql_result($res_schedule, $i, "team_2");
        $match_rate = getTeamRate($id_team1, 0) + getTeamRate($id_team2, 0);
        if ($match_rate > $top_rate) {
            $top_rate = $match_rate;
            $id_schedule = mysql_result($res_schedule, $i, "id_schedule");
        }
    }

    return $id_schedule;
}

/**
 * ��������� �������� ���������� ������ �� ������ �������������
 * @return int id ��������� �������� ����� ��� 0, ���� ����� ������ ���
 */
function resultOfWeek() {
    $weekago = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 7, date("Y")));
    $today = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));

    $res_results = mysql_query("SELECT * FROM protocol WHERE dt_protocol BETWEEN '$weekago' AND '$today'");
    $n_results = mysql_num_rows($res_results);
    $top_rate = 0;
    $id_protocol = 0;

    for ($i = 0; $i < $n_results; $i++) {
        $id_team1 = mysql_result($res_results, $i, "team_1");
        $id_team2 = mysql_result($res_results, $i, "team_2");
        $match_rate = getTeamRate($id_team1, 0) + getTeamRate($id_team2, 0);
        if ($match_rate > $top_rate) {
            $top_rate = $match_rate;
            $id_protocol = mysql_result($res_results, $i, "id_protocol");
        }
    }

    return $id_protocol;
}

/**
 * ��������� ��������� ������
 * @return string ������� �� �� ��������� ������
 */
function getTopFavouriteTeam() {
    $query = "SELECT COUNT(DISTINCT user_id) AS top_favourites
            FROM m_favourites
            GROUP BY team_id
            ORDER BY top_favourites DESC
            LIMIT 0,1";
    $res_top_team = mysql_query($query);
    return mysql_result($res_top_team, 0, "top_favourites");
}

/**
 * �������� ������ � ���������� ����
 * @param int $applicant ������������� � ������ applicant
 * @param int $protocol ������������� ���������
 * @return float ����� ������ � ��������� �����
 */
function playerPerformanceInGame($applicant, $protocol) {
    $query = "SELECT * FROM staff WHERE id_player = $applicant AND id_protocol = $protocol";
    $correct = mysql_num_rows(mysql_query($query));

    if ($correct == 1) {

        //������� �����
        $scored = mysql_result(mysql_query($query), 0, "scored");

        //���������� �� ������
        $query = "SELECT * FROM application WHERE id_applicant = $applicant";
        $competitor_id = mysql_result(mysql_query($query), 0, "id_competitor");
        $goalie = mysql_result(mysql_query($query), 0, "goalie");

        //������� ������� ����������� � �����
        $query = "SELECT COUNT(*) AS number_of_players FROM staff WHERE id_protocol = $protocol AND id_player IN (SELECT id_applicant FROM application WHERE id_competitor = $competitor_id)";
        $number_of_players = mysql_result(mysql_query($query), 0, "number_of_players");

        $query = "SELECT * FROM protocol WHERE id_protocol = $protocol";
        $res_protocol = mysql_query($query);
        $score_1 = mysql_result($res_protocol, 0, "score_end_1");
        $score_2 = mysql_result($res_protocol, 0, "score_end_2");
        $team_1 = mysql_result($res_protocol, 0, "team_1");
        $team_2 = mysql_result($res_protocol, 0, "team_2");

        if (($team_1 == $competitor_id AND $score_1 > $score_2) OR ($team_2 == $competitor_id AND $score_1 < $score_2)) {
            $result = 2; //������� �����, ���� ������� ��������
        } elseif (($team_2 == $competitor_id AND $score_1 > $score_2) OR ($team_1 == $competitor_id AND $score_1 < $score_2)) {
            $result = -1; //������� �����, ���� ������� ���������
        } elseif ($score_1 == $score_2) {
            $result = 1; //������� �����, ���� ������� ������� ������
        } else {
            $result = 0; //������� ����� ��� ���� ��������� �������
        }
    } else {
        $result = 0; //���� �� ���������� � ���� (�������� ������������ ������)
    }

    if ($goalie == 1) {
        $result = $result / 10 * 1.2; //������� ��������� �����������
    } else {
        $result = $result / $number_of_players; //��� ������� ����� ����� �� ���������� ������� � �����
    }

    $result = $result + $scored / 10; //��������� �� 0.1 �� ������ ������� ���
    $result = round($result, 3);

    return $result;
}

/**
 * �������� ������ �� ����� � �������
 * @param int $applicant ������������� � ������ applicant
 * @return float ����� ������ � ��������� ����� (-1 ��� ���������, 1 ��� ������, 2 ��� ������, 0 ���� �� ����������)
 */
function playerPerformanceInCompetition($applicant) {
    $query = "SELECT * FROM staff WHERE id_player = $applicant";
    $result = mysql_query($query);
    $value = 0;
    for ($i = 0; $i < mysql_num_rows($result); $i++) {
        $id_protocol = mysql_result($result, $i, "id_protocol");
        $value = $value + playerPerformanceInGame($applicant, $id_protocol);
    }
    return round($value, 3);
}

/**
 * �������� ���� ������� ������� � ������
 * @param int $competitor ������������� ������� � competitor
 * @return array ������ ������� � �� �������� �� ��������
 */
function playersPerformanceInCompetitor($competitor) {
    $query = "SELECT * FROM application WHERE id_competitor = $competitor";
    $result = mysql_query($query);
    $players = array();
    for ($i = 0; $i < mysql_num_rows($result); $i++) {
        $id_applicant = mysql_result($result, $i, "id_applicant");
        $players[$id_applicant] = playerPerformanceInCompetition($id_applicant);
    }
    arsort($players);

    return $players;
}

/**
 * ��������� �������� ��������-������������
 * @param string $entity �������� ��� ������: team|league|competitor
 * @param $id id ��������
 * @return array ������ chat_id �����������
 */
function getSubscribers($entity, $id) {
    if ($entity == "team") {
        $query = "SELECT DISTINCT (tg_chat_id) FROM m_favourites WHERE team_id = $id AND tg_chat_id IS NOT NULL";
    } elseif ($entity == "league") {
        $query = "SELECT DISTINCT (tg_chat_id) FROM m_favourites WHERE league_id = $id AND tg_chat_id IS NOT NULL";
    } elseif ($entity == "competitor") {
        $query = "SELECT DISTINCT (tg_chat_id) FROM m_favourites, competitor WHERE id_competitor = $id AND competitor.id_team = m_favourites.team_id AND tg_chat_id IS NOT NULL";
    }
    
    $result = mysql_query($query);
    $subscribers = array();
    for ($i = 0; $i < mysql_num_rows($result); $i++) {
        $chat_id = mysql_result($result, $i, "tg_chat_id");
        $subscribers[$i] = $chat_id;
    }

    return $subscribers;
}
