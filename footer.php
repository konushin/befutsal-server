<?php
/**
 * Created by PhpStorm.
 * User: Konushin
 * Date: 31.10.2020
 * Time: 8:46
 */

if (!isset($wall)) {
    $wall = getVKWall(10);
}

print('
            <!--SECTION FOOTER--> 
    <section id="footer-tag">
           <div class="container">
             <div class="col-md-12">
              <div class="col-md-3">
                 <h3>О проекте</h3>
                 <p><b>Живи мини-футболом!</b> это некоммерческая площадка для освещения соревнований по мини-футболу и футболу в регионах России и ближнем зарубежье. </p>
                 <p>Проект является официальным информационным партнером <b>Владимирской Городской Федерации футбола, пляжного футбола, мини-футбола и футзала</b>.</p>
              </div>
              <div class="col-md-3 cat-footer">
                <div class="footer-map"></div>
                <h3 class="last-cat">Разделы</h3>
                <ul class="last-tips">
                  <li><a href="tournaments.php">Все сезоны</a></li>
                  <li><a href="results.php">Матч-центр</a></li>
                  <li><a href="bans.php">Дисквалификации</a></li>
                  <li><a href="ranking.php">Рейтинг</a></li>
                  <li><a href="docs.php">Документы</a></li>
                  <li><a href="contact.php">Контакты</a></li>
                  <li><a href="privacy_policy_ru.php">Политика обработки данных</a></li>
                  <li><a href="https://befutsal.ru/season_menu.php?season=39">Старая версия</a></li>
                </ul>
              </div>
              <div class="col-md-3">
                 <h3>Новости</h3>
                 <ul class="footer-last-news">
');

$item_count = 0;

for ($i = 0; $i < 3; $i++) {
    $header_len = 150;
    if (isset($wall[$i]->copy_history)) {
        $header = cutString($wall[$i]->copy_history[0]->text, $header_len);
    } else {
        $header = cutString($wall[$i]->text, $header_len);
    }

    $picture_url = "/graphics/news_bullet.gif";

    print('
                    <li><img src="' . $picture_url . '" alt="" /><p><a href="https://vk.com/wall-37375522_' . $wall[$i]->id . '" target="_blank"><font color="#ffffff">' . $header . '</font></a></p></li> 
        ');

}

print("<script>
<!-- скрипт, который обработает нажатие на кнопку и отправит данные на сервер -->
// эта функция сработает при нажатии на кнопку
function sendJSON() {
    // с помощью jQuery обращаемся к элементам на странице по их именам
    var email = document.querySelector('#email');
    var password = document.querySelector('#password');
    // а вот сюда мы поместим ответ от сервера
    var result = document.querySelector('.result');
    // создаём новый экземпляр запроса XHR
    var xhr = new XMLHttpRequest();
    // адрес, куда мы отправим нашу JSON-строку
    var url = '../m/webauth.php';
    // открываем соединение
    xhr.open('POST', url, true);
    // устанавливаем заголовок — выбираем тип контента, который отправится на сервер, в нашем случае мы явно пишем, что это JSON
    xhr.setRequestHeader('Content-Type', 'application/json');
    // когда придёт ответ на наше обращение к серверу, мы его обработаем здесь
    xhr.onreadystatechange = function () {
        // если запрос принят и сервер ответил, что всё в порядке
        //если нужно только положительный ответ, то добавить в условие  && xhr.status === 200
        if (xhr.readyState === 4 && xhr.status === 200) {
            // выводим то, что ответил нам сервер — так мы убедимся, что данные он получил правильно
            var response = JSON.parse(this.responseText);
            result.innerHTML = 'Вы вошли как: ' + response.user.username;
        } else {
            // выводим то, что ответил нам сервер — так мы убедимся, что данные он получил правильно
            var response = JSON.parse(this.responseText);
            result.innerHTML = response.errorMsg;
        }
    };
    // преобразуем наши данные JSON в строку
    var data = JSON.stringify({'email': email.value, 'password': password.value});
    // когда всё готово, отправляем JSON на сервер
    xhr.send(data);
}
</script>");

print('
                 </ul>
              </div>');

if (!$is_active) {

    print('
              <div class="col-md-3 footer-newsletters">
                <div><h3>Регистрация</h3></div>
                    <form id="register_form" method="post" class="register-form" action="register.php?z">         
                        <div class="email">
                            <label for="email">* E-mail:</label><div class="clear"></div>
                            <input id="email" name="email" type="text" placeholder="example@domain.com" required=""/>
                        </div>
                        <div class="name">
                            <label for="name">* Имя:</label><div class="clear"></div>
                            <input id="name" name="name" type="text" minlength="3" maxlength="16" placeholder="Серхио Родригес" title="От 3 до 16 символов" required=""/>
                        </div>
                        <div class="name">
                            <label for="password">* Пароль:</label><div class="clear"></div>
                            <input id="password" name="password" type="password" minlength="4" maxlength="16" placeholder="********" required=""/>
                        </div>
                        <div id="register-submit">
                            <button>Отправить</button> 
                        </div>');

    if (isset($_GET["reg_err"])) {
        print('
                        <div class="label">
                            <p class="result" style="color: #ffffff">' . $_GET["reg_err"] . '</p>
                        </div>');
    }

    print('
                        
                     </form>
              </div>');
}

print('
              <div class="col-xs-12">
                <ul class="social">
                      <li><a href="https://vk.com/befutsal" target="_blank"><i class="fa fa-vk"></i></a></li>
                      <!-- <li><a href="https://www.instagram.com/be_futsal/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                      <li><a href="https://fb.com/befutsal" target="_blank"><i class="fa fa-facebook"></i></a></li> -->
                    </ul>
              </div>
             </div>
           </div>
    </section>
    <footer>
           <div class="col-md-12 content-footer">
		<p>© 2004 - ' . date("Y") . ' Живи мини-футболом! Ссылка на сайт обязательна. </p>
      </div>
	</footer>

<!--Mini Flexslide-->
<script src="js/minislide/jquery.flexslider.js" type="text/javascript"></script>

<!-- Percentace circolar -->
<script src="js/circle/jquery-asPieProgress.js" type="text/javascript"></script>
<script src="js/circle/rainbow.min.js" type="text/javascript"></script>

<!--Gallery-->
<script src="js/gallery/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="js/gallery/isotope.js" type="text/javascript"></script>

<!--Carousel News-->
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/jquery.mousewheel.js" type="text/javascript"></script>

<!--Carousel Clients-->
<script src="js/own/owl.carousel.js" type="text/javascript"></script>

<!--Count down-->
<script src="js/jquery.countdown.js" type="text/javascript"></script>

  <script src="js/custom.js" type="text/javascript"></script>   
</body>
</html>
');

?>